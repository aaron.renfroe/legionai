Perspective EXPERT INSIGHTS ON A TIMELY POLICY ISSUE
BRUCE MCCLINTOCK, DAN MCCORMICK, KATIE FEISTEL, AJAY K. KOCHHAR, MARY LEE, DOUGLAS C. LIGOR, JAN OSBURG, HENRI VAN SOEST,  
EMMI YONEKURA 
Select Space Concepts for the New Space Era

## Contents

About RAND
The RAND Corporation is a research organization that develops solutions to public policy challenges to help make communities throughout the world safer and more secure, healthier and more prosperous. RAND is nonprofit, nonpartisan, and committed to the public interest.

Research Integrity Our mission to help improve policy and decisionmaking through research and analysis is enabled through our core values of quality and objectivity and our unwavering commitment to the highest level of integrity and ethical behavior. To help ensure our research and analysis are rigorous, objective, and nonpartisan, we subject our research publications to a robust and exacting quality-assurance process; avoid both the appearance and reality of financial and other conflicts of interest through staff training, project screening, and a policy of mandatory disclosure; and pursue transparency in our research engagements through our commitment to the open publication of our research findings and recommendations, disclosure of the source of funding of published research, and policies to ensure intellectual independence. For more information, visit www.rand.org/about/research-integrity. RAND's publications do not necessarily reflect the opinions of its research clients and sponsors. 

 is a registered trademark.

Limited Print and Electronic Distribution Rights This publication and trademark(s) contained herein are protected by law. This representation of RAND intellectual property is provided for noncommercial use only. Unauthorized posting of this publication online is prohibited; linking directly to its webpage on rand.org is encouraged. Permission is required from RAND to reproduce, or reuse in another form, any of its research products for commercial purposes. For information on reprint and reuse permissions, 

## I

bined Central Intelligence Agency and Air Force design for the highly successful Corona program—a critically important program that provided 800,000 pictures from space on 2.1 million feet of film between 1960 and 1972.4 At the same time, RAND's analysis contributed to an approach for space that favored freedom of navigation in space, in part to allow reconnaissance satellites to overfly the Soviet Union and detect the build-up of a feared surprise nuclear attack. That freedom of exploration concept is enshrined in Article I of the Outer Space Treaty (OST).5
n its first 75 years, the RAND Corporation helped shape how humankind thought about and used space for the benefit of humanity. In 1946, more than 11 years before the orbiting of the Soviet Union's Sputnik—mankind's first artificial space satellite—Project RAND published its first report, Preliminary Design of an Experimental World- Circling Spaceship. The Air Force's own history comments that Project RAND, in its report drafted in just three weeks, accurately predicted the feasibility of a satellite orbiting the Earth and presciently described many of the uses of satellites that we take as common for both military and civil purposes, including the first discussion of the use of a geostationary orbit for communications.1 Other Air Force historians noted that the operating modes for satellites proposed by RAND in 1946 and in subsequent, more in-depth work, were "remarkably like that actually adopted when the United States began launching satellites 12 years later."2
RAND and its researchers also significantly added to civilian space research, making major contributions to the National Aeronautics and Space Administration's (NASA's) deep-space imaging and mapping. Mert Davies, a RAND researcher for 54 years, did not work alone, but he exemplified the legacy of contributions to civil space exploration. He used his experience in developing satellite observation systems to carry out photoreconnaissance of the entire solar system. For nearly 40 years, he was on NASA's imaging teams for unmanned space missions, such as Mariner, Voyager and Galileo, and, at the time of his death, it was said that he "singlehandedly observed more of the solar system than any other human."6 
RAND research on and recommendations for both military and civilian applications for space and the uses and risks of nuclear weapons went on to shape the United States' approach to reconnaissance activities in both air and space.3 RAND's research in the late 1940s and throughout the 1950s directly contributed to the development of reconnaissance satellites by helping shape the com-
Since 1946, RAND has published more than 1,200 
reports on space that have influenced the creation of highly classified satellites and helped guide their development (many of RAND's reports remain classified), provided handbooks for astronautics and its applications,7 offered designs for the United States Space Force,8 and provided recommendations for responsible space behavior for the future.9
RAND intends to have a similar impact on the next 
75 years of space sustainability, security, and governance. RAND's unique ability to make significant impacts across all research areas is due, in large part, to its unique and creative work environment: 
RAND, from its infancy, operated in an environment that facilitated and rewarded creativity, multidisciplinary research, the application of important issues of national security, and the artform of what some later have called "implementation research."10 
improving overall space security, and sustainability;12 then RSEI selected the ones seen in this Perspective for further development into short concept papers for public distribution. The selected concepts are not intended to reflect a complete and exhaustive list of concepts that should be explored. Rather, they are intended to provide a sampling of key concepts across the range of topic areas that were intentionally designed to explore less researched areas. 

Each of the following concept papers is organized the same way. The authors begin with an introduction of the topic, describing it in basic terms. They then provide more background information that is specific to the topic and a summary of past analysis or discussion on the subject. The authors then offer the motivation for a fresh look that explains why the time is right for more research on the topic. The authors then recommend the priorities for research on the topic and describe the recommended methodology for future research. Finally, they briefly summarize the potential benefits of this research to humanity and, if relevant, to national security. This final section is typically brief and would be expanded for all topics through in-depth research, but it is important to ensure that RAND researchers remain committed to RAND's mission to help make communities throughout the world safer and more secure, healthier and more prosperous.

We hope readers—ideally ranging from those familiar with space research to those new to the study of the domain—learn from these papers and are inspired to think creatively about humanity's future approach to space. We welcome feedback on these and other space topics as RAND works to help preserve and protect space for the long-term benefit of our species.

In that spirit of innovation and creativity, the RAND 
Space Enterprise Initiative (RSEI) used RAND internal funding to invite its space researchers to identify concepts that they believed were worthy of further research.11 RSEI 
intentionally avoided the most-common areas related to space security (such as space situational awareness, space domain awareness, deterrence, and crisis stability and warfighting, which RAND already extensively investigates for various sponsors) and sustainability (such as space traffic management, debris management, and debris mitigation, which RAND and others already examine). Instead, each of the ideas captured in this Perspective reflects a concept that has not been chosen by RAND sponsors for indepth research or about which there is less overall research that we believe warrants further attention. RSEI reviewed the submitted concept ideas for creativity, potential for 

## Chapter 1 Opportunities For The United States To Shape International Norms, Rules, And Principles Applicable In Space

Douglas C. Ligor and Dan McCormick and compliance make it difficult for states to identify and restrain negligent or willfully malicious behavior in space.

The United States is well positioned to lead in developing norms and rules for space because of its technological capabilities, robust resources, and strong interest in safe, sustainable space operations. Washington is aware of this opportunity and has expressed a commitment to lead the "responsible, peaceful, and sustainable exploration" of International space governance consists primarily of four principle-based treaties that lack detail and contain ambiguous or undefined language.13 For example, fundamental terms and concepts—such as outer space, exploration and use, national appropriation, debris, [space] object, due regard [to other states], *harmful contamination*, and harmful interference—are not defined and have no agreed meaning across the international community. Beyond these treaties, national governments exercise minimal regulatory authority; such authority is usually limited to launch and communications licensing over spacefaring actors under their jurisdiction. Additionally, a limited corpus of voluntary, nonbinding guidelines produced by international organizations and associations provides some basis for existing norm development in such areas as debris mitigation and launch and operations safety.14
As a result of this weak governance framework, state actors and, more recently, even some private actors have engaged in activities in space that have placed space safety, security, stability, and sustainability at risk. Rapid debris generation, rendezvous and proximity operations, dualuse capabilities, resource capture, and antisatellite testing are some of the pressing issues for which there is little or no international consensus or efforts toward rulemaking. Moreover, conflicting interests with respect to enforcement 

Fundamental terms and 
concepts—such as outer 
space, exploration and 
use, national appropriation, 
debris, [space] object, due 
regard [to other states], 
harmful contamination, 
and harmful interference—
are not defined and 
have no agreed meaning 
across the international 
community.

space while reducing the potential for conflict.15 However, for the space domain to continue growing as a safe and beneficial resource for all humankind, the United States must draw lessons from its past multilateral successes and lead the way to a new international space governance regime. Detailed research is needed to build a road map for how the United States can turn a largely ungoverned domain into something more akin to the air, maritime, or telecommunications domains. 

## Background

to describe the existing state of space law. The authors conclude with a forward-looking discussion of the means and methods for extending the benefits of space to all humankind. Likewise, the authors of Polycentricity and Space Governance analyze the nature and architecture of space governance and propose a polycentric, decentralized system that may enable continuous evolution to meet the changing needs and opportunities of space exploration and utilization.20 Finally, the authors of Assessment of Global Norms of Behavior and Legal Regimes Related to On-Orbit Activities examine how countries regard norms of behavior specifically related to on-orbit servicing, assembly, and manufacturing technologies and how that may evolve in the next ten to 15 years.21 In that report, the authors also examine the drivers and mechanisms that may lead to the development of norms.

On July 20, 2022, the Senate Select Committee on Intelligence introduced (by unanimous vote) the fiscal year 
2023 Intelligence Authorization Act,22 which, as of this writing, is pending before the full Senate. Section 710 of the Act requires the Director of National Intelligence; the Secretaries of Defense, Commerce, and State; and the NASA Administrator to produce a report on "International Norms, Rules, and Principles Applicable in Space." Among other elements, the report should "identify opportunities for the U.S. to influence international norms, rules, and principles in space, including through bilateral or multilateral engagement."23 Although not yet law as of this writing, this directive clearly shows that Congress is interested in Some research on space governance has focused on the development of norms and rules. From RAND, the authors of *Responsible Space Behavior for the New Space Era* summarize the development of space governance and key problem areas, identify challenges and barriers to further progress, and offer recommended first steps.16 In Considering Responsible Behaviors as Part of Managing Threats to Space Systems,17 the authors briefly summarize the multilateral discussions at the Wilton Park conference in support of the UN Open-Ended Working Group (OEWG) on Reducing Space Threats Through Norms, Rules, and Principles of Responsible Behavior. RAND researchers also authored a legal journal article for the North Atlantic Treaty Organization (NATO),18 which outlines the factors that underlie space congestion, contestation, and competitiveness, and offers recommendations for the existing system of space governance that are based on social contract theory. 

Several additional studies merit mention. In Global Space Governance: An International Study,19 the authors comprehensively report on the combined efforts of recognized academics, professionals, and other stakeholders research with an eye toward the future of the domain would play a constructive role.

Three principal areas of research merit further investigation, all of which relate to furthering the capacity of the United States to influence the creation and development of international norms, rules, and principles in space, including through bilateral or multilateral engagements. The United States has experience building international safety norms, from which this research will draw lessons for space governance. First, investigation of the U.S. role in building norms, rules, and principles with respect to maritime resource management could yield important insights for space. For example, in 1945, the global maritime community had debated the extent of territorial coastal seas for decades. The United States issued a set of proclamations claiming sovereign control over its continental shelf.32 
Multiple other states followed suit by claiming control out to 200 miles off their coasts, thereby crystalizing customary international law for what would become the Exclusive Economic Zone. The international community eventually formalized the U.S. position on territorial waters as part of the UN Convention on the Law of the Sea treaty.33
prioritizing U.S. leadership in the development of international space norms.

The existing international system of space governance is no longer sufficient to meet growing challenges and threats in space. The number of objects in orbit has vastly increased and will continue to do so,24 leading to a much greater need for multilateral schemes to manage space operations. This challenge is exacerbated by the significant increase in debris.25 Likewise, events in the space domain have played an increasingly important role in global affairs. For example, Ukrainian armed forces are relying on thousands of commercial satellite dishes to maintain internet and communications links during Russia's invasion.26 
Additionally, the UN General Assembly convened the OEWG,27 which held its second substantive session in September 2022, but has not—as of yet—produced consensus governance solutions.

Other high-profile events have drawn attention to the growing dangers of space collision or conflict. In November 2021, Russia conducted an antisatellite missile test,28 
generating approximately 1,800 pieces of debris in lowearth orbit (LEO). The United States responded to this by announcing a unilateral moratorium on direct-ascent antisatellite weapon (ASAT) testing (a nascent attempt at norm-building).29 In the past few years, the International Space Station has maneuvered multiple times to avoid collisions with debris from both ASAT tests and upper-stage rocket breakups.30 In August 2022, Russia caused significant concern in the United States by launching Kosmos 2558, allegedly an inspector satellite that will maneuver close to a high-value U.S. asset.31 The space domain sees new developments nearly every day, and therefore fresh Second, there are likely lessons to learn from the U.S. 

role in creating, implementing, and successfully promoting air traffic management and safety norms, rules, and principles prior to the 1944 Chicago Convention and the creation of the International Civil Aviation Organization. The circumstances of how the United States exercised its influence—despite refusing to adopt many initial international provisions—to ensure that the first major international aviation rules would not affect commercial access may also apply to the space domain.34
broad base of subject-matter expertise from hard science fields—such as aerospace, aeronautics, engineering, and astrodynamics—to ensure that the domain of space is properly examined. Equally as important, however, will be the inclusion of experts from the law, political science, international relations, and policy analysis fields. This expertise will be essential to ensure a robust analysis of governance frameworks and mechanisms.

## Potential Benefits Of This Research To Humanity And National Security

This research will assist in determining pathways through which the United States, its allies, and other likeminded governments can influence the development of a space governance regime of norms, rules, and principles through bilateral and multilateral mechanisms—even if opposed by adversaries. Establishing such a regime would foster the peaceful use of space, reduce the risk of conflict among near-peers, and continue creating a safe environment for commercial actors to explore and develop space resources. This research will also be designed to influence new international and national space laws and governance organizations.

Finally, the United States has played a role in developing norms, rules, and principles regarding the use and disposal of landmines. For example, in 2022, the United States announced changes to its anti-personnel landmine policy that bring it into greater alignment with the Ottawa Convention, the international treaty governing landmine use.35 
The announcement emphasizes U.S. leadership on this issue and the financial assistance that the United States has provided to states around the world in clearing minefields. U.S. leadership methods in this realm may be applicable to the space domain.

The primary methods of research and analysis that we will apply to the aforementioned areas of research will include qualitative analysis and coding of current space governance literature, including legal analysis and assessment; case study and historical comparative analysis; historiographic analysis; subject-matter expert interviews; and focus group data and information collection. In short, the research would combine a review of existing literature, historical data, and input from experts in the field to build recommendations that the U.S. government can act on to start normalizing safer space behavior. This research will require a multidisciplinary approach and demand a 

## It'S Time To Develop Sustainable Governance Frameworks For Lunar Resource Use

Jan Osburg and Mary Lee NASA has created the Artemis Accords,39 which are described as "a shared vision for principles, grounded in the OST of 1967, to create a safe and transparent environment which facilitates exploration, science, and commercial activities for all of humanity to enjoy,"40 as part of its effort to return to the moon. More than 20 other countries have already signed on to this framework.

Beyond multinational agreements, such as the Artemis Accords, the existing governance regime for all space activities is based in the aforementioned OST and the body of interpretations and precedent attached to it.41 But the OST lacks specificity and hails from a time when nationstates had just started to be active in space. The subsequent 1979 Moon Agreement states that neither the moon nor its resources "shall become a property of any State, international intergovernmental or non-governmental organization, national organization or non-governmental entity or of any natural person."42 However, neither the United The exploration and use of resources in deep space—i.e., space mining—is expected to become technically feasible and potentially economically viable over the course of the following decades because of advances in autonomous systems and low-cost, large-payload space launch concepts in the development pipeline (e.g., the SpaceX Starship).36 
However, space mining is subject to relatively little existing policy or governance, despite the potentially high economic, political, and societal stakes attached to it. When combined with the return of great-power competition on earth and in space,37 resource exploitation in space creates the potential for conflict among competing countries and among non-governmental actors: Despite the vastness of space, there are relatively few locations with resources that are accessible and exploitable in the near to mid-term (e.g., the moon, asteroids). Therefore, governance structures are needed that minimize the chance of conflict while encouraging cooperation among all stakeholders and fostering the efficient use of these resources.

## Background

NASA's Artemis Program aims to build a permanently inhabited base on the moon in the early 2030s; the base will serve as a springboard from which to prospect for and extract lunar resources.38 In support of this program, 

Despite the vastness 
of space, there are 
relatively few locations 
with resources that are 
accessible and exploitable 
in the near to mid-term.

States nor any other major spacefaring country has ratified the Moon Agreement.

There are multiple possible futures that could arise from a lack of collaboration in space governance. Recent studies, for example, by the Department of the Air Force and the Aerospace Corporation, have explored these multiple futures.43 Several organizations, including RAND 
and the Secure World Foundation, have started thinking about requirements and options for sustainable space governance.44 The Hague International Space Resources Governance Working Group has created Building Blocks for the Development of an International Framework on Space Resource Activities,45 but such concepts are nascent and require further development with additional stakeholders.

ice deposits at its south pole can support lunar exploration but can also enable the use of more-distant objects (e.g., mineral-rich objects in the Asteroid Belt),47 potentially leading to vast economic and societal opportunities and ultimately accelerating the expansion of humanity into space.

Multiple countries have independently developed policies to capitalize on these space resources and technological advancements. In 2015, Congress passed the U.S. Commercial Space Launch Competitiveness Act, which asserts that U.S. citizens are "entitled to any asteroid resource or space resource obtained . . . in accordance with applicable law, including the international obligations of the United States."48 To help set a precedent in this context, NASA 
selected four private companies to collect lunar samples and sell them to NASA, to "make sure that there is a norm of behavior" when extracting and using such resources.49 
Luxembourg, the United Arab Emirates, and Japan have each followed suit with their own unilateral laws for space resource mining.50
Recent technological advancements in space mining mean that several celestial bodies could be mined for resources in the coming years.46 The moon is the closest of these resource-rich destinations: The newly discovered water It has been reported that Russia and China likewise have developed ambitions in space mining.51 The two countries have agreed to collaborate with one another on moon missions,52 including establishing a research station in orbit or on the lunar surface. But it is unlikely that they will join such compacts as the Artemis Accords. China has criticized the concept of safety zones laid out in the accords,53 and the head of Russia's space program has stated that the accords are too U.S.-centric to agree on at present.54
Without further cooperation and agreement among the major and minor space powers, multiple, competing governance systems may be established, mirroring multiple possible stakeholder motivations and approaches. 

There is a first-mover advantage to the country—or group 

There are multiple 
possible futures that 
could arise from a lack 
of collaboration in space 
governance.

of countries—that takes the initiative in this arena and defines a more detailed governance framework. Thus, the time to address this issue is now.

## Priorities For Research Should An International Group Within The Un Or Another Body Regulate Space Mining And Resource Use?

techniques, such as game theory, wargaming, decisionmaking under uncertainty, and foresight-based approaches. For example, a series of foresight-based exercises with relevant stakeholders could explore and assess potential governance approaches in the context of a range of possible deep-space futures and potential adversarial behaviors.

The insights—and, ideally, the governance framework— resulting from additional research on this topic may help reduce conflict and increase cooperation among spacefaring countries. A comprehensive, fair, mutually agreed-on governance framework would lay a foundation for peaceful activities that could help spur additional exploration of outer space and lead to new discoveries. This will allow the United States and its allies to create a future in which the use of deep-space resources contributes to prosperity, security, and freedom on earth and throughout the solar system.

There are many potential alternatives for the governance of deep-space activities, but not all will have the desired impact of reducing the chance of conflict while maximizing economic and societal benefits and enabling NASA's goal of expanding humanity into space under the values of free countries.

Basic questions need to be addressed. For example, should an international group within the UN or another body regulate space mining and resource use? How can Russia and China be incentivized to cooperate, and what effects will there be on great-power competition? How will disputes be resolved? Should stakeholders regulate the buying and selling of extracted resources, and if so, how? Even within the United States, space resource extraction raises questions about which organizations should be responsible for the various elements involved in such endeavors. NASA is the main player, but there are open questions about what roles, if any, the U.S. Space Force, Department of State, Department of Transportation, and Department of Commerce should play. Assessing governance approaches and identifying the most-promising ones therefore is an important and highly complex next step.

Research on this topic will require subject-matter expertise in a wide variety of areas, including international affairs and law, space exploration systems and spaceflight technologies, political science, ethics, planetary sciences, economics, and geostrategy. Research will benefit from cutting-edge 

## Background Sustainable Habitats In Low Gravity: Bioengineering And Automation For Reliable Space- Based Environments

Ajay K. Kochhar As space exploration expands in scale, scope, intent, and technological capability, a robust understanding of the relevant bioengineering and automation required to support space-based activities is both timely and necessary to inform what is achievable in the near and long term in practical terms. What kinds of sustainable space-based habitats are expected in the future? How might bioengineering and automation be incorporated toward those habitats' realization?

In the words of Alexander Bleacher, "Science is exploration's toolbox for survival."55 Prior space missions have prioritized space exploration for fundamental science and the use of the earth's space environment for terrestrial communication,56 and these missions have monitored earth's environment and activity within it.57 Space habitat designs have centered on durability and safety for (1) specific and fixed-duration missions or (2) extended duration near-earth space stations amenable to earth-based servicing and resupply capabilities (e.g., the International Space Station). Examining the roles of assistive technologies, understanding space biology,58 scientific experimentation, and technology demonstrations have been priorities for human space exploration for decades, and these roles are reflected in NASA's road maps for technology area 6 (TA 6) (human health, life support, and habitation systems) and TA 7, human exploration destination systems.59 As of this writing, there is a heavy reliance on terrestrial services and resources for sustaining and executing space exploration missions.60 
Although adverse effects of space exploration and low gravity on living organisms have been noted, specifically those affecting DNA, bone, blood, and the nervous system,61 our understanding of mitigations and the potential practice of space medicine for human benefit is limited at present.62 
However, there is some indication that new lessons derived from space-based habitats can be useful on earth.63

What kinds of sustainable space-based habitats are expected in the future? How might bioengineering and automation be incorporated toward those habitats' realization?

The dimensions of sustainability are multifold and include a broad variety of design considerations in an integrated system-of-systems conceptualization for long-duration 

As the pace and scale of 
innovation that enables 
the exploration and use 
of the space environment 
increase, several 
categories of driving 
factors are converging in a 
way that supports space-
based activities.

Primary drivers for a fresh look are

- a rapid increase in commercial capabilities to independently develop and deliver commercial systems to space67
human space exploration that supports multiple types of missions.64 Space habitats may be required to host onboard scientific experiments of greater scale and sophistication,65 
facilitate experimentation or testing of new capabilities in space, serve as an intermediary base or depot for autonomous explorers (e.g., for refueling, servicing or assembly,66 retasking, and resupply), support rescue missions and more-sustainable decommissioning of orbital assets, and act as essential precursors for multiple deepspace missions. An increased scope for autonomy, reduced dependence on terrestrial systems for daily activities, and improved bioengineering of habitats are necessary for such missions and will alter characteristics of the space supply chain. The dimensions themselves are envisioned to expand the modularity of habitat designs and include greater composability and interoperability of component systems for larger-scale capabilities. These likely will require improved energy efficiency in designs as well as flexible capabilities that are adaptable and reusable for a variety of applications and operational circumstances.

As the pace and scale of innovation that enables the exploration and use of the space environment increase, several categories of driving factors are converging in a way that supports space-based activities and also changes the conventional view of space-based activities, motivating the value in a fresh look at space-based habitats.

- an increasing international presence in space and 
space exploration initiatives and larger scale and greater frequency of efforts68
- the emergence of space as a contested environment 
and the corresponding recent revisions to national space policy69
- advances in bioengineering,70 sensors,71 robotics,72 
computing,73 communication systems and resilient 
secure networks,74 methods for energy generation and storage,75 materials engineering,76 and the manufacturing and accessibility of space technologies are converging to support new baselines for humanled interactions with their environment. 77

Because understanding the interplay between a mission and a habitat's designed capabilities is important, novel arrangements of human-machine teams should be used to frame the potential employment of technologies during a mission. 

Key aspects of requirements for reliable, sustainable habitat designs are (1) a focus on multimodal capabilities for space habitat environments (i.e., for multipurpose, multiuse, extended duration missions), (2) the bioengineering to support missions with human operators, and (3) the necessary quality, character, and extent of automation made accessible or provided by the space habitat platform.

Essential biomedical, agricultural, material, communications, and energy needs for future crewed missions should be assessed to inform policy catalysts for industrial base requirements in these areas. Existing work from NASA and other groups that have developed breakdowns of space technology areas should be leveraged to identify relevant priorities and organize the assessment against templated missions.78 Research should draw on relevant connections to capabilities being introduced by commercial partners who have been awarded contracts to develop space stations for the Commercial LEO Destinations program and Artemis-supporting Gateway program.79 The availability of new technologies should be calibrated against the findings of the in-progress Decadal Survey on Biological and Physical Sciences Research in Space 2023–2032.80
Human-centered design and human factors will play central roles when designing a habitat where humans are present; correspondingly, these have the potential for substantial influence on successful mission outcomes. Research should focus primarily on technological and architectural aspects of physical designs that enable greater potential for sustainable space habitats in an integrated system, under the presumption that human factors will play a large role in the full specification of requirements.81 
Human-centered design and human factors will play central roles when designing a habitat where humans are present; correspondingly, these have the potential for substantial influence on successful mission outcomes.

A game would be set up to explore and prioritize capability needs and designs for sustainable habitats and the execution of space missions. This would help address the first question regarding the kinds of sustainable spacebased habitats to expect in the future. Preparation for the game would include cataloging technologies for their accessibility and their functional contributions to bioengineering and automation needs in space-based habitats. The approach would be developed via a combination of workshops and an extensive literature review, and would require substantial effort to develop a forward-looking perspective that corresponds to a roughly 75-year horizon. As the game is played out, issues that are raised will inform the incorporation of bioengineering and automation as well as ways to realize the space-based habits and missions of primary interest and achievability. There is potential to explore and weigh risks, trade-offs, and opportunities afforded by nascent or conceptual technologies to precisely gauge their roles. Space habitat technologies should also be assessed for use in nonspace applications or transferability to earthoriented sustainability missions.82

to national security include incorporating strategies and missions to enhance or preserve the integrity of national security capabilities.

Regarding space sustainability, research should prioritize developing a systematic articulation of the kinds of space missions supported by types of space habitats, along with corresponding sustainable architectures to provide a baseline for system designs and engineering of reliable space habitats and to support planning for a variety of space missions that can be used to achieve specific objectives. Policy catalysts for biotechnology and automation should complement such baseline designs to enable the industrial base to support space missions in the long term (e.g., to provide economic levers, quantify supply chain dependencies, stimulate international partnerships, and further enable the achievement of national research priorities). Exploring potential nonspace applications of technologies developed for space habitats and the potential of existing terrestrial technologies for space applications may further boost relevant industrial innovation, with benefits across biomes for medicine, agriculture, automation, communications, energy, and manufacturing. In doing so, there is also an opportunity to support the *cooperative* 
development of space-based systems that would be beneficial within Earth's biomes. 

Similarly, national security strategies and missions will be better supported by a clear understanding of the system designs that are most relevant to capabilities that are expected of space-based national security missions. Having such designs would potentially inform an understanding of the implications of defending space-based habitats and securely conducting space-based activities. There are opportunities for the government to establish standards A rapid growth in transformational technologies that support increased scale and scope of participation in space activities (internationally, nationally, and for scientific exploration) highlights the need for renewed and increased research in several areas, including space sustainability and humanity, as well as national security. Space sustainability and potential benefits to humanity are overlapping and, at a high level, can be considered together. Potential benefits policy levers for the peaceful use of space. Specifying the future roles of governmental organizations—such as NASA, the Defense Advanced Research Projects Agency, national laboratories, and the armed services—in implementing corresponding national strategies also requires further research to prepare a responsive and comprehensive national security ecosystem. 

for system architectures and key component subsystems, establish ownership of the capabilities of national significance, and ensure long-term industrial activities through policy enablers. These opportunities can be developed by establishing a framework to explore and identify strategic investments for space-based capabilities and coupling that framework with ways to demonstrate international leadership in space sustainability, perhaps enabling international 

## Designing A Space Rescue Service

Jan Osburg

## Introduction

capabilities.83 The SRS could send a spacecraft with 
a medical team and equipment to provide advanced treatment, evacuate the injured astronaut to a medical care facility, or do both.
- *Breakdown of a surface exploration vehicle* far away 
from its base, leaving its crew stranded. The SRS 
could send a lander with a replacement vehicle, coordinate a rescue by others who are within range after determining the stranded astronaut's location, or do both.
Future human spaceflight missions, both in open space and on the surfaces of celestial bodies, stand to benefit from the availability of a Space Rescue Service (SRS) that, in cases of malfunctions and mishaps, can save lives and limbs that cannot be addressed with onboard resources during a mission. Examples of such malfunctions and mishaps include the following:
Such a capability would also provide both the crews and the organizations that send them with peace of mind and reduce the amount of risk mitigation that each individual mission has to undertake. Finally, it would make some riskier missions possible and thus ultimately facilitate the expansion of humanity into space.

- *Failure of the main propulsion system* of a spacecraft 
in orbit—for example, because of engine damage or fuel leaks. The SRS could send a human-rated spacecraft to rendezvous with the damaged spacecraft and evacuate the crew. Another example is if the Thermal Protection System is damaged beyond repair and thus safe reentry becomes impossible, as happened with the last mission of Space Shuttle 
Columbia.
- *Multiple breaches of pressurized compartments* of 
a space station—for example, because of multiple micrometeoroid or orbital debris strikes and the resulting loss of atmosphere beyond onboard supplies' compensation abilities. The SRS could send a fast cargo spacecraft to deliver additional repair tools and materials and additional life support fluids.
- *Illness or injury of an astronaut on a lunar base* 
beyond what can be handled by the base's medical 

[The SRS] would make 
some riskier missions 
possible and thus 
ultimately facilitate the 
expansion of humanity into 
space.

## Background

steps because manned missions beyond those to the International Space Station—for which there already are rescue options in place—were few and far between, and thus space rescue remained a theoretical issue.

Previous efforts regarding astronaut rescue focused on LEO missions, because this was where human spaceflight was confined to in the decades following the *Apollo* missions. One of the most well-known rescue systems from that era was NASA's "rescue ball"84 or Personal Rescue Enclosure, which was designed for transferring astronauts from a damaged vehicle to another. After the *Columbia* 
accident, NASA also committed to having another Space Shuttle on standby to potentially enable a rescue mission.85 
In the 1960s and 1970s, U.S. industry developed multiple concepts for "deorbiting" individual astronauts in case their spacecraft were unable to return to Earth.86 Finally, beginning at the turn of the millennium, when the idea of manned missions beyond LEO was starting to get more attention again, higher-level discussions of space rescue emerged.87 However, this did not lead to concrete next Recent advances in high-payload, low-cost launch vehicle development, such as that of the SpaceX Starship;88 related technologies, such as autonomous systems; and the discovery of large water ice deposits on the moon are likely to lead to a substantial increase in human spaceflight activities over the next decade and beyond.89 These activities range from ambitious privately funded flights in LEO, to commercial space stations, to manned lunar bases and potential deep-space mining operations, to deep-space tourist flights.90 These new launch vehicles would also enable significant space rescue capabilities because of their expected operational responsiveness and flexibility. At the same time, research from 2021 highlights a persistent capability gap in space rescue.91 Thus, now is the right time to lay the foundations for a future SRS.

Designing an SRS requires first answering the questions of 
"what, who, when, where, and how":

- What should the mission of an SRS be? Should an 
SRS focus on manned missions only, or should it also be able to "rescue" high-value robotic vehicles? Should the SRS only conduct rescues (similar to the 
German Maritime Search and Rescue Association)92 
or should it also have pre-incident authorities, such 

New launch vehicles 
would also enable 
significant space rescue 
capabilities because of 
their expected operational 
responsiveness and 
flexibility.

as for conducting safety inspections (similar to the U.S. Coast Guard)?93

Even with the increased 
propulsive power 
promised by the launch 
vehicle concepts under 
development, the "tyranny 
of orbital dynamics" will 
still constrain what is 
possible.

- What type of organization should take on the SRS 
mission: governmental, multinational, supranational, non-governmental, or commercial?94 Should 
an existing organization take on the mission, or 
should a new one be created? What are the pros and cons of each approach, including acceptance by other spacefaring countries, and what are the effects on freedom and prosperity in space? What kind of personnel are needed?
- When should an SRS be stood up, and how should it 
be funded?
- Where should SRS capabilities be based: on Earth, 
in orbit, on the surfaces of celestial bodies, or some 
combination thereof? What area should be covered by an SRS in the foreseeable future—LEO, geosynchronous equatorial orbit (GEO), cislunar space, or translunar space?
- What technologies and standards are essential to 
effective and efficient space rescue, and which need to be prioritized for development?
- What legal and policy challenges would have to be 
overcome?
Potential downsides of an SRS also need to be considered, especially because some of the technologies and capabilities involved—for example, rapid rendezvous and docking with disabled spacecraft—have dual-use potential. Furthermore, even with the increased propulsive power promised by the launch vehicle concepts under development, the "tyranny of orbital dynamics"95 will still constrain what is possible. Physics-based modeling and simulation are therefore required to inform the answers to the aforementioned questions.

While many of these technologies and some of these topics have been addressed individually in the context of spaceflight safety and space rescue, what has been lacking is an integrative, comprehensive approach. This calls for close collaboration among all relevant organizations, such as the National Space Council, NASA, the U.S. Space Force, the Department of State, and federally funded research and development centers that can jointly cover the relevant fields: space mission design and spaceflight operations, space exploration and the space economy, human spaceflight history and lessons learned, search and rescue in maritime and other extreme environments, space law and space diplomacy, related policies and doctrine, modeling OST (particularly Article V),96 the Astronaut Rescue Agreement,97 and the Artemis Accords (particularly Section 6)98 and thus strengthen these essential elements of space governance and collaboration. Furthermore, while the developing great-power competition on earth and in space will make such collaboration more complex, it also makes collaboration more important.99 Experience on earth has shown that crew rescue in extreme environments is one area in which competitors can find common ground, as evidenced by the establishment of the International Submarine Escape and Rescue Liaison Office.100
and simulation, systems design and optimization, and— last but not least—grand strategy.

These organizations could survey existing and planned capabilities and projected needs, then conduct a series of workshops and, later, tabletop exercises with subject-matter experts to provide answers to the questions outlined previously. Quantitative modeling and simulation will anchor the discussions in the laws of physics. Such an effort would provide spaceflight stakeholders with the insights needed to start implementing an SRS. And even if an SRS is not established for a long time because of technical or cost challenges, conceptualizing the SRS design will lead to a better understanding of the factors and technologies involved and may result in interim small steps that can already save lives.

The bottom line is this: A well-designed SRS could greatly reduce the risk associated with human spaceflight missions and thus ultimately encourage the expansion of humanity into space. The country or countries establishing such a capability would not just be able to accrue international goodwill and other reputational benefits, they will also have a greater opportunity to shape space exploration and use for the foreseeable future.101
Beyond the direct value in saving lives and enabling deep-space missions, creating and operating an SRS will also represent a tangible step toward implementing the 

## Why Not Space Mirrors? Introduction Background

The use of large-scale space mirrors—that is, mirrors approximately the size of Brazil located at Lagrange Point L1 between the Earth and Sun (see Figure 1)—is one proposed geoengineering technology to reflect sunlight away from the earth and reduce the global warming that has been causing climate emergencies around the world.102 
Once an idea of science fiction, this technology has reentered the scientific conversation in the face of climate change mitigation and adaptation shortcomings.

Space mirrors have previously been written off as infeasible, overly costly, and unethical. However, rigorous analysis of these factors, given technological advances in space technology, has not been conducted. While the National Academies of Sciences, Engineering, and Medicine have recommended other ideas for reflecting sunlight to cool the planet (e.g., stratospheric aerosol injection, cloud seeding) as subjects of further research and governance,103 those other ideas involve putting things into Earth's atmosphere with potential global-scale secondary effects, a major concern in terms of public opinion.104 Placing space mirrors 1.5 million km out in space—though outside earth's biosphere—would present other governance and risk considerations that should be weighed against potential benefits.

RAND researchers recently conducted a review of geoengineering technologies that included a discussion of geopolitical risks arising from geoengineering implementation and the existing legal mechanisms that fall short of managing those risks.105 The review includes space mirrors; however, because of the sparsity of published research, the review depends on literature as much as 15 years old to determine the cost, technical readiness, and secondary effects. For reference, one feasibility study from 2006 estimates that a fleet of meter-sized spacecraft could be developed and deployed in 25 years, would have a lifetime of 50 years, and would cost trillions of dollars.106
The National Academies of Science, Engineering and Medicine issued a report in 2021; the authors argue in support of further research and associated governance on stratospheric aerosols, cloud seeding, or cirrus cloud thinning, all of which could have large regional negative side effects on earth's biosphere.107 Another report from the Council on Foreign Relations, published in 2022, also supports more research and international discourse on the same technologies to better understand the options if other climate change responses fall short.108 Neither report includes space mirrors as a method for reflecting sunlight.

There is one 2022 study that provides a review of the proposed approaches to space-based solar geoengineering (i.e., variations on the concept of space mirrors) and reviews findings from interviews with more than 100 experts. The authors find that some experts think that the concept of space mirrors is impractical in the near term, although they do not rule it out because of its appeal relative to other geoengineering technologies, the potential for a new source of renewable energy (i.e., space-based solar energy), and the potential for a commercial market to sustain the implementation.109 Experts also have concerns about weaponization of such technologies and the large uncertainty around efficacy and safety remaining without further research and development.110 Many arguments both in favor of and against space mirrors were brought up, but these arguments have yet to be rigorously analyzed.

There are new concepts and technologies being developed across the geoengineering landscape and in terms of space capabilities. One geoengineering concept akin to space mirrors comes from a Massachusetts Institute of Technology research group, which proposes a raft of reflective bubbles that would be assembled and movable in space.111 
This concept was not considered in the 2022 Council on Foreign Relations study. In terms of space capabilities, the cost of a space launch campaign necessary to implement any space mirror technology could be dramatically improved with a cheaper and reusable launch capability, such as SpaceX's Starship.112 Also worth considering are such engineering advancements as the hexagonal folding mirrors recently launched as part of the James Webb Space Telescope;113 the primary mirror was 6.5 meters across 

The cost of a space 
launch campaign 
necessary to implement 
any space mirror 
technology could be 
dramatically improved with 
a cheaper and reusable 
launch capability, such as 
SpaceX's Starship.

but folded into a lightweight, compact payload to enable a launch from Earth. 

There are also new considerations that have arisen from the standing up of the U.S. Space Force and the sense that space is a contested warfighting domain.114 Space norms are still being established and codified. Tensions between major space powers will influence the geopolitical feasibility of implementing a large-scale project, such as space mirrors—for instance, whether it would be implemented by an individual state or a coalition of state or nonstate actors.

Despite doubts, there is not enough information for space mirrors to be ruled out as an option, and the idea continues to be in the public dialogue. For example, one recent U.S. presidential candidate included space mirror research in his climate plan to use as a "last resort."115

feasibility and effectiveness, methods would include technical literature review and outreach to communities working directly on space mirror concepts and those working with relevant space infrastructure (e.g., launch, on-orbit servicing). To better understand the cost of proposed space mirror approaches, a full life cycle cost analysis would be performed to include costs associated with design, construction, launch, operations and maintenance, and disposal and termination.

To further study the associated risks, the results of the previously described literature review would be leveraged to create scenarios or detailed space mirror implementation plans to use in expert elicitation, during which experts would be asked to characterize probabilities and consequences of each risk relevant to their expertise. Important to both the outreach and expert elicitation would be to reach individuals at relevant U.S. organizations and research institutions and the international community of experts, including in the Global South.

First, there is a need for a better understanding, and in some cases an updated estimate, of the feasibility, the effectiveness, and the life cycle cost of different approaches to implementing space mirrors given today's technology and infrastructure.

The second area that deserves more research is the area of associated risks. This includes an assessment of (1) environmental risks and how they differ from other solar geoengineering approaches, (2) risks to space-based systems in a contested space environment under existing space governance, (3) risks posed by different actors implementing a space mirrors approach, and (4) risks posed by the creation of large space debris at the end of the space mirrors' 
lifetime. To reach a better understanding of space mirror 

Tensions between 
major space powers will 
influence the geopolitical 
feasibility of implementing 
a large-scale project, such 
as space mirrors.

be used for geoengineering (e.g., weather modification)118 
to place it in a position to take the lead. Even if the United States does not plan to implement a space mirror or solar geoengineering option, other actors implementing these options could have negative effects on U.S. international relations, economy, and regional climate. A better understanding of geoengineering and its risks would help the United States assert a clear stance on these technologies and prepare for a scenario in which another country, such as China, uses them.

On a more positive note, because space mirrors would likely be a larger undertaking than other geoengineering approaches, they might require and thus provide an opportunity for multinational cooperation and relationshipbuilding. Further research would help both the United States and humanity in general better consider the safest and most ethical approach to geoengineering.

Around the world, people are realizing that climate change is an existential threat to humanity.116 Given the potential consequences of climate change and a danger of reaching irreversible tipping points,117 there is an argument to be made that all options should be carefully considered with the best information available. If more research and development is done on space mirrors, they could be revealed to be a viable stopgap option to hold off rising temperatures while greenhouse gas emission reduction efforts continue, compounded with the potential co-benefit of creating a new source of solar energy.

Related to U.S. national security, solar geoengineering has a first-mover advantage in terms of being able to "set the thermostat," commercialize the service, or even target any negative side effects toward an adversary. China has made significant investments in technologies that could 

## The Return Of Space-Based Solar Power

ous governments.123 The successful development of spacebased solar power could have important implications for energy security, defense, and the exploration and development of space.

## Background Introduction

The first serious research into space-based solar power dates to 1968, when U.S.-based researcher Peter Glaser first patented a design for a space-based solar power satellite.124 
Throughout the 1970s and early 1980s, the United States, Europe, and Japan all conducted preliminary research into the concept, but none of these programs got past the planning stage.125 Research interest peaked again in the late 
1990s and 2000s.126 These programs focused on a variety Countries around the world are searching for reliable and sustainable sources of energy. More solar energy reaches the earth in an hour than all of the energy that humanity uses in a year.119 Successfully harvesting this solar energy therefore would be an important step in solving our energy issues.

Earth-based solar power has an intermittent generation pattern,120 which is heavily dependent on the season, weather, and geographic location of the power plant. This makes it hard to construct a reliable electricity supply using earth-based solar power. Space-based solar power does away with many of these limitations. Because of spacebased solar power systems' location in orbit, they are less affected by the atmospheric attenuation of solar radiation, seasonal diurnal changes, and geographic location. In addition, in GEO, sunlight is on average ten times more intense than on the earth's surface, and satellites can be in sunlight 99 percent of the time. This means that space-based solar power in GEO is not intermittent and that it can, in theory, serve as baseload power.121
For 50 years, countries have tried to develop spacebased solar power, so far without success. However, the past 15 years have seen significant improvements in the technology and economics of space-based solar power.122 
This has led to a renewed interest in the concept from vari-

Because of space-based 
solar power systems' 
location in orbit, they 
are less affected by the 
atmospheric attenuation of 
solar radiation, seasonal 
diurnal changes, and 
geographic location.

of designs,127 including energy collection through solar photovoltaic (PV) arrays or mirrors, space-to-earth energy transmission through microwaves or lasers, and concepts located in LEO and GEO (see Figure 2).

In the past few years, there have been several developments that, in theory, increase the attractiveness of space-based solar power. On the one hand, critical enabling technologies have drastically improved. First, the maximum efficiency of solar panels has increased (e.g., through the application of efficiency-enhancing perovskite strips to solar PV),128 and the price of solar PV has decreased substantially because of economies of scale.129 Second, the efficiency of power transmission through microwave has increased. Third, the development of reusable launch vehicles has drastically reduced launching costs. On the other side of the equation, energy prices are at record highs, which increases the potential revenues that space-based solar power may generate.130 Although the Russian war in Ukraine has certainly contributed to the rise in energy prices, the underlying issues are structural and have been unfolding for the past few decades.131 The forward  energy markets show that energy prices could remain high for the foreseeable future.132 These developments are unfolding Russian *солнечной космической электростанции*, solar space power station),142 and has developed a smaller-scale space-based solar power system to supply energy to remote regions of the world. Turkey has announced its Günebakan design,143 which it aims to put in orbit by 2035. In 2022, the European Space Agency announced a new proposal for such a system, named SOLARIS.144 Lastly, the UK has embraced space-based solar power in its quest to achieve net zero emissions; it has made an initial £4.3 million investment for developing the technology.145

against the background of the climate crisis,133 which is now so serious that we are forced to look for creative and unconventional solutions.134 If space-based solar power is scalable, it can be an important source of long-term, reliable, and clean energy.

Previous studies have consistently concluded that space-based solar power would be theoretically possible on the condition that the technology involved is refined and that the costs come down.135 The recent improvements in solar power, wireless energy transmission, and space launch technology and the corresponding decrease in costs mean that this condition might now be fulfilled. Recent cost-benefit analyses have concluded that space-based solar power can now be deployed economically and can provide competitively priced electricity.136
There are three priority areas for research relating to space-based solar power. The first is to assess the national security implications of the successful deployment of The United States is actively pursuing the development of space-based solar power applications, including some private-sector involvement, such as the Californian Solaren corporation.137 In 2023, Caltech confirmed that its prototype MAPLE, launched aboard a SpaceX rocket in January, was capable of wireless energy transfer in space using an array of microwave power transmitters for the first time in recorded history.138 However, countries around the world—both allies and competitors of the United States—have sprung into action to develop spacebased solar power. China has taken a great interest in the technology, and in June 2022, it successfully tested a space-to-earth energy transmission design.139 By 2030, the country wants to put a megawatt-scale space solar power system in orbit,140 which would weigh over 300 metric tons and span at least one kilometer (making it the biggest manmade object in space). By 2050, it wants to build a gigawattscale system.141 Russia is developing the SKES (from the 

Recent cost-benefit 
analyses have concluded 
that space-based solar 
power can now be 
deployed economically 
and can provide 
competitively priced 
electricity.

space-based solar power.146 Research is needed on the role that space-based solar power could play in supporting U.S. 

defense operations,147 safeguarding long-term U.S. energy security (including its transition to a low-carbon and resilient power system),148 and enabling further space exploration and development.149 Policy venues, including the Federal Energy Regulatory Commission, have yet to establish a legal framework for space energy systems, and significant entities, such as the Department of Energy, remain absent from the space power innovation ecosystem.150 Researchers should also investigate how space-based solar power systems can be protected from the actions of hostile powers. Finally, such research should assess what the security implications for the United States would be if other countries successfully deploy space-based solar power.

The second priority area is to investigate whether the existing governance structures (e.g., in space, energy, and arms control) are fit for purpose to accommodate spacebased solar power.151 It is in the interests of the United States to play a leading role in ensuring that appropriate governance structures exist. Research should address the potential dual-use nature of space-based solar power systems. Researchers should also look at the governance on space crowding and initiatives to avoid cascading collisions of objects in orbit, a phenomenon called *Kessler syndrome*. 

A first step is to scope the various space-based solar power concepts that have been proposed and are under development by countries around the world. This would help researchers understand the specifics of different designs, which will inform the research on the two priority areas. Such research would also provide an idea of the players in this field and their progress toward their stated goals. An index of relevant space-based solar power designs should be created and each concept broken down into its relevant subsystems. For each subsystem, the technology maturity and relevant physical characteristics (e.g., energy conversion ratios) should be assessed. For the economic dimensions, the capital expenditure and operating expenditure of space-based solar power should be estimated and compared with earth-based energy technologies to see whether these designs could be competitive.

For the assessment of national security implications, a taxonomy of national security benefits and risks related 

Policy venues, including 
the Federal Energy 
Regulatory Commission, 
have yet to establish a 
legal framework for space 
energy systems, and 
significant entities, such as 
the Department of Energy, 
remain absent from the 
space power innovation 
ecosystem.

to the successful deployment of space-based solar power should be created using the outcomes of workshops with internal and external experts, starting from the index of relevant space-based solar power designs mentioned earlier. Strategies and corresponding actions for remediating the risks identified in the taxonomy should be developed and tested for the effectiveness of these strategies and actions in a scenario-based wargame.

For the second priority, existing governance initiatives in the fields of space, energy, and arms control should be analyzed: Are the current arrangements fit for purpose, and which additional actions might need to be taken? This analysis could be based primarily on desk-based research, complemented with expert interviews.

For centuries, people have looked at the sun as a source of limitless power. Indeed, almost all energy on earth is ultimately derived from solar power. If space-based solar power is technologically feasible and economically viable, its rollout could be a long-term solution for our global energy needs. In addition, the energy generated could be beamed to other satellites or celestial bodies. It therefore could become key infrastructure for energy provision in space, and it could provide a stepping stone for the future interplanetary exploration and expansion of our species. The successful development of space-based solar power could therefore be an important enabling technology for the future flourishing of humanity.

Scientific studies show that space-based solar power is potentially possible, both technically and economically. 

The United States and other countries are developing space-based solar power. Strategic rivals, such as China and Russia, aim to deploy space-based solar power in the next two decades. The successful development and deployment of space-based solar power would have important ramifications for U.S. national security. If the United States can successfully deploy this technology, such technology could become an important part of U.S. military energy logistics, national energy supply, and space infrastructure. The technology could thus play a key role in safeguarding U.S. primacy in military and economic affairs. However, the potential dual use of space-based solar power is concerning, and the associated risks need to be well understood. 

The world is facing a serious energy issue. Even if we abstract the issue of human-induced climate change, we are quickly running down our fossil fuel reserves. In addition, the main renewable energy technologies, such as wind, solar, and hydro, suffer from critical weaknesses in availability and storage. These issues are amplified by climate change–induced variations in weather patterns. While nuclear fission can offer consistent and carbon-free power, the large rollout of nuclear power plants would take too long to halt climate change and would quickly reduce our reserves of uranium. Nuclear fusion, despite recent breakthroughs, is still a long way from achieving a net positive energy output. It is therefore of prime importance that we explore other energy solutions.
