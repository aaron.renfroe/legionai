
Dangerous Straits Wargaming a Future Conflict over Taiwan Stacie Pettyjohn, Becca Wasser, and Chris Dougherty

## Executive Summary

Until recently, U.S. policymakers and subject matter experts have viewed the People's Republic of China's (PRC's) forcible unification with Taiwan as a distant threat. But the mix of rapid Chinese military modernization, a narrow window for localized near-parity with the U.S. military, and growing pessimism about the prospects for peaceful unification may lead the PRC to perceive that it has the ability to pursue a successful operation against Taiwan. Beijing's lessons learned from Russia's invasion of Ukraine could prompt the People's Liberation Army (PLA) to adjust its war plans for Taiwan to become more effective and deadly. Coupled together, these developments may suggest an accelerated timeline for seizing Taiwan. It is therefore urgent that the United States, in conjunction with its regional allies and partners, identify ways to deter the PRC from invading Taiwan and prevent a future conflict. 

To do so, the Gaming Lab at CNAS, in partnership with NBC's *Meet the Press*, conducted a high-level strategic-operational wargame exploring a fictional war over Taiwan, set in 2027. The wargame sought to illuminate the dilemmas that U.S. and Chinese policymakers might face in such a conflict, along with the strategies they might adopt to achieve their overarching objectives. The game was intended to produce insights as to how the United States and its allies and partners could deter the PRC from invading Taiwan and could better position themselves to defend Taiwan and defeat such aggression should deterrence fail.

The wargame indicated there is no quick victory for either side if China decides to invade Taiwan. Neither side felt as though it had lost the fight over Taiwan, and even though China hoped to achieve a swift and decisive victory, it was prepared for a long fight. Beijing was faced with a dilemma: whether to keep the war limited and hope the United States did not become involved, or to preemptively strike U.S. targets to improve Chinese probability of success, but at the high cost of prolonging the conflict. In such a scenario, neither Beijing nor Washington is likely to have the upper hand after the first week of the conflict, which suggests a protracted conflict.

Moreover, a conflict over Taiwan may quickly lead to consequences far beyond what Beijing and Washington intend. The wargame demonstrated how quickly a conflict could escalate, with both China and the United States crossing red lines. There is a high risk that deterrent signals may be misread in a potential future fight due to differences in military strengths and weaknesses, and these shape the types of escalation Beijing and Washington are likely to select. As the wargame illustrated, despite its declared policy of no first use, China may be willing to brandish nuclear weapons or conduct a limited demonstration of its nuclear capability in an effort to prevent or end U.S. involvement in a conflict with Taiwan. 

The wargame highlighted an additional asymmetry in this tension: the role that capable U.S. allies and partners could play in a future conflict. Not only does China lack such relationships, but capable military partners on the U.S. side add significant combat power, depth, and strategic significance to efforts to defend Taiwan. This further complicates PRC decision-making about how it to may choose to invade Taiwan, and about how it may seek to deter U.S. and allied involvement. 

Ultimately, the wargame indicates that the United States and its allies and partners have an opportunity to take steps to significantly strengthen deterrence and ensure that the PRC never sees an invasion of Taiwan as a profitable option. But, in order to change the Indo- Pacific military balance in their favor and develop the advancements in capability, posture, and planning that can hold PRC aggression at bay, the United States and its allies and partners must take immediate steps in several key areas. 

First, the U.S. Department of Defense should make sustained investments in long-range precision-guided weapons and undersea capabilities, while also developing additional basing access in the Indo-Pacific region to facilitate operations and enhance survivability. The DoD should deepen its strategic and operational planning with highly capable allies such as Japan and Australia to improve their collective ability to respond to Chinese aggression against Taiwan. Additionally, DoD planning should move beyond defeating a rapid invasion to consider how to fight a protracted war and make changes to facilitate longterm operations and favorable war termination. Finally, the department ought to explore the risks of escalation in the context of a war with China, so that these can be anticipated, prevented, and managed.

Second, the U.S. Congress should enable key improvements through the Pacific Deterrence Initiative and should help shape Taiwan's military posture. Third, Taiwan must improve its defensive capabilities by investing in asymmetric, resilient, and attritable capabilities by increasing training for its active and reserve forces; and by stockpiling key weapons and supplies.

## Introduction

Russia's invasion of Ukraine in 2022 shocked the world, upending the assumption that a large-scale conventional war in Europe was inconceivable. Russian President Vladimir Putin's overt use of violence shattered assumptions that revisionist states such as Russia would pursue their aims through covert subversion and coercive statecraft, below the threshold of war.1 Russia's invasion has raised fears that the world faces an authoritarian assault on freedom and democracy. Ripple effects of the Ukraine war are global, and the lessons of this conflict extend far beyond Europe's borders. 

For decades, defense analysts have warned that Chinese military modernization was shifting the balance of power in East Asia, potentially enabling China to seize Taiwan.2 Taiwan has governed itself since 1949, when Chiang Kai-Shek's Kuomintang forces fled there after their defeat in the Chinese Civil War, but the People's Republic of China has insisted that Taipei is a breakaway province that must unite with Beijing. While cross-strait tensions have waxed and waned over the past 73 years, the potential use of overt military force against Taiwan is of utmost concern to U.S. policymakers, even more so after Russia's attack on Ukraine

Until recently, U.S. officials and experts have seen forcible unification as a distant threat because of the immense challenge and risk of launching an amphibious assault on Taiwan. But China's rapid modernization and professionalization of the People's Liberation Army call this assumption into question.4 When coupled with the lack of progress by the U.S. military on acquiring advanced technologies, developing a new American way of war, and improving posture in the Indo-Pacific region to strengthen deterrence, this may lead China to perceive it has the ability to pursue a successful operation against Taiwan.5 Some worry that Chinese experts are increasingly pessimistic about the prospects for peaceful unification and, as a result, may resort to military force.6 For instance, the head of U.S. Indo-Pacific Command, Admiral John Aquilino, announced that "this problem is much closer to us than most think,"7 while his predecessor maintained that China could invade Taiwan by 2027.8

Events in Europe have made these concerns more urgent. While the invasion of Ukraine does not necessarily portend an invasion of Taiwan, China is learning from Russia's missteps and thinking about how to address deficiencies in its own forces, plans, and strategy.9 Beijing is likely also assessing U.S. and international responses to aggression against Taiwan. For its part, Taiwan is drawing lessons about Ukraine's staunch resistance and considering the capabilities, training, and operational concepts that would be needed to defend itself against a Chinese invasion.10 More recently, U.S. President Joe Biden stated that the United States would commit military resources to Taiwan should China attack. Although the White House denied any change in official U.S. policy, the president's statement veers from the U.S. approach of "strategic ambiguity," in which it is unclear whether the United States would come to Taiwan's aid in case of an invasion, and may be viewed as Washington issuing a deterrent threat to Beijing.11
Considering these developments, the Gaming Lab at CNAS, in partnership with NBC News's Meet the Press, conducted a wargame to explore a fictional war over Taiwan, set in 2027.12 The game sought to illuminate the options that U.S. and Chinese policymakers might have in such a conflict, the dilemmas they might face, and the strengths and weaknesses of various strategies. Ultimately, this wargame aimed to identify steps that the United States and its allies and partners could take to deter the PRC from invading Taiwan, and to better position themselves to defeat such aggression should deterrence fail.

The wargame produced several critical insights about a potential near-term conflict over Taiwan between the PRC and the United States.13 This report highlights the key takeaways from the game. It concludes with concrete recommendations for how the United States and its allies and partners can improve defense planning and strengthen deterrence in the Indo-Pacific, with the intent of preventing a future war over Taiwan and, if that fails, improving U.S. ability to defend Taiwan. 

## Dangerous Straits: The Wargame

The CNAS Gaming Lab conducted a two-sided wargame in April 2022 to examine a potential conflict sparked by a Chinese invasion of Taiwan in 2027. This game focused on American and Chinese decision-making at the high operational and strategic levels. The intent of the wargame was twofold. First, by filming it for Meet the Press, CNAS sought to educate the general public about how a conflict could unfold between China and the United States over Taiwan. Second, the wargame aimed to provide insight into how China may choose to fight, and how the United States and its allies and partners could defeat Chinese aggression. 

The wargame featured 10 participants, current and former U.S. government officials and subject matter experts on U.S. and PRC defense strategy. Participants were assigned to either the PRC (Red) or the U.S. (Blue) team, representing senior defense officials advising their respective presidents. The Red team sought to unify Taiwan with the PRC while preventing the United States and its allies from defending Taiwan. The Blue team aimed to defend Taiwan from Chinese aggression and keep the PRC from forcibly subjugating Taiwan. The military forces of Taiwan, Japan, and Australia were represented by the White Cell, consisting of the CNAS team, which also adjudicated interactions between the Red and Blue teams' plans.

The wargame examined a scenario in which discussions over independence in Taiwan's legislature erupted into a political crisis during which the PRC demanded immediate unification talks. After Taipei refused to join talks while refuting moves toward independence, China mobilized PLA forces and undertook a significant military buildup in its Eastern Theater Command. As it moved key forces and capabilities to forward staging areas closer to Taiwan, U.S. intelligence officials confirmed that an attack on Taiwan was imminent. Thus, the wargame began with both teams preparing for a potential attack, posturing their forces to best achieve their objectives. The Blue team's first task was to identify ways to bolster deterrence, while crafting a strategy to defend Taiwan if deterrence failed. The Red team's first task was to develop their initial invasion strategy, having received orders from President Xi Jinping to invade.

The scenario posited a future in which the PRC, Taiwan, and the United States had shored up their military strength and addressed critical deficiencies. As a result, the wargame made optimistic assumptions that each was able to make improvements that have previously proved elusive. The scenario also posited that there was ample warning of China's military buildup. Moreover, the White Cell assumed that all three forces were competent at executing military operations-albeit with different levels of proficiency. As demonstrated by the Russian military performance in Ukraine, this is a critical assumption that could impact findings and recommendations.

It is worth noting that this wargame was run only once, and thus the insights derived from the game should be tested further. Moreover, wargames are indicative rather than predictive. The decisions and outcomes of this game are plausible, but that does not make them highly probable. Nonetheless, observations from this wargame yield lessons that should inform U.S. and allied and partner planning for defending Taiwan.

## Key Insights

Several critical insights emerged from gameplay and player discussion. These insights have significant implications for a future conflict over Taiwan in the Indo-Pacific, and they represent lessons learned from the wargame. 

### Illusions of a Short War
The Red team developed a plan focused on employing a large invasion force to achieve a quick victory. The team faced a dilemma: whether to concentrate on Taiwan and avoid striking U.S. forces in the hope that they would stay on the sideline, or to attack Blue forces to gain a military advantage and hope that Blue would be willing to negotiate after Taiwan fell. The former course of action could keep the war limited and improve the chances that Red could control Taipei before other countries intervened. The latter significantly increased the prospects of winning, if Blue did become involved, but also risked a larger, more difficult, and possibly longer conflict. The second course of action ultimately won out because Red was worried that Blue would intervene regardless, and it would be easier to strike at Blue earlier than later. The Red team simultaneously launched air and missile strikes against Taiwan's forces while also preemptively attacking key Blue bases, particularly those on Guam. Red's attacks were not sufficient to knock out U.S. forces and instead prompted a strong Blue and allied response to Red's aggression.

## Red'S Invasion Plan
The maps you've provided detail an invasion plan by the "Red" force against Taiwan, with the strategy employing a large and rapid assault to achieve a quick victory. The plan seems to include a combined air, sea, and land operation, with various military units positioned to execute simultaneous attacks.

### Strategy and Execution:

- The primary goal is to force a quick capitulation of Taipei by overwhelming Taiwan's defenses before significant reinforcements can be mobilized.
- Red seeks to decapitate the leadership, erode the will to resist, cut off communications, and then rapidly deploy ground forces to secure key positions in northern Taiwan.
- There is a clear intent to airdrop forces and to ferry additional ground forces to capture strategic points rapidly, likely to secure control of the area and create a favorable position for negotiations or complete control.
- Preemptive attacks on key bases, especially those on Guam, are part of the strategy to prevent or delay intervention from the "Blue" force (likely representing U.S. forces).

### Units and Movements:

From the maps, the following units and movements are depicted:

**Ships and Naval Forces:**
- DDG (Destroyer): Positioned off the eastern coast, they may serve as part of a naval blockade or for launching missiles.
- LHD (Amphibious Assault Ship): Also off the eastern coast, likely to launch helicopters and landing crafts carrying marines.
- LST (Landing Ship, Tank): These are positioned to land heavy vehicles and troops directly onto shore.
- Renhai-Class Cruiser: Equipped with advanced radar and missile systems for air defense and anti-ship roles.
- Luzheng III-Class Destroyer: Featuring in anti-air and anti-submarine warfare capabilities.
- Yuzhao-Class LPD (Amphibious Transport Dock): Used for transporting troops, vehicles, and landing craft.

**Aircraft:**
- J-16 (Fighter Aircraft): Potentially involved in air superiority and ground attack missions.
- J-11A/B/BS (Fighter Aircraft): These would also participate in achieving air superiority and possibly engage Blue force assets.

**Air Assault:**
- Helicopter air assault operations are designed to transport troops directly into combat or strategically important areas.
- These units are likely tasked with the rapid seizure of ports and airfields, such as the Port of Keelung and Taoyuan Air Base.
- Helicopters provide flexibility and speed, allowing forces to bypass traditional defenses and reinforce landed troops quickly.

**Airborne Paratroopers:**
- Airborne forces are depicted as being airdropped over northern Taiwan, which suggests a plan to capture critical infrastructure and establish a foothold inland.
- This type of operation typically aims to disrupt enemy rear areas, command and control, and logistics, contributing to a collapse of the defensive structure.
- Paratroopers can secure bridges, roads, and other key points to support the advance of the main invasion forces or to cut off potential retreats or reinforcements.

By employing both air assault and airborne units, the Red force would be able to project power quickly beyond the coastline, support the amphibious landings, and exploit any initial successes to press their advance. This two-pronged air strategy would complement the naval landings and help achieve the objective of a swift victory by seizing key objectives and spreading the defending forces thin.

**Staging Areas and Targets:**
- The units are staged in the waters east of Taiwan, indicating an eastern approach to the island.
- Planned movements are towards the northern ports and beaches of Taiwan, with airdrops and amphibious landings.
- Strikes on air bases in Taiwan, such as Taoyuan and Hsinchu, are indicated, likely aiming to neutralize Taiwan's air capabilities.

The Red team sought to force Taipei to capitulate before Blue's forces could recover from Red's opening blow. Red aimed to decapitate Taiwan's leadership, launch punitive strikes to erode Taiwan's will to resist, and cut off communications to the island to reduce Taipei's strategic messaging aimed at rallying international support. Alongside these strategic attacks, Red sought to rapidly ferry ground forces to beaches and ports in northern Taiwan, in addition to airdropping forces in the same region, to capture Taipei as quickly as possible. Red believed this would generate enough momentum to conquer the northern half of the island and compel the rest of the country to surrender. The United States and its allies would then face the unpalatable prospect of rolling back Red's territorial gains, which would be incredibly difficult-if not impossible- and ultimately conclude that it was not worth the cost.16 
Red's quick victory proved elusive. Enabled by its control of the skies over Taiwan, Red amphibious, airborne, and air assault invasion forces made it ashore, but encountered fierce resistance.17 Red forces occupied a beach and airfield north of Taipei but suffered heavy losses. When the game ended, Red's invasion force still had to traverse mountainous and heavily defended terrain to reach the capital. Moreover, Red also had to find a way to provide fuel, food, and ammunition to its forces ashore while Blue forces attacked its vulnerable lines of communication.18 
Ultimately, neither side was able to decisively win in the initial week of fighting. Both Red and Blue had expended large portions of their inventories of precision long-range missiles, lost many fighter aircraft, and needed to resupply and rearm forces under attack. However, Red still had civilian assets it could use to continue its cross-strait invasion, aided by its geographic proximity to Taiwan. 

We do not know what the long-term outcome might have been, but a protracted conflict appeared to be plausible. Red's rapid assault failed in this fictional scenario, due to Blue and Taiwan's smart investments in capabilities, training, and munitions. Blue's plans had focused on halting Red's cross-strait attack, particularly its navy. When Blue's submarines had fired all their torpedoes and its aircraft had launched most of their long-range precision munitions, it had limited options for immediately generating offensive combat power. Neither side felt as though it had lost the fight over Taiwan. Even though China hoped to deter U.S. and allied and partnered involvement to support Beijing's desired quick victory, it was prepared for a long fight. 

This suggests that should a Chinese invasion of Taiwan occur it would be essential for Taiwan and the United States not to lose the war in the opening days. But preventing China from its preferred quick triumph over Taipei would not equate to an American and Taiwanese victory. Therefore, U.S. and allied and partnered defense planners need to think beyond the opening week to consider how to win a protracted war.

## Falling Off Escalation Ladders

After Red attacked Blue forces on bases in Japan and Guam, the conflict spiraled in a series of titfor-tat escalations, as each team attacked the other's territory more aggressively in response to prior attacks. These offensives were not purely punitive. Oftentimes, they were also driven by military necessities. The Red team believed it could not risk allowing U.S. forces to thwart the initial wave of its amphibious assault and thus decided to preemptively hit American forces and bases in Japan and Guam. It also would have attacked a U.S. aircraft carrier if one had been within missile range. In response, the Blue team used bombers to launch cruise missile strikes at Red ships in port, which were easier targets than ships sailing toward Taiwan.

The escalations increased with each passing turn. Each side viewed its own attacks as proportional and constituting a message to the opponent that he must desist from certain actions or face more severe consequences. But the situation quickly increased the scope and intensity of the hostilities. Because Red had already attacked American territory, it felt it had little to lose by launching a cruise missile strike at U.S. bases in Hawaii in retaliation for Blue attacks on its ports. The Red team also launched missile salvos at U.S. allies including Japan, Australia, and the Philippines. To degrade Red's command and control and targeting, Blue attacked its Eastern Theater Command headquarters in Nanjing and early warning radars on Chinese territory. In response to the continued strikes on its territory, including a thwarted strike on an H-20 bomber base, Red wanted to target military installations in the continental United States, but did not have the forces to do so.

The Blue team possessed a significant ability to attack Chinese forces with precision-guided weapons in this wargame, although its stores of advanced long-range missiles quickly ran low. In contrast, Red's ability to project conventional power beyond the second island chain in 2027 remained quite limited. Red only had a small number of long-range bombers and aerial refueling tankers, relative to those that would have been necessary to retaliate against the continental United States with conventional weapons.19 While cyber weapons were considered as a means to reach such targets, the Red team favored conventional attack options because they carried a stronger signal. The Blue team likewise considered more aggressive actions in cyberspace but restrained itself out of concern that the United States likely presented a larger, and more vulnerable target for strategic cyberattacks.

Escalation in this game did not adhere to the traditional model of a symmetric ladder that each side climbed deliberately and could stop at any point.20 Instead, both sides had their own escalation ladder with different rungs, reflecting their different approaches, perceptions, and capabilities. But what was most striking was that their actions quickly led to consequences beyond the intentions of both teams. Neither seemed to receive the other's message that their actions were calibrated, proportional, and intended to force de-escalation. 

Moreover, Red and Blue had different strengths and weaknesses that shaped the type of escalation they chose. Red had a stronger hand initially, but it lacked credible conventional responses to attacks on its territory, leading it to turn to its nuclear arsenal. Blue had more conventional and nuclear options but was more concerned about escalation in cyberspace. Before they knew it, both Blue and Red had crossed key redlines, but neither was willing to back down. Therefore, escalation was less of a gradual and controlled climb to the top than a quick race to the bottom, where both teams had fallen off their ladders and ended up in uncharted territory.21 
This demonstrates how a conflict over Taiwan may quickly descend into an escalatory spiral. China and the United States may struggle to communicate redlines, leading to deterrent signals being misread by both Beijing and Washington in such a conflict.

## Thinking the Unthinkable: Limited Nuclear
Use In the first move of the wargame, the Red team took a page from Putin's playbook in Ukraine and threatened to use nuclear weapons to deter outside intervention in an "internal affair" as well as any attacks against the Chinese mainland.22 Despite China's policy of no first use, there was little debate in the Red team about brandishing nuclear weapons. The team agreed to issue the nuclear threat, and, in the pre-initiation of hostilities, to start deploying Chinese nuclear-missile submarines into waters where the weapons could reach the West Coast of the United States. Although preemption would provide a military advantage, the Blue team dismissed the idea because of China's nuclear weapons and fears of escalation. Blue also sought to reinforce its strategic posture and issued statements that Red would face severe consequences for being the first to use nuclear weapons. Throughout the game, the Blue team was also careful to avoid attacking targets that might threaten the Chinese regime. Ultimately, the Blue team believed the superior size and sophistication of its nuclear arsenal would deter Chinese nuclear use. 

The Blue team's faith in its strategic deterrent proved to be a mistake. As the conflict spiraled with tit-for-tat attacks on Red and Blue territory, the Red team felt that it needed to up the ante from its airbase attacks on Japan, Guam, and Hawaii to protect its core interests and prevent further attacks on its mainland. Lacking conventional long-range strike options, the Red team detonated a nuclear weapon not far from Hawaii to demonstrate the credibility of its threats. This high-altitude nuclear explosion resulted in an electromagnetic pulse that would have disrupted and potentially destroyed any unshielded or unprotected electronics on nearby ships or aircraft, but otherwise did not directly impact Hawaii. Nonetheless, it was the first use of a nuclear weapon during a conflict since World War II. Since this occurred during the last move of the wargame, we do not know how the Blue team would have responded, but this likely would have been viewed as a major escalation.

There is a risk that a future conflict between the United States and China could go nuclear. In a conflict over Taiwan, Beijing may be willing to forgo its stated no-first-use policy to brandish or detonate a nuclear weapon in an effort to prevent or end U.S. involvement in the war. While an unlikely possibility is an outright surprise strategic exchange as envisioned during the Cold War, there is a risk that China could employ nuclear weapons in a limited way. Should that occur, it is unclear where the conflict would go. This suggests that the United States and its allies and partners need to discuss and plan for potential scenarios centered around China's limited nuclear use. 

### Tit-For-Tat Bombing Attacks
The map shows a sequence of military strikes between two opposing forces, identified as Red and Blue. The locations marked on the map indicate strategic points that have been targeted by each side as part of a broader conflict scenario.

**Initial Red Strikes:**
- **Yokosuka Navy Base, Japan:** A key facility for the U.S. 7th Fleet, its proximity to Tokyo makes it an important strategic target for disrupting naval operations in the Pacific.
- **Yokota Air Base, Japan:** Essential for air operations in the Western Pacific, targeting it would affect the U.S. Air Force's ability to conduct missions in the area.
- **Tokyo:** Striking a political and population center like Tokyo would be a significant escalation, likely intended to shock and pressure the opposing government.
- **West of Iwakuni Air Base, East of Sasebo Air Base:** Both are important for the U.S. Marine Corps and Navy operations; attacks here would aim to weaken the U.S. military presence and capabilities in the region.
- **Kadena Air Base:** Located in Okinawa, this is a principal air hub for U.S. military operations in the Western Pacific.
- **Northern Mariana Islands:** This would disrupt U.S. military activities in this region and could impact the U.S. military's strategic positioning.
- **Guam:** As a major staging and forward operations base for the U.S., an attack here would be significant, aiming to reduce U.S. military projection capabilities.

**Follow-on Red Strikes:**
- **Tindal, Australia:** It houses important facilities for the Royal Australian Air Force and would be a strategic target to reduce the capabilities of U.S. allies.
- **Northern Philippines:** Likely targeting facilities that could be used by U.S. forces, it would be an attempt to diminish regional support for Blue.
- **Southwestern Japan, Southeast of Kure Naval Base:** Attacking this area would affect Japan's Self-Defense Forces and any U.S. forces operating in concert with them.
- **Southern Coast of Japan Directly South of Komatsu Air Base:** Aiming here would disrupt the Japanese Air Self-Defense Force's ability to support U.S. operations.
- **Northwest of Tokyo:** Another strike near the capital would compound the pressure on Japan, an ally of Blue.
- **Misawa Air Base:** A northern base that provides significant reconnaissance and intelligence capabilities to the U.S. forces.

**Blue Retaliatory Strikes on Mainland China:**
- The locations are not specified, but given their placement on the map, they are likely targeting military command centers, air bases, or other strategic assets inland and on the coast to degrade Red's military command and control as well as their air defense capabilities.

This pattern of strikes aligns with the provided context of a tit-for-tat escalation, where each side progressively expands the conflict in response to the other's actions. The Red team's initial wave of attacks aimed at crippling U.S. power projection capabilities in the Pacific, particularly focusing on bases and installations key to air and naval operations. This preemptive strategy is to ensure the success of their amphibious assault on Taiwan. In response, the Blue team's counterstrikes on mainland China are aimed at reducing Red's ability to command, control, and effectively project power, marking a significant escalation by targeting the adversary's homeland.

The initial Red strikes occurred as a preemptive measure to neutralize or impair Blue forces that could potentially thwart Red's amphibious assault on Taiwan. The specific timeline isn't given, but these strikes would likely have taken place at the very onset of hostilities or immediately after any perceived imminent threat from Blue forces stationed in or around Japan and Guam. The objective of Red was to gain a military advantage by striking first and attempting to diminish Blue's capacity for a swift and effective counter-assault.

The follow-on Red strikes would have occurred after the initial wave of attacks, possibly as a response to Blue's retaliatory actions or as a continuation of Red's initial offensive operation. These follow-on strikes were directed at further military targets to sustain the momentum of the initial attack and to keep Blue forces under pressure, degrading their operational capabilities.

The nuclear detonation near Hawaii, as shown on the second map, is likely a significant escalation that occurred after several rounds of tit-for-tat strikes and retaliations between Red and Blue. Given that Red had already attacked American territory and saw little to lose further, the nuclear strike near Hawaii could have been an attempt to demonstrate Red's resolve or to deter further Blue attacks on its territory and assets. The use of a nuclear detonation, even offshore, represents a grave escalation that would have severe strategic and geopolitical implications, signaling a departure from conventional warfare and indicating that the conflict had reached a new, more dangerous level of confrontation.

The strikes depicted in the map correspond to a sequence of events in a hypothetical scenario where each round of attacks escalates the conflict. 

- The **initial Red strikes** likely occurred at the outset of the conflict, meant as a preemptive move to cripple U.S. and allied forces' ability to respond effectively to the amphibious assault on Taiwan.  
- The **follow-on Red strikes** would have come after the initial strikes, potentially within the first 24 hours or extending into the second day. These follow-on strikes would indicate a continued effort to sustain the momentum of the initial assault, degrade further the opponent's military capabilities, and perhaps to respond to any immediate retaliatory actions taken by the Blue forces.
- The **nuclear detonation near Hawaii** represents a significant escalation, likely occurring after several rounds of tit-for-tat conventional strikes and after Red's perception that it had little to lose following attacks on its territory. The timing of such an event is not explicit in the initial description, but it would presumably happen after the initial conventional strikes had already escalated the conflict, possibly days into the engagement, given the gravity of using a nuclear weapon.

These strikes and the nuclear detonation are steps in an escalating conflict spiraling out of control, where each side sees its actions as proportional responses. The use of a nuclear weapon near Hawaii would be a substantial and grave escalation, representing a severe break in the conflict's pattern, and changing the nature of the hostilities dramatically.

## An Asymmetric Advantage: Allies and Partners

In the wargame, Red faced a coalition of states that helped defend Taiwan-not just Washington and Taipei. Japan and Australia not only provided Blue forces with access to bases, but also contributed their highly capable aircraft and ships to combat operations. They did so because of the shifting sentiment toward authoritarian aggression that had accelerated in the wake of the Ukraine war. Red air and missile strikes against bases on their territory further helped push them to intervene. The Philippines did not participate in operations but supported the coalition by permitting U.S. forces to operate from its territory. 

In contrast, China lacks allies. While experts debate whether some of China's partners, namely Russia, would join the fight, this was not represented in the game. Instead, the Red team discussed how North Korea and Russia could undertake actions to distract the Blue team, and how China could obtain advantageous basing access in a country such as Cambodia. These efforts paled in contrast to the contributions of Australia, Japan, and the Philippines to the Blue team's effort. This highlights a critical asymmetry in such a conflict: China would likely stand alone against a highly capable coalition of forces that regularly operate together. 

The Red team was acutely aware that if U.S. allies came to the defense of Taiwan, this would negatively shift the odds against China. Australia and Japan both provide sophisticated air and maritime forces that are in many cases superior to Chinese capabilities. Their entrance into the fight would augment Blue's capacity and help deny Red a rapid military victory. The Blue team was also aware of this advantage and tried to leverage the improved ties between Washington, Canberra, and Tokyo to strengthen deterrence in the first move. At the outset of the conflict, Blue naval ships were conducting a joint exercise with Japanese, Australian, and Taiwanese ships in the Philippine Sea. The combined fleet remained outside the reach of Red's intermediate-range ballistic missiles and therefore prioritized survivability over directly contributing to the defense of Taiwan. However, the fleet was intended to send a strong political deterrent signal, and this might have succeeded had the Red team not been ordered to force unification.

In the first move, the Red team had a heated discussion about how to separate Blue from its allies. Red believed that while it could not stop Blue from coming to Taiwan's defense, it could drive wedges between Washington, Tokyo, and Canberra. Because Red viewed Blue intervention as inevitable, its approach was to strike Blue forces hard at the onset of the conflict to weaken U.S. combat power, while refraining from attacks on Japanese or Australian forces. This was intended to send a deterrent message to Tokyo and Canberra that they would be spared if they remained out of the fray. 

However, in practice Red found this strategy difficult to implement. In its opening salvo, Red launched ballistic and cruise missile strikes against Blue bases and air defenses in Japan. Red intentionally avoided striking shared U.S.- Japanese bases or Japanese-only bases. Red, therefore, opted to leave unscathed a significant number of U.S. aircraft to keep Tokyo out of the fight. But because the security forces and civilians of host nations typically provide protection and services on facilities used by U.S. forces even when these are not co-located with those of the host nation's military, it was difficult for Red to discriminate between Blue and Japanese targets. Moreover, Red's base strikes still amounted to an attack on Japanese soil, leading Japanese forces to enter the waralthough they primarily bolstered defenses on their territory and conducted limited operations in the East China Sea. 

This highlights the tension in the Red team between its need to degrade Blue forces early in the conflict and its desire not to provoke additional intervention. Perversely and ironically, Red's actions were the driver for such third-party intervention. Red's actions also broadened the aperture of the conflict, because of the perceived need to take punitive actions against third-party nations for joining the fight. The Red team continually expended its high-end medium- and intermediate-range ballistic missiles and cruise missiles against Japanese, Australian, and Philippine military targets, while also targeting U.S. forces and launching an amphibious invasion and bombing campaign in Taiwan. This diluted the Red team's attention and stretched critical missile inventories, distracting from its primary operation: the invasion of Taiwan. 

China's lack of alliances and the U.S. emphasis on coalition warfighting would likely color a future conflict. By contributing combat power, critical access, and basing, U.S. allies and partners are of strategic significance to efforts to defend Taiwan. All these factors could turn the tide of a Chinese invasion of Taiwan and complicate China's decision-making, including its military strategy. This stresses how important it is that the United States identify a "coalition of the willing" in advance of such a conflict to provide ample time to develop war-winning strategies that integrate allies and partners and improve interoperability. 

## Recommendations For Improving Deterrence In The Indo-Pacific
The most significant takeaway from this wargame is that there will be no swift victory if China decides to invade Taiwan. While neither side felt as though it had lost the fight over Taiwan, neither had a decisive upper hand in the conflict after a week of fighting. Despite Red's hopes to achieve a swift and decisive victory, it remained prepared for a long fight due to the importance of unification. But an invasion would be an extremely risky gamble for the PRC, as it would require betting that its unproven forces would be able to competently execute an intensely complicated joint operation. If the Taiwanese population resisted as the Ukrainian population has, they could make a challenging operation very costly-at great expense to themselves. Even if China proved less capable than this wargame assumed and could not put ashore a large number of forces on Taiwan, it remains the case that China has the conventional firepower to cause widespread suffering, destruction, and devastation. 

Based on its modernization plans, China is projected to have a larger and increasingly sophisticated nuclear force by 2027 to complement its growing conventional capabilities. A conflict over Taiwan may be difficult to contain, given the operational advantages of striking the other's territory first. Given the nuclear risks and rapid escalation highlighted in this wargame, to say nothing of the catastrophic human and economic damage from a war, it is  critical for the United States, Taiwan, and allies such as Japan and Australia to strengthen deterrence to prevent a conflict over Taiwan from ever occurring. Moreover, U.S. policymakers need to place a renewed emphasis on escalation management, so that they can understand potential red lines and flashpoints and reduce the risk of inadvertent escalation.

For decades, U.S. policymakers believed they had ample time to respond to China's rise as a military power. Rather than pivoting or rebalancing to the Indo-Pacific, U.S. administrations have become distracted with wars, unrest, and instability elsewhere. This has allowed China to significantly close the gap in military power, to the point where it could believe that victory in a war over Taiwan is a distinct possibility. 

The lessons learned from this wargame directly lend themselves to actions to strengthen deterrence. Recommendations for the U.S. Department of Defense and Congress, and for Taiwan, are as follows:

## For The U.S. Department Of Defense

### Make sustained investments in long-range precision-guided weapons. 

The United States needs to build an inventory sufficient to strike the number of mobile and fixed targets that would be required in this conflict, thus denying China a swift and decisive victory. At present, the U.S. inventory of long-range munitions is insufficient to deny China a swift victory or weather a lengthy war of attrition. The DoD should also seek to enter into agreements with allies and partners, such as Australia, to enable sharing stockpiles of weapons during such a conflict.

### Increase Investments In Undersea Capabilities.

In the wargame, undersea capabilities proved to be a key U.S. advantage. While the current shipbuilding plan envisions procuring the maximum number per year of submarines that the industry can generate, the United States should explore options to expand its industrial base and augment its attack submarines with unmanned underwater vehicles. Additionally, the United States should explore ways to sustain undersea warfare in a forward theater by rearming and resupplying submarines at sea and in protected forward bases.

### Strengthen posture in the Indo-Pacific region. 
Bolstering U.S. posture requires obtaining additional basing access and developing a system of layered passive and active defenses on existing bases.25 The wargame illustrated that U.S. intervention in this conflict depended on being able to absorb a Chinese first strike by dispersing forces on bases throughout East Asia. The United States currently does not have sufficient access or supporting forces and equipment to enable this type of operation, despite the DoD's embrace of distributed operational concepts aimed at enhancing survivability.

### Deepen strategic and operational planning with Japan and Australia to improve preparations to respond to Chinese aggression. 

Advance planning would enable coordinating a swift response should a Chinese invasion appear imminent. While ground- and space-based sensors have made it difficult to disguise a large troop buildup or enact stratgegic surprise, a few months is not sufficient warning time to arrange the delicate political conversations that would be necessary regarding what forces and capabilities various nations would contribute, let alone to address the sensitive topic of political and legal constraints. Developing a common understanding of capabilities, national caveats, and strategic and operational preferences well in advance is essential to strengthening deterrence and developing coalition strategies that account for the preferences and mandates of allies and partners.

### Move beyond thinking about how to defeat a rapid invasion. 
China's envisioned quick invasion of Taiwan currently dominates thinking in the department about how a conflict with China over Taiwan could unfold. Planners should begin considering how to fight a protracted war, and how to make the subsequent investments and changes that the department would need to facilitate long-term operations and favorable war termination. Such changes may include focusing on contested logistics, including munitions stockpiles, spare parts, and fuel, so that the United States can project a sustained amount of combat power beyond the first few days or weeks of a war.27 The department needs to also consider how to get supplies and weapons to support Taiwanese defenders.

### Explore the risks of escalation in the context of a war with China to anticipate, prevent, and manage escalation. 

China is on its way to becoming a nuclear great power. Beijing is significantly expanding the size of its nuclear arsenal and developing a triad of nuclear delivery systems. With these enhanced capabilities, China may be more willing to undertake conventional aggression and brandish or use its nuclear weapons to deter outside intervention. The Pentagon must bolster its conventional forces to strengthen deterrence, but it also needs to be mindful of the risks of inadvertent escalation and take steps to manage it during a crisis and war.

## For The U.S. Congress

### Enable key improvements through the Pacific Deterrence Initiative (PDI). 

Congress has a key role to play in PDI oversight, which means it should ensure that DoD funding is going to priority efforts-including those mentioned here-such as improving posture and munitions stockpiles in the Indo-Pacific. Congress has the remit to improve PDI requests and redirect funds to priority efforts and capabilities. Should the DoD not budget for activities and investments that truly strengthen deterrence, it is incumbent upon Congress to ensure that resources are directed to those that do. A focal point of PDI should be making improvements to base infrastructure and investments in supporting equipment and personnel, so that U.S. forces can conduct distributed operations. If these investments are not made and Chinese missile attacks can concentrate on a handful of bases, U.S. forces could be knocked out in the opening blow.

### Help Shape Taiwan'S Military Development.

Congress has an oversight role for U.S. arms sales, including both foreign military sales (FMS) and foreign military financing (FMF). It should encourage all FMS and FMF to Taiwan to align with the capabilities and training that Taipei truly requires to repel a Chinese invasion. 

## For Taiwan

### Improve defensive capabilities by investing more resources in asymmetric, resilient, and, in some cases, attritable systems. 

This requires a shift away from the country's current acquisition approach, which emphasizes large surface ships and manned aircraft. In particular, Taiwan should acquire mobile air defenses, additional anti-ship and coastal defense cruise missiles, sea and land mines, artillery, anti-tank guided munitions, loitering munitions, and unmanned aerial vehicles. Taipei has made some strides in these areas, but this scenario posited more changes than are currently planned to Taiwan's defense posture and operations. 

### Invest in military training for asymmetric defensive operations against a capable adversary. 

Taipei must couple its acquisitions with improved training for Taiwan's armed forces to enable them to effectively use these weapons and operate while under attack. Training efforts should extend to Taiwan's reserve forces to improve their military effectiveness, which was higher in the game than current levels of readiness suggest. 

### Stockpile critical supplies, so that it is prepared to mount a strong and sustained defense against a Chinese invasion. 

Because resupply would be difficult during an open conflict, Taiwan should store stocks of critical provisions that it would need to fight a long war. These should include civilian materiel such as medicines and fuel, as well as weapons.

## After Action Report
Both Red (representing China's forces) and Blue (representing U.S. forces and their allies) engaged in a series of strategic and tactical maneuvers aiming to achieve their objectives. The following fictional account synthesizes the provided after-action report and maps into a timeline and analysis of the tactics and strategies employed:

**Day 1: Initiation of Hostilities**
- Red commences a large-scale invasion of Taiwan with a primary goal to force a swift capitulation of Taipei.
- Simultaneously, Red launches preemptive strikes on U.S. and allied forces, targeting key bases in Japan, Guam, and other locations critical to U.S. power projection in the Pacific, hoping to delay or deter Blue's intervention.
- Blue reinforces its strategic posture and begins to maneuver its naval forces to safe distances, out of range of Red's missile capabilities.

**Day 1-2: Blue's Tactical Response**
- Blue responds with precision-guided missile strikes on Red naval assets in port, seeing them as easier targets than the forces en route to Taiwan.
- Blue also targets Red's military command and control assets on the Chinese mainland, aiming to disrupt the invasion coordination.

**Day 3: Escalation of the Conflict**
- As a response to Blue's strikes, Red conducts follow-on attacks, expanding the scope of its initial strikes to include bases in Australia and the Philippines, alongside additional bases in Japan.
- Red's decision to continue with these follow-on attacks suggests a determination to degrade Blue's regional military capabilities and an assumption that Blue would engage regardless of Red's actions.

**Day 4: Blue's Extended Campaign**
- Blue further employs its undersea capabilities, leveraging its submarines to target Red's naval assets and protect the maritime approaches to Taiwan.
- Blue's regional allies, such as Japan and Australia, increase their defensive postures and contribute to the coalition efforts against Red.

**Day 5: Red's Display of Force**
- Facing persistent counterattacks from Blue and its allies, and with limited conventional long-range strike options, Red escalates dramatically by detonating a nuclear weapon offshore near Hawaii.
- This unprecedented act is a strategic message of Red's resolve and a demonstration of its willingness to use nuclear capabilities.

**Beyond Day 5: Protracted Warfare**
- Neither side achieves a decisive victory; instead, the conflict extends into a prolonged engagement, with both sides suffering significant losses and depleting their long-range precision munitions.
- Red continues its efforts to invade and hold Taiwanese territory, but faces stout resistance and logistical challenges.
- Blue and its allies regroup and focus on sustained operations, signaling a transition to a war of attrition.

**Strategic Overview and Aftermath:**
- Red employs a strategy of rapid dominance, aiming to decapitate Taiwanese leadership and control critical infrastructure swiftly.
- Blue focuses on deterrence, force preservation, and attritional warfare, leveraging its allies and advanced technology.
- The wargame demonstrates the risks of miscommunication and miscalculation leading to a rapid and uncontrollable escalation to nuclear use.
- Both Red and Blue face strategic dilemmas: Red grapples with the limits of its conventional power projection and the high stakes of nuclear brinkmanship, while Blue confronts the challenges of alliance management and the complexities of a multi-front conflict with an adversary capable of nuclear escalation.
- The aftermath is a tense standoff with global repercussions, including severe strain on international relations and the potential for a broader conflict.
