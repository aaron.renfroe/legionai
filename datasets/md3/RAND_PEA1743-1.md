Expert Insights PERSPECTIVE ON A TIMELY POLICY ISSUE

## U.S. Military Theories Of Victory For A War With The People'S Republic Of China

JACOB L. HEIM, ZACHARY BURDETTE, NATHAN BEAUCHAMP-MUSTAFAGA
FEBRUARY 2024

## Contents

The Realities of Strategic Conflict in the 21st Century ...................................3
Identifying Potential Theories of Victory .........................................................6
Identifying Potential Escalation Pathways ....................................................10 What Are the Prospects of Success for Military Cost-Imposition? .............16 What Are the Prospects of Success for Denial? ..........................................22
Conclusion ....................................................................................................32

About RAND
The RAND Corporation is a research organization that develops solutions to public policy challenges to help make communities throughout the world safer and more secure, healthier and more prosperous. RAND is nonprofit, nonpartisan, and committed to the public interest.

Research Integrity Our mission to help improve policy and decisionmaking through research and analysis is enabled through our core values of quality and objectivity and our unwavering commitment to the highest level of integrity and ethical behavior. To help ensure our research and analysis are rigorous, objective, and nonpartisan, we subject our research publications to a robust and exacting quality-assurance process; avoid both the appearance and reality of financial and other conflicts of interest through staff training, project screening, and a policy of mandatory disclosure; and pursue transparency in our research engagements through our commitment to the open publication of our research findings and recommendations, disclosure of the source of funding of published research, and policies to ensure intellectual independence. For more information, visit www.rand.org/about/research-integrity. RAND's publications do not necessarily reflect the opinions of its research clients and sponsors. 

 is a registered trademark.

Limited Print and Electronic Distribution Rights This publication and trademark(s) contained herein are protected by law. This representation of RAND intellectual property is provided for noncommercial use only. Unauthorized posting of this publication online is prohibited; linking directly to its webpage on rand.org is encouraged. Permission is required from RAND to reproduce, or reuse in another form, any of its research products for commercial purposes. For information on reprint and reuse permissions, please visit www.rand.org/pubs/permissions. For more information on this publication, visit www.rand.org/t/PEA1743-1. © 2024 RAND Corporation G
rowing tensions between the United States and the People's Republic of China (PRC) have led to increased concerns about a conflict breaking out across the Taiwan Strait (Beckley and Brands, 
2022; Rudd, 2022). While U.S. policy aims to deter such a war in the first place, the U.S. defense community needs to prepare for the risk that deterrence might fail by thinking carefully about what success would look like in a war over Taiwan and how to achieve it. Wars are much easier to start than to end, and the United States needs to have a clear vision from the outset of how it plans to end the war on favorable political terms while avoiding catastrophic escalation (Walt, 2022; Iklé, 2005). Unlike the conflicts the United States has fought against such regional adversaries as Serbia and Iraq, a war with a nuclear-armed great power would entail escalation risks that the United States has not faced since the Cold War. 

A *theory of victory* is a causal story about how to defeat an adversary. It offers a "story line explaining why we think things will turn out the way we wish" (Cohen, 2022b). That narrative should answer the question General David Petraeus posed in Iraq: "Tell me how this ends?" (O'Hanlon, 2022). This requires identifying the conditions under which the enemy will accept defeat and then outlining how to shape a conflict in a way that creates those conditions. Theories of victory are the "principal tenets" that explain how and why a strategy will work rather than strategies themselves, which involve more detailed considerations about how to link the desired ends with the available means (Cohen, 2022a, p. 33; Hoffman, 2020, p. 61; 
Roberts, 2020, pp. 27–28). Good theories of victory should have clear political objectives, be concise, consider how the enemy will react (both militarily and politically), and consider how other involved countries (e.g., coalition partners) will react. While a theory of victory can and should include all instruments of national power, the U.S. Department of Defense (DoD) has a particular need for a framework that explains how military operations will translate into achieving a war's political objectives. Military theories of victory have historically been crafted through discussion between a U.S. president and his most senior military advisor; for example, President Franklin D. Roosevelt and Admiral William D. Leahy during World War II (O'Brien, 2019).

The focus here is on wartime theories of victory rather than theories for how to succeed in peacetime competition. Once a war has started, the central task has shifted from deterrence to compellence, and this requires a different analytical approach.1 The theoretical frameworks and policies optimized to deter the PRC from gambling on a war over Taiwan in the first place may be different from those optimized to convince Beijing that the gamble has failed and that it should stop fighting rather than double down.

This paper outlines potential U.S. theories of victory for a war with the PRC over Taiwan and analyzes their associated escalation risks. We begin by emphasizing the importance of seriously considering theories of victory given the stakes of a hypothetical great-power war. Next, we identify the five theories of victory that are available to U.S. and coalition decisionmakers, and then we explore their viability and categorize them by the level of escalation risk entailed. Finally, we consider the prospects of success for the two most viable theories of victory: denial and military cost-imposition.

In our survey of the available theories of victory for the United States in a hypothetical conflict with the PRC over Taiwan, we find that a denial theory of victory offers the best chance for delivering victory while avoiding catastrophic escalation. The most popular alternative, military cost-imposition, has lower prospects of success and higher 

## Abbreviations

chances for catastrophic escalation. This is due primarily to the potential peril in navigating what we call the "Goldilocks Challenge," which requires identifying a sweet spot of PRC pressure points that are important enough to change Beijing's decisionmaking while avoiding unacceptable PRC escalation in response. 

There are two major reasons for contemporary defense planners to spend time assessing theories of victory for a future war and their escalation risks. First, the force structure choices the United States makes today will influence which theories of victory are viable a decade from now. Changes to force structure require large lead times, and the United States faces resource constraints that make it implausible to invest equally in all options without spreading resources thin. Second, studying theories of victory during peacetime is critical for preparing senior military officers and civilians to provide informed advice to future presidents if deterrence fails and war breaks out. Given the consequences of nuclear use or widespread conventional attacks against the U.S. homeland, senior officers and officials need to be conversant in the risks that different theories of victory create to provide the clearest and best advice possible to the president. Failing to strike an effective balance between the desire for operational success and the imperative to manage escalation with a nuclear-armed adversary poses an existential threat to the United States. 

| CCP                       | Chinese Communist Party               |
|---------------------------|---------------------------------------|
| DoD                       | U.S. Department of Defense            |
| GDP                       | gross domestic product                |
| IISS                      | International Institute for Strategic |
| and International Studies |                                       |
| PLA                       | People's Liberation Army              |
| PRC                       | People's Republic of China            |
| SIPRI                     | Stockholm International Peace         |
| Research Institute        |                                       |

## The Realities Of Strategic Conflict In The 21St Century

The vital ingredient for 
limited wars in the nuclear 
era is establishing limited 
political objectives.

Since the end of the Cold War, the United States has become accustomed to fighting wars that are fundamentally different from what it would face in a U.S.-PRC 
conflict. When fighting regional adversaries, the United States has had overwhelming power advantages and faced adversaries with limited escalation options. This placed few constraints on the United States, which could pursue maximal war aims, such as regime change, without concern for retaliation. By contrast, key features of a great-power war in the 21st century include the serious potential for U.S. defeat and catastrophic escalation. The growing power of the People's Liberation Army (PLA) has raised the possibility that the PRC could successfully seize Taiwan, and its survivable nuclear deterrent and nascent long-range conventional strike capabilities give it the ability to do serious damage to the U.S. homeland—an ability that will continue to grow by 2030.2 The PLA's test of a novel intercontinental missile (potentially a fractional orbital bombardment system) in 2021 is an example of a long-range strike capability that it could someday use for conventional or nuclear strikes against the United States (Sevastopulo and Hille, 2021). Beijing's cyber capabilities give it an additional tool to inflict damage directly on the U.S. homeland. 

Because the PRC has a robust nuclear arsenal, the United States must actively work to keep a war limited in a way that it has not had to consider since the Cold War. Any limited war between nuclear-armed great powers requires "deliberate restraint" because, unlike regional adversaries, both actors have the capacity to engage in catastrophic escalation (Brodie, 1959, p. 309). The vital ingredient for limited wars in the nuclear era is establishing limited political objectives. For example, the United States could narrowly define its political objective as preventing the PRC from physically controlling Taiwan, which falls short of such maximal war aims as forcing the PRC 
to accept political terms that recognize Taiwan's independence or overthrowing the Chinese Communist Party (CCP) regime. Because the United States would be fighting in a coalition alongside at least Taiwan and potentially others such as Japan and Australia, the United States will also face a coalition management challenge in persuading its allies to also pursue limited ends. Limiting the means employed can also play a stabilizing role, such as placing limits on the geography, weapons, or targets involved in a conflict (Halperin, 1963). Attacking the PRC's leaders or nuclear forces might convince it that the war was not, in fact, a limited one. Given the U.S. track record of pursuing maximal war aims and the inherent difficulty of knowing another country's intentions with certainty, the CCP could plausibly worry that the United States might feel tempted to pursue ambitious objectives that threaten Beijing's vital interests or even its survival. Beijing exhibits profound and chronic mistrust of the United States during peacetime, of the Falklands but the Argentinian regime survived the conflict and did not renounce its political claim to the islands. *Limited defeat* is the inverse of limited victory. 

Argentina's failure to hold the Falklands and Iraq's failure to keep Kuwait in the 1991 Gulf War are cases of limited defeat. *Broader defeat* is the inverse of total victory. Germany and Japan suffered this outcome in World War II. 

Finally, *destruction* goes beyond broader defeat to encompass the complete destruction of a society's fundamental underpinnings. Although this level of defeat is rare, nations have suffered it in the past, as Carthage did following the Third Punic War. Nuclear weapons make destruction a plausible outcome of any great-power war in the 21st century, even if one side's conventional forces were on the cusp of victory.

Figure 2 illustrates how the space of potential war results has skewed in favor of the United States in the post–Cold War era. The range of possible war results for the United States is on the y-axis, and those possible for a regional adversary such as Iraq are on the x-axis. Some outcomes are infeasible, such as both sides achieving total victory, and others are implausible, such as much weaker regional actors imposing broader defeat on the United States. Because none of the potential outcomes of U.S. wars over the past three decades have involved the serious and the fog of war and the further suspicions that active hostilities engender would only compound these concerns (Posen, 1991; Talmadge, 2017; Lieberthal and Jisi, 2012). Taken together, exercising restraint in the desired ends and the means employed to pursue them offers the best chance of succeeding while managing escalation.

To put this contrast between minimal and maximal war aims in a broader context, Figure 1 illustrates a spectrum of war results that ranges from total victory to destruction. *Victory* is the degree to which a state achieves its political objectives, which entails imposing its will to some extent on the defeated party (von Clausewitz, 1976; 
Bartholomees, 2008). *Total victory* enables the victor to dictate peace terms unilaterally, even if those terms infringe on the defeated party's vital interests. An example is the Allied total victory over Germany and Japan in World War II. Achieving total victory against a nuclear-armed great power is implausible because a defining feature of the nuclear age is that the "loser," even if its conventional forces have been defeated, can still annihilate the "winner" 
(Jervis, 1989). *Limited victory* entails accomplishing limited political objectives that fall short of what the victor would prefer if it had complete control over peace terms. The United Kingdom achieved a limited victory over Argentina in the Falklands War in 1982 because it regained control 

Broader Defeat
Limited to no ability 
to negotiate terms.
Limited Defeat
Did not achieve
limited objectives.
Limited Victory
Able to achieve
limited objectives.
Destruction
Fundamental 
underpinnings of 
industry and society 
destroyed.
Total Victory
Able to dictate terms. 
Could include regime 
change or an effort to 
reshape society.

In the 1990–2020 Era, the United States Could Pursue Maximal Objectives Against Regional 

## War Results For The United States War Results For Regional Adversary

potential for widespread conventional or nuclear attacks on the United States (illustrated with the green shading in Figure 2), U.S. leaders have become accustomed to pursuing maximal objectives and fighting in ways that would be extremely dangerous in a great-power conflict. 

Turning to the prospects of a U.S.-PRC war in 2030, the picture looks entirely different. Figure 3 replicates the map of war results and escalation risks from Figure 2, except the x-axis reflects war results against the PRC rather than a regional adversary. More potential outcomes become feasible because of the PRC's stronger conventional capabilities and survivable nuclear deterrent. The boundaries of the "limited war" region are fuzzy to reflect the inherent uncertainties involved in pursuing even limited objectives while keeping the war from spiraling out of control. In practice, the boundaries of this region in Figure 3 could be asymmetric if the PRC were more willing than the United States to resort to dramatic escalation in a bid to avoid limited defeat. The narrow range of acceptable war outcomes in Figure 3 differs not only from recent regional wars but also from past great-power wars in the pre-nuclear era. Escalation spirals resulting in nuclear use would likely occur long before either the United States or the PRC came close to suffering the kind of broader defeat or conventional destruction that Germany and Japan experienced in World War II. Because the PRC ultimately controls its own ability to escalate, measures aimed at creating a "lower risk" of escalation still can never guarantee "no risk." 

## War Results For The United States War Results For The Prc

NOTE: The limited war region does necessarily have to be symmetric; one side might be more willing than the other to escalate to avoid a limited defeat.

Given the narrow course that the United States and the PRC must navigate to avoid catastrophic escalation, the United States needs a theory of victory that can thread the needle between the operational decisiveness required to end a conflict and the escalation management necessary to avoid a pyrrhic victory. The next section turns to candidate theories of victory that the United States has available.

## Identifying Potential Theories Of Victory

Drawing from academic research on military strategy and coercion, we developed a typology of five potential theories of victory that are universal to all decisionmakers and to any conflict: (1) dominance, (2) denial, (3) devaluing, (4) brinkmanship, and (5) military cost-imposition. These are summarized in Table 1. Whereas dominance relies on brute force to eliminate the enemy's physical ability to continue fighting, the other pathways are coercive approaches that persuade the enemy to stop fighting even while it retains the capacity to resist (Schelling, 1966). Unlike brute force, coercion requires understanding the enemy's prefer-

## Typology Of Theories Of Victory

| Mechanism                                    | Variant                                 | Logic                                     |
|----------------------------------------------|-----------------------------------------|-------------------------------------------|
| Limitations                                  |                                         |                                           |
| Implausible when fighting a nuclear-armed    |                                         |                                           |
| great power                                  |                                         |                                           |
| Brute force                                  | Dominance                               | Comprehensively defeat the PLA and        |
| leave it physically unable to defend against |                                         |                                           |
| further U.S. attacks                         |                                         |                                           |
| Requires sufficient power to blunt an        |                                         |                                           |
| invasion and still creates escalation risks  |                                         |                                           |
| Decrease benefits                            | Denial                                  | Convince the PRC that it cannot           |
| successfully take Taiwan by destroying       |                                         |                                           |
| the capabilities directly supporting the     |                                         |                                           |
| invasion, such as sea- and airlift assets    |                                         |                                           |
| Impractical given the PRC's political        |                                         |                                           |
| interests in Taiwan and U.S. political       |                                         |                                           |
| obstacles to harming a partner               |                                         |                                           |
| Devaluing                                    | Convince the PRC that the benefits of   |                                           |
| taking Taiwan are too small, such as         |                                         |                                           |
| by destroying Taiwan's semiconductor         |                                         |                                           |
| industry                                     |                                         |                                           |
| Undesirable given the PRC's ability to       |                                         |                                           |
| retaliate in kind and the limited stakes at  |                                         |                                           |
| hand for the United States                   |                                         |                                           |
| Increase costs                               | Brinkmanship                            | Convince the PRC that the future costs of |
| fighting for Taiwan may become intolerably   |                                         |                                           |
| high by threatening escalation, such as      |                                         |                                           |
| nuclear use                                  |                                         |                                           |
| Relies on targeting sensitive pressure       |                                         |                                           |
| points without prompting unacceptable        |                                         |                                           |
| retaliation; unlikely to generate pressure   |                                         |                                           |
| quickly enough to stop a                     |                                         |                                           |
| fait accompli                                |                                         |                                           |
| Cost-Imposition                              | Convince the PRC that the costs are too |                                           |
| high to justify continuing a war over Taiwan |                                         |                                           |
| by using cost-imposing military measures,    |                                         |                                           |
| such as a blockade or strategic air attacks  |                                         |                                           |

Dominance uses brute force to leave the enemy physically incapable of continued resistance. Examples include the Allied victory in World War II and the U.S. invasion of Iraq in 2003. This mechanism has the appeal of simplicity, but it requires significant asymmetries in power that will not exist in the U.S.-PRC context. It is also infeasible against a nuclear-armed adversary with survivable retaliatory forces because grinding down the PRC's military forces and industrial capacity to the point that it could no longer defend itself or protract the conflict would threaten the CCP's vital interests and potentially its survival (Rovner, 2017). Removing the PRC's ability to resist entirely would require destroying a significant proportion of the ences and perceptions to manipulate costs and benefits such that it prefers a settlement over further fighting. The coercive mechanisms focus on either decreasing the benefits the enemy believes it may eventually gain if it keeps fighting or otherwise increasing the costs the enemy will incur if it protracts the conflict. Within these two mechanisms that prioritize either costs or benefits, there are two sub-variants that focus on either manipulating the *size* of the costs or the benefits themselves or else manipulating the *probability* that an enemy realizes those costs or benefits (Pape, 1996). These theories of victory are not mutually exclusive, so combining different approaches is possible, although doing so may entail trade-offs.

In the context of a U.S.-PRC war over Taiwan, denial would involve attriting the PRC's ability to seize and hold Taiwan, such as by interdicting the air- and sealift assets directly involved in the campaign.

imposition of more sweeping surrender terms. In the context of a U.S.-PRC war over Taiwan, this would involve attriting the PRC's ability to seize and hold Taiwan to present Beijing with military failure, such as by interdicting the air- and sealift assets directly involved in the campaign 
(Heginbotham and Heim, 2015; Colby, 2021). The United States would not allocate scarce resources toward striking industrial targets or PLA forces that are unrelated to this objective. The PRC could threaten to protract the war or escalate if the initial invasion attempt failed, which we address in the following sections, but denial aims to convince Beijing that these options will still fail to accomplish its goals and that ending the fighting is the least bad option. Even if the CCP never explicitly acknowledged defeat or made political concessions, it might still end the war once it realized it lacked the means to take Taiwan. For example, the Falklands War ended when Argentina lacked the military ability to retake the islands, even if it still refused to recognize British claims. Denial does not eliminate escalation risks, which are inherent to any great-power war, and it can be a demanding standard when facing a wealthy and sophisticated adversary in a distant theater. 

industrial capacity that also contributes to the country's peacetime prosperity and targeting the military and security forces that also backstop the CCP's domestic control. Pursuing such a large target set may require dismantling the PLA air defense networks protecting the mainland, further raising CCP anxieties about the U.S. ability to remove its nuclear deterrent or to attack its leaders directly. Dominance often leads to total victory, but that is not necessarily required—a country could restrain its political objectives to only limited victory, leaving the adversary regime intact. From the PRC's perspective on the receiving end of such a brute force approach, though, it may not be clear whether U.S. objectives are in fact limited. 

Denial persuades the enemy that it is unlikely to achieve its objectives and that further fighting will not reverse this failure (Pape, 1996, pp. 15–18, 29–32). Denial and the other theories of victory that follow are coercive strategies, in that they aim to persuade the adversary to stop fighting even when it could continue, whereas the brute force logic of dominance simply aims to leave the enemy no choice. Denial narrowly focuses on the expected benefits that motivate the enemy to keep fighting rather than on securing a broader victory that would enable the that brinkmanship deliberately ratchets up the risk of escalation with an adversary that can retaliate in kind. Brinkmanship is a "competition in risk-taking" and resolve, and the PRC rather than the United States may have more riskand cost-tolerance in a war for Taiwan (Schelling, 1966, p. 91). Because escalation would be costly to the United States as well, this creates another challenge of making credible threats to escalate in ways that would be so painful that the PRC would cave but not so painful that the PRC would doubt the sincerity of U.S. threats. If the PRC wrote off threats as bluffs, the United States might still feel pressure to follow through and find itself escalating a conflict beyond what it initially intended. There are no analysts who currently advocate relying on brinkmanship, but this theory of victory might gain traction if the United States found itself losing a war. The United States fears that its adversaries might use this kind of "escalate-to-deescalate" approach against it, and the PRC is reportedly concerned that the United States will actually be the one to embrace this logic during a conflict (Zhao, 2022). It is therefore still important to highlight the risks of brinkmanship for U.S. decisionmakers before the fighting starts, despite the theory's current lack of peacetime appeal. 

Military cost-imposition uses the military instrument to persuade the enemy that the costs of continuing the war outweigh the benefits (Pape, 1996, pp. 18 and 21–27). Analysts sometimes refer to this as "punishment," but "military cost-imposition" avoids connotations of retribution— imposing costs is important here because of its ability to shape future behavior rather than as a corrective for past actions. One common proposal in the U.S.-PRC context is a distant blockade of the PRC's maritime trade at chokepoints such as the Strait of Malacca (Hammes, Devaluing persuades the enemy that, even if it succeeded in securing its objectives, the benefits would be smaller than it initially hoped (Pape, 1996, p. 16). This generally involves scorched-earth tactics, such as those used in the Russian defense against Napoleon in 1812. There are also comparable cases in the business world in which companies engage in self-sabotage, such as taking on debt or selling valuable assets, to make themselves less appealing to a hostile takeover. Devaluing would offer little leverage to the United States in a conflict with the PRC over Taiwan because we have not found any military measures that could reduce the PRC's political attachment to Taiwan. Measures such as destroying Taiwan's semiconductor industry would not affect the central benefits that would likely animate the PRC war effort.3 Moreover, Taiwan would likely object strongly to such actions, and there would be domestic political obstacles in the United States to harming a partner without its consent. Scorchedearth strategies can produce pyrrhic victories if they ultimately destroy the interests that a state fought to defend in the first place.

Brinkmanship persuades the enemy to stop fighting by threatening escalation if it does not end the war. This theory of victory is essentially blackmail—the United States would threaten significant escalation, potentially up to nuclear use, if the PRC did not abandon the invasion. Brinkmanship is distinct from cost-imposition because it is about causing discomfort about the *potential* for future pain rather than the application of ongoing harm (Pape, 1996, pp. 18–19 and 28–29). If cost-imposition is repeatedly imposing costs on the enemy until it complies, brinkmanship seeks compliance by threatening to impose costs in new and more unpleasant ways. The central challenge is Brinkmanship faces the problem of deliberately ratcheting up the risk of escalation with an adversary that can retaliate in kind and that might have more risk tolerance. Finally, devaluing is not viable because military measures cannot reduce the PRC's political attachment to Taiwan, which might grow even stronger over the course of a war given the PRC's desire to justify its losses, and Taiwan would likely oppose such drastic measures.

## Identifying Potential Escalation Pathways

2012; Mirski, 2013; O'Hanlon, 2021). While there are no advocates of strategic bombing as an independent theory of victory, some argue that it could play a supporting role by "bringing the cost of war home to decision-makers" by striking at the "internal sources of an adversary regime's power base, political credibility, or logistical support and transportation systems in conjunction with other more battlefield-centric targets" (Venable and Lukasik, 2021). Wearing down the enemy's military forces in a war of attrition is also a form of military cost-imposition if the assumption behind this approach is that rising costs will bring the other side to the negotiating table (rather than lead to its collapse on the battlefield). Strategies relying on military cost-imposition must satisfy three conditions, and meeting all three would be difficult in a U.S.-PRC conflict. 

First, the United States must find a "sweet spot" of pressure points that are valuable enough to influence the PRC's decisionmaking but not so valuable that they trigger unacceptable escalation, which we call the "Goldilocks Challenge." Second, the United States must prosecute targets in the sweet spot at a sufficient speed and scale to generate the necessary coercive leverage within an operationally relevant time frame. Third, the United States must provide credible assurances to the PRC that the pain will stop if Beijing complies with the coercive demands. We elaborate on these challenges below.

In practice, debates about these five potential theories of victory generally collapse into considering denial, military cost-imposition, or a mix of the two.4 These two theories stand out largely because the alternative options have major shortcomings. Dominance is not a viable theory of victory against the PRC because of the country's considerable material power and survivable nuclear deterrent. 

Given that the three main options are denial, military costimposition, or a mix of the two, how should we evaluate the relative escalation risks associated with these approaches? 

Escalation is "an increase in the intensity or scope of conflict that crosses threshold(s) considered significant by one or more of the participants" (Morgan et al., 2008, p. 8). Because escalation is difficult to quantify, for simplicity, we focus on two key variables that would influence escalation risks across these theories of victory. First, do these theories of victory involve direct combat between U.S. and PLA forces? Second, do they rely on military cost-imposition? Table 2 summarizes how these variables map onto different theories of victory, with a combination of denial and military cost-imposition creating the highest relative risk and an indirect denial strategy that relies on proxy warfare 
(which we refer to as *denial-by-proxy*) providing the lowest relative risk.5 Because the focus here is on military theories of victory, we do not consider strategies that would exclusively use nonmilitary tools, such as sanctions and diplomatic pressure, even though this would very likely be a less escalatory option than proxy warfare. 

## Categories Of Escalation Risk For Theories Of Victory

Relative Severity of Escalation Risks
Direct U.S.-PLA 
Combat?
U.S. Reliance on 
Military Cost-
Imposition?
Example Theories  
of Victory
Example Escalation Pathways
Higher
Yes
Yes
Combinations of 
denial and military 
cost-imposition
- The United States deliberately escalates to impose costs 
on the PRC, and the PRC escalates in retaliation, potentially leading to mutual strikes on military and industrial targets in U.S. and PRC homelands. 
Intermediate
Yes
No
Pure denial
- The United States inadvertently erodes the PRC's nuclear 
deterrent through conventional military operations targeting PLA dual-use capabilities.
- The PRC deliberately escalates to impose costs and compel an 
end to U.S. intervention. 
Lower
No
No
Denial-by-proxy
- The PRC accidentally shoots down a U.S. airlift asset, and the 
United States retaliates.
- The PRC deliberately escalates to impose costs and compel an 
end to U.S. intervention. 

## Lower Escalation Risk: Denial-By-Proxy

Because it avoids direct U.S.-PLA combat and does not rely on military cost-imposition, a denial-by-proxy theory of victory that supports Taiwanese efforts to deny the PRC the ability to seize the island would generate the lowest relative risk of escalation (across courses of action that involve military measures). A somewhat comparable historical model is U.S. and international support for Ukrainian efforts to defeat Russia's invasion in 2022, which involved armament, logistical, and intelligence backing without direct involvement of U.S. forces in the fighting.6 Nevertheless, the lower escalation risk comes with the correspondingly lower odds of operational success, given the PRC's bilateral military advantages over Taiwan. This reflects the recurring dilemma of balancing operational effectiveness and escalation management. 

A denial-by-proxy theory of victory would still entail escalation risks and could plausibly lead to direct U.S.- PLA combat even if the United States hoped to avoid that outcome. There are three canonical types of escalation— deliberate, inadvertent, and accidental—that might occur 
(Morgan et al., 2008, pp. 20–28). *Deliberate escalation* 
involves intentionally crossing a known threshold, such as moving from nonkinetic to kinetic attacks. Deliberate escalation can be good or bad depending on the context, but it does involve crossing a significant threshold and carries the risk of retaliation. One challenge in predicting (and therefore managing) escalation is that different motives might drive the PRC's decision to escalate (Table 3), and these motives are not mutually exclusive (Talmadge, 2017). 

| Mechanism                                          | Logic                                   |
|----------------------------------------------------|-----------------------------------------|
| Suggests that the PRC could attempt to             |                                         |
| identify and target U.S. pressure points, similar  |                                         |
| to a U.S. cost-imposition campaign                 |                                         |
| Coercive                                           | Pressure the United States to change    |
| its behavior, potentially even ending its          |                                         |
| support for Taiwan entirely                        |                                         |
| Suggests that the PRC could attempt to focus       |                                         |
| on U.S. military capabilities relevant to the U.S. |                                         |
| theory of victory, such as U.S. and coalition      |                                         |
| logistics nodes                                    |                                         |
| Operational                                        | Reduce U.S. capacity to implement its   |
| theory of victory, such as by reducing             |                                         |
| its ability to blunt an invasion or to strike      |                                         |
| the PRC's pressure points                          |                                         |
| Symbolic                                           | Send signals to domestic and            |
| international audiences                            |                                         |
| Suggests that the PRC could attempt to             |                                         |
| focus on visible targets that resonate with the    |                                         |
| intended audiences                                 |                                         |
| Emotive                                            | Appease emotional sense of injustice or |
| outrage                                            |                                         |
| Offers less guidance on potential PRC focus,       |                                         |
| other than maybe a desire for reciprocity in the   |                                         |
| targets or retaliatory methods                     |                                         |

this is far less likely with proxy warfare than in scenarios such as U.S. strikes on targets that the PRC values highly). 

Misperceptions could further complicate efforts to prevent escalation. *Inadvertent escalation* occurs when one side takes actions that unwittingly cross a threshold that the other side considers important, with the first side not realizing that the other side would view those actions as particularly escalatory (Morgan et al., 2008, pp. 20–28). For instance, the PRC might believe that cyber attacks rather than kinetic attacks against U.S. satellites that were supporting Taiwanese forces would avoid escalation, but the United States might still consider the cyber attacks to cross an important threshold. Similar to the war in Ukraine, the United States would also face a dilemma between limiting the scope of U.S. aid to avoid inadvertently crossing potential PRC red lines and applying too many limits on U.S. 

For example, the PLA might engage in deliberate escalation by interdicting U.S. supply lines to Taiwan, such as ports of embarkation or U.S. ships and aircraft as they approached the island. It might do so to gain an operational advantage by ensuring that fewer weapons and supplies make it to Taiwanese forces, to coerce the United States to end its support for Taiwan by raising the costs and risks of bringing aid to Taiwan, or to send signals to domestic or international audiences that it will not allow U.S. actions to go unanswered. The costs and risks of having to fight the United States directly might deter the PRC from escalating, as they have with Russia in Ukraine, but U.S. leaders cannot guarantee that Beijing will make the same calculations as Moscow. It is also possible that PRC leaders might retaliate not because of careful and cool-headed calculations about the costs and benefits but because emotions such as outrage make inaction seem intolerable (though support that would dramatically reduce the odds of successfully thwarting the invasion (Cohen, 2022b).

Finally, *accidental escalation* is generally the product of chance, such as mishaps or mechanical failures that cross escalatory thresholds, though some theories of victory and courses of action may systematically increase the risk of accidents relative to others (Morgan et al., 2008, pp. 20–28). Unlike in Ukraine, U.S. airlifters or vessels would need to bring supplies directly to Taiwanese territory because there are no neighboring borders through which to funnel materiel, which raises the possibility that PRC air defenses might accidentally shoot down or sink a U.S. asset. The United States might then misinterpret Beijing's intentions or feel pressure to respond assertively regardless. One factor that amplifies these risks is *intentionality bias*, which is the human tendency to interpret actions as the product of another's deliberate and unified decisionmaking rather than chance (Jervis, 1976; Kertzer et al., 2022). The PRC's steadfast refusal to believe that the 1999 U.S. bombing of its embassy in Belgrade during the U.S. intervention in Kosovo was an accident rather than an intentional signal of hostility demonstrates this tendency. As the PRC's former leader Jiang Zemin told *60 Minutes*, "the United States has state of the art technology, so all the explanations they have given us for what they called a mistaken bombing are absolutely unconvincing" (Jiang, 2000). The PRC would likely be even more suspicious of U.S. intentions if the United States were supporting Taiwan during an on going war. 

## Intermediate Escalation Risk: Pure Denial

An intermediate category of risk between denial-byproxy and cost-imposition is a "pure" denial strategy that involves direct U.S.-PLA combat but forgoes additional cost-imposition. 

PRC escalation aimed at compelling the United States to back down and to degrade its ability to blunt an invasion is particularly likely because pure denial would have already breached the threshold of direct U.S.-PLA combat. 

The PLA has strong operational incentives to attack U.S. military forces even on American soil, especially in Guam and Hawaii, to make it harder for the United States to attack PLA invasion forces near Taiwan. Additionally, the PRC might resort to a military cost-imposition campaign targeting U.S. pressure points. Even if the PRC calculated that the United States had the military ability to blunt an invasion if it fully committed U.S. forces to that effort, the PLA might still believe that it could forestall defeat by making the war as costly as possible to undermine U.S. resolve to see it through. This would resemble Japan's theory of victory in World War II (Toll, 2012, pp. 486–487). Beijing might focus on maximizing U.S. military casualties, but a PRC coercion campaign could also extend to attacks on the U.S. homeland or nuclear brinkmanship. As we discuss below, the PRC's willingness to accept large escalatory risks may grow precisely when denial seems to be succeeding from the U.S. perspective: When losses start to appear certain, the human tendency of *loss aversion* 
nudges people to take greater risks and to look for lowprobability, high-impact courses of action to escape from a losing position (Kahneman, 2011; Levy, 1996). 

A key question when implementing pure denial is determining what escalatory thresholds the United States should deliberately risk crossing to gain an operational advantage. There are many courses of action that could contribute to denial, and each requires evaluating its poten-

There is a risk that Beijing 
may not perceive the 
United States to be as 
restrained as Washington 
views itself.

against Taiwan? Do large fuel and ammunition depots far 
from the conflict but necessary to sustain operations count 
as supporting the campaign? What distance defines the 
boundary of "near" Taiwan? How should the United States, 
Taiwan, and coalition partners categorize dual-use systems, 
such as the DF-26 intermediate-range ballistic missile, that 
contribute to both the PRC's conventional capabilities and 
strategic deterrent? 
    Because any theory of victory, including denial, is in 
the eye of the beholder, the United States and its coalition 
partners must consider how Beijing might interpret their 
wartime actions. There is a risk that Beijing may not per-
ceive the United States to be as restrained as Washington 
views itself. If the United States and its coalition partners 
adopt a denial theory of victory in part to help manage 
escalation risks, they need to think about how to ensure 
that pure denial does not end up still looking like domi-
nance or cost-imposition to Beijing, since the PRC military 
and civilian leadership is unlikely to directly know the U.S. 
theory of victory and might draw different conclusions 
based on the information available to them (e.g., focusing 
on which PRC targets are struck versus appreciating all the 
PRC targets the U.S. military does not attack).
    The potential for inadvertent escalation depends partly 
on both sides' baseline expectations about the war's likely 
contours. The PRC might view retaliatory strikes on forces 
in the continental United States as a proportional response 
to U.S. attacks against military targets on the PRC main-
land. If the United States has priced in this retaliation as 
part of the price for gaining the operational benefits of 
mainland strikes, then escalation might stop there. But if 
U.S. leaders instead viewed strikes on the United States as 
different from targeting the PRC mainland because the 

tial to add operational value with its potential to contribute to escalation. For example, air operations to interdict amphibious transports versus to paralyze the PLA's decisionmaking capacity are both measures that could reduce the PRC's odds of seizing Taiwan, but they differ significantly in their likely effectiveness and escalation risks (Venable, 2020; Pietrucha, 2015). Analysts have proposed various restrictions to limit the scope of U.S. deliberate escalation, such as on the types of targets it hits (e.g., only strike conventional forces directly supporting the invasion, avoid striking strategic forces or leadership targets), the geographic scope (e.g., do not strike the PRC mainland or only strike forces on the mainland near Taiwan), the operational tempo (e.g., limit or avoid strikes on the mainland in the initial stages of the war), or the means employed (e.g., use kinetic attacks only against some targets and rely on nonkinetic means for others) (Odell et al., 2022, pp. 233–235; Colby, 2021). But many of these proposals still raise unresolved implementation questions. How should the United States, Taiwan, and coalition partners define what counts as directly supporting the PLA campaign U.S. homeland is thousands of kilometers away from the conflict and therefore a dramatic expansion of the war's scope, the PRC might inadvertently cross a U.S. threshold and fuel escalation. 

Finally, there is room for accidental escalation. Examples include strikes on PLA bomber bases intended to target conventional H-6K bomber forces that accidentally destroyed dual-capable H-6N bombers that contribute to the PRC's nuclear deterrent. Because of the fog and friction inherent to war, careful planning can reduce the odds of these accidents occurring but not eliminate them. 

## Higher Escalation Risk: Military Cost-Imposition

[the adversary's] mental state," creating general panic and increasing public opposition to the war. The United States will likely retain an advantage in 2030 in terms of the scale at which it can attack the PRC mainland relative to the PLA's ability to strike U.S. territory, but the growth of the PRC's military capabilities will enable it to make the war increasingly painful for the U.S. population over time. 

Cost-imposition could also activate operational, symbolic, and emotive escalation pathways for deliberate escalation. The PLA might hit such targets as bombers based in the continental United States because it wants to attrit the long-range conventional strike capabilities the United States is using to impose costs on the PRC. The war in Ukraine provides a recent example of this: Ukraine launched air attacks on bomber bases inside Russian territory that were hosting the strike capabilities Russia was using to target Ukraine's energy infrastructure (Ilyushina, Stein, and Stern, 2022). Additionally, the PRC might escalate in dramatic ways to send signals to domestic and international audiences that it was standing firm and had the ability to punch back. The United States launched the Doolittle Raid against Tokyo in 1942, for example, because the symbolism of striking the Japanese home islands would provide a domestic political victory (Toll, 2012, p. 283). Moreover, emotional outrage at U.S. attacks against domestic targets in the PRC might reinforce the PRC leadership's urge to retaliate. The tendency for "retaliatory aggression" has such strong evolutionary roots that it "is one of the most zoologically common, well-recognized, and well-studied behavioral responses for dealing with threats and challenges" (McDermott, Lopez, and Hatemi, 2017, p. 69). The more highly the CCP values a given target set, and therefore the more coercive leverage that attack-
If the United States deliberately escalates a conflict by striking PRC pressure points that are not directly related to the invasion, the PRC will likely engage in its own countercoercion campaign against U.S. pressure points. Reciprocal cost-imposition campaigns could quickly devolve into a competition in pain tolerance and expand the war's scope far beyond Taiwan to include widespread conventional attacks on both the U.S. and PRC homelands. There is some evidence that the PLA is planning for this possibility. For example, a 2004 classified PRC military textbook for its missile forces states that if an enemy (clearly the United States) conducts a "high strength air raid" against the PRC, the PLA should consider launching an intercontinental ballistic missile with a conventional warhead against the adversary's territory as a "warning strike" (Yu and Li, 2004, p. 41). The purpose would be to "make the enemy unwilling to see its country suffer unbearable attacks and 
[thus] restrain its intervention operations" by "shaking 

## What Are The Prospects Of Success For Military Cost-Imposition?

Military cost-imposition is a viable theory of victory under three conditions, but satisfying all three in a war against the PRC would be difficult: 

1. finding a "sweet spot" of pressure points that are 
valuable enough to influence Beijing's decisionmaking but not so valuable that they trigger unacceptable escalation, which we call "the Goldilocks Challenge"
2. prosecuting targets in the sweet spot at a sufficient 
speed and scale to generate the necessary coercive leverage within an operationally relevant time frame
3. providing credible assurances that the pain will stop 
if Beijing complies with the coercive demands. 

## Can The United States Confidently Identify A Coercive Sweet Spot?

ing it provides the United States, the greater the risk that the PLA will resort to dramatic retaliation in response. Concerningly, these pathways can reinforce each other and interact in unpredictable ways, further compounding the difficulty of predicting whether certain targets of a U.S. cost-imposition campaign would provoke unacceptable escalation. 

If the United States strikes dual-use targets that impose pain on the PRC's general population, such as its energy infrastructure, this might cause the PRC to misperceive the scope of U.S. war aims and trigger inadvertent escalation. Beijing might wrongly view widespread U.S. attacks on PRC domestic targets as evidence that the United States was pursuing regime change by destabilizing PRC society and stoking unrest, which could convince the CCP that it was fighting a total war for its existence (which appears to be a real fear for Beijing) (Wuthnow, 2020). Because the PRC would be comparing the costs of accepting defeat with the costs of continued fighting, this misperception of U.S. war aims could encourage Beijing to protract or escalate the war, given that the alternative might appear even more threatening to the CCP's interests in comparison. Accidents, such as civilian casualties from U.S. strikes on dualuse targets, could further reinforce the PRC's suspicions about U.S. intentions. 

Combining denial and military cost-imposition has higher escalation potential because it would involve all the costs and risks of a denial strategy and layer on the additional challenges of cost-imposition. 

The United States would likely struggle to navigate the Goldilocks Challenge. On one side of the dilemma, if the United States does not attack targets that are sufficiently valuable, it will not persuade Beijing that the war is too costly and that it should accept defeat. In Figure 4, these targets fall short of Threshold 1. But on the other side of the dilemma, striking targets that are exceedingly valuable to the PRC could instead result in escalation (surpassing Threshold 2). The sweet spot between these thresholds could change during the war, possibly by becoming so narrow that it effectively no longer exists, and striking any potential pressure point will either fall below Threshold 1 
or cross Threshold 2. The goal is to maximize U.S. coercive leverage while minimizing the types of retaliation that are most threatening to U.S. interests, but the Goldilocks Challenge highlights why this optimization problem is so difficult to solve: The most influential coercive levers to pull are also the ones that generate the highest escalation risks. The United States would likely accept some escalation risks and some PRC retaliation as an acceptable price for gaining coercive leverage over Beijing, so U.S. defense planners need to assess whether breaching Threshold 2 would cause specific kinds of escalation that were unacceptable to U.S. political leaders (or were disproportionally costly relative to the value of the coercive leverage gained). Different U.S. administrations will likely have a variety of views about what constitutes "unacceptable escalation," but examples include widespread conventional attacks on the U.S. homeland, large-scale cyber attacks on U.S. critical infrastructure, and nuclear use. 

Finding targets that reach Threshold 1 is challenging because the CCP values Taiwan so highly. Because the CCP views Taiwan as one of its most important national interests, it would likely accept the loss of many other targets as an acceptable price to pay for controlling Taiwan. The Defense Intelligence Agency states that the CCP's strategic priorities are to "perpetuate CCP rule, maintain domestic stability, sustain economic growth and development, defend national sovereignty and territorial integrity, and secure the PRC's status as a great power" (Defense Intelligence Agency, 2019, p. 12). This likely means that Taiwan, as part of national sovereignty and territorial integrity, would be prioritized above the CCP's desire for greatpower status and its global interests. Regarding threats to the PRC's economic development, if PRC leaders had decided that controlling Taiwan is worth the risk of a direct military conflict with the United States, they would likely expect and accept a tremendous amount of economic pain before changing their decisionmaking. PRC leaders might The most influential coercive levers to pull are also the ones that generate the highest escalation risks.

also see the stakes of a conflict as extending far beyond just Taiwan to also include the PRC's status as a great power and its reputation for standing firm against external aggression (McDermott, 2017, p. 93; Powers and Altman, 2023). Given these stakes, the CCP may even start the war already assuming that it would lose such targets as its overseas military facilities in Africa. 

To be clear, this is different from the counterargument that military cost-imposition can never work because there is nothing Beijing values more highly than Taiwan. The decision to stop fighting would depend on the PRC's calculation about the potential benefits relative to the costs (e.g., taking Taiwan versus the economic costs of a blockade) weighted by the probability of securing those benefits, which is never certain in war (Pape, 1996, p. 16). To continue the example from above, PRC leadership might care more about Taiwan than the PRC's economic growth, but large costs from a blockade and uncertain prospects of success from continued efforts to take Taiwan might still compel it to stop fighting.7 Absent a robust Taiwanese denial effort, though, a blockade might be insufficient on its own to reach Threshold 1. Proponents might counter that a blockade could, in fact, surpass Threshold 1 because it targets "the Chinese economy and hence the legitimacy of the" CCP, and the CCP cares more about its domestic political control than Taiwan (Hammes, 2013). But if targeting the PRC economy threatens the regime's survival, how could U.S. leaders be confident that the PRC would not engage in significant escalation in retaliation? The more vital one believes that the PRC economy—or any other target—is to the CCP's survival, the more incredible it seems that the United States could attack it without running large risks of breaching Threshold 2. Of note, it is possible that this "sweet spot" of targets is bigger and thus easier to identify outside of a Taiwan scenario, where the CCP leadership feels it has less at stake—such as, perhaps, a South China Sea dispute.

Identifying targets that fall short of Threshold 2 poses its own challenges because Beijing has good reasons to exaggerate its red lines and because escalation is unpredictable. As Russia has illustrated in its war against Ukraine, there are incentives to bluff about what will lead to escalation because making an adversary act more cautiously can create operational advantages. The PRC could follow Russia's model of bluffing to make Threshold 2 seem lower than it really is. The problem from the U.S. perspective is that knowing that the PRC is exaggerating still leaves significant ambiguity about where exactly Beijing's red lines are. Compounding this challenge is that these thresholds would likely shift over time, and the PRC's leaders themselves might not necessarily know in advance how they will act given the emotions, imperfect information, and unpredictability of war. Moreover, the line between tolerable and intolerable forms of escalation is blurry because escalation spirals can make it difficult to anticipate the natural endpoint of a given retaliatory measure. Escalation depends in part on the interaction of (at least) two sides, and lower-level actions can spiral into a tit-for-tat cycle that pushes both over thresholds they did not originally plan to cross. RAND research has found that U.S. and PRC leaders repeatedly struggled to overcome these obstacles to identifying a coercive sweet spot during historical crises and conflicts.8
Given these constraints, what kinds of targets are candidates for the PRC's coercive sweet spot? Three rough but useful categories based on past cost-imposition campaigns are military, economic, and leadership targets.9 There are no advocates of targeting the PRC's leadership because this would almost certainly provoke unacceptable escalation by breaching Threshold 2 (and face serious practical difficulties that make it unlikely to succeed in any case) (Hosmer, 2001b). Most analysts focus on targeting the PRC's economy via a distant blockade, some envisioning it as a substitute for direct intervention in defense of Taiwan and others viewing it as a complement.10 Again, on its own, a blockade is unlikely to reach Threshold 1. The military requirements of a successful blockade are not trivial (Cunningham 2020; Priebe et al., 2022). Like targets of past blockades, the PRC could use "recycling, stockpiling, and substitution" to cushion the economic impact, meaning the blockade could take months to years to generate large costs (Mearsheimer, 2014, pp. 94–96). The PRC has long land borders that it can use for smuggling, and its tremendous market share of global trade will tempt many states and commercial actors to continue trading with it (Grinberg, 2021). If the blockade succeeded in generating large amounts of pain quickly, it is also possible that desperation to escape the economic pain would cause PRC leaders to escalate in unexpected ways rather than admit defeat, as was the case with Germany's response to blockades in World Wars I and II (Sand, 2020). Moreover, this would likely not come as a surprise to Beijing: Then–General Secretary Hu Jintao discussed this vulnerability as early as 2004, when he talked about the "Malacca Dilemma" and then spent billions attempting to mitigate this risk via overland pipelines (Storey, 
2006). The 2013 *Science of Military Strategy* states that "the United States and other major powers control the world's primary strategic lines of communication [SLOCs]" and warns that these SLOCs are "not owned by us, nor are they controlled by us. Once a crisis or war at sea occurs, our sea transport has the possibility to be cut off" (Academy of Military Science Military Strategy Department, 2013, pp. 81, 210). Directly attacking critical infrastructure on the PRC mainland through air and missile attacks is another potential option, but this might breach Threshold 2. The 
2004 *Science of Second Artillery Campaigns* identifies some adversary (U.S.) actions that may trigger "lowering the nuclear threshold," namely PRC nuclear signaling (Yu and Li, 2004, p. 294). These include even just threatening "medium or high strength" conventional strikes against Beijing, other major cities, or other major political and economic hubs and threatening nuclear strikes against the PRC's critical infrastructure, such as water and electricity facilities. Finally, military targets of cost-imposition might include PLA forces not directly involved in the invasion of Taiwan, such as those protecting the PRC's western borders with India or the PRC's overseas military bases and facilities.11 If the PRC started a war for Taiwan, its leaders would likely expect and accept heavy military losses as a necessary cost for victory. Targeting PLA forces is less likely to carry heavy risks of rapidly expanding the conflict to a worldwide NATO-Warsaw Pact war with the attendant risks of nuclear escalation. Hence, they do not offer an attractive alternative to concentrating on the conventional defense of Iran. (DoD, 1980, p. 3)
For example, whereas attacks on "Soviet regional facilities are unlikely to compel the Soviets to abandon their intervention in Iran since the economic and strategic value of Iran far exceeds the value of regional facilities" (falls short of Threshold 1), attacking "military or industrial targets in the Soviet Union not directly related to the Soviet invasion . . . would almost certainly evoke Soviet reprisals, possibly including attacks on U.S. territory" (breaches Threshold 2) (DoD, 1980, Annex A, pp. 1 and 5). While the Joint Staff initially assumed that cost-imposition was an easy alternative to the hard task of denial, U.S. analysts ultimately failed to identify a plausible coercive sweet spot that was necessary for this theory of victory to be viable.

## Can The United States Generate The Necessary Coercive Leverage Quickly Enough?

breach Threshold 2 than the alternatives, but it is unlikely to reach Threshold 1 unless the United States and Taiwan also succeed in directly defeating the forces involved in the invasion. 

U.S. defense planning to counter a Soviet invasion of Iran during the Cold War illustrates the Goldilocks Challenge's enduring dynamics. In 1980, U.S. defense officials debated how best to prevent the Soviet Union from seizing Iran and its significant oil reserves, a concern even after the 1979 Islamic Revolution overthrew the U.S.-backed shah. Because of concerns about the operational feasibility of denial, the Joint Staff proposed an alternative strategy that would strike Soviet targets unrelated to the invasion in order to impose coercive pain (cost-imposition) and to raise the prospect that further fighting would lead to escalation (brinkmanship) (Slocomb, 1980, pp. 1–2). After receiving pushback to specify exactly "what escalation and where" would accomplish U.S. objectives, the Joint Staff failed to identify any targets in the sweet spot that could change Soviet calculations but avoid escalation.12 The analysts acknowledged the difficulty of reaching Threshold 1: "There is no U.S. and allied riposte against Soviet interests . . . that would clearly equal or exceed in value the political, military, and economic gains the Soviet Union could be expected to achieve from control of Iran" (DoD, 1980, p. 2). Moreover, the measures that might reach Threshold 1 risked causing unacceptable escalation by breaching Threshold 2: 
The only category of ripostes which has the possibility of raising Soviet costs to a level commensurate with the gains of occupying Iran involves major escalation of the conflict. . . . Such actions, however, If such a sweet spot exists, the United States must be able to prosecute these pressure points at sufficient speed and scale to have a strategic effect within a relevant time frame. Coercive effects take time to build, especially through slow-moving measures, such as blockades, that need months or even years to reach their greatest impact, and this lag period matters for responding to rapidly unfolding scenarios, such as a *fait accompli*. If the PRC seizes Taiwan before the blockade can produce enough coercive pressure to change Beijing's decisionmaking, the United States will 

The PRC is highly 
suspicious of U.S. 
motives, and a painful 
cost-imposition campaign 
would not improve its 
image of U.S. intentions.

face potentially insurmountable challenges in reversing PRC control of Taiwan (Montgomery, 2020). Endowment effects—the psychological tendency for people to value objects more once they take possession of them—would make the bar for Threshold 1 even higher after a PRC invasion succeeded (Kahneman, 2011). Alternative forms of military cost-imposition that could generate significant amounts of pain quickly, such as widespread strategic air attacks on the PRC's energy infrastructure, risk instead going too far and prompting unacceptable retaliation, as the *Science of Second Artillery Campaigns* makes clear above. The measures that best satisfy the second condition therefore conspire to make it harder to achieve the first. 

## Will The Prc Trust U.S. Coercive Assurances?

implemented all-around containment, encirclement and suppression of the PRC, which has brought unprecedented severe challenges to our country's development" (Bradsher, 2023). If the United States attacks PRC domestic targets, such as energy, communications, or transportation infrastructure, this could reinforce these views and lead PRC leaders to wrongly conclude that military cost-imposition is really aimed at stoking domestic discontent and destabilizing the regime. This conclusion might be especially likely if the United States attacked the CCP's internal security forces.13 Given the fog, friction, and fear involved in wars, states often judge their adversary's intentions through a worst-case lens that tends to lead to these kinds of judgments (Posen, 1991). 

The more negatively Beijing views the United States and its intentions, the harder it will be to find a way to end the conflict because the stakes and risks of accepting limited defeat will appear higher. That U.S. leaders knew their own objectives were limited would not necessarily reassure Finally, there is a credible assurance challenge because the PRC must trust that the United States will stop imposing coercive costs once it accedes to U.S. demands. Coercive assurances that compliance will bring relief are critical. Otherwise, the target has no incentive to stop fighting, and it may even fear that concessions will embolden further coercion and harsher demands (Schelling, 1966, p. 75; Pauly, 2019). This requirement is concerning in the U.S.- PRC context because the PRC is highly suspicious of U.S. motives, and a painful cost-imposition campaign would not improve its image of U.S. intentions. For example, Xi has reportedly stated that the "United States is the biggest threat to our country's development and security" and also the "biggest source of chaos in the present-day world" (Buckley, 2021). More recently, in March 2023, Xi similarly said, "Western countries led by the United States have Beijing if PRC leaders mistakenly believed they were fighting a total war for the survival of the regime. This need for self-reflection extends beyond just DoD actions to the broader U.S. government, as Beijing may draw connections between uncoordinated U.S. diplomatic, economic, or other actions, as it reportedly did in the fall of 2020 
(Woodward and Costa, 2021). Practicing strategic empathy and providing reassurances are not acts of charity—they are calculating and pragmatic measures to reduce the risk of uncontrolled escalation. 

## What Are The Prospects Of Success For Denial?

A denial theory of victory offers the best chances of achieving U.S. objectives while avoiding catastrophic escalation. This section addresses several counterarguments to denial, including whether it is operationally feasible from a military standpoint to defeat a PRC invasion of Taiwan, how denial might deal with the possibility of Beijing shifting to a blockade or strategic air attacks against Taiwan after the initial invasion fails, and whether the PRC could ever accept even a limited defeat given Taiwan's importance to the CCP. 

## Is Denial Operationally Feasible?

detailed analysis than is possible here, but we highlight several reasons to believe that denial will remain an operationally feasible option for defending Taiwan even if U.S. advantages are not as overwhelming as they once were. 

The PRC's military modernization and expansion have created growing concerns about the U.S. military's ability to defeat an invasion of Taiwan.14 Two decades ago, around 2000, Taiwan could plausibly have defended itself from a PRC invasion with only limited U.S. support (Shlapak, Orletsky, and Wilson, 2000; O'Hanlon, 2000). One decade ago, around 2010, sustained PLA modernization had shifted the balance of power to the point that a large-scale amphibious assault was no longer unthinkable but remained "a bold and possibly foolish gamble on Beijing's part" (Shlapak et al., 2009, p. 118). A denial-by-proxy theory of victory therefore remained plausible, even if harder, despite PLA progress and unfavorable trendlines (Shlapak et al., 2009, pp. 139–140). By the mid-2010s, however, assessments began to note growing deficiencies in the U.S. military's ability to blunt an invasion (Heginbotham et al., 2015). Public accounts of U.S. Air Force wargames in 2018 and 2019 concluded that the "Air Force failed disastrously" (Insinna, 2021). Taiwan's failure to adjust its defense strategy in light of these changes further aggravated these fears.15 The 2010–2020 decade was when widespread concerns began to spread about the feasibility of denial. These concerns create the temptation to reach for military cost-imposition as a riskier but perhaps at least operationally feasible alternative. 

There are nevertheless compelling reasons to think that denial can remain feasible even if it has become more difficult (Ochmanek, 2022). First, the United States has an enduring advantage in that amphibious assaults are Unfavorable trends in the three-way military balance between the PRC, Taiwan, and the United States have made it harder to assure the success of a denial strategy over time. If denial is not operationally feasible, then it is not a viable theory of victory to achieve U.S. objectives. 

Fully assessing the feasibility of denial requires much more There are compelling reasons to think that denial can remain feasible even if it has become more difficult.

immensely complex and challenging operations. History suggests that the PRC will need local air and sea control for the invasion to succeed, whereas the United States only needs to deny the PRC that control, which is a far less demanding standard to meet (O'Hanlon, 2000). The PRC's reliance on scarce air- and sealift platforms to transport troops and supplies to Taiwan creates a bottleneck in PLA operations that the United States can exploit, and there is not a comparable opportunity for the PRC to focus on a single U.S. operational vulnerability to this extent. 

Second, the U.S. military still retains a qualitative and, in some areas, quantitative edge over the PLA. The United States has a significant advantage in undersea warfare because of a decades-long lead in technological investments and because geography makes it difficult for the PLA to find U.S. attack submarines in the Taiwan Strait's shallow waters and the wider Western Pacific (Cote, 2011). RAND research has shown that U.S. attack submarines are therefore particularly well positioned to sink large numbers of the PLA's amphibious transports (Heginbotham et al., 2015, p. 213). The U.S. military also has significant capabilities and experience with long-range precision strike, which provide another strong option for targeting the PRC's amphibious transports (Heginbotham et al., 2015, pp. 111–116). While the PLA Navy now has a larger number of surface combatants than the U.S. Navy, the United States still retains an advantage in larger and more capable classes of ship, such as carriers, cruisers, and destroyers— an advantage that the Office of Naval Intelligence (2020) projects will continue through 2030 despite the PRC's impressive investments in shipbuilding.16 Although it captures only a snapshot of the current balance of forces using 2022 data, Table 4 provides a rough-but-useful breakdown of the major naval and air platforms of the PRC, Taiwan, the United States, and likely U.S. coalition partners. In the air-to-air balance, the United States still has greater numbers of advanced fourth- and fifth-generation fighters, which are likely qualitatively more capable than their PRC counterparts (Heginbotham et al., 2015, pp. 80–82). This suggests that the U.S. military might still retain some advantages over the PLA in air-to-air combat that RAND research has identified (Heginbotham et al., 2015. p. 92). Although the PRC's ability to attack U.S. air bases has grown, which threatens to offset the United States' advantages by reducing the number of sorties it can generate, the United States is now prioritizing the ability to conduct distributed and resilient air operations to preserve its ability to generate land-based airpower in the face of PRC air and missile attacks (Lynch et al., 2023). 

Third, U.S. allies and partners have increased their investments and preparations for a conflict. Taiwan is increasing its defense spending and extending its conscription obligations from four months to one year, Japan is planning to double its defense spending over five years 

| Ships or Aircraft                | PRC    | U.S.   | Taiwan   | Japan   | Australia   |
|----------------------------------|--------|--------|----------|---------|-------------|
| Aircraft carriers                | 2      | 11     | -        | -       | -           |
| Light carriers                   |        |        |          |         |             |
| a                                |        |        |          |         |             |
| -                                | 9      | -      | 1        | -       |             |
| Cruisers                         | 6      | 24     | -        | 4       | -           |
| Destroyers                       | 36     | 68     | 4        | 35      | 3           |
| Frigates                         | 45     | 21     | 22       | 6       | 8           |
| Corvettes                        | 50     | -      | 1        | -       | -           |
| Attack submarines (nuclear)      | 9      | 53     | -        | -       | -           |
| Attack submarines (conventional) | 56     | -      | 4        | 22      | 6           |
| Bombers                          | 221    | 123    | -        | -       | -           |
| Fighters (5th generation)        | ~50    | 612    | -        | 52      | 24          |
| Fighters (4th generation)        | ~1,268 | 1,742  | 323      | 265     | 44          |

SOURCES: Features information from International Institute for Strategic Studies (IISS), 2022, pp. 51–52 (United States), 248–249 (Australia) 257–261 (PRC), 277–279 (Japan), 309–310 (Taiwan); DoD, 2022, pp. 53 and 166. NOTES: These estimates are for active-duty forces. Different sources may produce slightly different estimates. The estimates here for the PLA Navy use the numbers from DoD (2022). The biggest difference between these estimates and those in IISS (2022) is that the latter lists 72 corvettes, which is likely an accounting decision based on the PLA transferring 22 older corvettes to the PRC Coast Guard in 2021 because they lacked towed-array sonars for antisubmarine warfare. DoD did not include these ships in its estimates presumably because of their organizational designation as Coast Guard ships and their lesser capability.

a This refers to amphibious assault ships that can deploy such tactical aircraft as the F-35. Japan has converted one helicopter carrier to deploy F-35Bs and is currently converting a second. 

strain" the PLA and remains "a significant political and military risk" (DoD, 2022, p. 127). Recent open-source wargames suggest that the United States can continue to make an invasion quite difficult for the PRC through 2026, even under relatively pessimistic assumptions (but at the price of considerable losses to U.S. forces) (Cancian, Cancian, and Heginbotham, 2023). Furthermore, the United and is procuring new long-range strike capabilities, and Australia is significantly deepening its defense cooperation with the United States and Japan (Dooley and Ueno, 2022; Wang, 2022; Brands, 2022). 

Collectively, U.S. and allied adaptations can help offset the unfavorable trend lines of the past decade. DoD still assesses that an "attempt to invade Taiwan would likely States has several modernization programs that will begin to bear fruit in the late 2020s and early 2030s.17
Looking to the future, the U.S. ability to make denial sustainable over the long term depends in part on trends in the relative balance of power. In a pessimistic scenario, the PRC will benefit from robust economic growth and domestic stability, the PLA will enjoy sustained growth for defense spending as a result, and the PRC will continue gaining ground in the military competition. All else equal, this would make it more difficult for the United States and Taiwan to prevail with a pure denial theory of victory, although the full implications depend on other factors, such as whether U.S., Taiwanese, and coalition investments offset increased PRC defense spending and how emerging technologies will shape the ease or difficulty of amphibious assaults. Even in this pessimistic scenario for the United States, the PRC still has a considerable way to go before it surpasses the size of U.S. and allied economies and defense budgets. Figures 5, 6, and 7 visualize the current gaps between the United States, the PRC, and likely U.S. allies in military spending, the size of their economies as measured by gross domestic product (GDP), and the development of their economies as measured by GDP per capita.18
SOURCE: Features data from the SIPRI Military Expenditure Database (Stockholm International Peace Research Institute [SIPRI], undated), expressed in 2021 U.S. dollars. NOTE: The error bar represents uncertainty about how much China actually spends on defense. The lower end of the interval represents China's offcial defense budget, which was $207 billion in 2021 (IISS, 2022, p. 243). After accounting for military-related spending that the offcial budget does not include, IISS estimates that China's defense spending in 2021 was around $270 billion, which is the same as SIPRI's estimate and is the upper bound of the interval in Figure 5. SIPRI's estimates are generally larger than other sources, so it is less likely than alternatives to understate the extent of China's military spending. See Center for Strategic and International Studies, 2022. DoD (2022, p. 45) lists China's 2021 defense budget at $261 billion, which is just below the interval's upper bound shown here. 

that notional crossing point further into the future (Xie, 2022; Sharma, 2022; Ip, 2023). The PLA could fall prey to a military version of the middle-income trap, facing high personnel costs before it has fully caught up with the skills of the U.S. military. The PRC's recent economic setbacks and the updated projections of its future growth suggest that the optimistic scenario is at least plausible. 

In an optimistic scenario for the United States, the PRC's growth over the next two decades might be substantially lower than what it experienced for the past three decades. The PRC's power may have recently passed an inflection point, moving into an era of lower relative growth than other major states (Beckley and Brands, 2022, pp. 42–45; Pettis, 2013; Magnus, 2018). After predictions of ever-nearing dates when the PRC's economy would eclipse that of the United States during the late 2000s and early 
2010s, slowing PRC economic growth has begun to push 

## Market Exchange Rates Purchasing Power Parity

blockade is more straightforward than a denial campaign, this does not mean it is strategically feasible in the sense that it has better prospects for successful war termination. Attempting a more difficult denial campaign might still yield better odds and introduce fewer risks than resorting to military cost-imposition. It may ultimately be in the U.S. interest to accept a greater risk of a limited defeat over Taiwan than to cultivate a much greater risk of nuclear escalation. These are decisions that U.S. political leadership, not DoD, would make. 

## What If Beijing Keeps Fighting?

Another concern is that defeating an invasion would not guarantee that the PRC ends the war. Great powers have The United States has several options for how to respond to these different scenarios. In the pessimistic case of PRC military dominance, U.S. political leadership might feel compelled either to abandon Taiwan or to rely on brinkmanship and military cost-imposition as cheaper alternatives to the likely expensive task of keeping denial a viable theory of victory. The latter would essentially be a 21st century version of Eisenhower's New Look doctrine, with all the attendant escalation risks and credibility questions that such approaches entail (Gaddis, 2005). Another potential response by U.S. political leaders is to continue relying on pure denial and simply accept that there is a greater chance of operational failure. The limitations of military cost-imposition identified above suggest that even if the operational feasibility of strategic air attacks or a States to remove the PRC's ability to do things such as fire missiles at Taiwan or approaching ships, which would be extremely difficult. Washington only needs to persuade Beijing that these desperate measures will not change the outcome and enable it to achieve its goal of controlling Taiwan. 

Some analysts may accept that denial is a feasible and even desirable theory of victory but reject the argument that the United States should not mix denial with military cost-imposition because doing so could create extra leverage to compel Beijing to stop fighting. If the United States does not force the PRC to pay a high price to continue the enough resources to reconstitute and persist even after a major battlefield reversal, so Beijing could decide to keep fighting even if its invasion failed. This could involve the PRC shifting its own theory of victory from relying on brute force to military cost-imposition, such as a blockade or strategic air attacks—or potentially even nuclear brinkmanship (Henley, 2021; Henley, 2023). Given the poor historical record of coercive attacks in compelling targets to surrender their sovereignty, forcing the PRC to adopt improbable theories of victory would still be an important achievement for the United States and Taiwan (Pape, 1996). A denial theory of victory would not require the United attempt fails (Colby, 2021). This would likewise still confront the Goldilocks Challenge. The risk of breaching the threshold for intolerable escalation may be especially concerning in this scenario if U.S. leaders, buoyed by a potentially dramatic success in defeating the PLA's invasion fleet, succumb to "victory disease" and become overconfident about their ability to manage escalation against an adversary backed into a corner.

Third, if the United States already occupied a strong military position after blunting a PRC invasion, there are political reasons that a U.S. president might be hesitant to use force in ways that deliberately create significant hardships for the PRC civilian population. After Russia's brutal cost-imposition campaign against Ukrainian civilians in 2022–2023, including large-scale air attacks against Ukraine's energy and transportation infrastructure, U.S. political leaders may want to avoid operations that conjure associations with Russian military strategy among U.S. domestic or international audiences. In addition to potential normative concerns, there are pragmatic risks associated with losing international support for U.S. efforts to use diplomatic and economic means to pressure the PRC to end the war. 

If the United States does not get the leverage to end the war from military cost-imposition, where might the necessary pressure come from? Mounting PLA casualties and the loss of military capabilities it took decades to accumulate might serve as one source of pressure, especially if a sustained and robust U.S. denial effort demonstrated to Beijing that these costs served little practical purpose. Skeptics may counter that the PRC has invested heavily in the PLA for just such a war over Taiwan, so it would be willing to lose as many soldiers and ships as necessary and would war, why would Beijing not protract the conflict in a bid to reverse its initial military failures? Even confronted with the possibility of a protracted war, there are three potential reasons U.S. leaders might prefer pure denial. 

First, layering military cost-imposition on top of denial would increase the risk of escalation. Skeptics may argue that moderation is folly in war and that pure denial puts the United States at a disadvantage by leaving opportunities unexplored, but this logic makes sense only when the costs and risks of additional action are low.19 Especially if military cost-imposition involved expanding the war's scope to include domestic targets in the PRC, the risks of unacceptable escalation might outweigh the potential coercive benefits. Put differently, U.S. leaders might calculate that they would rather accept some greater risk of protraction at a lower level of intensity than accept a greater risk of unacceptable escalation in a gamble to end the war sooner.

Second, there might be operational trade-offs to combining military cost-imposition and denial during the initial stages of a conflict. Splitting the difference between multiple theories of victory risks watering down the effectiveness of each. Using heavy bombers for military costimposition, for example, might divert these scarce strike resources from a denial campaign.20 The least acute tradeoff might involve a distant blockade with surface ships that are too vulnerable to contribute to denial operations near Taiwan. This would not necessarily resolve the escalation risks or other practical difficulties associated with a blockade, but it would help address the operational trade-offs. Alternatively, the United States could address this concern by sequencing these theories of victory. It could first focus on pure denial and then turn to military cost-imposition if the PRC refuses to end the war after the initial invasion The point of denial is to provide new information to Beijing that throws cold water on prewar enthusiasm about its ability to seize Taiwan.

tively modest organic sealift capability, which it will likely supplement with dual-use civilian shipping, but once it exhausts these assets it will need to rebuild its amphibious transport capability (DoD, 2023). The PRC has significant shipbuilding capabilities that it could use to build new transports, but if it refuses to end the war, then it will have to engage in a massive shipbuilding effort in docks that are vulnerable to U.S. attacks (as opposed to rebuilding these capabilities during peacetime). The PRC could try to speed up the process by using vast numbers of small ships in a "reverse Dunkirk," but these could only transport small numbers of light infantry at a time (without artillery or armored vehicles to support them), and the PLA would struggle to sustain them with enough supplies once they reached Taiwan.21 The outcome would more likely resemble Japan's experience on Guadalcanal than a reverse Dunkirk, with the PLA landing too few troops relative to the size of the defending forces, facing a dramatic inability to deliver enough food and ammunition to keep them combat-effective, and gradually losing more and more of the already-limited lift capacity needed to hold onto the lodgment it initially established (Toll, 2012, pp. 174–176). In sum, protraction would not clearly solve the problems that caused the initial invasion attempt to fail in the first place, have priced those losses into the decision to start the conflict. But the point of denial is to provide new information to Beijing that throws cold water on prewar enthusiasm about its ability to seize Taiwan, even if it was initially willing to pay a high price for the privilege of doing so when it seemed feasible. If further losses would take even more time to reconstitute without a realistic chance of success, Beijing might decide that it is better off ending the fighting so that it is easier to rebuild its forces in peacetime and try again in the future. The international diplomatic pressure and economic sanctions referenced above provide another source of leverage. Despite their limitations, sanctions could build pressure on top of a successful denial defense of Taiwan while creating lower escalation risks than physically destroying the PRC's economic infrastructure. 

But could protracting the war actually benefit Beijing militarily and eventually enable it to seize Taiwan in the face of a sustained U.S. denial effort? According to this line of thinking, the PRC's larger population and industrial base, coupled with its greater stakes in the conflict, give it stronger capabilities and motivation than the United States to prevail in a protracted war. But protraction would not necessarily solve the central operational problem the PLA faces, which is transporting and then sustaining large numbers of forces to Taiwan. The PLA has a rela-
There is a narrow range of scenarios that can enable great powers to wage a protracted war without it becoming a nuclear war.

which suggests that mutual vulnerability at the strategic level should enable limited aggression with conventional weapons, given the belief that the other side will ultimately not risk destruction by escalating to nuclear use (Jervis, 1989, pp. 19–22). It is theoretically possible that these fears could also prevent nuclear escalation even after extensive conventional attacks during a protracted war, but it is unlikely in practice. It is doubtful that the PRC would be risk-acceptant enough to start a war with the United States but so risk-averse that it would unequivocally rule out nuclear use even if its vital interests were at stake. Conversely, if the PRC were to ravage the U.S. industrial base with conventional strikes and cripple such critical civilian infrastructure as transportation networks, then it is plausible that the United States would at least consider threatening nuclear retaliation to try to get Beijing to stop the attacks. Given that the exhaustion from protracted wars often poses an existential threat to the loser's regime, the side that feels victory slipping away has rational incentives to gamble with limited nuclear use despite (and partly because of) its potential to spiral out of control. 

Second, both sides could engage in deliberate, mutual restraint to limit the intensity of a protracted war. This pathway is more plausible but creates its own dilemma of how to persuade the enemy that the costs of continuso protraction should not be assumed to automatically favor Beijing. 

Independent of which side has better operational odds, can nuclear-armed great powers even realistically fight a long, grinding, and painful war without escalating to nuclear use?22 If the United States could defeat the PRC in a protracted war, but only at the price of running tremendous risks of nuclear use, then the PRC might gain an advantage if it were more resolved to accept those risks. There is tension between, on the one hand, the historical tendency for great-power wars to become protracted conflicts and, on the other, the constraints that the contemporary nuclear era imposes on how severe a conflict can become before resulting in catastrophic escalation (Nolan, 2017). Assessing the "solutions" to what we call the "Nuclear-Armed Protracted War Paradox" shows that there is a narrow range of scenarios that can enable great powers to wage a protracted war without it becoming a nuclear war.

First, the threat of mutual destruction could deter nuclear escalation, even if both sides engaged in the kinds of provocative conventional operations that have often characterized protracted wars, such as striking the enemy's industrial base and population centers. This is effectively a wartime cousin of the "Stability-Instability Paradox," 
great power, and other theories of victory may face the same challenge. The more leverage one assumes is necessary to coerce the PRC, given its resolve to incur large costs even in the face of long odds, the greater the possibility that military cost-imposition would also fail and that Beijing would rather risk nuclear escalation than accept defeat in a protracted conflict. 

## Can Beijing Accept A Limited Defeat?

A third concern is that, once they start the war, the PRC's leaders may then view success as critical to the regime's survival. If so, a successful denial campaign could force the PRC into a desperate position in which it would take tremendous risks, potentially including nuclear coercion, to gamble for resurrection. This is a legitimate concern but not one that is unique to denial: Any U.S. theory of victory that successfully prevents the PRC from achieving its political objective of unification would generate these risks. Moreover, it is at least plausible that the PRC will not view the outcome of the war as existential and instead look for exit ramps, such as claiming it successfully taught Taiwan a lesson, as it did with Vietnam in 1979.23 Given the higher stakes the PRC has in Taiwan compared with Vietnam, it will likely accept much greater costs compared with 1979 and exhaust its options before looking for these exit ramps, but the point is that the PRC could again look for face-saving measures and refuse to explicitly acknowledge its defeat. Unlike other theories of victory, denial at least avoids automatically expanding the scope and stakes of the war early on, before the PRC has the chance to realize the invasion will fail and to look for exit ramps. 

ing the war outweigh the benefits while abstaining from military measures that could maximize the enemy's pain or hamstring its ability to sustain the war (Rovner, 2017). For example, destroying the PRC's industrial base would reduce its ability to replenish its forces, but hollowing out the PRC economy might also pose an existential threat to the CCP's survival. A mix of fear and temptation could make restraint difficult in practice. As the war drags on, both sides might become increasingly risk-acceptant as they look for ways to bring a costly conflict to an end. The emotive desire for revenge and retaliation as losses mount could also undercut calls for restraint. Alternatively, early expectations of a protracted war might create temptations for preemption: If either side believes the war will be a long, grinding one, it has an incentive to start attacks to cut the other's production capacity as soon as possible. In short, mutual restraint is necessary to avoid catastrophic escalation, especially in the case of a protracted war with a nuclear-armed adversary, but this restraint is not guaranteed and requires deliberate cultivation and management to achieve.

A pure denial theory of victory accepts the risk of delaying the start of a military cost-imposition campaign in order to leave space for Beijing to decide that now is not the time to resolve the Taiwan question. The United States could immediately begin to impose costs and then increase those costs to compel Beijing to capitulate after a failed invasion, but this risks overreaching and pushing Beijing from fearing it would face a limited defeat (failing to seize Taiwan) toward fearing it would face a broader defeat (potentially regime change). That denial cannot absolutely guarantee that the PRC will end the conflict is simply a reality of fighting a limited war against a nuclear-armed 

## Conclusion

Whether Beijing is willing to accept limited defeat depends in part on what exactly that entails. The more ambitiously the United States defines its war aims, the harder it will be to persuade the PRC to accept defeat. U.S., coalition, and Taiwanese leaders might feel tempted to resolve the Taiwan question once and for all rather than accepting a more limited victory that could leave Beijing still claiming Taiwan (and perhaps determined to try again in five or ten years). This argument has an emotional appeal. It would be challenging to keep war aims limited after Americans and coalition troops started dying, ships started sinking, and aircraft losses started accumulating. However, to keep the war limited, both sides must keep their war aims limited. Although it would be frustrating not to permanently resolve the Taiwan question, this is the price to reduce the risk of catastrophic escalation created when a limited war turns into a total war for survival. Just as deterring conflict by itself does not resolve the underlying competitions that can lead to wars, limited victories by themselves do not resolve all the underlying disagreements that caused the war. Argentina still claims the Falkland Islands, for example, but the United Kingdom decided that the additional cost of attempting to compel Argentina to renounce its claims was not worth it. The PRC has greater capacity and motivation than Argentina to protract the conflict, so war termination will not necessarily be as quick and straightforward as in the Falklands War, but this historical case does provide a useful example of two states that found a way to stop fighting without agreeing to a comprehensive resolution of the political issues motivating the war. 

Any conflict between the United States and the PRC has the potential for catastrophic escalation, and this requires the United States to think differently about how to fight and win limited wars than it has become accustomed to in the post–Cold War era. A denial theory of victory offers the best balance between the desire to maximize the chances of U.S. success and the imperative to manage escalation. Denial focuses on attriting the power-projection capabilities the PRC needs to seize Taiwan in order to persuade PRC decisionmakers that they are unlikely to accomplish their objectives and that further fighting will not change the eventual outcome. The causal storyline for military cost-imposition, by contrast, aims to convince PRC decisionmakers that the war is too costly to continue and that they should abandon Taiwan to protect their other interests. The central obstacle to military cost-imposition is navigating the Goldilocks Challenge by identifying a sweet spot of targets that are valuable enough to influence Beijing's decisionmaking but not so valuable that they prompt retaliation and unacceptable escalation. For either theory of victory, the United States would use limited means to pursue the limited ends of leaving Taiwan's independence intact. It would not define victory in terms of a formal settlement that resolves Taiwan's status once and for all or that forces the PRC to make other painful concessions. 

Clearly understanding the range of potential theories of victory for future conflicts is critical, because today's force structure decisions will shape the decision space for tomorrow's president. If DoD does not build a robust denial capability, then a future president might have to choose among higher escalation risk options, such as A denial theory of victory offers the best balance between the desire to maximize the chances of U.S. 

success and the imperative to manage escalation.

still need to think carefully about how U.S. operational plans to implement a denial strategy strike an appropriate balance between operational effectiveness and escalation risks. 

Once DoD is confident that it can implement a denial theory of victory effectively, it should then prepare for other theories of victory to provide a spectrum of options to future presidents. Future U.S. leaders might have a diverse range of preferences and face scenarios in which different theories of victory seem more or less appropriate. For example, if the United States intervenes to protect Taiwan from a coercive PRC air and missile campaign rather than an amphibious invasion, a distant blockade might be a symmetrical response that can help manage escalation. The priority, though, should remain on developing the forces required for denial, including denial-byproxy and pure denial variants. DoD can explore alternative courses of action to provide U.S. leaders with a flexible set of options, but these alternatives cannot slow or displace preparations for the denial theories of victory.

military cost-imposition and brinkmanship. The different theories of victory imply different requirements for force planning and investments. For example, if one believes that the United States could compel the PRC to end an invasion through cost-imposition measures, such as strategic air attacks against key economic targets, then that could create greater demand for heavy bombers and less demand for air base defense for fighters.

General officers, flag officers, and senior civilian officials should prepare to discuss the risks of different theories of victory and the trade-offs between managing escalation risks and prevailing in a conflict. The United States has fallen out of practice with this kind of thinking after three decades of smaller regional conflicts. Shifting mindsets and once again becoming conversant in escalation dynamics is essential to providing the best possible advice to the president. That a denial theory of victory has lower escalation risks relative to the alternatives does not mean that it has low risks in absolute terms, so defense planners 

## Notes

1  Deterrence can still play an important supporting role once a conflict starts, especially through deterring an adversary from escalating.

2  On the PRC's growing ability to challenge the U.S. military, see Heginbotham et al. (2015), Ochmanek (2022), and DoD (2023).

3  For a proposal to destroy Taiwan's semiconductor fabrication plants to "render Taiwan not just unattractive if ever seized by force, but positively costly to maintain," see McKinney and Harris (2021, p. 30).

4  See, for example, Sisson (2022) and Colby (2021). 5  In theory, a fourth combination implies the possibility of the United States using military cost-imposition while avoiding direct combat, but this is implausible in practice. Even if the United States imposed a distant blockade, for example, a blockade is an act of warfare that would almost certainly lead to direct fighting. 

6  On potential escalation pathways stemming from U.S. support for Ukraine, see Frederick et al. (2022).

7  Because a war over Taiwan would be at least a three-player game, a robust Taiwanese denial effort could still weigh down the PRC's odds of success even if the United States pursued a pure military cost-imposition strategy.

8  Forthcoming work led by Alexandra Evans and Emily Ellinger. See, also, Byman and Waxman (2002).

9  See, for example, the coercive U.S. air campaign against Serbia in 
1999 (Hosmer, 2001a). 

10  For an example of a proposal for a blockade without a direct defense, see O'Hanlon (2019). For a proposal to combine a blockade with direct defense but avoid strikes on the PRC mainland, see Hammes (2012). 

11  For a critical appraisal of these options, see Colby and Ochmanek 
(2019).

12  Komer, 1980, p. 1. Potential courses of action included supporting insurgencies in Soviet client states, striking Soviet military facilities in the Indian Ocean, invading Yemen, striking Cuba, NATO mobilization, a coordinated attack with the PRC, attacking Soviet ships in the Indian Ocean, striking military and industrial targets in the Soviet Union, and a partial or worldwide blockade (DoD, 1980, p. 1).

13  Some analysts have hinted at striking targets important to the CCP's power and legitimacy—"Strategic attack might focus on internal sources of an adversary regime's power base, political credibility, or . . ." (Venable and Lukasik, 2021)—but do not provide specific examples of those targets. Elbridge Colby rejects this idea in some publications but in others suggests it is a candidate for cost-imposition if the PRC refuses to end the war after the initial invasion fails. In the latter case, Colby proposes "employing cost imposition selectively, conditionally, and as a complement to denial operations . . . targets might include nonnuclear military forces beyond those relevant to the Taiwan battle, as well as internal security assets or critical war-supporting economic infrastructure" (Colby, 2021, pp. 183–184). 

14  For an overview, see Heginbotham et al. (2015). 15  For example, the failure of the Taiwanese defense establishment to implement the Overall Defense Concept. See Hunzeker (2021).

16  The Office of Naval Intelligence estimates that, by 2030, "the U.S. 

Navy's force of 'Large Surface Combatants' [that is, carriers, cruisers, and destroyers] will still be substantially more than that of China" (Office of Naval Intelligence, 2020). For a broader comparison of U.S.- PRC naval shipbuilding through 2040, see Congressional Research Service (2023, p. 11).

17  Among the key modernization efforts that will begin to bear fruit in the late 2020s are Next Generation Air Dominance (NGAD), the B-21 Raider, and the Hypersonic Attack Cruise Missile (HACM).

18  GDP is also an incomplete measure of a country's military potential, but for this concise discussion it suffices as a heuristic. There are also debates about whether measuring GDP in terms of market exchange rates or purchasing power parity is more appropriate for comparing material power, and we generally err toward using exchange rates but have included both for reference. See, for example, Heim and Miller (2020, pp. 3–5) and Beckley (2018).

19  Or, as Admiral John "Jackie" Fisher put it in 1899, "The essence of war is violence! Moderation in war is imbecility!" (quoted in Massie, 1991, p. 431). 

20  In some cases, such as attacking the PRC's overseas facilities in Djibouti, the United States could use short-range tactical aircraft, such as the F-35, to make the trade-off less acute, although this still encompasses the challenge of reaching Threshold 1 of the Goldilocks Challenge. 

21  On the concept of reverse Dunkirk, see Larson (2020). On the operational challenges of this approach, see O'Hanlon (2000).

22  On the possibility of a protracted U.S.-PRC war, see, for example, Krepinevich (2020) and Brands and Beckley (2021).

23  For more on the PRC's recalibration of its war against Vietnam, see Garver (2016) and Gompert, Binnendijk, and Lin (2014). 

## References

Colby, Elbridge, and David Ochmanek, "How the United States Could Lose a Great-Power War," *Foreign Policy*, October 29, 2019. 

Academy of Military Science Military Strategy Department [军事学学
院战略研究部], *Science of Military Strategy* [战略学], Beijing: Academy of Military Science Press [军事科学出版社], 2013.

Congressional Research Service, China Naval Modernization: Implications for U.S. Navy Capabilities—Background and Issues for Congress, May 15, 2023.

Bartholomees, J. Boone, "Theory of Victory," *Parameters*, Vol. 38, No. 2, Summer 2008. 

Cunningham, Fiona, "The Maritime Rung on the Escalation Ladder: 
Naval Blockades in a US-China Conflict," *Security Studies*, Vol. 29, No. 4, 2020.

Beckley, Michael, "The Power of Nations: Measuring What Matters," 
International Security, Vol. 3, No. 2, 2018.

Defense Intelligence Agency, China Military Power: Modernizing a Force to Fight and Win, January 2019.

Beckley, Michael, and Hal Brands, Danger Zone: The Coming Conflict with China, W.W. Norton, 2022.

## Dod—See U.S. Department Of Defense.

Bradsher, Keith, "China's Leader, with Rare Bluntness, Blames U.S. 

Containment for Troubles," *New York Times*, March 7, 2023.

Dooley, Ben, and Hisako Ueno, "Japan Moves to Double Military Spending, with a Wary Eye on China," *New York Times*, December 16, 
2022. 

Brands, Hal, "Why Australia Is Gearing Up for Possible War with China," Bloomberg, November 9, 2022. Brands, Hal, and Michael Beckley, "Washington Is Preparing for the Wrong War with China: A Conflict Would Be Long and Messy," Foreign Affairs, December 16, 2021. 

Frederick, Bryan, Samuel Charap, Scott Boston, Stephen J. Flanagan, Michael J. Mazarr, Jennifer D. P. Moroney, and Karl P. Mueller, Pathways to Russian Escalation Against NATO from the Ukraine War, RAND 
Corporation, PE-A1971-1, 2022. As of March 21, 2023: https://www.rand.org/pubs/perspectives/PEA1971-1.html Brodie, Bernard, *Strategy in the Missile Age*, RAND Corporation, CB-137-1, 1959. As of March 21, 2023: https://www.rand.org/pubs/commercial_books/CB137-1.html Gaddis, John Lewis, Strategies of Containment: A Critical Appraisal of American National Security Policy During the Cold War, 2nd ed., Oxford University Press, 2005. 

Buckley, Chris, "'The East Is Rising': Xi Maps Out China's Post-Covid Ascent," *New York Times*, March 3, 2021.

Garver, John W., China's Quest: The History of the Foreign Relations of the People's Republic of China, Oxford University Press, 2016.

Byman, Daniel, and Matthew Waxman, The Dynamics of Coercion: American Foreign Policy and the Limits of Military Might, Cambridge University Press, 2002. 

Gompert, David C., Hans Binnendijk, and Bonny Lin, Blinders, Blunders, and Wars: What America and China Can Learn, RAND 
Corporation, RR-768-RC, 2014. As of May 30, 2023: https://www.rand.org/pubs/research_reports/RR768.html Cancian, Mark, Matthew Cancian, and Eric Heginbotham, The First Battle of the Next War: Wargaming a Chinese Invasion of Ukraine, Center for Strategic and International Studies, 2023. 

Grinberg, Mariya, "Wartime Commercial Policy and Trade Between Enemies," *International Security*, Vol. 46, No. 1, 2021.

Center for Strategic and International Studies, "What Does China Really Spend on Its Military?" updated June 29, 2022.

Halperin, Morton, "The Limiting Process in the Korean War," Political Science Quarterly, Vol. 78, No. 1, 1963. 

von Clausewitz, Carl, *On War,* trans. Michael Howard and Peter Paret, Princeton University Press, 1976. 

Hammes, T. X., "Offshore Control: A Proposed Strategy for an Unlikely Conflict," *National Defense University Strategic Forum*, No. 278, 2012. 

Cohen, Eliot, "The Strategy That Can Defeat Putin," *The Atlantic*, March 7, 2022a. 

Hammes, T. X., "Strategy and AirSea Battle," *War on the Rocks*, July 23, 
2013. 

Cohen, Raphael, "The Escalation Fixation," *The Hill*, May 6, 2022b. Colby, Elbridge, The Strategy of Denial: American Defense in an Age of Great Power Conflict, Yale University Press, 2021. 

Heginbotham, Eric, and Jacob Heim, "Deterring Without Dominance: 
Discouraging Chinese Adventurism Under Austerity," Washington Quarterly, Vol. 38, No. 1, 2015. 

Heginbotham, Eric, Michael Nixon, Forrest E. Morgan, Jacob L. Heim, Jeff Hagen, Sheng Tao Li, Jeffrey Engstrom, Martin C. Libicki, Paul DeLuca, and David A. Shlapak, The U.S.-China Military Scorecard: Forces, Geography, and the Evolving Balance of Power, 1996–2017, RAND 
Corporation, RR-392-AF, 2015. As of March 21, 2023: https://www.rand.org/pubs/research_reports/RR392.html Heim, Jacob, and Ben Miller, Measuring Power, Power Cycles, and the Risk of Great-Power War in the 21st Century, RAND Corporation, RR-2989-RC, 2020. As of November 8, 2023: https://www.rand.org/pubs/research_reports/RR2989.html Henley, Lonnie, "PLA Operational Concepts and Centers of Gravity in a Taiwan Conflict," Testimony Before the U.S.-China Economic and Security Review Commission Hearing on Cross-Strait Deterrence, February 18, 2021. 

Henley, Lonnie, Beyond the First Battle: Overcoming a Protracted Blockade of Taiwan, China Maritime Studies Institute, 2023.

Hoffman, Frank, "The Missing Element in Crafting National Strategy: 
A Theory of Success," *Joint Force Quarterly*, Vol. 97, No. 2, 2020. Hosmer, Stephen, The Conflict over Kosovo: Why Milosevic Decided to Settle When He Did, RAND Corporation, MR-1351-AF, 2001a. As of March 21, 2023: https://www.rand.org/pubs/monograph_reports/MR1351.html Hosmer, Stephen, *Operations Against Enemy Leaders*, RAND 
Corporation, MR-1385-AF, 2001b. As of March 21, 2023: https://www.rand.org/pubs/monograph_reports/MR1385.html Hunzeker, Michael A., "Taiwan's Defense Plans Are Going off the Rails," 
War on the Rocks, November 18, 2021.

IISS—See International Institute for Strategic and International Studies.

Iklé, Fred Charles, *Every War Must End*, Columbia University Press, 
2005. Ilyushina, Mary, Jeff Stein, and David L. Stern, "Ukrainian Drones Hit Two Air Bases Deep Inside Russia in Brazen Attack," *Washington Post*, December 6, 2022. Insinna, Valerie, "A US Air Force War Game Shows What the Service Needs to Hold Off—or Win Against—China in 2030," *Defense News*, April 12, 2021. 

International Institute for Strategic Studies, *The Military Balance*, Vol. 122, No. 1, 2022.

International Monetary Fund, "GDP, Current Prices: Billions of U.S. Dollars," IMF Datamapper, 2022a.

International Monetary Fund, "GDP, Current Prices: Purchasing Power Parity; Billions of International Dollars," IMF Datamapper, 2022b. International Monetary Fund, "GDP per Capita, Current Prices: U.S. Dollars per Capita," IMF Datamapper, 2022c. International Monetary Fund, "GDP per Capita, Current Prices: Purchasing Power Parity; International Dollars per Capita," IMF Datamapper, 2022d.

Ip, Greg, "Why Xi Can No Longer Brag About the Chinese Economy," 
Wall Street Journal, November 14, 2023. Jervis, Robert, *Perception and Misperception in International Politics*, Princeton University Press, 1976. 

Jervis, Robert, *The Meaning of the Nuclear Revolution*, Cornell University Press, 1989. Jiang Zemin, interview with Mike Wallace, "60 Minutes," CBS, September 3, 2000. As of December 1, 2022: https://www.youtube.com/watch?v=1tNMH2M_jJ0
Kahneman, Daniel, *Thinking, Fast and Slow*, Farrar, Straus and Giroux, 
2011. Kertzer, Joshua, Marcus Holmes, Brad LeVeck, and Carly Wayne, 
"Hawkish Biases and Group Decision-Making," International Organization, Vol. 76, No. 3, Summer 2022. 

Komer, R. W., "Memorandum for SECDEF—Info," U.S. Department of Defense, October 10, 1980.

Krepinevich, Andrew, *Protracted Great-Power War*, Center for a New American Security, 2020. 

Larson, Erik, *The Splendid and the Vile*, Crown, 2020. 

Levy, Jack, "Loss Aversion, Framing, and Bargaining: The Implications of Prospect Theory for International Conflict," International Political Science Review, Vol. 17, No. 2, 1996. Lieberthal, Kenneth, and Wang Jisi, Addressing U.S.-China Strategic Distrust, Brookings, 2012. 

Lynch, Christopher, Rachel Costello, Jacob L. Heim, Andrew Karode, Patrick Mills, Robert S. Tripp, and Alan J. Vick, Operational Imperative: Investing Wisely to Bolster U.S. Air Bases Against Chinese and Russian Attacks, RAND Corporation, PE-A1996-1, 2023. As of March 21, 2023: 
https://www.rand.org/pubs/perspectives/PEA1996-1.html Magnus, George, *Red Flags: Why Xi's China Is in Jeopardy*, Yale University Press, 2018.

Massie, Robert K., *Dreadnought: Britain, Germany, and the Great War*, Ballentine Books, 1991.

McDermott, Rose, "The Evolved Psychology of Coercion," Comparative Strategy, Vol. 36, No. 2, 2017. 

McDermott, Rose, Anthony Lopez, and Peter Hatemi, "'Blunt Not the Heart, Enrage It': The Psychology of Revenge and Deterrence," Texas National Security Review, Vol. 1, No. 1, 2017. 

McKinney, Jared, and Peter Harris, "Broken Nest: Deterring China from Invading Taiwan," *Parameters*, Vol. 51, No. 4, 2021. Mearsheimer, John, *The Tragedy of Great Power Politics*, W.W. Norton, 
2014. Mirski, Sean, "Stranglehold: The Context, Conduct and Consequences of an American Naval Blockade of China," Journal of Strategic Studies, Vol. 36, No. 3, 2013. Montgomery, Evan, "Primacy and Punishment: US Grand Strategy, Maritime Power, and Military Operations to Manage Decline," Security Studies, Vol. 29, No. 4, 2020. 

Morgan, Forrest, Karl Mueller, Evan Medeiros, Kevin Pollpeter, and Roger Cliff, Dangerous Thresholds: Managing Escalation in the 21st Century, RAND Corporation, MG-614-AF, 2008. As of March 21, 2023: 
 https://www.rand.org/pubs/monographs/MG614.html Nolan, Cathal, The Allure of Battle: A History of How Wars Have Been Won and Lost, Oxford University Press, 2017. O'Brien, Phillips, The Second Most Powerful Man in the World: The Life of Admiral William D. Leahy, Dutton Caliber, 2019.

Ochmanek, David A., Determining the Military Capabilities Most Needed to Counter China and Russia: A Strategy-Driven Approach, RAND Corporation, PE-A1984-1, 2022. As of March 21, 2023: https://www.rand.org/pubs/perspectives/PEA1984-1.html Odell, Rachel Esplin, Eric Heginbotham, John Culver, Eric Gomez, Brian Killough, Steven Kosiak, Jessica J. Lee, Brad Martin, Mike Mochizuki, and Michael D. Swaine, Active Denial: A Roadmap to a More Effective, Stabilizing, and Sustainable U.S. Defense Strategy in Asia, Quincy Institute for Responsible Statecraft, 2022. Office of Naval Intelligence, "UPDATED China: Naval Construction Trends vis-à-vis U.S. Navy Shipbuilding Plans, 2020–2030," February 6, 2020. O'Hanlon, Michael, "Why China Cannot Conquer Taiwan," 
International Security, Vol. 25, No. 2, 2000. 

O'Hanlon, Michael, The Senkaku Paradox: Risking Great Power War over Small Stakes, Brookings Institution Press, 2019. O'Hanlon, Michael, The Art of War in an Age of Peace: U.S. Grand Strategy and Resolute Restraint, Yale University Press, 2021. O'Hanlon, Michael, "Tell Me How the US-China War Ends," *The Hill*, January 17, 2022. 

Pape, Robert, *Bombing to Win*, Cornell University Press, 1996. Pauly, Reid, "Stop or I'll Shoot, Comply and I Won't": Coercive Assurance in International Politics, Massachusetts Institute of Technology, 2019. 

Pettis, Michael, *Avoiding the Fall: China's Economic Restructuring*, Carnegie Endowment for International Peace, 2013. Pietrucha, Mike, "The Five-Ring Circus: How Airpower Enthusiasts Forgot About Interdiction," *War on the Rocks*, September 29, 2015. Posen, Barry, Inadvertent Escalation: Conventional War and Nuclear Risks, Cornell University Press, 1991. 

Powers, Kathleen, and Dan Altman, "The Psychology of Coercion Failure: How Reactance Explains Resistance to Threats," American Journal of Political Science, Vol. 67, No. 1, January 2023. 

Priebe, Miranda, Kristen Gunness, Karl P. Mueller, and Zachary Burdette, The Limits of Restraint: The Military Implications of a Restrained U.S. Grand Strategy in the Asia-Pacific, RAND Corporation, RR-A739-4, 2022. As of November 8, 2023: https://www.rand.org/pubs/research_reports/RRA739-4.html Roberts, Brad, *On Theories of Victory, Red and Blue*, Lawrence Livermore National Laboratory, 2020. Rovner, Joshua, "Two Kinds of Catastrophe: Nuclear Escalation and Protracted War in Asia," *Journal of Strategic Studies*, Vol. 40, No. 5, 2017. Rudd, Kevin, The Avoidable War: The Dangers of a Catastrophic Conflict Between the US and Xi Jinping's China, Public Affairs, 2022.

Sand, Erik, "Desperate Measures: The Effects of Economic Isolation on Warring Powers," *Texas National Security Review*, Vol. 3, No. 2, 2020. Schelling, Thomas, *Arms and Influence*, Yale University Press, 1966. 

Sevastopulo, Demetri, and Kathrin Hille, "China Tests New Space Capability with Hypersonic Missile," *Financial Times*, October 16, 2021.

Sharma, Ruchir, "China's Economy Will Not Overtake the US Until 
2060, If Ever," *Financial Times*, October 24, 2022.

Shlapak, David A., David T. Orletsky, Toy I. Reid, Murray Scot Tanner, and Barry Wilson, A Question of Balance: Political Context and Military Aspects of the China-Taiwan Dispute, RAND Corporation, MG-888-
SRF, 2009. As of March 21, 2023: https://www.rand.org/pubs/monographs/MG888.html Shlapak, David A., David T. Orletsky, and Barry Wilson, Dire Strait? Military Aspects of the China-Taiwan Confrontation and Options for U.S. Policy, RAND Corporation, MR-1217-SRF, 2000. As of March 21, 2023: 
https://www.rand.org/pubs/monograph_reports/MR1217.html Slocomb, Walter, "Memorandum for the Secretary of Defense: Horizontal Escalation Paper," U.S. Department of Defense, October 9, 1980. Stockholm International Peace Research Institute, SIPRI Military Expenditure Database, undated. As of November 7, 2023: https://www.sipri.org/databases/milex Storey, Ian, "China's 'Malacca Dilemma,'" *China Brief*, April 12, 2006.

Talmadge, Caitlin, "Would China Go Nuclear? Assessing the Risk of Chinese Nuclear Escalation in a Conventional War with the United States," *International Security*, Vol. 41, No. 4, Spring 2017.

Toll, Ian, *Pacific Crucible: War at Sea in the Pacific, 1941–1942*, W.W. 

Norton, 2012. U.S. Department of Defense, "Horizontal Escalation as a Response to Soviet Aggression in the Persian Gulf," October 3, 1980.

U.S. Department of Defense, Military and Security Developments Involving the People's Republic of China, 2022.

U.S. Department of Defense, Military and Security Developments Involving the People's Republic of China, 2023.

Venable, Heather, "Paralysis in Peer Conflict? The Material Versus the Mental in 100 Years of Military Thinking," *War on the Rocks*, December 1, 2020. 

Venable, Heather, and Sebastian Lukasik, "'Bombing to Win' at 25," War on the Rocks, June 15, 2021. Walt, Stephen, "Why Wars Are Easy to Start and Hard to End," Foreign Policy, August 29, 2022. 

Wang, Joyu, "Taiwan to Extend Mandatory Military Service in Face of Chinese Pressure," *Wall Street Journal*, December 27, 2022. Woodward, Bob, and Robert Costa, *Peril*, Simon & Schuster, 2021. Wuthnow, Joel, System Overload: Can China's Military Be Distracted in a War over Taiwan? National Defense University, China Strategic Perspectives 15, June 2020. Xie, Stella, "China's Economy Won't Overtake the U.S., Some Now Predict," *Wall Street Journal*, September 2, 2022. Yu Jixun [于际训] and Li Tilin [李体林], eds., Science of Second Artillery Campaigns [第二炮兵战役学], PLA Press, March 2004. 

Zhao, Tong, "Political Obstacles in the US-China Nuclear Relationship," video, Massachusetts Institute of Technology Security Studies Program, 2022. As of March 21, 2023: https://www.youtube.com/watch?v=6CW24WxR3eA

## About The Authors

Jacob L. Heim is a senior policy researcher at RAND. His research has focused on the military balance in the Western Pacific, U.S. Air Force overseas force posture, wargaming, and the challenge posed by anti-access, area-denial capabilities. Heim served as a strategic analyst in the office of the Deputy Assistant Secretary of Defense (DASD) for Strategy and Force Development, where he advised the DASD on U.S. defense strategy and the evolution of military capabilities. He holds an M.A. in international relations.

Zachary Burdette is an adjunct researcher at RAND. His research interests include the intersection of great-power politics with trade and technology, the military balance in the Western Pacific, and grand strategy. Burdette is currently a Ph.D. candidate in political science at the Massachusetts Institute of Technology and a research fellow at Harvard's Belfer Center for Science and International Affairs. He holds an M.A. in security studies. 

Nathan Beauchamp-Mustafaga is a policy researcher at RAND. His research interests include PRC foreign policy, PRC military strategy, PRC– North Korea relations, the PRC's Belt and Road Initiative, the Korean Peninsula, and U.S. Indo-Pacific Command posture. Prior to joining RAND, Beauchamp-Mustafaga was the editor of the Jamestown Foundation's China Brief, a biweekly publication focusing on strategic PRC-related issues utilizing indigenous language sources, and prior to that he lived in the PRC for over three years. He holds an M.Sc. and an M.A. in international affairs.

## About This Paper

forces. Research is conducted in four programs: Strategy and Doctrine; Force Modernization and Employment; Resource Management; and Workforce, Development, and Health. The research reported here was prepared under contract FA7014-22-D-0001. Additional information about PAF is available on our website: www.rand.org/paf/ This report documents work originally shared with the DAF in September 2022. The draft report, dated March 2023, was reviewed by formal peer reviewers and DAF subject-matter experts.

The return of strategic competition and the war in Ukraine have highlighted the stark risks of conflict with nucleararmed great powers: Russia and the People's Republic of China (PRC). Such a conflict would entail escalation risks that the United States has not seriously considered since the Cold War. A key question for the U.S. Department of Defense is how to prevail in such a war while limiting the scope of escalation. A better understanding of this challenge can help inform force development and prepare future leaders for high-stakes deliberations on the risks and trade-offs involved in fighting another great-power war, should this war come to pass.

## Acknowledgments

We thank Brig Gen Mark Pye, Brig Gen Ty Neuman, and Jessica Powers (Headquarters Air Force, Concepts and Strategy Directorate [HAF/A5S]) for sponsoring this project. We received valuable input from the AF/A5S staff and particularly thank Gerry Sohan, Maj Herbert Williams, Maj Jim Lodge, and Maj Kelvin Puk (Headquarters Air Force, Strategic Competition Division [HAF/A5SD]) for their support.

This paper outlines potential U.S. theories of victory for a war with the PRC and analyzes their associated escalation risks. The paper is intended for defense strategists and force planners interested in preparing for future conflict with a nuclear-armed adversary. The research was commissioned by the Department of the Air Force and conducted within the Strategy and Doctrine Program of RAND Project AIR FORCE as part of a fiscal year 2022 project, "Winning Wars While Managing the Risks of Escalation." Data collection and analysis began in October 2021 and continued through September 2022.

## Rand Project Air Force

At RAND, we extend special appreciation to David Shlapak and David Frelinger, who shared their expertise and experience on these topics and provided mentorship and feedback throughout the writing of this paper. Nina Miller also provided helpful feedback on a draft of the paper. Hal Brands and David Shlapak provided constructive reviews that strengthened this document. We are also grateful to RAND Project AIR FORCE Strategy and Doctrine Program Director Raphael Cohen for his suggestions and support throughout this study. 

RAND Project AIR FORCE (PAF), a division of the RAND Corporation, is the Department of the Air Force's (DAF's) federally funded research and development center for studies and analyses, supporting both the United States Air Force and the United States Space Force. PAF provides the DAF with independent analyses of policy alternatives affecting the development, employment, combat readiness, and support of current and future air, space, and cyber military conflict between the United States and the People's Republic of China (PRC) would entail escalation risks that the United States has not seriously considered since the Cold War. The authors of this paper consider how the United States can prevail in a limited war with the PRC while avoiding catastrophic A
escalation. The authors do so by considering *theories of victory* for the United States in a war with China. A theory of victory is a causal story about how to defeat an adversary: It identifies the conditions under which the enemy will admit defeat and outlines how to shape the conflict in a way that creates those conditions. The authors consider five theories of victory and identify two as most viable: denial (persuading the enemy that it is unlikely to achieve its objectives and that further fighting will not reverse this failure) and military cost-imposition (using military force to persuade the enemy that the costs of continuing the war outweigh the benefits). The authors maintain that denial offers the best chance for delivering victory while avoiding catastrophic escalation, whereas military cost-imposition has lower prospects of success and higher chances for catastrophic escalation.