
## Perspective Expert Insights On A Timely Policy Issue Defending Without Dominance Accelerating The Transition To A New U.S. Defense Strategy T

he last two U.S. National Security Strategies have outlined a shared vision of the most serious security challenge facing the United States: hostile and partly revisionist great powers, specifically China and Russia, that pose a threat to regional peace and stability. After years of focusing on counterterrorism (CT) and counterinsurgency (COIN) missions, U.S. defense policy must grapple with the very different operational and strategic demands of deterring— and, if necessary, fighting wars against—peer or near-peer competitors.

Confronting this renewed requirement for major warfare, the U.S. defense establishment has rushed to make up for 20 years' worth of lost time. Default approaches from before the Global War on Terror (GWOT), which were appropriate for confrontations with second- or third-tier regional powers, are manifestly inadequate for meeting the larger, more technologically sophisticated, and more competent Russian and Chinese military threats. Across the U.S. Department of Defense and military services, a shared recognition has emerged that the military dominance the United States has enjoyed since 1989 is vanishing and cannot be restored. The way the United States approaches regional wars must change.

Since about 2012–2013, the Defense Department has responded to this new reality by pursuing ideas and capabilities designed to enhance deterrent and warfighting capabilities for major conflict. I summarize some of these capabilities below, and they represent a clear impulse toward change. Senior defense officials recognize the institutional challenges the department faces and are working diligently to address them. Yet the central contention of this Perspective is that the resulting list of accomplishments, as impressive as it is, falls short of solving the problems with the default U.S. strategy from the post–Cold War era. Deeper reforms to U.S. defense strategy and policy continue to be obstructed by a wide variety of bureaucratic, institutional, and political barriers that are not giving way as quickly as the urgency of U.S. reform efforts seems to demand.

The U.S. defense establishment intellectually understands the challenges it faces and many of the changes 

## Abbreviations

| A2/AD   | anti-access and area denial            |
|---------|----------------------------------------|
| ACE     | Agile Combat Employment                |
| C2      | command and control                    |
| COIN    | counterinsurgency                      |
| CT      | counterterrorism                       |
| DIU     | Defense Innovation Unit                |
| EABO    | Expeditionary Advanced Base Operations |
| FMS     | foreign military sales                 |
| FY      | fiscal year                            |
| GWOT    | Global War on Terror                   |
| JADC2   | Joint All-Domain Command and Control   |
| JASSM   | joint air-to-surface standoff missiles |
| JWC     | Joint Warfighting Concept              |
| LRASM   | long-range anti-ship missile           |
| MDO     | Multi-Domain Operations                |
| NATO    | North Atlantic Treaty Organization     |
| NDS     | National Defense Strategy              |
| O&M     | operation and maintenance              |
| OODA    | Observe, Orient, Decide, and Act       |

required.1 The country is investing very significant resources in enhancing U.S. warfighting capacity. Yet, on its current path, U.S. defense institutions are likely to fall short of needed change because of bureaucratic rigidity, the constraining effects of institutional traditions, and path dependence. Pouring more resources into the U.S. defense program today is like tuning the engine on an exquisite car without replacing its two flat tires: It can continue to grind forward out of pure muscle, but the real barrier to its performance is not being solved.

Discussions with numerous defense analysts and people working in the defense establishment suggest that this impression is widely shared. Although many individual reforms are underway, there is still something missing in the larger U.S. effort to reshape the overall defense strategy. The U.S. Marine Corps may count as a partial exception to this rule. Marine Corps commandant David Berger's bold concept of a future Marine Corps role, as well as significant reforms in service force structure and modernization plans, represent one example of a scope and pace of reform that arguably meets the moment.2 Yet the contrast with the much more limited changes in other parts of the U.S. defense program only clarifies the slower pace and scope of reform elsewhere. It is difficult to escape the sense that repeated public calls from defense leaders for innovation and adaptation are not being matched by substantial reforms in concepts, doctrine, force design, or procurement strategies. The most fundamental decisions are being deferred.

To make this case, I first briefly survey grand strategy principles that call for a U.S. ability to project power to help deter major war in key regions. I then define the U.S. 

defense strategic approach of the post–Cold War era—
world politics. A vigorous debate has arisen over exactly this issue, with a growing chorus of scholars, analysts, and members of Congress suggesting that the United States should significantly draw back from its global military posture. If it were to do so, the context for defense strategy and policy would change dramatically, and it would be a strategic mistake. 

To safeguard core U.S. interests and promote the stability of key regions, there is no responsible substitute for some version of a basic U.S. approach that has been termed deep engagement.4 That concept refers to a United States that is decisive expeditionary force—and lay out its key assumptions in defense strategy and the wider realm of defense policy.3 I explain how these assumptions were undermined by various developments after about 1995–2000 and how the U.S. defense establishment responded to the growing infeasibility of its core approaches. I then suggest six leading barriers to reform—reasons why the Defense Department's responses have been unable to generate the degree of change required. Finally, I lay out the principles of a revised strategy and identify critical initiatives in the wider context of defense policy that can address the sources of this issue.

Among other things, this argument suggests that the most important barriers to greater U.S. military effectiveness are not budgetary; they are institutional and strategic. 

Adding hundreds of billions of dollars to the U.S. longterm defense program without addressing the conceptual and bureaucratic challenges outlined here would be inefficient and, very possibly, ineffectual. The United States should first ensure that it wrings the most defense capability from every dollar it spends and the greatest military effect from every capability it commits to a mission. Only then should the United States determine whether more resources are required.

## The U.S. Role In Promoting Global Stability And Security

- globally active in diplomatic terms, helping promote 
stability, resolve conflicts, and shape the alignment 
of global power
- strongly engaged in global institutions, processes, 
and networks
- committed to the defense of treaty allies whose 
independence and security are critical to U.S. interests
- willing to work in coalitions to punish violations 
of international norms in ways short of going to war, such as by encouraging nonaggression and nonproliferation
- supportive of efforts of many countries to respond 
to coercion or harassment by aggressive powers
- generous with foreign assistance, humanitarian and 
disaster relief, and other forms of global goodwill and support for people and societies in need.
Such global engagement benefits the United States in numerous ways. First and most fundamentally, the United States is inextricably a global power with interests—
Any U.S. defense posture must serve the overall strategic interests and objectives of the country. In their broadest form, these objectives are timeless, but the ways and means the United States employs to advance them are shaped by the strategies it chooses. These strategies, in turn, are a function of the role the United States chooses to play in economic, political, and strategic—around the world. Major instability in any region will have costs for the United States, including economic disruption, migration, terrorism, and escalating crises that demand U.S. attention. It is easy to say that the United States should turn a blind eye to distant threats and conflicts, but such threats will often generate second-order effects with significant impacts on Americans. The war in Ukraine is only the most recent example: The United States cannot remain aloof from a crisis and war that destabilizes international energy and food markets, sets off mass migration to neighboring areas, and risks escalation to a larger war in Europe.

Simply put, the safety and well-being of the citizens of the United States are unavoidably affected by events abroad. The only question is whether the United States acts in meaningful and anticipatory ways to shape world events to reduce threats to U.S. interests and the homeland. 

Some form of proactive and deep U.S. engagement in the world is essential to shape the larger global context in ways that safeguard vital U.S. interests and goals.

That forward-looking action need not include large-scale military interventions, regime change operations, or other questionable means employed over the last half-century. But some form of proactive and deep U.S. engagement in the world is essential to shape the larger global context in ways that safeguard vital U.S. interests and goals.

Second, U.S. grand strategy has, with good reason, long embodied a core principle of preventing any hostile power from dominating key regions. Should Russia overawe Europe, should China eject the United States from Asia and become the unquestioned regional hegemon, or should Iran or an outside hegemon come to dominate geopolitical and energy dynamics in the Middle East, the alignment of world power would shift significantly against the United States. Many specific U.S. interests, such as free access to critical materials and supply chains and open trade networks, would be imperiled. Being engaged in at least these three regions is essential to forestall such developments, although this need not imply large standing military deployments to each region.

Third, the U.S. effort to build a postwar, rules-based order has generated significant advantages for the United States.5 These include direct economic benefits, such as increasing trade and improving terms in trade agreements, underwriting the stability of the international financial order and dampening the effects of financial crises, restraining global proliferation, achieving vast cost savings by deterring regional wars, and increasing collective action in such areas as peacekeeping and counterpiracy. U.S. global engagement provides information about emerging dangers, along with networks and relationships the United States can use to head them off. One RAND Corporation study concluded that these developments produced such benefits for the United States as increasing economic growth by 2 percent per year or more for multiple years; 
avoiding the worst effects of financial crises and an associated 5- to 10-percent decline in gross domestic product and saving more than 4 million jobs; and increasing productivity by 15 to 40 percent over a period of years through the efficiencies of shared standards.6

Judged not by a standard 
of perfection but by 
the major goals the 
United States set for 
itself after World War II, 
the U.S. strategy of 
global engagement has 
succeeded.

But U.S. leadership is needed to motivate and coordinate 
allies' actions and investments. And, in some cases, when 
U.S. interests demand that it seek to deter aggression 
regardless of the posture of allies, the United States will not 
be able to wait on those allies to ensure adequate deterrence 
and warfighting capabilities.
    To be sure, a sustainable U.S. defense posture must 
take seriously many arguments of restraint-oriented think-
ers, who are responding in part to an undeniable post–
Cold War U.S. habit of overreach and excessive ambition. 
This mindset has been evident in U.S. interventions and 
nation-building exercises in Afghanistan, Iraq, and Libya; 
the transformative goals of the United States' post-9/11 
GWOT; support for the continual expansion of NATO; 
and the global use of classic and secondary economic sanc-

    The case for U.S. engagement is strengthened by the 
important areas of success of that approach over the past 
70 years.7 The record of U.S. foreign policy has hardly been 
free of mistakes. But it has contributed to important—even 
stunning—areas of progress in world politics: peace on the 
Korean Peninsula since 1953, stability in Europe, Japan's 
emergence under the U.S. security umbrella to be a global 
leader of the democratic world, and the prevention of 
major interstate conflict. Meanwhile, the global economy 
has grown from $1.39 trillion in 1960 to $96.1 trillion in 
2021.8 During the same period, the percentage of people 
living in extreme poverty worldwide has declined from 
more than 50 percent to about 10 percent,9 and the income 
gap between developed and developing nations has nar-
rowed dramatically. Judged not by a standard of perfection 
but by the major goals the United States set for itself after 
World War II, the U.S. strategy of global engagement has 
succeeded.
    These reasons are persuasive, even for many advocates 
of retrenchment who do not fundamentally disagree with 
the need for a significant U.S. global military role. Some 
harbor serious worries about the threat posed by China 
and fully endorse a U.S. military balancing role in Asia.10 
Others continue to support U.S. membership in the North 
Atlantic Treaty Organization (NATO) alliance and merely 
want the United States to push NATO allies to do more. 

tions to compel agreement to U.S. goals. The United States has discarded several useful constraints on rival nuclear ambitions—notably, the Agreed Framework with North Korea and the Joint Comprehensive Plan of Action with Iran—in search of absolute versions of nonproliferation goals in ways that have imposed a greater burden on U.S. defense capabilities and have committed the United States to forms of coercive punishment with major secondary costs. Part of the route to a more sustainable U.S. defense strategy is to reduce rather than exacerbate the difficulty of the missions the United States must perform.

More broadly, U.S. foreign and defense policy must resist the temptation to engage in endless wars and excessive confrontations with regional rivals and instead emphasize the need to seek areas of common interest with China and Russia where possible. A critical foundation for the defense strategy proposed here is a powerful appreciation of the value of discrimination and limits, along with the realization that the answer to the crisis in U.S. defense strategy and policy is not to try to recapture U.S. global dominance or military overmatch. Primacy, as it was recently understood, was largely a function of squaring off against second-tier powers; it is gone, and it is not coming back.

Critics of the goal of primacy are also correct when they suggest that most geopolitical disputes cannot be resolved with more military power alone. Deterrence of conflict is not a linear equation with some clear threshold of success. In most cases, U.S. strategy must lead with bold and clever diplomacy, including a willingness to show recognition of rivals' interests and provide assurances about U.S. intentions in ways that do not compromise essential U.S. interests.11
The fact that some U.S. commitments and adventures have been excessive does not mean that all of them are. Dealing with selective overreach does not call for the abandonment of the global engagement that has served the United States so well for the past 70 years. To address its current strategic challenges, the United States need not embrace the extremes in the debate over grand strategy— primacy or retrenchment—but can instead mine the very large and potentially fruitful middle ground between these two extremes. The goal should be to discover more discriminate, targeted, and innovative ways of underwriting military balances in key regions without a massive defense buildup or dangerous new doctrine. This is what defense without dominance can mean: providing sufficient military power to achieve important military objectives without dominating every aspect, domain, or battle of a future war. 

U.S. defense officials and institutions have recognized this basic trend and the need to operate without dominance. Since the U.S. defense establishment began reorienting from a CT and COIN focus to challenges posed by China and Russia—roughly in the period from 2012 to 2017—U.S. policy discussions and strategic statements have admitted that many core assumptions of its post–Cold War defense strategy are no longer valid. Two subsequent National Defense Strategies (NDSs) and other defense documents and plans have begun recalibrating U.S. approaches for a new era. However, while the Defense Department has identified many core problems and is working to resolve them, it faces two serious risks: that proposed changes will not be comprehensive enough and that the department will fail to tackle the issue holding back more-significant reforms—the institutional sclerosis of the defense establishment.

## The Legacy U.S. Defense Strategy: Decisive Expeditionary Force

The absence of a peer competitor after 1989 allowed the United States to embrace that approach with a vengeance, beginning with the 1991 Gulf War. The United States was able to project power across vast distances and achieve a decisive victory against enemy-fielded forces. 

The result, when combined with the Revolution in Military Affairs—an emerging theory of technology- and information-based systems warfare that provided U.S. forces with absolute superiority over regional adversaries— was a form of military primacy based on force projection and technological superiority. This same dominance was later used to displace governing regimes in Afghanistan, Iraq, and Libya. More-potent U.S. rivals, notably Russia and China, took note and came to fear that these capabilities could one day be deployed against them—fears that helped spur significant military reforms in both countries.12
This military primacy was not to last, however. Even in the Gulf War, knowledgeable observers pointed out that the United States was going up against an adversary whose forces had low levels of operational competence and overall effectiveness.13 It was a strategy for intimidating second- or third-tier powers rather than contending with the challenges of conflict against a major power with highly capable conventional forces and nuclear weapons.

Over time, it became clear that the default decisive expeditionary force approach rested on many underlying assumptions that would not hold to the same degree—if at all—when the strategy was aimed at a true peer or near-peer military power. In some cases, trends in warfare and world politics also conspired to undermine some U.S. defense strategy continues to be shaped by the legacy of the strategy that served the United States well following the end of the Cold War. During the Cold War, the United States employed significant forward deployed forces—in Europe, South Korea, and Japan—and relied on the presence of thousands of nonstrategic nuclear weapons to deter aggression. This posture relied heavily on large-scale re inforcements; the forward deployed forces were not assumed to be capable of prevailing on their own, even with the support of allies. The posture represented a balance of forward defense, promises of substantial force flow and reinforcement in the event of war, and the risk of nuclear escalation should those combined forces not suffice.

With the end of the Cold War, the disappearance of the Soviet threat, and the dramatic reduction in U.S. forward deployed capabilities, the default U.S. approach shifted to a strategy of *decisive expeditionary force*, which prevailed through about 2015 to 2017 and, in many ways, continues to exercise a critical influence over U.S. defense strategy. This strategy held that, when confronted with a major aggressor threatening U.S. interests somewhere in the world, the United States would gather overwhelming conventional force, project that power to the region (and, perhaps, to the homeland of the enemy), and impose its will on that country. This strategy relied on long-distance force projection in service of classic ideas of victory through major force-on-force engagements and through the submission of the enemy.

of the strategy's core assumptions, which included the following:14

- U.S. forces will arrive in time and in sufficient numbers to make a decisive difference. This assumption 
is related to the first two but also has to do with the amount of strategic lift the United States possesses and the number of active duty forces it maintains. 
This assumption is essentially a math problem: How many forces can move how quickly and with what rate of loss relative to the adversary's speed of advance and attainment of objectives? Given the distance from the United States of planned contingencies, adversary concepts for achieving objectives quickly, and limited strategic lift, this assumption likely will not hold for many future contingencies.16
- The United States will have time to marshal and forward deploy large conventional forces before initiating decisive operations. Employing decisive expeditionary force takes time, both to mobilize the forces required and to deploy them to distant theaters. From the Gulf War to the 2003 Iraq War, the United States took months to accumulate the combat power in the target theater needed to achieve decisive effect. Yet since 2003 and even before, having recognized this pattern, potential U.S. adversaries have evolved doctrines for striking forcefully and winning quickly before the United States has time to gather the needed forces.
- U.S. forces will transit to the theater of operations 
and be able to build up marshaling and logistical functions relatively unmolested. From the end of the 
Cold War through the GWOT, U.S. forces enjoyed almost complete immunity from attack during global transit and were able to assume that allies who allowed and supported this force flow would remain relatively immune to attack. Given Chinese and Russian long-range precision weapons, nuclear weapons, cyber capabilities, mines, submarines, and other tools, along with their published intent to interdict U.S. forces while underway, this transit sanctuary no longer exists. Once in the theater, U.S. forces have enjoyed an ability to operate relatively invulnerable logistical bases just behind the front 
lines,15 something that Chinese and Russian conventional missile assets would call into question.
- U.S. air power and associated advantages, such as 
space capabilities and drone targeting, will allow U.S. forces to inflict a crippling initial blow on the enemy and provide persistent air supremacy. Recent 
U.S. adversaries have not possessed multilayered air defenses or cutting-edge air defense technologies. The United States could count on air and missile attacks paralyzing an enemy, annihilating its air component, and substantially degrading its ground forces to allow decisive maneuver by U.S. ground elements. With U.S. dominance in the air, space, and electromagnetic domains largely uncontested, U.S. ground operations could proceed at relatively low risk. This would not be the case against a nearpeer adversary, especially one that has specifically identified this U.S. advantage as the major threat to its operational success and sought to develop multiple ways of mitigating it, as China and Russia have.
- U.S. forces will have the leeway to strike at any targets deemed militarily useful, including the most 
sensitive command and control (C2) or leadership sites, because the adversary has limited escalatory options. When the United States went to war with 
Iraq in 1991 and 2003, it was able to target all of 
Iraq's known C2 and leadership facilities.17 Especially in 2003, when the United States was overturning the regime, Iraq's ability to hit back in kind or escalate was very limited. That is no longer the case: U.S. freedom of action likely will be much more constrained, and key military targets may be offlimits in a contingency. Targeting a nuclear-armed adversary will involve many more constraints. 
- *Space will remain a sanctuary*. During much of the 
Cold War, and certainly since its end, potential U.S. 
adversaries have not had the capacity to disrupt U.S. space operations, including sensing and communications. That has changed: Russia and China are each seeking the capacity for signal-jamming, cyber disruption, and direct kinetic attack to place U.S. space assets at significant risk in time of conflict.
- The United States can choose the time and place for 
initiating conflict. In all its post–Cold War conflicts, 
the United States has had the luxury of determining when, where, and, to a significant degree, under what conditions war has begun. It could build up forces in a distant theater for months and attack when and how it pleased. That is unlikely to be true in future conflicts, especially given Russian and Chinese doctrines that stress the importance of surprise and achieving decisive momentum early in conflicts.
- The United States can project large-scale forces into 
more than one theater at a time. During the Cold 
War and afterward, the United States generally 
planned for the risk of *simultaneity*—the potential 
requirement to fight more than one war at the same time. U.S. officials understood that, at times, some such requirements (such as the Cold War planning 
standard of two-and-a-half wars) were well beyond 
U.S. capabilities,18 and the requirement has gradually shifted to a modified one-war strategy, with the demand to exercise some deterrent and holding 
power in a second theater.19 But some emerging 
contingencies will be so demanding, especially in the air and maritime domains, that they will impose severe limits on what the United States can deploy into other theaters at the same time. The most 
challenging trade-off between theaters will be in key enabling capabilities, such as battle C2, lift, air defense, sustainment, and cyber operations.
- Because of its ability to achieve decisive operational 
effects against secondary powers, the United States can embed highly ambitious operational objectives into its military plans. One hallmark of the ambitious and sometimes absolutist U.S. approach to strategy since the end of the Cold War has been to think expansively, and sometimes abstractly, about the specific goals and objectives of military operations, including war plans. The result has been a planning process that tends to generate goals that are extremely difficult to meet in terms of war outcomes, especially against nuclear-armed adversaries. In such conflicts, it is not clear that victory in any comprehensive form can be the goal.
- Wars will be fought in weeks and months rather than 
in years. This is not an explicit assumption of the 
a challenge as an opportunity. This assumption is now challenged by the nature of the wars the United States proposes to fight, which generally involve supporting a beleaguered ally in the defense of its own territory. In the Indo-Pacific theater especially, 
the United States will be dependent on allies and partners for access and various forms of support to prosecute a war effectively.
- U.S. forces will have all necessary access to facilities 
and territory in the potential theater of operation, both before and during a conflict. In Korea and Vietnam, U.S. regional allies allowed the use of bases for U.S. force flow and support activities. In the Gulf and Iraq Wars, access began to become a more contested issue—with Turkey, for example, denying the United States the ability to conduct ground operations into Iraq in 2003. The assumption may still be valid for Europe, where a major war with Russia would engage all NATO allies, but it certainly does not hold true in the Indo-Pacific, where many countries would want to remain aloof from a U.S.-China conflict.
- U.S. forces will have decisive technological superiority 
over their adversaries, at least in critical areas. This 
was not an assumption of the default U.S. strategy until the end of the Cold War, but since that time, it has been a cardinal advantage of U.S. operations. For example, when dealing with secondary regional powers, the United States has been able to count on absolute mastery of the air and electromagnetic domains. This is no longer true. Indeed, in some 
areas, such as ballistic missiles, hypersonic weapons, 
strategy, but it is an implicit one arising from typical planning horizons for military operations, the inventories of preferred munitions available, and the defense industrial base's capacity to produce new systems. The United States simply does not have 
the active-duty or mobilization potential to fight a protracted large-scale conflict against a highly capable nation-state adversary. Although U.S. rivals seek to quickly win themselves, if they do not, the stakes they will have placed in proposed scenarios suggest that they may continue fighting the United States even with second-tier equipment. Although it is difficult to find explicit, open-source statements from official Chinese and Russian sources, there 
is reason to believe that both may believe that they can outlast the United States in a protracted contest. The assumption that the United States will be able to win within a defined period therefore seems imprudent.
- Allies offer some political benefit to a coalition but 
are not necessary to military operations and can be a military impediment. In the post–Cold War period, 
U.S. defense and military leaders often viewed supporting allied military contributions as hindrances rather than essential supporting capabilities. This was somewhat less true during the Cold War, when NATO allies built substantial military forces, trained extensively with U.S. units, and prepared to fight alongside them. Since the end of the Cold War, however, in operations in Vietnam, the Gulf War, the Iraq War, and other contingencies, the 
U.S. military has typically assumed that integrating allied units would, in some cases, be as much 
the conflict may not be able to stay limited to conventional weapons.
- U.S. casualties will be modest compared with those 
in previous global conflicts. Because of all the 
assumptions noted above, U.S. leaders and military planners could generally bank on the fact that U.S. forces would suffer relatively few casualties. Since the end of the Cold War, the United States has not had to face the serious prospect of losing hundreds of service members on a single day, as it did, on average, every day of World War II. It has not faced the risk of losing multiple naval ships or dozens of combat aircraft. In a future conflict with a near-peer competitor, this assumption would not 
hold, even if the war remained conventional. In one recent wargame, for example, the opening weeks of a U.S.-China war over Taiwan resulted in U.S. losses of 500 aircraft, more than 20 surface vessels, and 
many other assets.20 The American public has no 
living memory of such losses.
or electronic warfare, U.S. forces in a future conflict may operate at a distinct disadvantage.
- U.S. forces could fight and win outside the shadow 
of nuclear weapons. This assumption held for many 
postwar conflicts and became a dominant assumption in the post–Cold War version of the default strategy. During that period, the United States has fought regional adversaries—in either COIN or combined-arms conflicts—that did not possess nuclear weapons. This was true of such countries as Iraq and Libya, but even earlier, the United States operated free of direct nuclear risk in Korea and Vietnam. This assumption is no longer valid, even when dealing with the regional adversary of North 
Korea. 
- The U.S. homeland would be largely immune from 
attack. This assumption held true for the post–Cold 
War version of U.S. strategy, when nuclear escalation with the Soviet Union was no longer a risk. Even during the Cold War, however, U.S. expeditionary force projection was applied to limited wars in ways that left the homeland untouched. In Korea, Vietnam, the Gulf War, and the Iraq and Afghan Wars, the United States had the ability to contain the scope and implications of the conflict while preserving dominance in air, maritime, and space domains. This assumption no longer holds, not only because of the risk of nuclear escalation (even with such adversaries as North Korea) but also because of advanced cyber and biological weapons and the potential for long-range conventional attacks. In 
any future conflict, the United States will have to 
assume that the homeland will be drawn in and that 
The crisis in U.S. defense strategy that began to be discussed around 2012 emerged from a growing recognition that these assumptions were being invalidated by major trends in the strategic environment. Beginning in the 2010s, U.S. defense officials and experts began to diagnose problems with the prevailing strategy and sketch out thoughtful ways to resolve those challenges. The problem is that the goals they laid out remain largely unmet.

Meanwhile, separate from the discrete challenges to these assumptions, another broad trend has been underway since the 1990s: technological and conceptual advances that may be changing the character of warfare. Trends in precision weapons, C2 and reconnaissance-sensing netating this pace of change, and making the institutional reforms necessary to empower that change, is the dominant challenge in defense policy today.

## The Defense Department Responds: Strategic Innovations Of The 2018 And 2022 National Defense Strategies

Much of the behavior of the U.S. defense establishment is appropriate for a world in which the character of war remains static.

The trends undermining the assumptions of the decisive expeditionary force strategy and the case for evolution in the character of warfare became widely appreciated by about 2010.23 Even earlier, beginning as early as the late 
1990s, defense analysts and senior officials were speaking openly about the need for the transformation of U.S. approaches to defense. The discussion was grounded in emerging technologies and the pace and scope of modernization efforts but also incorporated discussions of doctrine and force structure.24 The 2001 Quadrennial Defense Review Report discusses the need for forces that are "lighter, more lethal and maneuverable, survivable, and more readily deployed and employed in an integrated fashion."25 In the early to mid-2010s, U.S. defense leaders began more urgently signaling the need for shifts in U.S. defense strategy, policy, and investment. This trend of thinking would reach fruition in the 2018 and 2022 NDSs, which laid out multiple courses of action to revalidate U.S. power projection credibility and warfighting dominance. 

The recognition of the need for change accelerated during the administration of President Barak Obama, as publicly expressed, in part, by Deputy Secretary of Defense Robert Work. U.S. officials had by this time fully recogworks, artificial intelligence, autonomous systems, and other defense-related technologies are creating the potential for major engagements to be fought in new and different ways.21 Many emerging defense concepts are built around the integration of kinetic military operations and a variety of nonkinetic activities, including cyber manipulation, disinformation, and psychological operations, to achieve decisive strategic effects.

Many public statements of senior U.S. defense leaders, along with the rhetoric in some public concepts and service strategies, accept the seriousness of these trends, including the potential for a break point in the character of major warfare. But the actual policies of defense institutions do not match the diagnosis of the need for change. Much of the behavior of the U.S. defense establishment—the weapons it buys, the forces it builds, the kinds of posture it seeks to develop, and the career paths and skills it emphasizes—
is appropriate for a world in which the character of war remains static, or at most changes very slowly.22 Accelerdrafting of top-level strategic guidance, the development of new operational concepts, and the development and procurement of transformational new systems. Multiple official statements have acknowledged that the United States needs to move on from an assumption of seeking victory through comprehensive dominance of all domains of warfare and have offered ideas for revised operational concepts and new technologies and capabilities that would support such concepts. The intellectual response of the Defense Department, services, and wider national security community to the crisis of the default model has been impressive.32
nized the threat posed by Russian and Chinese anti-access and area denial (A2/AD) capabilities and the ways in which they could undermine several of the assumptions in the post–Cold War default strategy. Work became perhaps the leading voice warning of the ebbing U.S. technological advantage and the need for what he called a "third offset"—a technological leap, like the nuclear revolution and the Revolution in Military Affairs, designed to overcome the conventional advantages of potential adversaries and reestablish U.S. superiority. Although in many of his speeches he continued to emphasize the threat of terrorism, in a December 2015 talk, Work made clear that he saw a shift in primary threats to the United States: "[A]mong the most significant challenges in this 25 years, and one in my view that promises to be the most stressing one, is the reemergence of great power competition."26 
Work's focus was almost exclusively on an eroding technological edge, not on the other assumptions behind the default U.S. strategy.27
Many congressional and nongovernmental studies and analysts were coming to the same conclusion: Secular trends in military power, changes in military technology, and resulting shifts in the character of warfare demanded a new approach.28 A 2020 report by the U.S. House Armed Services Committee explicitly called out the shift from post–Cold War U.S. dominance to a new era of greater challenges to U.S. defense strategy.29 Multiple RAND studies chronicled an era of ebbing U.S. dominance.30 One major 2016 RAND study, in particular, identified many of the challenges with a defense strategy that is reliant on large-sale force projection.31
This response has been evident in two NDSs and several supporting domain- or issue-specific documents. The 2012 Defense Strategic Guidance called for a joint force 
"that will be smaller and leaner, but will be agile, flexible, ready, and technologically advanced."33 This document, however, was just beginning a reorientation of U.S. defense policy to major threats and is much less explicit about the failing assumptions of the default approach than later strategies would be.

The 2018 NDS reflects the next major high-level guidance addressing these issues.34 This NDS recognized that the ability to deploy mass military strength from the continental United States was now in question, and defense strategy had to evolve. As the strategy put it, the United States was "emerging from a period of strategic atrophy, aware that our competitive military advantage has been eroding."35 It acknowledged that many of the assumptions of the default approach were imperiled and highlighted areas that needed attention—advanced weapons, operational concepts, allied integration, and institutional reform, among others. The unclassified 2022 NDS continued this line of thought and picked up on many themes of its 2018 
Recognizing these trends, U.S. defense planners sought to refresh U.S. defense strategy, including through the predecessor.36 It offered a similar variety of initiatives to address challenged assumptions, including accelerated development of new technologies, integration of defense capabilities and concepts, and closer engagement with allies and partners.

Similar sentiments have emerged in service documents and plans. By far the most ambitious is the transformational reform program of the U.S. Marine Corps (termed Force Design 2030), which is grounded in strategic assessments about the need for change in traditional missions and concepts.37 An important recent example of such thinking is the 2020 white paper titled Accelerate Change or Lose, by General Charles Q. Brown, Chief of Staff of the Air Force. He argues that 

- embracing a variety of emerging technologies in 
ways that grant competitive advantage, specifically ubiquitous sensors, artificial intelligence, autonomous systems, and human-machine pairings
- significantly relying on long-range strike systems to 
compensate for the operational challenges of denied environments, as well as specific, niche systems (such as stealth aircraft) that can degrade the effectiveness of enemy A2/AD capabilities
- dispersing U.S. forces to increase the targeting 
challenges of an adversary, especially in the Indo- Pacific, given China's immense and growing missile inventory
- giving major attention to vulnerabilities in space 
and information systems
- integrating U.S. capabilities more tightly to achieve 
greater synergies of effects
- relying on allies and partners for more-significant 
contributions, if not in the direct war fight, then in various supporting roles.
[o]ur Air Force must accelerate change to control and exploit the air domain to the standard the Nation expects and requires from us. If we don't change—if we fail to adapt—we risk losing the certainty with which we have defended our national interests for decades. We risk losing a high-end fight. We risk losing quality Airmen, our credibility, and our ability to secure our future.38
Taken together, these documents, programs, and statements offer a revised concept of the default U.S. defense strategy. This new approach is not entirely explicit or laid out in one place. But it can be cobbled together from both official documents and unofficial analyses by experts in the U.S. defense community. The successor to decisive expeditionary force might be termed distant defeat of major aggression, and it has several leading components. Specifically, distant defeat of major aggression involves In parallel at the operational level, the military services, the Office of the Secretary of Defense, and the Joint Staff have been developing a variety of concepts to help flesh out specific elements of this revised approach. These concepts include Multi-Domain Operations (MDO), Expeditionary Advanced Base Operations (EABO), Agile Combat Employment (ACE), and Joint All-Domain Command and Control (JADC2). All of these concepts embody the broad principles of the new approach, including dispersal, coordinated and integrated fires, and the use of artificial intelligence and other emerging technologies.

These new ideas have led to investments in emerging technologies and military systems recommended by the revised approaches.39 Among other changes, a small selection of major initiatives since 2010 includes

- efforts to gradually shift from legacy systems, notably U.S. Air Force efforts to deactivate hundreds of 
older aircraft—including 108 older airframes in the FY 2021 budget—and U.S. Navy choices to decommission littoral combat ships, dock landing ships, and some F-18 aircraft40
Defense institutions have recognized the challenge posed by the growing inadequacy of the post–Cold War strategy of decisive expeditionary force and have begun to make tangible changes in service of that recognition. Yet despite the best efforts of concerned senior leaders, defense bureaucracy, culture, tradition, and politics continue to stymie truly significant reforms. U.S. defense strategy simply does not embody the degree of change that U.S. strategic assessments and public statements demand.

## Transformation Postponed: The Limits Of Recent Reforms

- the U.S. Army's process of night court program 
evaluations, which generated a list of dozens of 
lower-priority and legacy programs to cancel that saved up to $35 billion over several years, in theory to be reinvested in modernization41
- the U.S. Marine Corps' Force Design 2030 initiative, 
which represents one of the boldest comprehensive 
service redesign plans in the postwar era
- the introduction of innovation labs and maker 
spaces in many services and military institutions42
The accumulation of recent reforms is impressive. Yet U.S. defense strategy remains imperiled, in part because these steps have been incomplete; sometimes half-hearted; and obstructed at nearly every turn by bureaucratic, regulatory, legislative, and institutional cultural barriers. The great risk is that the rhetoric of transformational defense reform and new strategic perspectives will convey an impression of progress that has not yet been made, and that more-

- the new Replicator initiative announced by Deputy 
Secretary Hicks, designed to "field attritable autonomous systems at scale of multiple thousands, in multiple domains, within the next 18-to-24 months"43
- a major review of arms sales policies that produced 
more than 80 recommendations for speeding the 
process of delivering arms to allies and partners through the foreign military sales (FMS) program44
- a wider effort within DoD aimed at "systematically mapping and debugging the DoD innovation ecosystem," which has identified and is implementing dozens of reforms to accelerate and streamline 
defense processes.45

Defense bureaucracy, 
culture, tradition, and 
politics continue to stymie 
truly significant reforms. 

significant and -tangible reforms will be permanently delayed.46

## Incomplete Progress On Leading Challenges To Assumptions Of The Default Strategy

Part of the problem is that the emerging vision of U.S. 

defense strategy still relies on some variant of the decisive expeditionary force model to achieve its goals. Indeed, the difference between the two strategies may be less than meets the eye. In the event of a conflict in Europe or Asia, according to open-source documentation, the United States would still plan to flow massive forces from the homeland to the theater.47 Public statements by the services, including multiple posture statements, seem to suggest an intent to overcome rival A2/AD capabilities and reestablish dominance in key domains and in the general campaign.48
Rendering a verdict on defense reforms in progress is a tricky endeavor. Even if progress appears to be slow, the basis for change may be being laid. There are no truly objective indicators to demonstrate whether change is happening quickly enough or in the right ways. In any service, office, or command, a visitor can typically discover dozens of initiatives and emerging ideas that suggest an intent to change. But whether these steps add to the degree of big choices and major transformation required is open to question. In this section, I describe six major barriers to more-potent reform: the incomplete progress in dealing with challenged assumptions, the absence of strategic concepts with a clear theory of success, institutional rigidities, continued reliance on exquisite systems, barriers to innovation, and structural vulnerabilities. Unless they are more directly and powerfully confronted, these factors are likely to leave U.S. defense reform well short of its promise for the indefinite future.

In the broadest terms, shifts in U.S. defense strategy, policies, and investments since about 2012 have begun to address all the failing assumptions of the prior approach, but they have done so incompletely. As Table 1 documents, U.S. defense strategy documents have at least called attention to the problems. In some cases, significant investments and policy reforms are underway that appear set to make important progress. The Defense Department is working to implement the 2022 NDS and has highlighted priorities and issued directives that are producing action on many issues. However, in most of these areas of challenged assumptions, what has happened so far is more rhetoric than policy change or major reform.

One symptom of this partial progress that crops up in unclassified wargames is what might be termed the future capability escape hatch. U.S. defense planners often conduct wargames based on the systems and capabilities they believe the United States *intends to have* in a future contingency rather than a hard-edged assessment of what it is *likely* to have. However, continuing problems in defense policy and strategy may mean that many assumptions being made in games and analyses will not, in the end, be met—meaning that the outcome of the games will not reflect future reality.

In short, although the U.S. defense establishment has fully recognized the challenges posed to every major assumption of its prior default strategy, the work to build a force that is capable of overcoming those challenges remains incomplete. The problem is partly in accepting the 

## Key Assumptions Of The U.S. Defense Strategy: Current Status

| Assumption                                                                                                      |
|-----------------------------------------------------------------------------------------------------------------|
| Time needed to marshal and deploy                                                                               |
| forces                                                                                                          |
| Acknowledged but only partly addressed, especially in the theater of the pacing threat:                         |
| U.S.                                                                                                            |
| planners know that this is a problem, but public discussions of U.S. thinking appear to continue to rely        |
| on extended gathering and deploying of forces; there are no clear concepts that allow for success in the        |
| absence of such extended force gathering and deployment.                                                        |
| a                                                                                                               |
| Effective, unmolested force flow                                                                                |
| Acknowledged but fundamentally unaddressed:                                                                     |
| This is widely discussed in open-source treatments                                                              |
| and wargames, but there is no publicly discussed solution to threats (cyber or kinetic) to the marshaling,      |
| loading, transport, and delivery of U.S. forces to distant theaters.                                            |
| b                                                                                                               |
| U.S. forces arrive in time and in                                                                               |
| numbers needed to make a decisive                                                                               |
| difference                                                                                                      |
| Acknowledged but fundamentally unaddressed:                                                                     |
| U.S. planners appreciate this challenge, but general                                                            |
| discussions continue to refer to large-scale reinforcement and force flow as a core component of U.S.           |
| campaigns. Open-source discussions of U.S. concepts do not reflect a decisive break from a force flow           |
| model.                                                                                                          |
| U.S. air superiority provides critical                                                                          |
| advantage                                                                                                       |
| Partly acknowledged, mitigation efforts underway:                                                               |
| U.S. services and the Department of Defense                                                                     |
| agree that the United States may have to operate in areas without air supremacy; multiple systems and           |
| concepts seek to provide alternative ways of achieving goals, but an operational alternative to at least        |
| limited air superiority is only gradually taking shape.                                                         |
| c                                                                                                               |
| Acknowledged, discussed, but unresolved:                                                                        |
| This dilemma emerges in many discussions and                                                                    |
| unclassified wargames involving Russian and Chinese contingencies but remains unresolved; issues of             |
| the status of mainland Chinese targets, for example, arise in every unclassified game and always remain a       |
| strategic dilemma.                                                                                              |
| U.S. forces will have the leeway to                                                                             |
| strike at any targets deemed militarily                                                                         |
| useful because the adversary has                                                                                |
| limited escalatory options                                                                                      |
| Space will remain a sanctuary                                                                                   |
| Acknowledged and multiple mitigation efforts underway:                                                          |
| U.S. officials have made clear that they                                                                        |
| anticipate a significant threat to U.S. space assets in war (or even peace), and Department of Defense          |
| documents identify many programs to address this growing risk.                                                  |
| The United States can choose the                                                                                |
| time and place for initiating conflict                                                                          |
| Acknowledged but full implications remain underappreciated:                                                     |
| Given that the core U.S. defense                                                                                |
| planning challenge is major aggression undertaken by others, it is widely understood that the United            |
| States will not dictate the timing of conflict, but U.S. strategy has not fully grappled with the ways in which |
| aggressors can manipulate warning periods or the likely effect of massive initial strikes by aggressors.        |

The United States can project forces into more than one theater at a time Acknowledged and partly mitigated: The 2018 and 2022 NDSs have explicitly discussed the challenge of simultaneity and (building on work going back to the 2001 Quadrennial Defense Review) have scaled back U.S. ambitions for fighting in multiple theaters at the same time.

Seldom discussed in explicit terms: The issue of objectives is often avoided by assuming simplistic goals, such as defeating an initial attack or invasion. The problem of the ambitiousness of larger wartime goals is not generally assessed. 

Because of its military overmatch, the United States can embed highly ambitious operational objectives into its military plans

| Assumption                                                                                                      |
|-----------------------------------------------------------------------------------------------------------------|
| Wars will be fought in days, weeks,                                                                             |
| and months rather than in years                                                                                 |
| Widely acknowledged but strategies and plans do not deal with implications                                      |
| , especially in terms of                                                                                        |
| the need to achieve critical military effects within 72 hours to 7 days, or (on the other side of the spectrum) |
| the industrial, logistical, and political challenges of extended conflicts.                                     |
| Acknowledged in conceptual terms; requirements for true integration with the most militarily                    |
| capable allies has only begun.                                                                                  |
| Work on these issues is accelerating, but many barriers to deeper                                               |
| integration with allies and partners remain in place, and the United States has not clearly defined the roles   |
| and missions that it hopes allies and partners can fulfill.                                                     |
| Allies and partners can offer some                                                                              |
| political benefit but remain a military                                                                         |
| impediment to the efficient prosecution                                                                         |
| of operations                                                                                                   |
| Understood but often wished away                                                                                |
| in discussions about U.S. posture, which sometimes assume that                                                  |
| U.S. partners will be there when needed, at least for U.S. conduct of combat operations.                        |
| U.S. forces will have all necessary                                                                             |
| access to facilities and territory in the                                                                       |
| potential theater of operation, both                                                                            |
| before and during a conflict                                                                                    |
| U.S. forces will have decisive                                                                                  |
| technological superiority over their                                                                            |
| adversaries, at least in critical areas                                                                         |
| Acknowledged, but full implications being worked out:                                                           |
| U.S. planners clearly recognize that the U.S.                                                                   |
| military technological edge has narrowed. Some broad ideas are in place to deal with the narrowing gap,         |
| but they are more ad hoc than coherent, and strategic and operational concepts are not yet taking full          |
| account of the challenges.                                                                                      |
| U.S. forces can fight and win outside                                                                           |
| the shadow of nuclear conflict                                                                                  |
| Understood but seldom examined in depth:                                                                        |
| What victory means against an opponent with nuclear                                                             |
| capabilities is seldom assessed with rigor.                                                                     |
| The U.S. homeland will be immune                                                                                |
| from attack                                                                                                     |
| Acknowledged, still largely unaddressed:                                                                        |
| The 2018 and 2022 NDSs clearly recognize growing threats                                                        |
| to the U.S. homeland, but U.S. strategies lack a clear concept to project power from the continental            |
| United States while under attack and provide support to civilian agencies while managing an overseas            |
| contingency.                                                                                                    |
| d                                                                                                               |
| Acknowledged but implications not yet taken on board                                                            |
| in political terms, in necessary medical                                                                        |
| preparations, and in the effects of casualties on U.S. capacity for a protracted conflict.                      |
| U.S. casualties will be modest                                                                                  |
| compared with those in earlier                                                                                  |
| conflicts                                                                                                       |

SOURCES: Author's review of U.S. defense strategy documents and discussions with U.S. defense officials; RAND subject-matter expertise on specific issue areas.

cures to achieve its wartime aims.50 Each concept relies on contributing elements—including new capabilities, such as advanced amphibious ships in the case of EABO; access granted by partners, as with ACE and MDO; and logistics and sustainment support (for all of them)—whose budgetary or political feasibility is in question.51
scope of the combined challenge: These problems demand a fundamentally different way of approaching distant contingencies against high-capability rivals, not merely fixes around the edges, such as adding 5th-generation aircraft to the mix or planning to break up U.S. ground forces into smaller, more distributed packets. But precisely what that bolder change looks like is not apparent.

## The Continuing Lack Of A Coherent Strategic Concept With A Theory Of Success

Perhaps most importantly, few of the new concepts are fully aligned among the military services, and the ways in which each service's concepts would nest into the others' 
ideas is often poorly understood.52 From true interoperability in such areas as C2 or battle management to conceptual agreement on the shape of a future fight, the degree of true joint integration among the services lags the revolutionary assumptions of most of the emerging concepts. (This is one reason why, for all its conceptual vagueness, the notion of integrated deterrence in the 2022 NDS highlights a truly essential issue—the need for much deeper integration between the services.) As one recent study concluded, the "inability to effectively articulate new operational concepts and the linkages between these concepts and supporting technology programs (and their funding) is endemic in all military services."53
Part of the problem is that U.S. defense strategies and statements have not defined future warfare in precise terms. Most unclassified strategy documents begin with a discussion of the operating context or operational environment, which typically refers to a handful of capabilities that could be employed in future fights—precision strike, heavy use of electronic and cyber warfare, a growing role for artificial intelligence and autonomy, tight integration of capabilities in kill webs, and so on.54 But these documents stop short of describing exactly how such components will work together to achieve desired outcomes. They tend to A second persistent challenge facing U.S. defense strategy is that none of the extant ideas amounts to a truly comprehensive strategy—a coherent design for the alignment of means to achieve ends through specified ways. Recent NDSs have offered a suite of useful conceptual buzzwords, such as blunt force, integrated deterrence, and campaigning. To their great credit, the U.S. Department of Defense and individual services have started thinking through new concepts to take account of changes in warfare, concepts that generally point in the same direction—toward a more dispersed, low-signature approach to military operations that relies increasingly on larger numbers of unmanned, less expensive systems.

But the development of actionable concepts has only just begun. No one yet knows how well these emergent ideas will work in practice, whether hoped-for technologies will really support them, whether required systems will be procured, or what it will take to fully implement any of these ideas.49 Most of the emerging concepts do not specify the theory of success through which the United States expects the employment of the military it designs and proreflect descriptions of *what* U.S. (or adversary) forces will want or try to achieve, but not how they will employ specific capabilities to achieve those goals.

More broadly, the concepts embedded in recent strategies often represent suggestive notions rather than a formal strategy. Consider a key paragraph from the unclassified 2018 NDS summary:
A long-term strategic competition requires the seamless integration of multiple elements of national power—diplomacy, information, economics, finance, intelligence, law enforcement, and military. More than any other nation, America can expand the competitive space, seizing the initiative to challenge our competitors where we possess advantages and they lack strength. A more lethal force, strong alliances and partnerships, American technological innovation, and a culture of performance will generate decisive and sustained U.S. military advantages.55
Force Employment," and a "Global Operating Model," with four "layers" termed contact, blunt, *surge*, and *homeland*. 

Many of these concepts represent appropriate and useful directions for U.S. defense policy, but they do not embody an actual strategy. Some of the terms have largely disappeared from defense policy; in most other cases, defense leaders have struggled to define their precise meaning. The same is true with newer concepts mentioned in the 2022 
NDS, most especially *integrated deterrence*.

These examples reflect one of the most obvious symptoms of U.S. strategic incoherence: a widespread reliance on buzzwords rather than clear, rigorously defined concepts designed to solve specific operational problems.56 It is very common to see slides in Microsoft PowerPoint briefings that are built around a clever graphic, with geometric shapes and arrows and color-coding, filled with dozens of obscure words and terms. It is less common to see clear, straightforward statements of a strategic concept relating means to ends. As the RAND scholar Michael Spirtas has pointed out, even the basic issue of operating across The document goes on to refer to such concepts as being "strategically predictable but operationally unpredictable," "fostering a competitive mindset," "Dynamic The United States lacks a coherent and agreed-on theory of the application of military power for the most demanding operational problems or the specific warfighting challenges associated with U.S. efforts to plan for military contingencies.

domains has spawned multiple buzzwords—multidomain operations, *integrated deterrence*, and many more.57
should fight in a future conflict." Milley hints at the spirit of the JWC by noting that "it challenges the warfighter to make a fundamental shift in the way we think about maneuvering through space and time in a fast-paced, hightech, rapidly changing, and exceptionally challenging and lethal environment."63
Milley's 2023 essay publicly describes the central concepts of the new JWC for the first time. It includes an emphasis on

- an *integrated, combined joint force* that embodies a 
"seamless integration of all military Services across all warfighting domains"
- *expanded maneuver*, an idea that "challenges 
the warfighter to think creatively about moving 
through space and time, including—but not limited to—maneuver through land, sea, air, space, cyber, the electromagnetic spectrum, information space, and the cognitive realm"
- *pulsed operations*, which Milley defines as "a type 
of joint all-domain operation characterized by the deliberate application of Joint Force strength to generate or exploit our advantages over an adversary"
- *integrated command and agile control*, which is 
described as "seamless command and control across all domains" to "integrate sensors, platforms, and decisionmaking processes to achieve real-time battlespace awareness and enable rapid decisionmaking"
- *global fires*, or the "integration of lethal and nonlethal fires to deliver precise, synchronized global effects across all domains and multiple areas of 
responsibility"
As a result, the United States lacks a coherent and agreed-on theory of the application of military power—one that reflects a clear and feasible theory of victory58—for the most demanding operational problems or the specific warfighting challenges associated with U.S. efforts to plan for military contingencies.59 There is no joint understanding of how each component will fight as part of a unified whole, and individual services continue to develop major concepts largely in isolation. More resources—and the added systems they will acquire—will not compensate for a failure in operational concepts or other problems with U.S. defense policy, such as challenges in access to bases in other countries or the logistical sustainment of military forces and operations. The risk is that the United States will arrive at the onset of a conflict with half-finished concepts that the forces are unable to implement.60 The 2018 NDS 
Review Commission highlighted many of these issues, focusing on shortcomings in operational concepts,61 but most of the challenges the commission identified continue.

One Department of Defense planning process, underway for several years, is designed to close this gap with the development of a JWC. The concept itself is classified, but defense officials have discussed the process and noted a few broad principles behind the JWC on many occasions. After two initial drafts were published since about 2021, the Defense Department is on the verge of issuing JWC 3.0, which has been billed as "a true watershed moment for the joint force."62 Chairman of the Joint Chiefs of Staff General Mark Milley has described the JWC as "our roadmap to the future. It is a threat-informed, operational concept that provides an overarching approach to how the Joint Force new joint futures cross-functional team, although as of spring 2023, this remained in the conceptual and planning stages.66

- *information advantage*, which is defined as using 
advanced information technologies "to collect, analyze, and disseminate information rapidly, enabling decision superiority and action"
- *resilient logistics* to allow for "rapid movement of 
personnel, equipment, and supplies to places and times of our choosing."64
The JWC process represents by far the most serious effort to create an overarching vision of future warfighting that can inform Department of Defense efforts. It is being used to generate requirements for innovation and technological advances, and some services are tying the adaptive thinking in the JWC and related processes to their professional military education curricula.65 The Department of Defense is reportedly seeking to institutionalize the on going process of rethinking defense concepts in a 

The United States 
continues to have only a 
very limited idea of what 
victory looks like against 
a nuclear-armed foe in a 
conflict on that adversary's 
borders.

    These are welcome developments. Yet it is far too early 
to declare victory in the quest to build an overarching con-
cept for future warfighting. Many previous concepts failed 
to generate truly joint integration across the services, in 
part because of strong service cultures. Although public 
sources provide only a hint about the full JWC content, the 
concepts that have been revealed appear to be broad prin-
ciples for warfighting and do not specify a clear design of a 
campaign. The eventual statutes and authorities of a joint 
futures function remain to be determined.
    Most of all, the important and impressive intent of 
the JWC could be stymied by three of the most powerful 
barriers to reform discussed in this Perspective. Persistent 
bureaucratic obstacles to innovation in systems—classic 
problems in the acquisition process—could prevent signifi-
cant or timely movement in the direction of revolutionary 
new capabilities, except at the margins. Powerful service 
demands for the control of force design and employment 
concepts could limit the degree of force transformation 
in service of the new concept. And department-wide and 
service-specific institutional policies, especially around 
talent management, could keep the Department of Defense 
from attracting and retaining the kinds of personnel 
needed to pursue bold new visions of military operations.
    Similar concerns apply to the centerpiece U.S. initiative 
to create linked information networks to support future 
concepts—the JADC2 initiative. It represents more of an 
effort to join together existing service capabilities rather 
than push them into a truly unified system.67 Defense 
analysts Travis Sharp and Tyler Hacker have argued that 

Efforts to refresh and revalidate U.S. defense strategy 
continually run aground on problems of bureaucracy, 
programmatic limitations, regulations, and institutional 
culture.

must try at least to offer options for how to conceptualize 
success in ways that are feasible given the nature of these 
contingencies.

## Institutional Rigidity

Efforts to refresh and revalidate U.S. defense strategy continually run aground on problems of bureaucracy, programmatic limitations, regulations, and institutional culture—a third barrier to bold defense reform. Left unsolved, this issue is likely to undermine the potential for achieving the goals laid out above.

Part of the problem is what has become an institutionalized risk aversion—an aversion to the risk of making the wrong decision, endorsing the wrong policy or system, or simply taking responsibility for making a choice. Every decision must transit endless coordination processes to be signed off on by others in the process—and when a single objection crops up, that can stop a process in its tracks for weeks. These institutional flaws were serious enough when the United States possessed clear dominance and the security environment was more predictable. Today, at funding for JADC2 programs may not match its "colossal ambitions of transforming joint force C2 capabilities."68 
They warn that mixed progress in JADC2 goals could have significant implications for U.S. warfighting effectiveness.

Beyond those practical and institutional challenges, one of the especially serious gaps in current concepts is that the United States continues to have only a very limited idea of what victory looks like against a nuclear-armed foe in a conflict on that adversary's borders. It is one thing to plan to defeat aggressive invasions of NATO members in Europe or an invasion of Taiwan. However, presumably defeating an initial attack would not end the war in either case, but especially regarding China and Taiwan. The U.S. response to Russian aggression in Ukraine, even though the United States is not directly involved in the war, provides yet another example of an inability to discover feasible end states in conflicts against major powers, especially nuclear-armed ones. Yet U.S. defense concepts do not speak much to protracted war or to any sense of how such conflicts might be concluded short of nuclear escalation. This problem may be impossible to solve because it is inherent to the dilemmas of these cases. But any theory of victory 

- policies on personnel, careers, recruitment, and 
retention that hinder flexibility in military career paths, recruitment, and retention
- security clearance and classification procedures 
that remain slow, cumbersome, and unnecessarily 
restrictive on many issues. There has been talk of reforming the problems of clearance-granting and overclassification for years without any apparent progress.73
a time of ebbing U.S. overmatch and accelerating change, these flaws are unacceptable if the United States wishes to meet its security challenges in a sustainable way. The irony, of course, is that these habits do not avoid risk at all: They accept powerful medium-term risks—of ineffectiveness, institutional calcification, an inability to set feasible goals or push major reform, and misplaced investment—to avoid the short-term risk of trying new ways of doing things and breaking out of path-dependent ways of doing business.

Institutional roadblocks to change have emerged in many issue areas.69 A few leading examples are

- information-sharing and strategy and planning processes with allies and partners that remain tangled in 
a thicket of rules, regulations, laws, and habits, and which most allies and partners continue to see as a major hurdle to closer military collaboration. Current 
U.S. practice, one recent assessment concludes, 
- an acquisition system that, although it has been 
tweaked around the edges with such initiatives as the Defense Innovation Unit (DIU), remains slow 
and ponderous and that, even with dual-use technologies being bought essentially off-the-shelf from commercial manufacturers, imposes significant delays and inefficiencies70
- a budgeting system that, overlapping with aspects 
of acquisition and procurement, imposes years of 
delays on new programs or ideas and adds layers of destructive complexity to the defense policy process71
hampers innovation and undermines trust, just at the moment when both are needed in spades to compete with China. . . . Without bold action by military leaders, the administration, and Congress, the "allies and partners" strand of the *National Defense Strategy* will remain a bumper-sticker slogan rather than a real source of advantage in the strategic competition with U.S. adversaries, potentially hamper key U.S. initiatives in the Indo-Pacific, and undermine partnership with NATO allies.74

- FMS regulations and policies that continue to generate long wait times for sales, impose sometimes confusing and unnecessary constraints on what the United States can share, and, as a result, cause potential partners to look elsewhere for their military kit.
- long weapon development timelines, which have 
grown from two to eight years for such major systems as aircraft in the 1950s and 1960s to around 15 years for more-recent systems, such as the Patriot and Stinger. The time to market of major aircraft systems increased from an average of five years through the mid-1970s (for such enduring systems as the F-15, F-16, and C-141) to more than 20 years 
for current systems, such as the F-35.72
It is very difficult to identify clear, objective thresholds for measurable progress in dealing with these issues. Any set of indicators of progress would have to include both objective measures (e.g., the wait for clearances, specific categories of information able to be shared with allies) and perceptual factors (i.e., whether people see and feel an institutional context that is more fast-moving and accepting of risk).

## Continued Reliance On Highly Expensive Exquisite Systems

ical advantage will not be able to prevail in battles in which U.S. systems are outnumbered five, seven, or ten to one.

Defense policy appears to be stalled at the cusp of a transition from an era of large, few, expensive, and exquisite systems to one of smaller, cheaper, numerous, and often unmanned systems, along with the relevant concepts for such a new era.78 When a military service has made bolder choices, they have provoked bitter debates about whether the service is moving too fast to discard old ways of fighting before it understands new ones.79 There is a potential argument for iterative rather than leapfrog plans for change,80 although no one has yet defined what such a middle ground would look like. One implication of these challenges is that the United States faces a critical moment in military modernization during the remainder of this decade, when many platforms and systems reach critical ages without sufficient procurement to sustain current numbers.81
A central problem in U.S. defense policy is that larger budgets are buying smaller and smaller numbers of major weapon systems. As the price tag of those systems balloons (and often runs well over budget), the number of systems that the United States can purchase shrinks. This is likely to continue: The latest U.S. nuclear-powered aircraft carrier costs $13 billion. Future Next Generation Air Dominance aircraft have been estimated to cost hundreds of millions per aircraft.75 Furthermore, U.S. military services are deploying fewer major combat systems. To take one example, the U.S. Air Force maintained an inventory of roughly 2,600 fighter aircraft as recently as 1990. Today, it has about 1,600 (but only about 540 5th-generation models), a number that is planned to continue shrinking as the Air Force divests itself of more aircraft than it is buying.76
One result is that the effort to shift truly dramatic resources to smaller, cheaper, and more numerous weapons and systems has proved to be a slow process. In terms of munitions purchases, for example, the story is of a slow and steady shift in the needed direction—more precision weapons purchased with a greater proportion of longerrange munitions that are more appropriate for large theater engagements. But the numbers remain modest. The Air Force, as of 2022, had about 5,000 joint air-to-surface standoff missiles (JASSMs) and proposed to buy only about 500 more per year over the Future Years Defense Program.82 One analysis suggests that if just 41, or roughly half, of the Air Force's nonstealthy bombers (B-52s and B-1s) were devoted to delivering these weapons, the U.S. 

inventory as of around 2021 would be depleted in a week.83 
This trend exacerbates a related asymmetry in defense spending that aids U.S. rivals: the effect of purchasing power on what defense budgets can buy.77 When other countries are already getting more for each yuan or ruble than each U.S. dollar is buying for U.S. forces, relying on prohibitively expensive exquisite systems only widens the gap in terms of the numerical balance between the United States and its rivals. A classic military maxim is that quantity has a quality all its own; even a moderate U.S. technolog-

The valley of death, or the gap between initial system 
development and procurement and deployment, remains; 
the United States needs years—and often decades—to 
operationally deploy a system.

Even tighter constraints apply to the critical long-range 
anti-ship missile (LRASM): The Air Force is on track to 
have an inventory of only 179 LRASMs by FY 2027, enough 
for a set of nine B-52s to fly a single strike mission each 
against maritime targets.84 In other areas of cheaper or 
asymmetric capabilities, such as mine warfare, the United 
States is making few investments.85 The Navy has been 
slow to shift from a focus on large platforms; one recent 
report quoted a Navy official who described investments 
in unmanned naval systems as "the dust particle on the 
pocket lint of the budget."86

testing, evaluation, and procurement of relatively simple 
emerging technologies.87 The valley of death, or the gap 
between initial system development and procurement and 
deployment, remains a very real phenomenon, meaning 
that the United States needs years—and often decades—to 
operationally deploy a system.88 Increasingly, given the 
role of commercial technologies in military applications, 
a critical variant of this problem is the inability to iden-
tify simple, often inexpensive systems or tools—typically 
derived from dual-use commercial technologies—to 
accomplish important goals and then to buy them in suffi-
cient numbers or in a timely way. The Defense Department 
and military services impose complex and often excessive 
requirements on potential equipment and, as a result, often 
refuse to accept off-the-shelf or third-party options, even 
for relatively simple needs. Services have little budget flex-
ibility to respond to new threats without waiting for years-
long budgeting processes.89

    In trying to evaluate the adequacy of reform in this area, 
the Defense Department will be challenged by the absence 
of any objective standard for how fast the transition must 
go. At a minimum, the Defense Department could lay out 
a rough schedule for the shift away from the dominance 
of legacy systems—force structure targets that incorporate 
greater and greater proportions of newer systems.

## Barriers To Innovation

A fifth challenge to defense reform efforts is that the Defense Department remains poor at rapid and effective To address these issues, the Defense Department and services have created a set of innovation labs, "rapid experimentation funds," and other efforts to end-run the acquisition process and find ways to field warfighting systems more rapidly.90 But these efforts remain mostly at the margins of formal procurement efforts. The most prominent of these, the DIU, has funded dozens of new technologies, some of which have gone on to become programs of record.91 But the DIU's total budget is a tiny percentage of overall Pentagon procurement funding. And its long-time director, Michael Brown, recently left, and in the process, he made clear that the overall defense acquisition ecosystem remains stubbornly resistant to change.92
A 2023 study by the Defense Innovation Board's Task Force on Strategic Investment Capital concluded that classic barriers to scaled innovation—including the valley of death between research and development and full production—remain a significant impediment. The various innovation labs and centers listed earlier have generated useful systems and technologies but have trouble scaling them to full production. Meanwhile, the defense offices or services involved have not replicated the mindset of these innovation offices more broadly.93 A 2023 U.S. Government Accountability Office report agreed that acquisition policies continue to obstruct innovation. It found that the average life cycle of a procurement program lengthened between 2020 and 2022: "The Department of Defense (DOD) continues to face challenges quickly developing innovative new weapons. These challenges persist even with recent reforms to its acquisition process intended to help deliver systems to the warfighter in a timelier manner."94

## Structural Vulnerabilities

The U.S. defense posture is also vulnerable and arguably unsustainable in some important structural ways. 

For one thing, personnel and operation and maintenance 
(O&M) costs are slowly consuming the defense budget. 

Because of the need to offer competitive salaries; burgeoning military health care and retirement costs; and the expenses of a globally deployed, highly active force, these components of the budget have been growing as a proportion of the total, crowding out procurement or resources for added force structure. Between 1952 and 2016, for example, total U.S. military personnel numbers declined by 64 percent—and yet, personnel costs more than doubled in real terms. Between 2000 and 2012 alone, total cost per service member grew by 64 percent. Active duty and retiree health costs in nominal terms grew 170 percent between 
2000 and 2012; retirement payments grew 50 percent in that same period. Personnel costs have risen (in constant FY 2021 dollars) from less than $50 billion in 1948 to more than $200 billion as of this writing.95 Meanwhile, O&M 
expenses, which include some personnel costs, hit 40 percent of the overall Department of Defense budget in 2015.96 
A second source of structural vulnerability is in the U.S. defense industrial base, which imposes severe constraints on mobilization and military operations.97 U.S. 

defense policy must also grapple with the fact that the U.S. defense industrial base has a very limited capacity for surge production, which will impose severe constraints on U.S. operations in a conflict when losses begin to be significant.98 For example, U.S. production of the Javelin anti-tank missile has been publicly estimated at roughly 2,100 missiles per year, which is less than one-third of the total number shipped to Ukraine and a small percentage of the expenditure by Ukrainian forces in just a few months of war. Production goals have been ramped up to 4,000 missiles annually, but this will reportedly take several years to achieve.99 The FY 2022 defense budget provided for the procurement of 400 JASSMs in FY 2021 and 525 JASSMs would demand trillions of dollars in extra defense spending over the decades following a major conflict.

## Summing Up: Important Progress But A Sluggish Transformation

in FY 2022, between 40 and 50 LRASMs, roughly 100 precision strike missiles, and about 6,500 Guided Multiple-
Launch Rocket Systems.100 Comparing these numbers with Russian use rates in Ukraine, "in three months of combat, Russia has burned through four times the U.S. annual missile production."101
A third source of structural vulnerability relates to components and materials. Current U.S. defense strategies assume that the Defense Department can preserve access to key components, especially semiconductors, unique subcomponents produced abroad, and rare earth materials that are essential to the production of major combat systems and technologies. The supply of such components would be compromised in any conflict, further constraining the U.S. ability to make up losses or grow its force in any reasonable time frame.

These industrial and resource issues will affect potential U.S. combat power and the credibility of its global commitments in the wake of any major conflict. If the United States were to sustain significant losses, it would take years—and in some cases, decades—to replace lost systems. A 2021 Center for Strategic and International Studies report suggests that, based on current defense industrial capabilities, it would take between a decade and 40 years to replace current inventories of key systems, even at an accelerated rate of production beyond current peacetime figures: ten years for the current suite of F-15EX aircraft, close to 15 years for the F-35 inventory, more than 20 years for amphibious ships, 40 years for major surface combatants, and half a century for aircraft carriers.102 And that analysis assumes that the United States could afford a massive recapitalization effort: Replacing significant losses If we take roughly the years 2012 to 2015 as the period when the U.S. defense establishment became truly aware of the looming crisis in the default strategy of decisive expeditionary force, we can then ask what concepts, programs, and actions have taken place since that time and whether they amount to sufficient progress. Table 2 summarizes some of the reforms and changes in defense policy since that time—both accomplishments and limitations. The message of this record seems clear enough: Progress has arguably been slower and more limited than the advocates of change hoped and intended when they called for major reform.

One symptom—the institutional parallel to the proliferation of buzzwords in operational concepts—is what defense analyst Mackenzie Eaglen has termed defense reform theater. Since about 2010, she notes that U.S. defense officials have announced dozens of defense innovation initiatives and concepts for institutional reform: repeated efforts to cut excess spending in unnecessary programs or commands, budgetary changes, Strategic Choices and Management Reviews, Entrepreneur-in-Residence programs, and many others.103 As she explains, many of these concepts have washed over U.S. defense policy without deeply transforming the ways of doing business or even the amounts spent on various categories of military capabilities.

## The Record Of U.S. Defense Transformation Since 2012: Selected Examples

| Issue Area                                             |
|--------------------------------------------------------|
| Building transformative new                            |
| strategies and concepts                                |
| - Two NDSs have embraced a parallel set of major       |
| strategic priorities                                   |
| - Services have developed the outlines of new          |
| operational concepts: MDO, EABO, ACE, Distributed      |
| Maritime Operations, etc.                              |
| - The JWC process (and the supporting concept of       |
| JADC2) has begun an effort to knit joint warfighting   |
| together                                               |
| - DARPA and FFRDCs have contributed parallel ideas     |
| (such as mosaic warfare and Blue A2AD)                 |
| Acquiring new, asymmetric                              |
| military capabilities                                  |
| - Force design initiatives: Army MDTF, Army SFABs,     |
| USMC Force Design 2030, and MLR                        |
| - Publication of modernization strategies and          |
| priorities and operational imperatives (in NDS and     |
| service-specific form)                                 |
| - Major new systems developed or acquired since        |
| roughly 2012, including Army squad weapons,            |
| advanced training systems, M-SHORAD systems,           |
| PrSMs, and NMESIS anti-ship missiles                   |
| - Unmanned systems in development, including USAF      |
| CCA and Navy unmanned submersibles                     |
| Defense policy reforms                                 |
| to address specific                                    |
| challenges                                             |
| - Formal commitment to faster, more-agile acquisition  |
| processes; the creation of new offices and acquisition |
| categories to accelerate procurement                   |
| - Growing discussion of the need to improve            |
| information-sharing with allies, some targeted policy  |
| directives (such as the CSAF letter on NOFORN usage)   |
- There remains no unified, comprehensive theory of 
success for large-scale contingencies embedded in current documents
- Current concepts rely more on clever applications 
of existing capabilities and ways of thinking—an extension of network-centric warfare—than truly new ideas
- Publicly discussed concepts and visions of success 
do not account for issues of protracted conflict or what victory looks like against nuclear-armed opponents
- Many concepts remain embryonic—suggestive 
and interesting phrases and concepts rather than fleshed-out operational plans
- Jointness in concept development remains partial at 
best
- Developing and deploying new military systems—or 
even buying a dual-use capability from the private sector—takes far too long: a decade or more even for missiles and infantry weapons
- The new force designs in place do not offer 
transformational capabilities against major threats; the MDTF and MLR are more test beds than warfighting entities
- The balance between legacy systems and autonomous 
or cheaper proliferated systems remains too heavily weighted against newer capabilities
- Changes in acquisition remain partial at best; the time 
required to procure major systems remains long, and 
programs are subject to significant cost overrunsa
- Reforms in personnel and career paths remain modest - Restrictions of information-sharing remain significant 
and a continuing frustration for allies
| Issue Area                                              |
|---------------------------------------------------------|
| Improving adaptiveness                                  |
| and innovation                                          |
| - Emphasis from senior defense leaders on innovation    |
| b                                                       |
| - Defense-wide innovative offices led by DIU, including |
| Project Maven (algorithmic warfare tiger team), NSIN,   |
| and JAIC                                                |
| c                                                       |
| - Service-specific commands and offices with a future   |
| and transformative orientation: AFC, Army AI Task       |
| Force, Army ERDCWERX, USAF Rapid Capabilities           |
| Office, Air Force Techstars Accelerator, Air Force      |
| STRIKEWERX, SpaceWERX, SURFDEVRON 1                     |
| and DEVGRU, DCNO for Warfighting Development            |
| (OPNAV N7), Air Force Research Laboratory, and, in its  |
| AFWERX tech lab, SOFWERX                                |
| c                                                       |
| Better integration with allies                          |
| and partners                                            |
| - Rhetorical commitment to this objective in two NDSs   |
| - Since 2021, an impressive variety of new engagements  |
| and initiatives with allies and partners, from NATO to  |
| India to Japan, Australia, Oceania, and the Philippines |

NOTE: AFC = Army Futures Command; AFWERX = Air Force Works; AI = artificial intelligence; CCA = Collaborative Combat Aircraft; CSAF = Chief of Staff of the U.S. Air Force; DARPA = Defense Advanced Research Projects Agency; DCNO = Deputy Chief of Naval Operations; DEVGRU = development group; ERDCWERX = Engineer Research and Development Center Works; FFRDC = federally funded research and development center; ITAR = International Traffic in Arms Regulations; JAIC = Joint Artificial Intelligence Center; MDTF = Multi-Domain Task Force; M-SHORAD = Maneuver Short Range Air Defense; MLR = Marine Littoral Regiment; NMESIS = Navy Marine Expeditionary Ship Interdiction System; NOFORN = Not Releasable to Foreign Nationals; NSIN = National Security Innovation Network; OPNAV = Office of the Chief of Naval Operations; PrSM = precision strike missile; SFAB = Security Force Assistance Brigade; SOFWERX = Special Operations Forces Works; SpaceWERX = Space Force Works; SURFDEVRON = U.S. Navy Surface Development Squadron; USAF = U.S. Air Force; USMC = U.S. Marine Corps.

a One recent assessment that supports the general idea of partial, somewhat stalled progress is the U.S. Government Accountability Office's most recent update on the Defense Department in its assessed list of high-risk federal acquisition efforts in April 2023; see U.S. Government Accountability Office, High-Risk Series: Efforts Made to Achieve Progress Need to Be Maintained and Expanded to Fully Address All Areas. See also U.S. Government Accountability Office, Weapon Systems Annual Assessment: Challenges to Fielding Capabilities Faster Persist.

b See, for example, U.S. Department of Defense, "Deputy Secretary of Defense Kathleen Hicks Keynote Address at the Ash Carter Exchange on Innovation and National Security" and Hicks, "The Urgency to Innovate." 
c Many of these examples are derived from Scharre, *Four Battlegrounds: Power in the Age of Artificial Intelligence*, p. 198.

- Bureaucratic barriers to larger-scale reform remain 
powerful and largely uncontested
- Programs granted partial exceptions to constraining 
regulations have made targeted progress, but bigger procurement programs continue to have long timelines and significant cost overruns
- Service-specific innovations are not yet drawn 
together into any larger joint innovation process
- Still no well-defined plans for the roles allies and 
partners will play
- Continuing barriers to information-sharing with allies 
because of ITAR and other constraints
- The core FMS process remains sclerotic and 
uncompetitive with other providers
- Gaps remain in U.S. advisory and partnering 
capabilities

## Principles To Guide A More Transformative Defense Strategy

The U.S. defense establishment confronts the need to accomplish something that nations very rarely do—a radical overhaul of defense strategy and policy in peacetime. Modern militaries have seldom overcome institutional, cultural, political, and economic barriers to change and traded out defense approaches wholesale without the forcing function of loss in a major war. The task for any advocate of more-rapid change, therefore, is not to urge such radical reform but to outline those specific areas in which more rapid and transformative innovation could make a catalytic impact in terms of both deterrence and warfighting.104 The essential purpose of a new approach would be to accelerate the change already underway since 2012 and overcome barriers to the faster reform of capabilities and institutions. Such an approach would build on important conceptual and system-level innovation that is already underway and reflects many principles stated in the 2018 and 2022 NDSs and in various service statements.

The broad goal of a renewed reform push would be to take a series of more-decisive steps in the direction of a new way of fighting, including the investment, force design, personnel, and other change needed to empower the approach. This push would demand a clearer and more coherent statement about the emerging character of war and the specific ways in which more deeply integrated asymmetric capabilities can achieve operational goals.105 
The conceptual picture must become much clearer— exactly how the United States will employ emerging technologies to win and the ways in which each component of the joint force fits into that puzzle.

This more coherent top-level concept could help generate ideas for achieving a tremendously difficult goal: a theory of war termination for limited conflicts under the nuclear shadow and short of absolute victory on terms acceptable to the United States. The implicit theory that exists in most U.S. discussions is some variant of the "punish until we produce a breaking point" idea that has informed failed U.S. strategies in Vietnam and Afghanistan.106 It is not clear that this strategy would work any better with Russia in Ukraine or China over Taiwan than it did with the North Vietnamese. U.S. thinking on this must begin with a clear appreciation for rivals' theories of war termination—first mitigating those and then developing a U.S. theory of success. As noted above, this process is well underway, but it has not yet led to a clear and coherent theory of success—a notion of how the United States can employ a carefully curated set of capabilities that make a decisive impact in major wars.

An effort to accelerate transformation in defense strategy would be guided in part by key principles that reflect an assessment of the strategic environment and the needed changes in the U.S. approach. The following paragraphs describe some essential principles of such a change. These partly represent the upshot of the challenged assumptions listed earlier and the implications for U.S. strategy. The principles refer to both institutional and substantive changes, which together would be designed to overcome some of the barriers to effectiveness summarized in Table 2.

Form executive-legislative coalitions to achieve longdelayed reforms in a handful of critical areas. The United States has begun to show a sense of urgency about policies and investments in the strategic competition with Russia This principle calls for developing concepts that can work without making the broad variety of assumptions that characterized the post–Cold War U.S. strategy of decisive expeditionary force. To serve the goals of its global security role, the United States need not (and increasingly cannot) think in terms of achieving decisive military outcomes against distant peer or near-peer competitors. It can build capabilities for much more targeted ways of threatening decisive responses to localized aggression without relying on the larger forms of air, information, space, and maritime dominance enjoyed by U.S. forces in regional conflicts since 1945.

Such principles could reflect a concept of defense without dominance, similar to the idea of "deterring without dominance" proposed by Eric Heginbotham and Jacob Heim. They argue for an "active denial strategy" designed to deter Chinese adventurism by maintaining a capability to refuse China the benefits of military aggression. This is different than maintaining all-aspects dominance. Dominance is fast becoming prohibitively expensive and may well be inadvisable when confronting a nuclear-armed great power. Instead, active denial involves both defending the territory of U.S. allies and partners as well as making retaliatory strikes against Chinese combat forces involved in offensive operations.108
and China, but it has yet to demonstrate a similar resolve in achieving the institutional and policy reforms necessary to keep other actions and investments from being wasted. More than added funding, the United States needs a series of initiatives to build on the important initiatives undertaken in recent years, including current efforts to identify and mitigate barriers to innovation. It may be time for a wide-ranging, bipartisan executive-legislative task force to review existing studies on key required reforms and draw up the legislative and regulatory changes needed to support defense leaders who are trying to overcome these barriers.

Such an effort could target three to five important initial issues, such as procurement and personnel policies. There is no shortage of ideas and proposed reforms from which to choose. New studies or commissions are not needed—what is needed is focused effort by a dedicated, cross-government team to review proposals for reform on various issues, develop a coherent and actionable plan, and work to put into place regulatory or policy changes or new legislation to make the reforms possible.

Aim to deter without dominance, in part by regulating U.S. operational ambitions. Post–Cold War U.S. 

defense strategy has often sought to achieve elaborate objectives that infused defense plans with a degree of ambition that can no longer be realized when dealing with nuclear-armed, often near-peer military powers. A critical component of any future defense strategy will be to pare down objectives, including in classified war plans, into the realm of the feasible, especially when dealing with nuclear powers against whom no comprehensive victory will be possible.107 This again requires a theory of how to get to war termination without a complete victory.

Heginbotham and Heim recommend "resilient U.S. 

combat power" with better capacity to survive attacks of various kinds, systems for attacking conventional forces engaged in aggression, and improved contributions from allies and partners. 

The approach suggested here also closely mirrors that in a major 2016 RAND study on future defense strategy by Terry Kelly, David Gompert, and Duncan Long, which proposed concepts built around what some analysts have termed *Blue A2/AD* as the core of U.S. deterrent and warfighting strategy. This is an approach designed to turn the tables on A2/AD approaches, building powerful, integrated webs or networks of strike systems—swarms of missiles and unmanned systems, sensing and targeting grids, and related technologies to defeat attacks even without achieving general dominance in any domain. The authors suggest that the United States can and must recast and enhance its strategy as one of *power projection*, not just force projection, while concentrating militarily on preventing enemies from projecting force under the shield of their A2AD—in brief, a concept that entails exploiting U.S. advantages for the main purpose of preventing international aggression.109 
operational and industrial base efforts. Moreover, given the nonmilitary threats facing the American people, broader national security goals mandate a focus on domestic resilience and safety. A future defense strategy must begin with the homeland—to a degree not yet undertaken by any NDS—and build outward from there. This focus implies investments in information security; national systemic resilience; air and missile defense; and active component, Guard, and reserve component units oriented to homeland defense.

Accelerate the development of a true joint concept for how U.S. forces will fight in future major contingencies across all domains. As noted above, the Defense Department and services have been working for years on a suite of new operating concepts that begin to reflect the revised approach suggested here. But these concepts remain more aspirational than real in terms of the capabilities needed to implement them, the logistical and sustainment architectures to support them, and the ally and partner access required to make them work. Building a true, detailed, and transformative plan for power projection rather than force projection; imposing A2/AD on an enemy rather than fighting through their effort to do the same; and conceptualizing the fight as relying dominantly on asymmetric technologies and systems will require another major phase in this concept development process.111
In parallel, the United States needs a concept for how it proposes to undertake a protracted conflict after the first battle. One challenge of U.S. defense strategy since the Gulf War is that leaders have generally thought in terms of operations rather than wars: envisioning U.S. forces conducting a grand air and ground campaign over a short period that leads to temporary victory over a brittle enemy military.112 In the absence of complete and unconditional The result would be a strategy that would "imply a more defensive but still forward and influential U.S. role in contested regions" and whose investment priorities would focus on resilient, multilayered sensing grids;110 large numbers of relatively inexpensive precision weapons and unmanned platforms; heavier reliance on survivable platforms, such as submarines; and complementary capabilities that allow the United States—working closely with the threatened ally or partner—to threaten a decisive response to aggression.

Increase the emphasis in defense planning on homeland protection and resilience. In future conflicts and in peacetime campaigning, the U.S. homeland will be a major target of both kinetic and nonkinetic attacks in ways that have not been true of any major war the United States has fought. Adversaries will seek to create political effects, reduce the U.S. will to fight, and disrupt military a future battlespace rather than the targeting architectures that will be necessary for such weapon systems to work.

Focus on approaches that are designed to support allies and partners, aiming to both increase the resilience of the targets of aggression and make any response to aggression multilateral. U.S. strategy will be in service of supporting allies or partners that face threats of attack; it must therefore emphasize developing allied and partner capabilities, creating true interoperability, and procuring systems designed to provide the decisive force in contingencies. This approach therefore relies on increased and more-efficient defense investments by key allies and partners, beginning with Taiwan. An especially important aspect of such partner engagement would be enhancing the fast and effective mobilization and warfighting capacity of ally and partner reserve forces, which offer a potentially decisive advantage against attackers.114
victory (such as invading and occupying the enemy country, which is not plausible with nuclear-armed adversaries), however, it is not clear what the military requirements of a protracted war would be. If the United States managed to crush a Chinese invasion of Taiwan, for example, it is not at all clear that Beijing would simply sue for peace. Yet the U.S. defense industrial base—especially if it comes under continuous cyber and even kinetic attack—cannot produce the amounts and kinds of major systems necessary to sustain a major war over time. The Defense Department needs to conceptualize an approach to the months and possibly years of war that follow the initial battles in ways that provide competitive advantage; and it needs to do so without imagining a vast, World War II–style buildup of major combat systems.

Redouble the focus on and investments in sensing and targeting grids and battle-management systems. 

These systems are in many ways the critical intelligenceand awareness-generating foundation for any U.S. operational concept based on rapid interdiction of aggression. In the Taiwan Strait and elsewhere, the United States needs a new generation of multilayered webs of sensing and targeting capabilities with significant redundancies.113 Equally important are automated battle-management systems capable of employing suites of weapons to achieve desired effects, often in the space of minutes or even seconds. For layered defenses, complex long-range precision attacks, and other elements of emerging U.S. doctrine to work as intended, the actions of many systems must be coordinated in ways that go beyond current U.S. capabilities. At the moment, the Defense Department appears to be prioritizing the development of the weapon systems that will populate In terms of investments and posture, this component of defense strategy highlights the importance of significant forward-deployed forces to train and build interoperability with allied forces; U.S. unit types oriented toward such partner engagement, such as the Army's Security Force Assistance Brigades; sufficient foreign military financing to help develop those militaries; shared educational experiences through foreign military participation in U.S. professional military education; and reforms in arms sales and information-sharing practices to engage more fully with partners and allies.115 
This principle also demands much more in-depth planning to identify the relative roles of U.S. and allied or partner militaries, both in the initial phases of a conflict and over time—in everything from performing various missions in the conflict to providing sustainment capabilities.

Shift a significant focus of investment to the capabilities needed for the distant defeat of aggression— power projection rather than force structure. The time has come to direct a much more significant proportion of U.S. procurement dollars to the more asymmetric capabilities designed to support a Blue A2/AD approach to regional deterrence. One RAND study concluded, based on multiple scenario analyses, that the top-priority systems were submarines, drones, mobile medium- and long-range strike systems, and integrated air defense capabilities.116 
A more recent RAND assessment, which proposed a similarly broad approach, recommended investments in a much thicker and multilayered sensing and targeting grid over key territory, long-range missile-firing aircraft (both bombers and perhaps cargo aircraft firing palletized munitions), inexpensive and runway-independent unmanned aerial vehicles, large unmanned underwater systems, mobile long-range missile launchers, and large numbers of precision strike weapons. That report also recommends significant investments in passive defenses, such as hardened airfield shelters and significant numbers of static and actively emitting decoys.117
approach implies an increased emphasis on forward posture, but simply basing more forces and assets at vulnerable locations will not suffice. Future forward forces must be based in ways that feature mobility, hardening, dispersal, deception, and active and passive defenses so that they can withstand intensive attacks by accurate long-range strike systems. This principle suggests the need to build 11th-hour reinforcement and signaling packages that can be deployed quickly with existing U.S. lift assets and that are built around the highest-leveraged U.S. capabilities—small, highimpact ground and (in some cases) air assets that can be deployed during a period of strategic warning to signal U.S. commitment and provide critical capabilities that enhance the partner nation's capacity for self-defense without making an assumption of large-scale, timely force flow once the conflict is underway. This principle also demands transformative thinking about how U.S. capabilities, especially in the cyber, space, and information domains, can project nonkinetic power—and how reserve and Guard units and personnel can support such operations with specialized skills.

Plan to achieve initial operational objectives with the magazine available on day one. U.S. strategy must plan for the United States to achieve its initial goals with platforms and weapons on hand at the beginning of a conflict and then include approaches to gain further advantage without a massive mobilization of defense industrial production. The current U.S. defense industrial base is simply not capable of a massive surge of wartime production as occurred in World War II. U.S. strategy must plan to win the initial engagements of a war with stockpiles on hand or supplies available within weeks. It must also develop an approach to the first 12 to 24 months of a potentially extended conflict that assumes current or even lower production rates Plan to achieve initial operational objectives with forces in place plus modest reinforcements. U.S. strategy must plan to fight and win the initial battles with the forces and support assets in place in key theaters, long-range supporting efforts, and modest numbers of forces that can be brought to the theater in survivable ways within one to two weeks of an adversary initiating aggression. Rather than a mindset of flowing winning forces to a theater, the United States must take a more calibrated and tailored approach, designing modest early-phase reinforcements to fill very specific gaps and achieve decisive effects. This of major systems because supply chains and U.S. industrial facilities will be disrupted. This component of the strategy could emphasize, among other things, using dual-use technologies that can be produced in more places and more quickly, stockpiling large numbers of precision weapons and other long-lead time items, and altering operational concepts to avoid large losses of irreplaceable systems early in a war.

Revolutionize the role of the Reserve Component in all phases of U.S. military operations. Several trends in the security environment suggest that the United States could employ its Reserve Component in creative new ways to enhance its capacity in both peacetime campaigning and major war. Several emerging technology areas, including cyber, artificial intelligence, and autonomous systems, draw on capabilities that are the most advanced in commercial industry, suggesting the possibility for reservists who bring professional skills to defense initiatives. The importance of enduring engagement with allies and partners highlights the value of lasting Reserve Component engagement activities, such as the State Partnership Programs. The combination of local and remote activities points to the opportunity to use distant reserve units and specialists to accomplish wartime tasks, while the priority on homeland defense creates obvious roles for reserve and Guard capabilities. For these and other reasons, the Defense Department should continue to rethink the ways in which reserve units and skilled service members could be integrated more fully into peacetime and combat roles.

## Conclusion: Transformation On Hold

defense establishment have recognized many challenges with the default post–Cold War U.S. defense strategy and set a course for a more feasible and sustainable approach. Official statements have identified many challenges with existing assumptions of U.S. defense policy. Hundreds of innovative ideas, concepts, and technologies have been developed or are in the pipeline. All of this represents a robust agenda of responsiveness and change.

And yet, there are equally powerful signs that this recognition and intent is not likely to produce the pace or degree of change required to sustain deterrence, ensure a warfighting edge, and engage in day-to-day military competition during the coming decades. Since around 2000 and with growing intensity since 2012 to 2015, leaders, officials, and analysts in the U.S. defense establishment have recognized the need to shift from the default approaches to warfighting, weapon design and acquisition, personnel management, and many other areas—but the scope and pace of change have not matched these ambitions. Constraints on long-distance force projection mean that the United States ought to be moving toward a much more dramatic and fundamental shift in the way it plans to fight and in the military it seeks to build than is likely to be delivered by the gradual accretion of a thousand small changes. Bigger decisions must be made.

The United States cannot muscle its way past these problems with bigger budgets or expanded capabilities. As Bryan Clark and Dan Patt have argued, any approach that aims to reaffirm U.S. dominance "by growing today's munition stocks and force structure is doomed to fail."118 
The current moment demands strategic and institutional clarity and reform more than added capabilities.

The United States has arrived at a difficult moment in the history of its approach to defense strategy. Leaders of the This analysis points to the needed content and mindset of the next NDS. By 2025–2026, the time will have arrived for more-decisive moves in the directions laid out in this Perspective. The need for accelerated and audacious change is clear. The ability of the U.S. defense establishment to overcome the hurdles described above and realize the full promise of its current direction remains highly uncertain.

## Notes

1 Deputy Defense Secretary Kathleen Hicks, for example, recognized the need for more-rapid change and the continuing barriers to it in an important recent speech. See Hicks, "The Urgency to Innovate."
2 As a result of these reforms, the U.S. Marine Corps has encountered severe criticism and pushback, including from many retired marines. For an assessment of the criticisms and a response to them, see Work, "Marine Force Design: Changes Overdue Despite Critics' Claims."
3 In this Perspective, the term *defense strategy* refers to the joint and service-specific concepts and approaches developed to perform key military missions. *Military capabilities and posture* provide the means needed to carry out that strategy. *Defense policy* refers to the Defense Department's institutional capabilities and policies that set the larger context for the U.S. defense program.

4 Brooks, Ikenberry, and Wohlforth, "Don't Come Home, America: The Case Against Retrenchment."
5 Brooks, Ikenberry, and Wohlforth, "Don't Come Home, America: The Case Against Retrenchment"; Mazarr and Rhoades, Testing the Value of the Postwar International Order.

6 Mazarr and Rhoades, Testing the Value of the Postwar International Order.

7 Mazarr, "Rethinking Restraint: Why It Fails in Practice." 8 World Bank, "GDP (Current US$)." 9 Rosnick, "A History of Poverty Worldwide: How Poverty Reduction 
Accelerated in Recent Decades—Effectiveness of Growth." 
For such a transition to occur, defense leaders will have to place intense emphasis on reforming the environment for defense strategy to allow for the more rapid and efficient acquisition of weapons, easier sharing of military systems and information with partners and allies, and a general bureaucratic streamlining of defense processes. The biggest barrier to effectiveness is arguably not defense spending or the lack of 10 or 20 percent more of any one system or capability. It is a crushing bureaucratic managerialism that, in so many overlapping ways, drains the lifeblood from U.S. defense endeavors. Even those who see the need for change across the department are hemmed in by countless assumptions; rules; laws; requirements; cultural habits; bureaucratic knife-fights; and a pressing demand for daily talking points, memos, and Microsoft PowerPoint slides that keep most senior defense officials from doing anything other than keeping their heads above the rushing bureaucratic water.

Defense analysts commonly refer to the importance of John Boyd's feedback loop for understanding the dynamic of tactical engagements, consisting of Observe, Orient, Decide, and Act (OODA). Yet the United States remains trapped in an incredibly slow *institutional* OODA loop, one that keeps the Department of Defense from making choices, implementing new warfighting concepts, or deploying systems on anything but an incredibly long timeline. At the same time, it imposes significant administrative costs and damage to morale and dynamism in defense institutions. Potential adversaries will not need to get inside the tactical or operational OODA loops of U.S. forces if they consistently beat its institutional OODA loop for another decade or more.

10 Mearsheimer and Walt, "The Case for Offshore Balancing: A Superior U.S. Grand Strategy"; Walt, The Hell of Good Intentions: America's Foreign Policy Elite and the Decline of U.S. Primacy, p. 269; Posen, Restraint: A New Foundation for U.S. Grand Strategy, pp. 95–105.

11 A good example of what is meant by this is the Cuban Missile Crisis. 

President John F. Kennedy was willing to offer a public promise not to invade Cuba and a private assurance that U.S. missiles would be removed from Turkey to help bring the crisis to an end. Neither compromised vital U.S. interests, but some U.S. officials and outside commentators opposed both steps, arguing for an inflexible U.S. position. But dissuading aggression can be as much about convincing rivals that they do not have to attack as it is about threatening them with consequences if they do.

12 Farley, "What Scares China's Military: The 1991 Gulf War." 13 Pollack, Armies of Sand: The Past, Present, and Future of Arab Military Effectiveness.

14 Many of the same themes appear in Dougherty, Why America Needs a New Way of War.

15 Dougherty, *Why America Needs a New Way of War*, p. 7. 16 See Clark, Walton, and Lemon, Strengthening the U.S. Defense Maritime Industrial Base: A Plan to Improve Maritime Industry's Contribution to National Security; Tegler, "Air Force Under Pressure as Airlift Capacity Falls"; and Busler, "Strategic Mobility in the Context of U.S. National Defense Strategies." The U.S. Navy has recognized the sealift problem and is working to recapitalize the fleet, though slowly. See Katz, "Navy Admiral Touts Sealift Recap, Stops Short of Saying 2025 Problem Is Solved."
17 Dougherty, *Why America Needs a New Way of War*, p. 8. 18 In his memoirs, Henry Kissinger described reviewing the thencurrent two-and-a-half-war planning construct—which he described as a "paper exercise" because "we never chose to build the conventional forces envisaged by this ambitious strategy." After a policy review, he recommended a revised and more limited plan to "maintain forces capable of either a NATO initial defense or a defense against a fullscale Chinese attack in Korea or Southeast Asia." Kissinger felt that there was simply "no realistic prospect that the Chinese and the Soviets would move against us at the same time" (Kissinger, *White House Years*, pp. 220–221).

19 Mitre, "A Eulogy for the Two-War Construct."
20 Katz and Insinna, "A 'Bloody Mess' with 'Terrible Loss of Life': How a China-U.S. Conflict over Taiwan Could Play Out."
21 Brose, The Kill Chain: Defending America in the Future of High-Tech Warfare.

22 I am indebted to COL Marco Lyons for insights related to this point. 23 As one recent study concluded, "There is broad recognition in each service that, since the end of the Cold War, the U.S. military has become accustomed to operating with advantages it no longer has. These advantages include those in time and space, as well as technological advantages it held over adversaries" (Mahnken, Montgomery, and Hacker, Innovating for Great Power Competition: An Examination of Service and Joint Innovation Efforts, p. 50).

24 See, for example, Binnendijk, *Transforming America's Military*. For a discussion of former Secretary of Defense Donald Rumsfeld's transformation intent and efforts to adjust force structure and procurement, see Scarborough, Rumsfeld's War: The Untold Story of America's Anti- Terrorist Commander, pp. 111–144.

25 U.S. Department of Defense, *Quadrennial Defense Review Report*, p. 32. Quoted in Binnendijk, *Transforming America's Military*, p. 105.

26 Work, "Remarks by Defense Deputy Secretary Robert Work at the CNAS Inaugural National Security Forum." 
27 "[I]t's become very clear to us that our military's long comfortable technological edge . . . [is] steadily eroding," Robert Work argued in one speech. "Now, we still believe we have a margin, but the margin is steadily eroding and it's making us uncomfortable. We believe this is one of the greatest strategic challenges facing the department." And these challenges will ultimately affect "America's leadership around the globe" (Work, "The Third U.S. Offset Strategy and Its Implications for Partners and Allies"). See also, Work, "Remarks by Deputy Secretary Work on Third Offset Strategy." 
28 One example is Heginbotham and Heim, "Deterring Without Dominance: Discouraging Chinese Adventurism Under Austerity."
29 U.S. House of Representatives, Committee on Armed Services, Future of Defense Task Force Report 2020.

30 See, for example, Heginbotham et al., The U.S.-China Military Scorecard: Forces, Geography, and the Evolving Balance of Power, 1996–2017.

32 These changes were partly reflected in a series of joint force conceptual documents beginning as early as 1996. See Milley, "Strategic Inflection Point: The Most Historically Significant and Fundamental Change in the Character of War Is Happening Now—While the Future Is Clouded in Mist and Uncertainty," p. 12.

33 U.S. Department of Defense, Sustaining U.S. Global Leadership: Priorities for 21st Century Defense, generally referred to as the 2012 Defense Strategic Guidance.

34 Jim Mitre, former senior Pentagon official who worked on the 2018 
NDS, conversation with the author, June 2023.

35 U.S. Department of Defense, Summary of the 2018 National Defense Strategy of the United States of America: Sharpening the American Military's Competitive Edge, p. 1. 

36 U.S. Department of Defense, 2022 National Defense Strategy of the United States of America. Similar sentiments were expressed in Austin, 
"Message to the Force." 
37 See Berger, Commandant's Planning Guidance: 38th Commandant of the Marine Corps.

38 Brown, *Accelerate Change or Lose*, p. 2. 39 For example, almost 36 percent of the fiscal year (FY) 2023 defense budget was devoted to research, development, test, and evaluation and procurement, and the budget allocated $2.7 billion to support the retirement of legacy systems to clear the way for new ones (Janeway and Eastman, "U.S. DoD FY23 President's Budget Request [PBR]: 'First Look'").

40 Maucione, "DoD Budget Largely Flat, Cuts Legacy Systems for Modernization"; Cancian and Saxton, "What's in a Name? Billions in Cuts Depend on Defining 'Legacy.'" Yet as Cancian and Saxton point out, different actors in the process define *legacy* in distinct ways.

41 This number comes from the Association of the United States Army, 
"Army's Night Court Could Be Model for DoD." 
42 Paul Scharre describes many of these facilities in Four Battlegrounds: 
Power in the Age of Artificial Intelligence, pp. 194–198. See also Blanken, Dimayuga, and Tsolis, "Making Friends in Maker-Spaces: From Grassroots Innovation to Great-Power Competition."
43 Hicks, "The Urgency to Innovate." See also Lauren Kahn, "Scaling the Future: How Replicator Aims to Fast-Track U.S. Defense Capabilities."

44 Lubold, "Pentagon Trying to Get Weapons to Allies Faster." 45 Hicks, "The Urgency to Innovate."
46 In a sense, what may be emerging is a new expression of a recurring pattern—one described with frustration by former Secretary of Defense Rumsfeld in his "anchor chain" memo. Shortly after returning to the job of defense secretary, Rumsfeld—who came to office with an agenda for bold transformation of defense policy—was getting a sense of the obstacles in his way: "After 2 months on the job, it is clear that the Defense establishment is tangled in its anchor chain. . . . The maze of constraints on the Department," many of them a product of congressional action, "force it to operate in a manner that is so slow, so ponderous and so inefficient that whatever it ultimately does will inevitably be a decade or so late" (Woodward, *State of Denial: Bush at War, Part III*, pp. 25–26). 

See also a public speech by Rumsfeld on defense bureaucracy (Rumsfeld, "DoD Acquisition and Logistics Excellence Week Kickoff—Bureaucracy to the Battlefield").

47 See, for example, Wong et al., New Directions for Projecting Land 
Power in the Indo-Pacific: Contexts, Constraints, and Concepts.
48 To take just one example, the U.S. Air Force's FY 2022 posture statement argues that "[c]ombat power, regardless of Service, often depends on the Air Force's ability to deliver air superiority." The statement makes a concession in referring to the need to "use the right mix of capability and capacity to control the air while creating windows of air superiority—no matter the threat," presumably implying that generalized superiority (as opposed to specific windows) is out of the question 
(Kendall, Raymond, and Brown, Department of the Air Force Posture Statement: Fiscal Year 2022, p. 6). 

49 In this sense, the basic concern of the 2018 NDS Review Commission—that the operational concepts emerging in U.S. defense strategy were insufficient—remains the case five years later. The commission concluded that "[p]roposed fixes to existing vulnerabilities— concepts such as 'expanding the competitive space,' 'accepting risk' in lower-priority theaters, increasing the salience of nuclear weapons, or relying on 'Dynamic Force Employment'—are imprecise and unpersuasive" (Edelman et al., Providing for the Common Defense: The Assessment and Recommendations of the National Defense Strategy Commission, pp. vii–viii). 

50 For an argument on this score about the FY 2023 defense budget, see Cordesman, The Fiscal Year 2023 Defense Budget Submission: A Strategic Intellectual Vacuum. 

51 One recent review of conceptual innovation in the Department of Defense concluded that 

conceptual and organizational innovation at the joint level struggles from ill-defined concepts and poorly supported approaches that hinder senior leader buy-in, experimentation, and the 
development of the intellectual capital needed for transformative change. Like the previous development of AirLand Battle, the current [Joint Warfighting Concept] and JADC2 efforts must overcome a culture of stovepiping and resistance to change. 
(Mahnken, Montgomery, and Hacker, Innovating for Great Power Competition: An Examination of Service and Joint Innovation Efforts, p. ii) 
52 This is partly a product of the decline of joint concept and doctrinal development. See Angevine, "Time to Revive Joint Concept Development and Experimentation."
53 Mahnken, Montgomery, and Hacker, Innovating for Great Power Competition: An Examination of Service and Joint Innovation Efforts, p. 56.

54 A good example is U.S. Army Training and Doctrine Command Pamphlet 525-92, The Operational Environment and the Changing Character of Warfare. See also "Character of Warfare 2035." 
55 U.S. Department of Defense, Summary of the 2018 National Defense Strategy of the United States of America: Sharpening the American Military's Competitive Edge, p. 4.

56 Wicker, "Full-Spectrum Integrated Lethality? On the Promise and Peril of Buzzwords."
57 Spirtas, "Toward One Understanding of Multiple Domains." 58 Roberts, "On the Need for a Blue Theory of Victory." J. Boone Bartholomees concludes that the United States is developing a reputation much like Germany had in the twentieth century of being tactically and operationally superb but strategically inept. Often stated as a tendency to win the war but lose the peace, this problem has a huge theoretical component that the national security community has only recently begun to address. In fact, the concept of victory is the biggest theoretical challenge facing security professionals today (Bartholomees, "Theory of Victory").

59 In theory, the Joint Warfighting Concept (JWC) ought to achieve some of these goals; for background, see Gould, "Hyten Explains New Acquisition Directives to Industry." The JWC is a classified document. 

However, for various reasons, the JWC as it is currently conceived will solve the problem of a unified operational strategy.

60 The fate of one famous recent U.S. doctrine—Field Manual 3-24, the manual for COIN—highlights the risks involved (Army Field Manual 3-24, "Insurgencies and Countering Insurgencies"). That publication represented a sophisticated summary of key points from the scholarly literature on insurgency and COIN and outlined principles for success. But it did not produce that success in practice in recent conflicts, in part because some of its primary assumptions—for example, that the counterinsurgent could enhance the quality of governance in the partner's society—proved unworkable.

61 Edelman et al., Providing for the Common Defense: The Assessment and Recommendations of the National Defense Strategy Commission. 

The authors concluded that "[u]nfortunately, the innovative operational concepts we need do not currently appear to exist" (p. viii).

62 Vice Chairman of the Joint Chiefs Admiral Christopher Grady, quoted in Decker, "Upcoming Release of New Joint Warfighting Concept Touted as a 'Watershed Moment.'"
63 Milley, "Strategic Inflection Point: The Most Historically Significant and Fundamental Change in the Character of War Is Happening Now— While the Future Is Clouded in Mist and Uncertainty," pp. 9–10.

64 These quotes come from Milley, "Strategic Inflection Point: The Most Historically Significant and Fundamental Change in the Character of War Is Happening Now—While the Future Is Clouded in Mist and Uncertainty," p. 12.

65 Heckmann, "Sea-Air-Space News: Joint Warfighting Concept 3.0 
'Definitely Coming,' Official Says."
66 Vincent, "Milley's Team Readying Analysis for Establishing New 
'Joint Futures' Organization for DOD." The idea of such an organization is also mentioned in Milley, "Strategic Inflection Point: The Most Historically Significant and Fundamental Change in the Character of War Is Happening Now—While the Future Is Clouded in Mist and Uncertainty," p. 13.

67 Hoehn, Joint All-Domain Command and Control: Background and Issues for Congress, pp. 13–17. 

69 One short essay judged U.S. Department of Defense processes against an eight-part framework for assessing organizational agility and found that it fails on most counts. See Modigliani, "DoD's Lack of Agility Is a National Security Risk." 
70 For a helpful description of delays imposed in various phases of this process, see Modigliani, "The Program Side of the Valley of Death." A 2023 British House of Commons report made very similar arguments about the British procurement system (House of Commons Defence Committee, It Is Broke—and It's Time to Fix It: The UK's Defence Procurement System).

71 MacGregor, Modigliani, and Grant, "Pentagon Needs a Six-Pillar Foundation."
72 Greenwalt and Patt, Competing in Time: Ensuring Capability Advantage and Mission Success Through Adaptable Resource Allocation, pp. 20–22.

73 Connelly and Irvin, "How Secrecy Limits Diversity: The Security-
Clearance Process Keeps Many Qualified People Out of America's National Security Workforce." 
74 Monaghan and Cheverton, "What Allies Want: Delivering the U.S. 

National Defense Strategy's Ambition on Allies and Partners." There are emerging indications that such information-sharing challenges continue to plague one of the flagship alliance initiatives of recent years, the AUKUS initiative. See Corben and Greenwalt, "Breaking the Barriers: Reforming U.S. Export Controls to Realise the Potential of AUKUS."
75 Losey, "Future NGAD Fighter Jets Could Cost 'Hundreds of Millions' 
Apiece."
76 The current number of U.S. Air Force fighter aircraft is drawn from Janes Country Intelligence, "The United States, Air Force." See also Deptula and Gunzinger, Decades of Air Force Underfunding Threaten America's Ability to Win, p. 8.

77 Robertson, "Debating Defence Budgets: Why Military Purchasing Power Parity Matters"; Kofman and Connolly, "Why Russian Military Expenditure Is Much Higher Than Commonly Understood (as Is China's)"; "Nominal Spending Figures Understate China's Military Might."
78 Brose, The Kill Chain: Defending America in the Future of High-Tech Warfare. See also Hamilton and Ochmanek, Operating Low-Cost, Reusable Unmanned Aerial Vehicles in Contested Environments: Preliminary Evaluation of Operational Concepts; and Hammes, "America Is Well Within Range of a Big Surprise, So Why Can't It See?" An agenda much like this is suggested by Flournoy and Brown, "Time Is Running Out to Defend Taiwan: Why the Pentagon Must Focus on Near-Term Deterrence."
79 West, "Are the Marines Inventing the Edsel or the Mustang?"; Gould, 
"Pentagon Budget Will Shake Up 'Legacy' Systems. Lawmakers Are Shaking Back."
80 Chief of Naval Operations Admiral Michael Gilday, for example, has been quoted as endorsing an evolutionary approach to unmanned systems; his intent is "to get to the point, hopefully in the 2030s, where we really do have a hybrid fleet, where we can make Distributed Maritime Operations come alive in a way that would be highly effective if we actually had to fight" (Pincus, "What We Know About Future Maritime Wars").

81 Eaglen, *The 2020s Tri-Service Modernization Crunch*.

82 Pettyjohn and Dennis, Precision and Posture: Defense Spending 
Trends and the FY23 Budget Request, p. 9. 
83 Gunzinger, Affordable Mass: The Need for a Cost-Effective PGM Mix 
for Great Power Competition, p. 19.
84 Pettyjohn and Dennis, Precision and Posture: Defense Spending 
Trends and the FY23 Budget Request, p. 10.
85 Wester and Mancini, "Lessons Learned or Lessons Observed: The 
U.S. Navy's Relationship with Mine Warfare."
86 Lipton, "Faced with Evolving Threats, U.S. Navy Struggles to Change."
87 A powerful recent statement of the problem and a bold agenda for reform is in Lofgren, McNamara, and Modigliani, Commission on Defense Innovation Adoption: Interim Report.

88 One recent example of this timeline was the use of Segway-mounted moving targets to improve marine marksmanship. The systems worked beautifully, and the call to expand their use across all military training would languish for more than a decade in a protracted series of tests and user evaluations by the Army and Marine Corps. Because of inertia, budget infighting and an opaque bureaucratic process, the robotic targets had fallen into what the defense world calls the Valley of Death—the chasm between a promising new military technology and its ultimate adoption and employment. (Seck, "Robots, Marines and the Ultimate Battle with Bureaucracy")
89 Decker, "Will SecAF's Budget-Flexibility Proposal Die on the Hill?" 90 Albon, "Pentagon Wants at Least $377 Million over Five Years for New Rapid Experimentation Fund." The Pentagon requested $70 million in FY 2023 funds for a Rapid Defense Experimentation Reserve.

91 According to one account, 
[i]n fiscal year 2022, DIU turned 17 prototype contracts into products ordered by other Defense agencies to the tune of $1.3 billion. That doubled its 2021 effort and brought DIU's total to 52 follow-on contracts to 48 companies worth a total of $4.9 billion. Sixteen of those technologies have become programs of record. The agency awarded 81 new prototype contracts worth about $205 million in fiscal [year] 2022. (Williams, "The Pentagon's Innovation Shop Wants More Influence in 2023")
92 Mehta, "Exclusive: Outgoing DIU Head 'Frustrated . . . We're Not Supported More' by Big Pentagon." An important symptom was the fact that Brown's nomination to move from DIU to hold the senior defense acquisition role was ruined by what ethics investigators determined to be unfounded allegations—one relating to precisely the sorts of flexible operating authority that DIU had been granted (Kaplan, "The National Security Disaster You Probably Missed Last Week"). The complaints that led to Brown backing out were later disproved. See Gill, "Former DIU Director Cleared of Ethics Investigations"; and Greenwalt, "DIU's Director Tried to Overcome a Calcified Defense Innovation System. It Beat Him. Now What?"
93 Defense Innovation Board, Task Force on Strategic Investment Capital, Terraforming the Valley of Death: Making the Defense Market Navigable for Startups. 

94 U.S. Government Accountability Office, Weapon Systems Annual Assessment: Programs Are Not Consistently Implementing Practices That Can Help Accelerate Acquisitions. 

95 Daniels, *Assessing Trends in Military Personnel Costs*. 96 Congressional Budget Office, Trends in Spending by the Department of Defense for Operation and Maintenance. 

97 Jordan and Mapp, "In the Dark: How the Pentagon's Limited Supplier Visibility Risks U.S. National Security."

98 Cameron, "U.S. Struggles to Replenish Munitions Stockpiles as 
Ukraine War Drags On"; Jones, Empty Bins in a Wartime Environment: The Challenge to the U.S. Defense Industrial Base.
99 Newdick, "Production of In-Demand Javelin Missiles Set to Almost Double."
100 Office of the Under Secretary of Defense (Comptroller), Chief Financial Officer, Program Acquisition Cost by Weapon System: United States Department of Defense Fiscal Year 2022 Budget Request.

101 Vershinin, "The Return of Industrial Warfare." 102 Cancian, Industrial Mobilization: Assessing Surge Capabilities, Wartime Risk, and System Brittleness.

103 Eaglen, Beyond Monopsony: Pentagon Reform in the Information Age.

104 There is something of an analogy with the defense procurement and strategic innovations in the interwar period. In the case of the German military, much of it remained stuck in World War I–era thinking and organization, but innovations in selected force design, operational concepts, and system procurement created the capacity for decisive operational success through the blitzkrieg. In the U.S. military, elements of the Navy and Army remained wedded to anachronistic operational capabilities, such as the battleship (at least as the leading instrument for large-scale sea battles) and the cavalry. But a critical mass of reformers, supported with just enough of a shift in investment and procurement, managed to lay the groundwork for carrier-based operations and large armored maneuver forces. It is those targeted pockets of innovation that are similarly required today—with the important additional demand that future wars likely will not provide years of time to adjust on the fly. The United States probably needs new ideas and capabilities in place at the outset of future conflicts.

105 In this sense, what it required is an elaboration and specification of the ideas embodied in such concepts as mosaic warfare. See, for example, Jensen and Paschkewitz, "Mosaic Warfare: Small and Scalable Are Beautiful."
106 Mueller, "The Search for the 'Breaking Point' in Vietnam: The Statistics of a Deadly Quarrel."
107 For an analysis of the dynamics of conflicts involving nucleararmed adversaries, see Ochmanek and Schwartz, The Challenge of Nuclear-Armed Regional Adversaries.

108 Heginbotham and Heim, "Deterring Without Dominance: Discouraging Chinese Adventurism Under Austerity," p. 191.

109 Kelly, Gompert, and Long, *Smarter Power, Stronger Partners*, Volume I: Exploiting U.S. Advantages to Prevent Aggression, p. xvi.

110 Kelly, Gompert, and Long, Smarter Power, Stronger Partners, Volume I: Exploiting U.S. Advantages to Prevent Aggression, p. 140.

111 See chapter 2 in Ochmanek et al., Inflection Point: How to Reverse the Erosion of U.S. and Allied Military Power and Influence. See also Ochmanek, Determining the Military Capabilities Most Needed to Counter China and Russia: A Strategy-Driven Approach.

112 I am grateful to Marco Lyons for this insightful suggestion. 113 For a helpful description of one agenda for achieving this priority, see Mahnken et al., Implementing Deterrence by Detection: Innovative Capabilities, Processes, and Organizations for Situational Awareness in the Indo-Pacific Region.

114 Balestrieri and Koo, "South Korea Needs a Wake-Up Call on Its Reservist Crisis."
115 An example in the air component is to develop ally and partner hosting arrangements, along with "warm" airfield facilities (which may not be in active use but could be ready for operations in a relatively short period of time) and locally stockpiled munitions, to allow a significant and rapid surge of self-deploying U.S. air assets to a theater. A relatively thin tail of U.S. maintenance personnel could be supported by a significant host nation logistical operation and airfield security capabilities.

116 Kelly, Gompert, and Long, Smarter Power, Stronger Partners, Volume I: Exploiting U.S. Advantages to Prevent Aggression, pp. 162–165.

117 Ochmanek, Determining the Military Capabilities Most Needed to Counter China and Russia: A Strategy-Driven Approach.

118 Clark and Patt, *Campaigning to Dissuade*.

## References

Albon, Courtney, "Pentagon Wants at Least $377 Million over Five Years for New Rapid Experimentation Fund," *C4ISRNet*, April 26, 2022. 

Alman, David, "Convoy Escort: The Navy's Forgotten (Purpose) 
Mission," *War on the Rocks*, December 30, 2020.

Angevine, Robert G., "Time to Revive Joint Concept Development and Experimentation," *War on the Rocks*, January 23, 2020.

Army Field Manual 3-24, *Insurgencies and Countering Insurgencies*, Department of the Army, May 2014. Association of the United States Army, "Army's Night Court Could Be Model for DoD," February 22, 2021. Austin, Lloyd, "Message to the Force," memorandum for all Department of Defense employees, U.S. Department of Defense, March 4, 2021. Balestrieri, Brendan, and Won-Geun Koo, "South Korea Needs a Wake-
Up Call on Its Reservist Crisis," *War on the Rocks*, July 26, 2022. Bartholomees, J. Boone, "Theory of Victory," *Parameters*, Vol. 38, No. 2, Summer 2008.

Berger, David, Commandant's Planning Guidance: 38th Commandant of the Marine Corps, Department of the Navy, 2019. Binnendijk, Hans, ed., *Transforming America's Military*, National Defense University Press, 2002. Blanken, Leo, Romulo G. Dimayuga II, and Kristen Tsolis, "Making Friends in Maker-Spaces: From Grassroots Innovation to Great-Power Competition," *War on the Rocks*, January 12, 2021.

Brooks, Stephen G., G. John Ikenberry, and William C. Wohlforth, "Don't Come Home, America: The Case Against Retrenchment," 
International Security, Vol. 37, No. 3, Winter 2012–2013.

Brose, Christian, The Kill Chain: Defending America in the Future of High-Tech Warfare, Hachette Books, 2020.

Brown, C. Q., and Ryan Evans, "A Conversation with Gen. C. Q. Brown, Chief of Staff of the Air Force," *War on the Rocks*, April 25, 2023. Brown, Charles Q., *Accelerate Change or Lose*, U.S. Air Force, August 
2020. Busler, Bruce, "Strategic Mobility in the Context of U.S. National Defense Strategies," *Joint Force Quarterly*, Vol. 107, 2022. 

Cameron, Doug, "U.S. Struggles to Replenish Munitions Stockpiles as Ukraine War Drags On," *Wall Street Journal*, April 29, 2023.

Cancian, Mark F., Industrial Mobilization: Assessing Surge Capabilities, Wartime Risk, and System Brittleness, Center for Strategic and International Studies, January 8, 2021. Cancian, Mark, and Adam Saxton, "What's in a Name? Billions in Cuts Depend on Defining 'Legacy,'" *Breaking Defense*, March 10, 2021. 

"Character of Warfare 2035," Mad Scientist Laboratory blog, December 7, 2020. As of August 9, 2023: https://madsciblog.tradoc.army.mil/290-character-of-warfare-2035/ 
Clark, Bryan, and Dan Patt, *Campaigning to Dissuade*, The Hudson Institute, 2023.

Clark, Bryan, Timothy A. Walton, and Adam Lemon, Strengthening the U.S. Defense Maritime Industrial Base: A Plan to Improve Maritime Industry's Contribution to National Security, Center for Strategic and Budgetary Assessments, 2020.

Congressional Budget Office, Trends in Spending by the Department of Defense for Operation and Maintenance, January 5, 2017. 

Connelly, Matthew, and Patricia Irvin, "How Secrecy Limits Diversity: The Security-Clearance Process Keeps Many Qualified People Out of America's National Security Workforce," *Foreign Affairs*, May 12, 2023. 

Corben, Tom, and William Greenwalt, Breaking the Barriers: Reforming U.S. Export Controls to Realise the Potential of AUKUS, The United States Studies Centre, University of Sydney, May 16, 2023.

Cordesman, Anthony H., The Fiscal Year 2023 Defense Budget Submission: A Strategic Intellectual Vacuum, Center for Strategic and International Studies Commentary, March 29, 2022. 

Daniels, Seamus P., *Assessing Trends in Military Personnel Costs*, Center for Strategic and International Studies, September 9, 2011. Decker, Audrey, "Upcoming Release of New Joint Warfighting Concept Touted as a 'Watershed Moment,'" *Inside Defense*, October 18, 2022. 

Decker, Audrey, "Will SecAF's Budget-Flexibility Proposal Die on the Hill?" *Defense One*, May 11, 2023. Defense Innovation Board, Task Force on Strategic Investment Capital, Terraforming the Valley of Death: Making the Defense Market Navigable for Startups, 2023. Deptula, David A., and Mark A. Gunzinger, Decades of Air Force Underfunding Threaten America's Ability to Win, Mitchell Institute Policy Paper, Vol. 37, September 2022.

Dougherty, Christopher M., *Why America Needs a New Way of War*, Center for a New American Security, June 12, 2019.

Eaglen, Mackenzie, *The 2020s Tri-Service Modernization Crunch*, American Enterprise Institute, March 23, 2021.

Eaglen, Mackenzie, Beyond Monopsony: Pentagon Reform in the Information Age, American Enterprise Institute, March 2, 2023.

Edelman, Eric, Gary Roughead, Christine Fox, Thomas Mahnken, Kathleen Hicks, Michael McCord, Jack Keane, Michael Morell, Andrew Krepinevich, Anne Patterson, Jon Kyi, and Roger Zakheim, Providing for the Common Defense: The Assessment and Recommendations of the National Defense Strategy Commission, United States Institute of Peace, November 2011. 

Farley, Robert, "What Scares China's Military: The 1991 Gulf War," 
National Interest, November 24, 2014.

Flournoy, Michèle, and Michael Brown, "Time Is Running Out to Defend Taiwan: Why the Pentagon Must Focus on Near-Term Deterrence," *Foreign Affairs*, September 14, 2022.

Gill, Jaspreet, "Former DIU Director Cleared of Ethics Investigations," 
Breaking Defense, September 14, 2022. 

Gould, Joe, "Pentagon Budget Will Shake Up 'Legacy' Systems. 

Lawmakers Are Shaking Back," *Defense News*, May 27, 2021.

Gould, Joe, "Hyten Explains New Acquisition Directives to Industry," 
Defense News, July 26, 2021.

Greenwalt, Bill, "DIU's Director Tried to Overcome a Calcified Defense Innovation System. It Beat Him. Now What?" *Breaking Defense*, August 29, 2022.

Greenwalt, William C., and Dan Patt, Competing in Time: Ensuring Capability Advantage and Mission Success Through Adaptable Resource Allocation, Hudson Institute, February 2021. Gunzinger, Mark, Affordable Mass: The Need for a Cost-Effective PGM Mix for Great Power Competition, Mitchell Institute Policy Paper, Vol. 31, November 2021.

Hamilton, Thomas, and David A. Ochmanek, Operating Low-Cost, Reusable Unmanned Aerial Vehicles in Contested Environments: Preliminary Evaluation of Operational Concepts, RAND Corporation, RR-4407-AF, 2020. As of August 9, 2023: https://www.rand.org/pubs/research_reports/RR4407.html Hammes, T. X., "America Is Well Within Range of a Big Surprise, So Why Can't It See?" *War on the Rocks*, March 12, 2018. 

Heckmann, Laura, "Sea-Air-Space News: Joint Warfighting Concept 3.0 
'Definitely Coming,' Official Says," *National Defense*, April 5, 2023.

Heginbotham, Eric, and Jacob L. Heim, "Deterring Without Dominance: Discouraging Chinese Adventurism Under Austerity," 
Washington Quarterly, Vol. 38, No. 1, Spring 2015.

Heginbotham, Eric, Michael Nixon, Forrest E. Morgan, Jacob L. Heim, Jeff Hagen, Sheng Tao Li, Jeffrey Engstrom, Martin C. Libicki, Paul DeLuca, David A. Shlapak, David R. Frelinger, Burgess Laird, Kyle Brady, and Lyle J. Morris, The U.S.-China Military Scorecard: Forces, Geography, and the Evolving Balance of Power, 1996–2017, RAND 
Corporation, RR-392-AF, 2015. As of August 9, 2023: https://www.rand.org/pubs/research_reports/RR392.html Hicks, Kathleen, "The Urgency to Innovate," speech given at the NDIA Emerging Technologies for Defense Conference, Washington, D.C., August 28, 2023.

Hoehn, John R., Joint All-Domain Command and Control: Background and Issues for Congress, Congressional Research Service, March 18, 
2021. 

House of Commons Defence Committee, It Is Broke—and It's Time to Fix It: The UK's Defence Procurement System, Ninth Report of Session 
2022–2023, July 11, 2023. Jackson, Kimberly, and David H. Berger, "Readiness Redefined: Now What?" *War on the Rocks*, June 12, 2023.

Jane's Country Intelligence, "The United States, Air Force," webpage, updated August 14, 2023. Janeway, Diane, and Guy Eastman, "U.S. DoD FY23 President's Budget Request (PBR): 'First Look,'" Janes Intelligence Briefing, March 30, 2022. Jensen, Benjamin, and John Paschkewitz, "Mosaic Warfare: Small and Scalable Are Beautiful," *War on the Rocks*, December 23, 2019. Jones, Seth G., Empty Bins in a Wartime Environment: The Challenge to the U.S. Defense Industrial Base, Center for Strategic and International Studies, January 23, 2023. Jordan, Nicholas, and Jennifer Mapp, "In the Dark: How the Pentagon's Limited Supplier Visibility Risks U.S. National Security," War on the Rocks, June 14, 2023.

Kahn, Lauren, "Scaling the Future: How Replicator Aims to Fast-Track U.S. Defense Capabilities," *War on the Rocks*, September 20, 2023.

Kaplan, Fred, "The National Security Disaster You Probably Missed Last Week," *Slate*, July 19, 2021. 

Katz, Justin, "Navy Admiral Touts Sealift Recap, Stops Short of Saying 
2025 Problem Is Solved," *Breaking Defense*, December 8, 2022.

Katz, Justin, and Valerie Insinna, "A 'Bloody Mess' with 'Terrible Loss of Life': How a China-U.S. Conflict over Taiwan Could Play Out," Breaking Defense, August 11, 2022. Kelly, Terrence K., David C. Gompert, and Duncan Long, Smarter Power, Stronger Partners, Volume I: Exploiting U.S. Advantages to Prevent Aggression, RAND Corporation, RR-1359-A, 2016. As of August 9, 2023: https://www.rand.org/pubs/research_reports/RR1359.html Kendall, Frank, John W. Raymond, and Charles Q. Brown, Jr., Department of the Air Force Posture Statement: Fiscal Year 2022, U.S. Air Force, 2022.

Kissinger, Henry A., *White House Years*, Little, Brown, and Company, 
1979. Kofman, Michael, and Richard Connolly, "Why Russian Military Expenditure Is Much Higher Than Commonly Understood (as Is China's)," *War on the Rocks*, December 16, 2019. 

Lipton, Eric, "Faced with Evolving Threats, U.S. Navy Struggles to Change," *New York Times*, September 4, 2023.

Lofgren, Eric, Whitney M. McNamara, and Peter Modigliani, Commission on Defense Innovation Adoption: Interim Report, Atlantic Council, April 2023. Losey, Stephen, "Future NGAD Fighter Jets Could Cost 'Hundreds of Millions' Apiece," *Defense News*, April 28, 2022.

Lubold, Gordon, "Pentagon Trying to Get Weapons to Allies Faster," 
Wall Street Journal, June 13, 2023.

MacGregor, Matt, Pete Modigliani, and Greg Grant, "Pentagon Needs a Six-Pillar Foundation," *The Hill*, March 7, 2022.

Mahnken, Thomas G., Evan B. Montgomery, and Tyler Hacker, Innovating for Great Power Competition: An Examination of Service and Joint Innovation Efforts, Center for Strategic and Budgetary Assessments, 2023. Mahnken, Thomas G., Travis Sharp, Christopher Bassler, and Bryan W. 

Durkee, Implementing Deterrence by Detection: Innovative Capabilities, Processes, and Organizations for Situational Awareness in the Indo- Pacific Region, Center for Strategic and Budgetary Assessments, 2021.

Maucione, Scott, "DoD Budget Largely Flat, Cuts Legacy Systems for Modernization," Federal News Network, May 28, 2021. Mazarr, Michael J., "Rethinking Restraint: Why It Fails in Practice," 
Washington Quarterly, Vol. 43, No. 2, 2020. Mazarr, Michael J., "Toward a New Theory of Power Projection," War on the Rocks, April 15, 2020.

Mazarr, Michael J., "Time for a New Approach to Defense Strategy," 
War on the Rocks, July 29, 2021. Mazarr, Michael J., and Ashley L. Rhoades, Testing the Value of the Postwar International Order, RAND Corporation, RR-2226-OSD, 2018. 

As of August 9, 2023: https://www.rand.org/pubs/research_reports/RR2226.html Mearsheimer, John J., and Stephen M. Walt, "The Case for Offshore Balancing: A Superior U.S. Grand Strategy," *Foreign Affairs*, July–
August 2016. 

Mehta, Aaron, "Exclusive: Outgoing DIU Head 'Frustrated . . . We're Not Supported More' by Big Pentagon," *Defense News*, May 10, 2022. 

Milley, Mark A., "Strategic Inflection Point: The Most Historically Significant and Fundamental Change in the Character of War Is Happening Now—While the Future Is Clouded in Mist and Uncertainty," *Joint Force Quarterly*, Vol. 110, July 2023. Mitre, Jim, "A Eulogy for the Two-War Construct," Washington Quarterly, Vol. 41, No. 4, 2018.

Modigliani, Pete, "The Program Side of the Valley of Death," MITRE blog, March 17, 2022. As of August 9, 2023: https://aida.mitre.org/blog/2022/03/17/program-valley-of-death/ Modigliani, Pete, "DoD's Lack of Agility Is a National Security Risk," MITRE blog, November 15, 2022. As of August 9, 2023: https://aida.mitre.org/blog/2022/11/15/dods-lack-of-agility-is-anational-security-risk/ Monaghan, Sean, and Deborah Cheverton, "What Allies Want: Delivering the U.S. National Defense Strategy's Ambition on Allies and Partners," *War on the Rocks*, July 24, 2023.

Mueller, John E., "The Search for the 'Breaking Point' in Vietnam: The Statistics of a Deadly Quarrel," *International Studies Quarterly*, Vol. 24, No. 4, December 1980. Newdick, Thomas, "Production of In-Demand Javelin Missiles Set to Almost Double," *The War Zone*, May 9, 2022. "Nominal Spending Figures Understate China's Military Might," The Economist, May 1, 2021. Ochmanek, David A., Determining the Military Capabilities Most Needed to Counter China and Russia: A Strategy-Driven Approach, RAND Corporation, PE-A1984-1, June 2022. As of August 9, 2023: https://www.rand.org/pubs/perspectives/PEA1984-1.html Ochmanek, David A., Anna Dowd, Stephen J. Flanagan, Andrew R. Hoehn, Jeffrey W. Hornung, Michael J. Lostumbo, and Michael J. 

Mazarr, Inflection Point: How to Reverse the Erosion of U.S. and Allied Military Power and Influence, RAND Corporation, RR-A2555-1, 2023. 

As of August 9, 2023: https://www.rand.org/pubs/research_reports/RRA2555-1.html Ochmanek, David A., and Lowell H. Schwartz, The Challenge of Nuclear-Armed Regional Adversaries, RAND Corporation, MG-671-AF, 
2008. As of August 9, 2023: https://www.rand.org/pubs/monographs/MG671.html Office of the Under Secretary of Defense (Comptroller), Chief Financial Officer, Program Acquisition Cost by Weapon System: United States Department of Defense Fiscal Year 2022 Budget Request, May 2021. Pettyjohn, Stacie, and Hannah Dennis, Precision and Posture: Defense Spending Trends and the FY23 Budget Request, Center for a New American Security, November 17, 2022.

Pincus, Walter, "What We Know About Future Maritime Wars," Cipher Brief, April 26, 2022. Pollack, Kenneth M., Armies of Sand: The Past, Present, and Future of Arab Military Effectiveness, Oxford University Press, 2019. Posen, Barry R., *Restraint: A New Foundation for U.S. Grand Strategy*, Cornell University Press, 2014.

Roberts, Brad, "On the Need for a Blue Theory of Victory," War on the Rocks, September 17, 2020. 

Robertson, Peter, "Debating Defence Budgets: Why Military Purchasing Power Parity Matters," VoxEU, October 9, 2021. As of August 9, 2023: https://cepr.org/voxeu/columns/debating-defence-budgets-whymilitary-purchasing-power-parity-matters Rosnick, David, "A History of Poverty Worldwide: How Poverty Reduction Accelerated in Recent Decades—Effectiveness of Growth," Center for Economic and Policy Research, May 21, 2019. Rumsfeld, Donald H., "DoD Acquisition and Logistics Excellence Week Kickoff—Bureaucracy to the Battlefield," speech delivered at the Pentagon, September 10, 2001. 

Scarborough, Rowen, Rumsfeld's War: The Untold Story of America's Anti-Terrorist Commander, Regnery Publishing, 2004. Scharre, Paul, Four Battlegrounds: Power in the Age of Artificial Intelligence, W. W. Norton, 2023. 

Seck, Hope Hodge, "Robots, Marines and the Ultimate Battle with Bureaucracy," *Politico*, June 23, 2022. Sharp, Travis, Slow and Steady: Analysis of the 2022 Defense Budget Request, Center for Strategic and Budgetary Assessments, July 30, 2021. Sharp, Travis, and Tyler Hacker, Big Centralization, Small Bets, and the Warfighting Implications of Middling Progress: Three Concerns about JADC2's Trajectory, Center for Strategic and Budgetary Assessments, April 2023. Spirtas, Michael, "Toward One Understanding of Multiple Domains," 
RAND Blog, May 2, 2018. As of August 9, 2023: 
https://www.rand.org/blog/2018/05/toward-one-understanding-ofmultiple-domains.html Tegler, Jan, "Air Force Under Pressure as Airlift Capacity Falls," 
National Defense Magazine, June 6, 2022. U.S. Army Training and Doctrine Command Pamphlet 525-92, The Operational Environment and the Changing Character of Future Warfare, October 2019. U.S. Department of Defense, *Quadrennial Defense Review Report*, September 30, 2001.

U.S. Department of Defense, Sustaining U.S. Global Leadership: Priorities for 21st Century Defense, January 2012. U.S. Department of Defense, Summary of the 2018 National Defense Strategy of the United States of America: Sharpening the American Military's Competitive Edge, 2018. U.S. Department of Defense, 2022 National Defense Strategy of the United States of America, October 27, 2022. 

U.S. Department of Defense, "Deputy Secretary of Defense Kathleen Hicks Keynote Address at the Ash Carter Exchange on Innovation and National Security," May 9, 2023. 

U.S. Government Accountability Office, Weapon Systems Annual Assessment: Challenges to Fielding Capabilities Faster Persist, June 2022. 

U.S. Government Accountability Office, High-Risk Series: Efforts Made to Achieve Progress Need to Be Maintained and Expanded to Fully Address All Areas, April 2023. 

U.S. Government Accountability Office, Weapon Systems Annual Assessment: Programs Are Not Consistently Implementing Practices That Can Help Accelerate Acquisitions, June 2023.

U.S. House of Representatives, Committee on Armed Services, Future of Defense Task Force Report 2020, September 23, 2020.

Vershinin, Alex, "The Return of Industrial Warfare," Royal United Services Institute for Defence and Security Studies, June 17, 2022. Vincent, Brandi, "Milley's Team Readying Analysis for Establishing New 'Joint Futures' Organization for DOD," *DefenseScoop*, May 24, 
2023. 

Walt, Stephen M., The Hell of Good Intentions: America's Foreign Policy Elite and the Decline of U.S. Primacy, Farrar, Straus, and Giroux, 2018.

West, Owen, "Are the Marines Inventing the Edsel or the Mustang?" 
War on the Rocks, May 27, 2022.

Wester, Tom, and Joe Mancini, "Lessons Learned or Lessons Observed: 
The U.S. Navy's Relationship with Mine Warfare," *War on the Rocks*, August 4, 2023.

Wicker, Elena, "Full-Spectrum Integrated Lethality? On the Promise and Peril of Buzzwords," *War on the Rocks*, May 17, 2023.

Williams, Lauren C., "The Pentagon's Innovation Shop Wants More Influence in 2023," *Defense One*, January 25, 2023.

Woodward, Bob, *State of Denial: Bush at War, Part III*, Simon and Schuster, 2006. Wong, Jonathan P., Michael J. Mazarr, Nathan Beauchamp-Mustafaga, Michael Bohnert, Scott Boston, Christian Curriden, Derek Eaton, Gregory Weider Fauerbach, Joslyn Fleming, Katheryn Giglio, Dahlia Anne Goldfeld, Derek Grossman, Timothy R. Heath, John C. Jackson, Michael E. Linick, Eric Robinson, Lisa Saum-Manning, Ryan A. Schwankhart, Michael Schwille, Stephan B. Seabrook, Alice Shih, and Jonathan Welch, New Directions for Projecting Land Power in the Indo-Pacific: Contexts, Constraints, and Concepts, RAND Corporation, RR-A1672-1, 2022. As of August 9, 2023: https://www.rand.org/pubs/research_reports/RRA1672-1.html Work, Robert, "The Third U.S. Offset Strategy and Its Implications for Partners and Allies," transcription of speech, Washington, D.C., January 28, 2015. As of August 9, 2023: https://www.defense.gov/News/Speeches/Speech/Article/606641/thethird-us-offset-strategy-and-its-implications-for-partners-and-allies/ Work, Robert, "Remarks by Defense Deputy Secretary Robert Work at the CNAS Inaugural National Security Forum," Center for a New American Security, December 14, 2015. As of August 9, 2023: https://www.cnas.org/publications/transcript/remarks-by-defensedeputy-secretary-robert-work-at-the-cnas-inaugural-national-securityforum Work, Robert, "Remarks by Deputy Secretary Work on Third Offset Strategy," transcription of speech given in Brussels, Belgium, April 28, 2016. As of August 9, 2023: https://www.defense.gov/News/Speeches/Speech/Article/753482/ remarks-by-deputy-secretary-work-on-third-offset-strategy/ Work, Robert, "Marine Force Design: Changes Overdue Despite Critics' 
Claims," *Texas National Security Review*, Vol. 6, No. 3, Summer 2023.

World Bank, "GDP (Current US$)," webpage, undated. As of August 9, 2023: https://data.worldbank.org/indicator/NY.GDP.MKTP.CD 

## About This Perspective

This Perspective examines the gap between U.S. defense aspirations and the reality of current policy. In this Perspective, the author surveys grand strategy principles that call for a U.S. ability to project power to help deter major war in key regions, define the U.S. defense strategic approach of the post–Cold War era and its key assumptions, and explain the reasons why these assumptions are no longer valid. The author suggests six leading barriers to reform—reasons why the U.S. Department of Defense's responses have been unable to generate the degree of change required. The author concludes by describing the principles of a revised strategy.

RAND National Security Research Division This Perspective was sponsored by the Smith Richardson Foundation and conducted within the International Security and Defense Policy Program of the RAND National Security Research Division (NSRD). NSRD conducts research and analysis for the Office of the Secretary of Defense, the U.S. Intelligence Community, the U.S. State Department, allied foreign governments, and foundations. For more information on the RAND International Security and Defense Policy Program, see www.rand.org/nsrd/isdp or contact the director (contact information is provided on the webpage).

The RAND Corporation is a research organization that develops solutions to public policy challenges to help make communities throughout the world safer and more secure, healthier and more prosperous. RAND is nonprofit, nonpartisan, and committed to the public interest. 

Research Integrity Our mission to help improve policy and decisionmaking through research and analysis is enabled through our core values of quality and objectivity and our unwavering commitment to the highest level of integrity and ethical behavior. To help ensure our research and analysis are rigorous, objective, and nonpartisan, we subject our research publications to a robust and exacting quality-assurance process; avoid both the appearance and reality of financial and other conflicts of interest through staff training, project screening, and a policy of mandatory disclosure; and pursue transparency in our research engagements through our commitment to the open publication of our research findings and recommendations, disclosure of the source of funding of published research, and policies to ensure intellectual independence. For more information, visit www.rand.org/about/research-integrity. RAND's publications do not necessarily reflect the opinions of its research clients and sponsors. 

 is a registered trademark.

C O R P O R A T I O N
Limited Print and Electronic Distribution Rights This publication and trademark(s) contained herein are protected by law. This representation of RAND intellectual property is provided for noncommercial use only. Unauthorized posting of this publication online is prohibited; linking directly to its webpage on rand.org is encouraged. 

Permission is required from RAND to reproduce, or reuse in another form, any of its research products for commercial purposes. For information on reprint and reuse permissions, please visit www.rand.org/pubs/permissions. For more information on this publication, visit www.rand.org/t/PEA2555-1. © 2023 RAND Corporation www.rand.org

## About The Author

Michael J. Mazarr is a senior political scientist at the RAND Corporation. His primary interests are U.S. defense policy and force structure, disinformation and information manipulation, East Asian security, nuclear weapons and deterrence, and judgment and decisionmaking under uncertainty. Mazarr holds a Ph.D. in public policy.

## Acknowledgments

This Perspective represents the views of the author and is partly 
informed by a wide variety of RAND work on the future of the interna-
tional order and strategic competition. The author would like to thank 
Andrew Hoehn and David Ochmanek for their leadership of the larger 
project and support for this Perspective, including detailed comments 
that immeasurably improved this Perspective. He would like to thank 
Jim Mitre of RAND and Zack Cooper of the American Enterprise Insti-
tute for helpful peer review comments, and Jim in particular for an 
extended discussion that helped shape the argument. The author is 
especially grateful to Marin Strmecki and the Smith Richardson Founda-
tion for inspiring and sponsoring this work. The foundation has a well-
earned reputation for supporting research devoted to improving public 
policy and strengthening the vitality of U.S. government institutions, 
and the author considers it an honor to have been given the chance to 
contribute to the foundation's mission. 

