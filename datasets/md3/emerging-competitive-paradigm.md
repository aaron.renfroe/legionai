
## Michael J. Mazarr, Alexis Dale-Huang, Matthew Sargent The Emerging Competitive Paradigm A Contest Of Effective Governance M

any factors determine the relative strength of nations, including great powers engaged in long-term rivalries. In the larger project of which this paper is one part, we examine the societal characteristics that produce competitive advantage—the qualities of nations that improve a nation's relative standing.1 We identified seven major characteristics that appear to be associated with success in almost any era or context: national ambition and willpower, unified national identity, shared opportunity, an active state, effective institutions, a learning and adapting mindset, and diversity and pluralism.

In addition to these seven characteristics, the study's first phase also highlighted an intriguing, related theme. Many of our cases suggested that competitive advantage also emerges from meeting the demands of the prevailing competitive paradigm—the essential character of any era that sets the context for national success.2 There were particular times in history when a confluence of economic, social, and military developments created an opportunity for competitive advantage by nations with strong fitness to the demands of an era also gain domestic legitimation rewards, both material and ideological. As much as some great powers benefit from their strategic context, others are effectively defeated by it. An important lesson of the industrial era was, as our firstphase report concluded, that "not all societies could effectively embrace this recipe," and, as a result, 

## Abbreviations
| CCP                               | Chinese Communist Party               |
|-----------------------------------|---------------------------------------|
| COVID-19  |                       coronavirus disease 2019                |
| DI                                | digital infrastructure                |
| GDP                               | gross domestic product                |
| IMF                               | International Monetary Fund           |
| OECD                              | Organization for Economic Cooperation and Development |
| R&D                               | research and development              |

meeting the demands of the era, thus separating successful from failed nations. The first-phase study put it this way:
A nation whose politics, socioeconomic patterns, and established history and culture are at odds with key elements of the model will be blocked from easily adopting its core components and benefiting from its competitive effects. Such a society then fall [sic] into a long, slow decline, sometimes punctuated by defeat at the hands of more-competitive nations and then anger and scapegoating.5 
In the cases of Rome, Britain, and the United States, the dominant powers were countries whose package of societal advantages matched the demands of the era, whether they be demographic muscle and national will, industrial output and commercial connections, or grassroots initiative and technological advance. Partly as a product of these factors but also as the result of strategic choice, these nations then placed themselves at the hub of regional or international networks of economic, intellectual, and cultural exchange. The result was that they came to dominate critical components of the international orders of their day.3
This is the story, in part, of unsuccessful great powers—those that lost or fell irretrievably behind in long-term rivalries. They simply could not keep up with the times.6 The Ottoman Empire, Imperial Spain, and the Soviet Union are leading modern examples of this pattern.

Britain's trajectory since the 18th century offers one of the most telling examples of the power of the competitive paradigm to shape national fates. Britain gained a staggering degree of competitive advantage by becoming the dominant economic and trade power in the world. This was not replicated in all cases of alignment to an era's demands, but it demonstrates how the dynamic can work. The initial phases of the Industrial Revolution (sometimes termed the *first Industrial Revolution*) matched Britain's potential extremely well. As Eric Hobsbawm argued in his classic study of British economic history, Nations with greater alignment to the competitive paradigm of the period gain direct and tangible advantage in terms of economic output, technological sophistication, and military power. But they also benefit because other countries see them as one of the exemplars of the age, which produces an opportunity to become a gravitational center for the era's networks of exchange.4 And The Industrial Revolution marks the most fundamental transformation of human life in the history of the world recorded in written documents. For a brief period it coincided with the history of a single country, Great Britain. An entire world economy was thus built on, or rather around, Britain, and this country therefore temporarily rose to a position of global influence and power unparalleled by any state of its relative size before or since, and unlikely to be paralleled by any state in the foreseeable future.7
Paul Kennedy has documented the results. Britain, which had reflected a modest 1.9 percent of world economic output in 1750, by 1880 represented almost onequarter (22.9 percent) of world gross domestic product (GDP). On a scale of social industrialization in which 1900 
Britain ranked as 100, 1880 Britain stood at 87—compared with the United States at 38, France at 28, Germany at 25, and Japan at a meager 9. Britain was benefiting disproportionately from the "staggering increases in productivity emanating from the Industrial Revolution."8 Yet as the first more grassroots and entrepreneurial phase of the Industrial Revolution gave way to the comprehensive industrialization and mass production of the second phase, Britain proved unable to keep up. Its global position, first economically and then geopolitically and militarily, suffered a long, gradual decay.

Our review of great powers and competitive paradigms suggested a troubling lesson: Failure to meet the test of new competitive paradigms appears to be the norm, rather than the exception. Imperial Spain's slow adjustment to an emerging age of scientific progress and market-based economic power; the inability of the Austro-Hungarian or Ottoman empires to keep up with the modernizing processes in other great powers; Britain's lagging competitiveness in the second industrial era; the Soviet Union's incapacity to adapt to the demands of the later, informationbased industrial age; and other cases of major powers failing to master transitions are so common as to suggest that a failure to adapt to an emerging era is the default pattern. 

The United States may confront a historical tendency with worrying implications for the future of its standing.

This analysis highlights an important related finding: The benefits and costs of a nation's degree of fitness to the demands of an era reach well beyond economic and technological domains. One critical measure of fitness to the strategic environment is a nation's standing in the era's basic economic model, its approach to value creation. But another measure deals with social and political structures and norms. Nations have fallen out of alignment with a competitive paradigm because their domestic institutions of governance and social interaction—and their social contract broadly defined—become anachronistic. They fall behind not merely because they cannot keep up with technology or industrial production but also because their society and politics cannot organize themselves to produce the necessary dynamism and adaptation.

This might be the most critical point of comparison between the United States and China today: whether one of these rivals is developing social and political institutions and processes with greater fitness to the emerging competitive paradigm. The Cold War, argues one new source, was "fundamentally a contest over which system of government could best harness industrial modernity to improve the lives of its people."9 This is a useful shorthand for the essence of the new competition. It is a contest over which approach to governance and societal problem-solving can best harness the postindustrial context to improve the lives and attend to the needs of its people.

## Objectives And Approach Of This Analysis

To assess these questions in this paper, we first define the character of a competitive paradigm and look to history for examples and their lessons. Also using historical sources as a guide, we propose a two-part framework for understanding the essential requirements for any nation to meet the demands of a paradigm. We then turn to the future, defining the specific characteristics and demands of the emerging 21st century paradigm. We conclude by summarizing the major implications of the analysis, both for diagnosing the current U.S.-China rivalry and for shaping U.S. strategic choices.

An important choice was time frame. Previous competitive paradigms have lasted between several decades and centuries, and so, at a minimum, any such analysis must look ahead into the medium-term future. From the standpoint of the main implication of these findings, regarding the U.S.-China rivalry, the period of the next 20 to 30 years is often held to be critical in determining the outcome of the contest. In broad terms, then, we sought to assess the competitive paradigm likely to set the context for national success out to roughly 2050 but with a broader general timeline of 50 years in mind.10
level of such individual judgment is unavoidable when trying to anticipate the interactions of many complex variables. The rigor in the approach comes from the range and depth of sources consulted, the frameworks developed to organize thinking about competitive paradigms, and the discipline and transparency with which we translated trend and historical data into future expectations.

Any effort to identify meaningful eras in history also confronts the challenge of distinguishing evolution from more-fundamental break points. No period represents a wholesale abandonment of the habits and patterns that went before. Any competitive paradigm will reflect a mix of continuing and fresh characteristics.

Indeed, our analysis suggests that many of the essential characteristics of the later industrial-era competitive paradigm remain relevant to national competitive advantage. Those factors include key realities of the strategic context, such as the existence of nuclear weapons and a complex world trade and financial system; the centrality of worldclass manufacturing and competitive position in critical technology areas; the role of information technologies in underwriting growth and productivity; the importance of synergistic networks of economic and technological advantage (i.e., public research and development [R&D], education and research institutes, and private sector deployment of technologies);11 and the critical role of strong national financial institutions and sound public sector financial management. What it takes to succeed in the emerging competitive paradigm will continue to reflect many requirements for success during the industrial era.

Yet these characteristics will also be different in decisive ways that will help determine the fates of nations. This analysis contends that the late industrial era—a period It is important to qualify the nature of the claims we make with this research. We gathered data from many historical and current trend-assessment sources, but different observers could draw divergent lessons from those data. 

This report offers a research-based, subjective judgment—a tions (written or unwritten) that does two things: (1) it establishes or defines boundaries; and (2) it tells you how to behave inside the boundaries in order to be successful."14
that might be thought of as the scientific-bureaucraticindustrial age—has reached an inflection point involving a series of intersecting crises in its fundamental assumptions and institutions. Combined with accelerating technological changes, these trends are producing an era in which transformations in governance have profound implications for national competitive advantage. We make this argument and spell out these implications in the following sections.

## Defining A Competitive Paradigm

But Barker goes beyond just ideas. "A paradigm . . . 

tells you that there is a game, what the game is, and how to play it successfully." A paradigm shift represents "a change to a new game, a new set of rules."15 This is precisely the change in epochs we have in mind with this analysis— periods when the basic game that great powers are playing, for national dynamism and power, changes, and a new set of rules comes to govern their efforts. He goes on to argue that old paradigms collapse when they can no longer solve key social problems. "Sooner or later," Barker contends, "every paradigm begins to develop a very special set of problems that everyone in the field wants to be able to solve and no one has a clue as to how to do it."16 The shift in a paradigm involves multiple intersecting changes that collectively undermine the validity of existing assumptions.17
Paradigms can prevail in specific domain areas—the current paradigm in chemistry or psychology or housing 

A typical definition of a 
paradigm focuses on a set 
of beliefs or mental models 
of how the world works 
that helps to explain it.

The concept of a competitive paradigm simply asserts that at any historical moment, the economic, technological, military, and other characteristics of the era or time create a context for national competitive success. In the late– and post–Cold War era, for example, with the rise of precision weapons and networked information architectures, countries that could not master the precision strike revolution operated at a competitive disadvantage. In an earlier period, countries that fell behind in industrialization were fatally handicapped, both in economic and military competitions. The various elements of industrialized national power constituted the competitive paradigm of that period.

A typical definition of a *paradigm* focuses on a set of beliefs or mental models of how the world works that helps to explain it.12 Adam Smith defined a paradigm as "a shared set of assumptions. The paradigm is the way we perceive the world; water to the fish. The paradigm explains the world to us and helps us to predict its behavior." Paradigms limit the vision and imagination of actors: "When we are in the middle of the paradigm, it is hard to imagine any other paradigm."13 The business theorist Joel Arthur Barker defined a paradigm as "a set of rules and regulathe essential components of a competitive paradigm from the larger kaleidoscope of the era's characteristics, in part because many shifting elements of that kaleidoscope will be somehow related to the core paradigm.

In addition, a given competitive paradigm will not dictate a common response from all political actors or a singular societal model. Recipes for success evolve over time even under the influence of a single competitive paradigm. As one survey of developmental models concludes, for example, "quite different constellations of characteristics appear to have been compatible with successful developconstruction, for example. But in this analysis, we have something more encompassing in mind: the global paradigm for national competitive success. Historically, such competitive paradigms tend to have six primary elements. We outline them in Table 1.

A competitive paradigm is not a catalog of every economic, social, political, technological, institutional, and military fact about an age. Instead, the concept of a paradigm refers to a subset of those realities, a few essential characteristics of an age that reflect the crux of national competitive advantage. It can be difficult to distinguish 

| Element                                                                                              | Definitions, Examples, Objectives                                                                            |
|------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| Economic                                                                                             | Sources of economic growth—human capital versus industrial production; role of demography; role of           |
| finance and capital; essential requirements for growth                                               |                                                                                                              |
| Objective: Growth in general economy, per capita wealth, productivity; over time, shared and         |                                                                                                              |
| equitable growth of varying degrees                                                                  |                                                                                                              |
| Technological                                                                                        | Leading technological areas of the time; scientific and technological requirements for economic and military |
| power                                                                                                |                                                                                                              |
| Objective: Reaching global standards in selected areas or more generally                             |                                                                                                              |
| Military                                                                                             | Nature of military power at a given time and requirements for success; industrial-era militaries versus      |
| preindustrial; reconnaissance strike complexes versus prior forms of military organization and power |                                                                                                              |
| Objective: Avoiding decisive ineffectiveness or weakness as baseline; achieving local or global      |                                                                                                              |
| military predominance as extreme goal                                                                |                                                                                                              |
| Political                                                                                            | Political models associated with competitive advantage—nation-state versus feudal patterns; regime type if   |
| associated with success                                                                              |                                                                                                              |
| Objective: Essential legitimacy of the political system; political stability; coherence; effective   |                                                                                                              |
| addressing of policy issues                                                                          |                                                                                                              |
| Institutional                                                                                        | Types and character of institutions that produce effective competitive outcomes; financial institutions as a |
| basis for national power; effective industrial bureaucracies                                         |                                                                                                              |
| Objective: Capable, legitimate institutions which create the basis for success in other goals        |                                                                                                              |
| Social                                                                                               | Social patterns that provide competitive advantage; existence of a middle class and its effect on economic   |
| outcomes; degree of nationalism and commitment to national goals                                     |                                                                                                              |
| Objective: Social stability and coherence                                                            |                                                                                                              |

ment and growth."18 Even once a state has correctly diagnosed the existing paradigm, it still faces certain strategic choices about the mix of means and ways it will employ to flourish in that environment. A competitive paradigm is not equivalent to the strategies and policies of the leading states—those respond to, and are sometimes a function of, the elements of the strategic context and define the era. 

But the range of choice is not unlimited. The lesson of history animating this analysis is that political actors must align themselves to the characteristics of the prevailing paradigm to be competitive. A preindustrial state could not match the overall national power of an industrial state in the 20th century. Even a state that had dominated the early Industrial Revolution in historically unprecedented ways— Britain—was not guaranteed to thrive as later industrialization changed the game for great powers. The critical point for analysis is to identify the baseline factors that any state must meet to succeed under a given paradigm and, thus, to also discover the areas of optionality.

Transitions between paradigms can be difficult to identify and assess. As we have noted, these eras tend to overlap rather than offer clear dividing lines. When a transition is in progress, those involved are rarely aware of it. Nations that ended up thriving in the new paradigm often did so because of happenstance—their social characteristics, size, culture, or other qualities happened to align to the requirements of the emerging period. There are exceptions to this, especially from the two industrial eras, when countries like Russia and Japan actively recognized the risks of being left behind and sought to catch up to the exemplars of the age. But, in general, the transitions between eras can be challenging to comprehend.

The rise of a new competitive paradigm, and the pressures it places on nations, represents a version of the dynamic of challenge and response at the heart of Arnold Toynbee's theory of history. Toynbee highlighted the role of significant external challenges in spurring national development and argued that civilizations advanced most powerfully when facing challenges and effectively responding to them. Societies that faced no meaningful challenge or failed in their response eventually collapsed or faded into irrelevance. "Immunity from the challenge of encounters with civilizations is a very serious handicap," he argued, even nominating a general "social law which may be expressed in the formula, 'the greater the challenge, the greater the stimulus.'"19 In this sense, the rise of a new age or paradigm, and the demands it places on nations, can be a stimulus to greatness—if a nation has the qualities necessary to capitalize on the change rather than be victimized by it.

For the United States, the most urgent question today might be whether the emerging competitive paradigm will allow for—or even benefit—closed, centrally planned systems like China's or whether the advantage of more open, free, grassroots, and market-based systems will continue. The answer will partly be determined by material outcomes, such as the degree to which competing systems master the sources of value creation and generate growth or their relative military power. But it will also—and perhaps more fundamentally—be determined by human factors: the achievement of an effective, updated social contract that creates a sense of legitimacy, justice, and dignity in societies; underwrites national and local solidarity; and addresses issues of ontological security.

to economic advantage, specific efficiencyproducing technologies

## National Alignment To A Paradigm: Essential Components

 - Germany and Japan in the late 19th and earlyto-mid-20th centuries: industrialization leading to manufacturing and military power
 - The United States in the 20th century: manufacturing power, technological innovation, computing power and integration
 - South Korean state-led economic development, which produces world-leading technological power, manufacturing, and trade
 - Negative cases and proof from failure to meet 
requirement: later Italian Renaissance citystates, Ottoman Empire, Soviet Union
- A valid social contract appropriate in the era
 Ȥ Component elements
Having defined the concept of a competitive paradigm, we then looked to a variety of historical sources for clues about what it takes for nations to meet the demands of such a paradigm. To do this, we reviewed historical cases surveyed for the first phase of this work and conducted for this paper. We assessed candidate elements with subject-matter experts on international relations, including at a workshop held for the purpose. The result is a hypothesis, not a definitive finding—it represents one potential interpretation of the evidence, which seems to best capture the available cases. Our research suggests that all processes of aligning to a given competitive paradigm share two common elements. These are discussed in the following sections and summarized as follows:

 - Political relations: level and type of 
representation
- Leadership of the era's economic, technological, and 
human sources of value creation
 - Economic relations: economic guarantees and 
forms of public assistance
 Ȥ Component elements
 - Security relations: types of safety guaranteed 
by contract
 - Primary sources of economic growth and 
development
 Ȥ Evidence (cases supporting this argument)
 - Leading technologies and sources of technological innovation
 - Forms and sources of human capital - Natural resource and energy endowments
 Ȥ Evidence (cases supporting this argument)
 - Britain in the early industrial era: aristocracy 
with enduring legitimacy offering widening opportunity across classes and political participation, economic growth and scientific advances legitimizing the system, rising social protections embodying shared contract
 - Netherlands in the Golden Age: trade and 
financial power
 - Britain in the early industrial era: sources of 
energy, beginning of mass and factory production era, application of scientific method 
 - Germany and Japan in the late 19th and earlyto-mid-20th centuries: elements of aristocratic or oligarchic social patterns but with increasing social mobility and opportunity, economic 
growth, nationalistic credibility from rising relative power
 - The United States in the 20th century: significant social mobility, shared opportunity, widening civil rights granting legitimacy to national narrative; economic growth, technological progress; global relative power legitimize system; rising social protections embodying shared contract
 - South Korean system grounded in cultural 
values and post–Korean War nationalist drive for success: sufficient degrees of shared opportunity and social mobility, emergence of democracy legitimizing overall social contract
with the core value creation model of the industrial era was a centerpiece of the national advantage enjoyed by Great Britain and the United States and was also characteristic of other major powers who became industrial juggernauts, including Germany and Japan. The U.S., Japanese, Korean, Taiwanese, and, in many cases, European alignment with the 1990s-era information age value creation model provided significant competitive advantage. In earlier periods, a combination of financial sophistication (an economy benefiting strongly from trade and, in some cases, from the acquisitive value of colonies) and domestic social contexts with strong scientific and intellectual qualities helped the Renaissance Italian city-states and such countries as the Netherlands align themselves to their era's value creation models.

## Leadership Of The Era'S Economic, Technological, And Human Sources Of Value Creation

This factor embodies a nation's ability to thrive in an era's basic avenues to national economic development and technological sophistication. The most well-known example of such a dynamic is the Industrial Revolution, which brought into being a new standard of value creation against which all great powers were judged. The late industrial or information age imposed new and even more challenging demands; the Soviet Union could not meet them, and the transition accelerated its demise.

Whatever the source of their ability to compete in general terms, to succeed, nations must meet the demands of this most essential economic-technological component of any paradigm. Our first-phase analysis identified numerous examples of this basic relationship. Alignment 

The most well-known 
example of such a 
dynamic is the Industrial 
Revolution, which brought 
into being a new standard 
of value creation against 
which all great powers 
were judged.

## An Effective Social Contract Appropriate To The Times

- It underwrites the attractiveness and appeal of a 
nation abroad and helps affirm the effectiveness of its basic societal model.21
The other essential component of any competitive paradigm, certainly in the modern era, is the character of a nation's *social contract*—the implicit agreement among members of a society governing mutual behavior and the balance of rights and benefits, especially in terms of social collaboration for common good.20
The strength of a social contract can be measured in several ways. It can be a function of the degree to which norms of interpersonal contact are understood and respected, related to the degree of interpersonal trust in a society. Strong institutions, especially nongovernmental ones underwriting social ties, contribute to a social contract, as does an effective and equitable rule of law. Perhaps most of all, a robust social contract relies on the objective reality and perception of the strength of the essential bargains involved—the rights and benefits people acquire and the common good that is being promoted. A widespread sense of victimization, unfairness, and concentration of wealth and power are all inimical to a resilient social contract.

An effective and era-appropriate social contract lays the foundation for competitive advantage in the following ways:
Earlier research in this project supports this claim in two major ways. First, many of our case studies turned up evidence for the causal relationship between a strong social contract and sustained competitive success. The nature of the social contract on offer differs radically over time, but there are clear patterns of political, economic, and security elements of effective contracts at work in the Roman Republic and early empire, Italian Renaissance city-states at their peak, the Netherlands during its Golden Age, mid to late Victorian Britain, and the United States from the early 20th century through at least the 1970s. In each of these cases, we found identifiable links between the effects of those social contracts and national success.

Project research also supports the connection between an effective social contract and sustained competitive success in narrower ways backed by empirical research. Providing a basic sense of economic opportunity and security appropriate to the era is one part of this concept—research confirms the link between social mobility, economic equality, and perceived opportunity and economic growth. Sociopolitical instability—which can be a product of a failed social contract—negatively affects economic growth and can undermine a nation's pace of innovation and its capacity to organize a coherent competitive strategy.22

- It is essential for a basic legitimacy of governing 
institutions. 
- It is a leading foundation of social resilience against 
internal and external threats, in part by promoting the political cohesion of the country. 
A strong social contract and governing legitimacy implies, in turn, a related social quality associated with effective alignment to a competitive paradigm: solidarity. In its discussion of both a unified national identity and national ambition and will, our first-phase study suggests that nations that have successfully mastered the competi-

## What Success Looks Like

tive paradigm of their age did so in part through the positive effects of solidarity at national and local levels. The resulting degree of social stability, mutual commitment, and will together offers a critical competitive advantage in responding to the demands of a new paradigm. Measures of the degree of social solidarity can, therefore, provide an important clue to a nation's competitive potential.

There is also an international parallel to the domestic social contract, although it is not the central requirement and is generally only an echo of the domestic piece. Great powers typically offer some sort of thin social contract to other nations (and groups) that agree to align with them in significant ways, often through participation in institutions. A leading example is the U.S.-sponsored, postwar international order, which offered bargains in terms of trade, investment, (in some cases) security, and participation in a growing set of international institutions that set standards and trained government officials. 

Our research did not allow us to establish a specific threshold for sufficiency—how robust of a social contract a nation needs to flourish. Historically, societies have been able to muddle through with imperfect social contracts. In some cases, countries embody a social contract among critical subcomponents of their populations only, which may be enough for stability. Saudi Arabia, for example, could be said to have one social contract for its citizens and another for its broader population, including guest workers. Our research supports the connection between a robust and effective social contract and meeting the requirements of a competitive paradigm but does not provide a basis for making fine-tuned judgments about what precise degree of robustness is required in any given context.

Many historical cases give some indication of what it can look like to succeed at meeting or failing to meet the demands of a competitive paradigm. Broadly speaking, we have in mind a relatively straightforward distinction. 

Those who gain competitive advantage from a paradigm tend to accelerate their economic growth and technological development, bolster their relative military power, and experience periods of social dynamism and stability that combine to push them to the forefront of the hierarchy of world power. Britain in the first Industrial Revolution and the United States in the second are classic examples of the relative power implications of meeting the requirements of a paradigm.

Yet some national experiences represent partial and incomplete examples of the same phenomenon—cases that complicate the task of identifying clear winners and losers from new paradigms. In earlier periods, for example, pure size could count for much. In 1820, according to Angus Maddison's estimates, China and India had the world's largest GDPs by far, at least measured in purchasing power parity terms.23 Aided by land mass, geography, weather, and masses of peasant soldiers, Russia defeated Napoleon in 1812, although France—with its advanced scientific community and Napoleonic development of state capacity—was far more aligned to the scientific and first industrial revolutions. Although falling ever further behind the leaders of the scientific and industrial age, the Ottoman Empire limped on, remained an important regional player, and might have lasted much longer if not for the shock of World War I.

Then there are examples of countries with domestic societies and economies well aligned to the requirements of technological and economic progress for a country that had been a primarily agricultural nation.24 Coming out of World War II, Japan experienced what has been called "the Japanese economic miracle"; the country sustained annual growth rates over 7.5 percent from the 1950s through 1973 and continued at impressive levels into the early 1990s.25 By the 1980s, numerous U.S. commentators were warning that the United States risked being economically and technologically surpassed by a more-driven and efficient Japan.26
And then, all at once—sparked, in part, by a financial crisis—the miracle gave way to stagnation as Japan entered a 30-year period of slow growth and social disquiet. As one source notes, an era but with less relative geopolitical power because of pure issues of scale or as a result of national choice. Countries like the Netherlands and the Nordic nations are excellent modern examples, as are South Korea and Singapore. They gain competitively in many ways—including achieving high relative standing in specific industries—from their ability to meet the demands of the era. But they are too small to compete with great powers for relative geopolitical influence.

The case of Japan provides a fascinating example of the challenges of assessing whether and to what degree a country succeeded or failed to align to a competitive paradigm. In the Meiji era, Japan's government acquired lessons from abroad and catapulted itself into the industrial age. By the late 1930s, it had achieved remarkable In the early 1990s, the nation lost its status as an economic juggernaut—the model to emulate in industrial policy, management techniques, and product engineering—and found itself a beleaguered nation in its worst recession since World War II. Japan's political process now appears hopelessly stalled, its bureaucracy top-heavy and meddlesome, and its business practices entrenched and inflexible. The competitiveness debate of the 1980s has petered out as the resurgent U.S. economy leads the way into the Information Age. It is as if Japan, the eager pupil of U.S. business success, had briefly become the teacher only to be demoted after a few lectures.27
Japan's economic growth since 1990 has generally hovered between 0 and 2 percent, with an initial period of economic doldrums so stark it gained the dismal term of the "Lost Decade." Living standards in the country continued to fall through 2023.28
The case of Japan provides a fascinating example of the challenges of assessing whether and to what degree a country succeeded or failed to align to a competitive paradigm. 

## The Modern Context For U.S. Predominance: The Second Industrial Era

Competitive paradigms have shaped the rise, stagnation, decline, and collapse of major powers. The most important and recent of these was the Industrial Revolution—by far the clearest example in history of an economic, technological, social, and military change in the context for competition, which provided some nations with a competitive advantage and pushed some downward. Even before the Industrial Revolution, however, other periods in history appear to have reflected a similar phenomenon.

## Preindustrial And Early Industrial Paradigms

The causes of this reversal are numerous and partly contested: the collapse of asset price bubbles, demographic decline, government and private sector hesitancy in pursuing needed reforms and stimulus, rising levels of debt, and many others. In 1970, most observers would have counted Japan as one of the greatest competitive success stories of the second Industrial Revolution—both in terms of its rapid late 19th and early 20th century catch-up and the postwar miracle. By 2000, its fate served mainly as a cautionary tale.

In some ways, Japan proved to be one of the mostcapable nations in the world of gaining competitive advantage through industrialization. But it illustrates that continuing to benefit from a paradigm is as much about sensible policy and responsiveness to challenges and setbacks as it is about a few very broad socioeconomic trends. However, Japan cannot be viewed as a complete failure in dealing with the demands of the late industrial era: Its leading firms continue to display incredible industrial sophistication, its people enjoy long life spans and high standards of living, it has one of the region's most effective militaries, it generates significant cultural power, and it continues to enjoy strong national identity. It clearly belongs in a category of countries that are just below global geopolitical titans but still display significant degrees of success in being competitive within the parameters of the era.

There is no single or definitive list of competitive paradigms across history. The premodern and early modern eras offer several examples of such paradigms, although the definitions, periods, and characteristics of these eras are contested. They blur into one another rather than offer clear cut-off points in history. Our research was not designed to offer a comprehensive roster of previous competitive paradigms, but the research did suggest at least two examples and a third that overlaps with the second.

One premodern paradigm might be termed the societal revolution, describing an initial paradigm of simple state capacity—the ability of a nation (or empire) to generate national power through both the resources at its disposal and the degree of political coherence it can build. This was the era of the first truly great political entities capable of large-scale state endeavors, including A third paradigm—one overlapping with and not always distinct from the scientific revolution—is a period commonly termed the *first Industrial Revolution*. Beginning in the mid-18th century and running for roughly one hundred years, this period saw the first explosion of technological advances in various areas of economy and society in ways that began the hockey stick growth of productivity. Britain was the first and dominant beneficiary of this revolution for reasons that have been debated ever since. The first Industrial Revolution embodied advances in areas such as steam power, localized manufacturing, improvements in infrastructure and transportation, and better agricultural techniques.30 These changes had dramatic social implications as well, creating both intense challenges (pollution, grinding factory work, rising inequality) 
and opportunities. Britain (and, soon enough, the United States) began to gain significant competitive advantage from being exemplars of these revolutions, but the competitive hierarchy was not yet as stark as it would become in the second Industrial Revolution.

Each of these previous paradigms—and the most significant paradigm so far, the second Industrial Revolution, discussed in the following sections—reflects several characteristics or factors common across political organizations of the age. First, each of the paradigms embodies a basic social contract that provides coherence and legitimacy to the governing structures. For the societal revolution, it was a form of mutually agreed, highly stratified roles and responsibilities in which masses of lower-class people accept the right to rule of a sometimes-hereditary elite. In the scientific and first industrial revolutions, the social contract began to embody greater degrees of expanded franchise, equal opportunity, and individualwar-making, building and infrastructure, and the economic management of large areas. This paradigm arguably demanded sufficient sources of wealth, domestic or acquired, to fund state activities; a limited but sufficient and loyal managerial bureaucracy, often fed by organized training or recruitment systems; large, reconstitutable armies; a deep capacity for resilience and recovery (i.e., resources, manpower); and a coherent political identity generating loyalty through effective rule and national energizing myths. Examples of the premodern empires and states that met these demands included Egypt; Rome; some Chinese dynasties; and perhaps, as a late example, the early Ottoman Empire.

A second period commonly discussed as a possible competitive paradigm is often termed the scientific revolution. Its origins were in the 16th and 17th centuries, when the scientific method emerged as a distinct form of knowledge and was applied to hundreds of applications, catalyzing economic growth and technological progress.29 
It empowered competitive advantage in many large and small nations through technique, professionalization, and technology. This paradigm was characterized by a richer set of intellectual institutions, such as universities and republics of letters; improved mechanisms for information storage and exchange; increasingly professionalized public functions, such as bureaucracies and militaries; stronger financial institutions; a growing role for intellectual and business entrepreneurs; and added emphasis on legitimacy through rational governance and performance rather than myth and heredity. Examples of nations that gained competitive advantage in this period include the Italian Renaissance city-states; Britain in the 17th and 18th centuries; and, later, Prussia in the 18th and 19th centuries.

Our review of this history also suggests that competitive paradigms begin to enter crises when their ideas and habitual solutions no longer appear to deal with the problems they encounter—a transition that can be evident when nations begin to experience negative marginal returns from additional investments in its core model of national power and legitimacy.32 Citizens and elite alike lose faith that the existing model will deliver, and the system can enter a spiral of lost credibility and demonstrated failures.

## Defining The Second Industrial Era

These phases of history culminated in the second late industrial era, which has prevailed since roughly the late 
19th century and offers perhaps the most important example of the way that successful (or failed) alignment to an era's fundamental demands decisively affects national competitive advantage. This paradigm became for the United States what the first industrial era was for Britain: an era for which the country was, in many ways, ideally suited (or became so by making essential decisions, such as the expansion of the size and catalytic role of the federal government) and during which the country advanced to world leadership. This was true in the mature or late industrial era (the period during which Britain began to struggle to keep up) and remained so during the following but closely related information age.

As a competitive paradigm, the second industrial era reflected the following several major characteristics:
ism. The social contract of the Industrial Revolution was grounded firmly in economic advances and the promise of a better life.

Second, each of the eras reflects different versions of the primary model for success, as well as secondary or outlier approaches that achieve narrower but still important competitive advantage in different ways. During the societal revolution, smaller city-states used trade, scientific and intellectual sophistication, financial power, and other means to build power beyond their social mass (the Italian Renaissance city-states were perhaps the apogee of this model). Some nations managed to remain geopolitically potent and competitive during the first Industrial Revolution even without fully embracing its technological and social changes, partly by relying on their pure size and heft 
(as in China, Russia, or the Ottoman Empire). In the Industrial Revolution, some smaller nations that could not compete in the mass industrialization model of the era were able to achieve significant measures of success by specializing in one or more technologies or areas of manufacturing (or even services, such as tourism). Others have advanced through immense natural resource reserves.

Third, each era embodies a fundamental pattern of warfare. War in the societal paradigm was waged by mass and, often, partly professional armies that employed relatively similar weapons and tactics for many centuries. The scientific and first industrial revolutions brought moreprofessionalized and technically sophisticated armies to the fore, in some cases wielding useful although not always decisive new technologies. The industrial paradigm saw a return to mass, with vast armies employing massproduced weapons and organized in hierarchical military organizations.31

- high growth (created the basis for addressing many 
social problems and created a basic context for 
national power and legitimacy)
- an urbanized and increasingly cosmopolitan environment, eventually globalized
- an essential grounding in science and technology 
as the catalysts of economic activity and national strength
- a domestic division of labor creating thousands 
of specialty careers with an associated rise in the importance of skilled labor in key industries and fields
-  a rise of truly mass manufacturing and industrial 
management principles
- a growing scale of businesses, larger firms, mergers
- a growing size and sophistication of bureaucracies, 
public as well as private
- an increasing emphasis on individual rights, entitlements, self-expression
- openly shared knowledge: scientific, technological, social, political, often governed by a clear set of common information sources—at least among the elite—that generate some degree of a shared narrative about reality and events
- a breakdown of purely aristocratic class; permeability of class boundaries; end to officially sanctioned slavery and various forms of indentured servitude; an expanded capacity for upward mobility, especially intergenerationally; compared with the feudal era immediately preceding it, very different, and, compared with all previous eras, more comprehensive and assumed for all the population
- a rise of middle classes as the dominant sociological 
component of industrial societies.
growing private sector capable of forming into big firms, an increasingly educated workforce with essential skills, a potent and rapidly growing industrial sector, a nation with a burgeoning middle class experiencing growth in standards of living and intergenerational mobility, and a political system able to support these advantages with helpful interventions. This variety of factors suggests that the era could perhaps best be understood as a scientificindustrial-bureaucratic era—one involving a combination of the scientific revolution, industrial production models, and bureaucratic-administrative institutions.

As noted, analysts often subdivide the industrial era into multiple periods (most commonly a first and second Industrial Revolution), each of which has many characteristics of a somewhat independent competitive paradigm. 

The distinctions emerge, in part, in the fact that countries that mastered one of the sub-eras did less well in others: Britain trying to make the transition from the early, family-firm capitalism of the first industrial era to the mass production, massive industrial elements of the second; the Soviet Union achieving significant early growth (especially under the New Industrial Policy) in the second Industrial Revolution but being utterly unable to keep pace in the computer age. It is possible to see the emerging paradigm (as some have indeed said) as merely a fourth version or type of an industrial age. But it is still an identifiable paradigm with its own demands.

Much as Britain did in the early Industrial Revolution, the United States came to be the gravitational hub of its later periods. Immediately following World War II, with the devastation suffered by many other industrial economies, the United States represented roughly half of world economic output. In 1950, its GDP of $381 billion (in 1964 
The United States turned out to be well-positioned to benefit from these: a large country with a powerful and make effective use of information technology. Its openness to immigrant talent helped it benefit from the essential contributions of thousands of foreign-born engineers and entrepreneurs in the technology sector. Meanwhile, the Soviet Union could not keep up. As two scholars have argued, dollars) was over triple that of the Soviet Union ($126 billion) and, incredibly, more than the Soviet Union, Britain, France, West Germany, Japan, and Italy combined.33 Even by 1960, the U.S. economy still represented 40 percent of world output. That share steadily (although unevenly) declined over time with the recovery and eventual rapid growth of many other countries. Apart from some momentary surges, the U.S. economy has remained about 25 percent of world GDP since the 1970s.34
The collapse of the Soviet system occurred precisely at a time when the mission of industrialization had been exhausted and when the imperatives of postindustrial society had begun to assert themselves. The Soviet system turned out to be incapable of responding to these imperatives and it was this that brought about its downfall.36
The so-called information age built on these essential characteristics with a growing and increasingly decisive role for information technologies. The information age is sometimes treated as its own unique paradigm, a new era distinct from the Industrial Revolution; much like most such paradigms, they overlap and mix. For this analysis, we conceptualized the information revolution as a late industrial era phenomenon, under which many defining features of that industrial period—mass industrial production, mass education, the basic structures of societies and militaries— remained relatively constant. Perhaps the central defining feature of the era was the effect of computing power and software applications in supporting technologies and processes of greater speed, efficiency, and effectiveness. These advances powered the expansion of key sectors of the economy, some degree of enhanced general productivity (although the scale remains disputed), and transformed military operational effectiveness.35
The result was a further surge of U.S. growth and competitive advantage. During the 1990s, U.S. growth and productivity reached levels not seen since the 1960s. Between 1995 and 1999, labor productivity grew by 2.7 percent per year, economic growth ran at 4.5 percent a year, and unemployment fell to 4.0 percent.37 The period from 
1993–2000 constitutes the longest U.S. economic boom of the postwar period. Although the sources of this economic surge remain contested,38 many studies tied these results, at least in significant part, to the efficiency- and outputenhancing effects of computers and software, the cost of which was declining rapidly in this period.39 The United States, therefore, reached a peak of power during the midto late Industrial Revolution (roughly the 1930s through the 1950s) that represented a rough analogue to Britain's position earlier in the modern era. It became the predominant global economic and eventually military power, one that represented the driving force of the age, the hub of its key networks, and the dominant cultural power.

Here, again, the United States found itself in an enviable competitive position. Its innovation ecosystem, including the rise of venture capital firms, helped nurture the companies that would become the early giants of the information economy. The pressure for competitive advantage in the U.S. private sector led to a race to adopt and 

## The Emerging Competitive Paradigm: A Governance Revolution

an initial list of possible trends. We then followed up with issue-specific research on trends underway in each of the broad categories that comprise a competitive paradigm.41
The following list outlines the resulting set of trends that will fuel the emerging competitive era:42 

- Society
 Ȥ aging and smaller populations in much of the 
world; demographic growth in selected regions
 Ȥ challenges to the effectiveness and legitimacy of 
social and governing institutions
 Ȥ growing perception of the rule of an oligarchy or 
kleptocracy; power in society benefits an elite
 Ȥ persistently high rates of alienation, anxiety, 
social stress
 Ȥ rising political polarization, domestic fragmentation and extremism
 Ȥ continuing threats of pandemics.
- Global context for power and influence
 Ȥ growing influence for nonstate actors, especially 
in open societies
 Ȥ interdependent supply chain networks Ȥ integrated, increasingly digital financial 
networks
 Ȥ competition in digital infrastructures (DIs).
- Economy
 Ȥ secular stagnation; long-term decline in growth 
and productivity rates 
 Ȥ rising levels of wealth and income inequality; 
stagnant intergenerational mobility
 Ȥ transition to post–fossil fuel economy; competition to dominate new energy era
We have so far attempted to define the idea of a competitive paradigm, describe the essential requirements for successfully aligning to the demands of such a paradigm, and review several previous competitive paradigms. From this essentially historical assessment, we then looked forward and sought to capture the most-important characteristics of the competitive paradigm likely to prevail over roughly the next three to five decades. The perspective is broad and global, although ultimately designed to generate implications for the United States. Many of the examples used to illustrate the analysis (particularly in the online annex, which provides more detail on the trends producing this new paradigm) thus refer to the United States—but the paradigm we describe will apply equally to all nations.

Developing a future-oriented portrait of a strategic context is one of the most difficult challenges in strategic analysis. Any such endeavor represents an exercise in imaginative forecasting with all the attendant risks of predictive analysis.

Our basic approach was inductive. We evaluated an extensive array of sources on the major trends underway in the defining components of a paradigm (economy, society, military affairs, and geopolitics) and derived general patterns from them. We first reviewed the work of the firstphase study and identified factors and issues that helped to shape prior competitive paradigms as a guide to what to look for in the future. We then surveyed a core set of broad-based assessments of global trends, which themselves reflect broad literature reviews, expert surveys, and independent assessments.40 From that work, we derived 

 Ȥ increasing applications of and threats from 
biotech
 Ȥ growing role for advanced manufacturing Ȥ pressure of rising public and private debt loads.
- General and contextual
 Ȥ role of artificial intelligence (AI)/machine learning and automation in all economic, social, and military processes
 Ȥ fragmenting and corrupted information 
environments
 Ȥ intensifying environmental pressures, disasters, 
and crises, including water issues
 Ȥ rising threats to homelands
 Ȥ greater multipolarity in geopolitical and geoeconomic terms
 Ȥ demand for partial national self-sufficiency in 
critical materials, resources, and technologies
 Ȥ high levels of uncertainty, driven by recurring, 
overlapping crises.
paradigms but represents a significant course-correction from the scientific-bureaucratic-industrial era. We have termed the emerging paradigm the *governance revolution* 
to indicate that it has to do with the ability of societies to overcome the calcifying, fragmenting, and polarizing tendencies and self-generated risks of the late industrial era by employing new approaches to problem-solving and decisionmaking.

In one sense, the term *governance* has much in common with the well-established concept of state capacity—a state's ability to realize outcomes it decides to achieve.43 Sustaining truly meaningful state capacity (and offering an effective and legitimate social contract) in the emerging paradigm will demand new ways of doing business and reforms to existing institutions that comprise a governance revolution. "The main political challenge of the next decade will be fixing government," John Micklethwait 

In one sense, the term 
governance has much in 
common with the well-
established concept of 
state capacity—a state's 
ability to realize outcomes 
it decides to achieve.

The next step in our assessment was to translate the complex mosaic of trends noted above into an overarching theme. No single phrase or concept will capture such a rich tapestry of trends and developments. Nonetheless, locating a single dominant thread in an era can help interpret events and provide a clear frame for policy responses. Settling on such a leading, singular theme will require an admittedly subjective interpretation of the data. Our interpretation is supported by evidence: not only the data from individual trends but other indications of the most-significant themes derived from assessments of the current context cited throughout this essay. Nonetheless, as noted above, this analysis can suggest only one plausible interpretation of the emerging future.

Our analysis suggests that a new pattern might be emerging that builds on the previous three competitive and Adrian Woolridge argue. "Countries that can establish 'good government' will stand a fair chance of providing their citizens with a decent standard of life. Countries that cannot do this will be condemned to decline and dysfunction." A governance revolution, they predict, "will be the heart of global politics."44
and the governing institutions of the industrial age may no longer be adequate to deal with them. Nations that can develop and employ reformed or new governing models to take decisive action in dealing with risks, remain adaptive and resilient, and seize opportunities will gain competitive advantage in this era. This development certainly requires participating in the technological advances of the age and being able to generate late industrial, information-powered, and automation-powered economic progress.

The need to keep up technologically, in part to align to the era's value creation model, has, therefore, not disappeared. But the essential story of competitive advantage in the emerging era is not breakneck progress in areas such as AI and biotech but rather the creation of governing models (public and private) capable of sustaining or restoring vibrancy, efficiency, and legitimacy to national power structures. Whereas competitive advantage in the industrial paradigm came primarily from mastering the era's sources of value creation, advantage in the governance paradigm will derive more fundamentally from an improved social contract and the governing principles and institutions that support it. Nations that solve the ongoing crisis of legitimacy and institutional efficacy—and, in the process, take positive steps to deal with growing risks—will win in the emerging paradigm.

We do not argue that the pursuit and achievement of a governance revolution is inevitable for great powers any more than the scientific, technological, and organizational aspects of the Industrial Revolution were an inexorable outcome of the previous era (as Russia, China, and dozens of poor countries learned, they were not). We argue that multiple trends are creating a context in which effective governance will become a predominant source of competitive But we use the term *governance* broadly to refer to actors and initiatives beyond state capacity or actions. A major theme of the emerging era is that competitively successful societies will generate problem-solving governance functions from many sources.45 Many definitions of governance speak to authority structures beyond government. One United Nations agency, for example, suggests that the term "refers to all processes of governing, the institutions, processes and practices through which issues of common concern are decided upon and regulated."46 A wide variety of public, nongovernmental, and private sector actors participate in such processes and are thus part of the architecture of governance in a society. This usage overlaps with two characteristics highlighted in the first-phase study— effective institutions and an active state—but is broader than either and refers to a general priority on effective operation of social organizations. Critically, too, it implies that social organizations are capable of acting efficiently but also in their own long-term self-interest.

Figure 1 depicts the basic defining factors of the governance revolution. Its core message is that the main barrier to competitive advantage now emerges from two sources: the gradual ossification of industrial-age governing institutions and the risks and threats posed to nations by the multiple challenges to modernity. Put simply, late modernity has generated a raft of economic, environmental, social, and technological risks, threats, and opportunities, 

## A Governance Revolution: Primary Defining Elements A Governance Revolution

Catalyst 1: Challenges to Core Characteristics and Governing Mechanisms of the Scientifc-Bureaucratic-Industrial Age
- Fragmenting information environments and shared realities - Ossifying bureaucracies, declining problem-solving capacities - Loss of faith in scientifc-technocratic solutions to problems - Slowing growth in key nations; ebbing promise of rising standard of living - Rising inequality and broader perception of societal unfairness - Rising public debt constraining problem-solving ambition and responses to crises
Catalyst 2: Threats and Risks to Security and Prosperity of Homelands and Peoples
- Ecological threats both long-term and in the form of extreme weather events - Risks of domestic violence from terrorism and domestic extremism - Vulnerabilities associated with issues of interdependence and fow security, including 
economic and fnancial crises
- Risk of health crises, including pandemics; growing dangers from bioweapons
Nations will possess a competitive advantage that can embody revalidated and creative approaches to problem-solving governance across public and private spheres, including
- Ability to design and implement effective problem-solving approaches to leading 
challenges
- Ability to break down old bureaucratic patterns to generate more fexibility and 
adaptiveness
- Ability to transcend political sclerosis to gain consensus on major reforms - State-guided but not-dominated application of new technologies, including controls to 
mitigate risks

A competitive paradigm in which advantage will 
critically depend on the quality and effectiveness 
of problem-solving institutions of governance in 
societies, both public and private

Implications:
- Inability to address social challenges, including 
direct threats to citizens' well-being
- Loss of faith in public and private institutions - Declining public trust - Rising levels of alienation and social instability
Sources of competitive advantage in the governance revolution and (2) there are threats to national security and well-being that now demand decisive and effective governance to combat.

## Characteristics Of An Era Under Strain

advantage. The age demands effective governance—which is not the same as saying that the emerging context will be one of effective governance. Whether nations respond to the era's rising demands with policies that create strong fitness relative to these demands is a different question.

We define *governance effectiveness* via the following principles, which apply to societies as a whole rather than merely state or government institutions:47
Much of the international competitive story of world politics between about 1750 and 1950 involved the tremendous competitive advantage granted by industrialization in various phases and the accompanying processes, such as the scientific revolution and the information revolution. That competitive paradigm could be termed, broadly, the scientific-industrial-bureaucratic era, and it is composed of several major component characteristics or hypotheses:48

1. the capacity to design and implement at least partially effective and efficient action to deal with 
challenges, opportunities, or risks, which creates a measurable improvement in the issue area
2. being perceived to be acting in these ways by elite, 
expert, and public communities and, thus, sustaining a significant degree of legitimacy in their eyes
3. retaining a sufficient degree of accountability, transparency, and representativeness in their operations, both to enhance the quality of their action and to sustain public support
4. routinely incorporating considerations of longterm versus immediate goals (including foresight and anticipatory thinking and processes), as well as elements of common and public interest versus self-interest in their calculations, again generating a perception of governance quality among elites and the public alike.
Our portrait of the emerging competitive paradigm implies that innovative, effective, and legitimate governance structures and approaches that meet these criteria are the central route to competitive advantage. This is true for two fundamental reasons: (1) There is a crisis in the essential structures and habits of the current paradigm, 

- Human society has the capacity to produce consistent and steady progress, as measured in standard of living and individual opportunity. Economic development is the core trend and value of the era.
- Science and technology are the fundamental driving 
force of this progress.
- The scientific method and associated scientific 
understanding of the world allows for solving specific problems and a more general mastery of human and natural affairs.
- A marketplace of ideas in the public sphere produces debates over claims and counterclaims that objectively advance understanding of key issues and form the essential foundation for democratic deliberation.
- Societal advances can be expected to produce significant gains in shared economic prosperity.
- These advances also tend gradually to generate 
more human autonomy, freedom, and dignity in 
the current paradigm. Jürgen Habermas offers a useful way of defining a period of crisis for any era or the established institutions of the time: an inability of the "self-healing powers" of a society to keep up with the challenges to its vigor. He emphasizes the importance of effective action to confront challenges. "Crises arise when the structure of a social system allows fewer possibilities for problem solving than are necessary to the continued existence of the system." The result is a perceived and actual loss of effectiveness of the "steering mechanisms" of a society, which strikes at the very heart of its purpose: to master the complexity of the world in ways that offer physical and ontological security to its members.50

various categories, expanding the scope of social and political freedoms.
- Networks of exchange, notably in trade in goods 
and resources, generally work to common advantage and provide another spur to progress and development.
- Large bureaucratic-administrative entities are 
essential to the organization of modern societies and can effectively manage key social and economic sectors.
- Military power is primarily a function of two variables: technological sophistication as translated into military capabilities and the industrial might to produce hardware on a large-scale basis. Security is a function of the ability to fight major conventional 
wars.
The upshot of many of the trends reviewed for this analysis, however, is at least a partial challenge to these central characteristics of the scientific-industrialbureaucratic era with significant implications for the effectiveness of governance and the legitimacy of nations.49 
Table 2 reviews many of these challenges—some objectively measurable, others the product of more-subjective but still evidence-based analysis. The accumulating effect of these trends has the potential to generate a culminating point at which the challenges to the current paradigm become critical enough to open the possibility of new sources of competitive advantage and disadvantage.

## A Crisis Of Governance And Legitimacy

This is the core of the 
challenge to the scientific-
bureaucratic-industrial 
age: Actual and perceived 
failures in the objective 
capacity and perceived 
legitimacy of mechanisms 
of self-healing are reaching 
an inflection point.

Many of these trends add up to a comprehensive challenge 
for the problem-solving and society-shaping institutions of 

## Challenges To Key Characteristics Of The Scientific-Industrial-Bureaucratic Era

| Characteristic of Scientific-Industrial-Bureaucratic Era                              |
|---------------------------------------------------------------------------------------|
| Expectation of consistent progress as measured in                                     |
| standard of living and individual opportunity; economic                               |
| development as leading value                                                          |
| - Secular decline in productivity growth rates                                        |
| - Slowing economic growth rates                                                       |
| - Demographic shifts and aging societies                                              |
| - Stagnating or even reversing levels of intergenerational social mobility            |
| - Persistently high and rising public debt and its implications                       |
| - Increasing salience of post-materialist values other than economic development      |
| a                                                                                     |
| - Ecological limits to growth                                                         |
| - Challenges in resource availability (fossil fuels, rare minerals, battery and solar |
| components)                                                                           |

Science and technology as the fundamental driving force of progress; scientific approaches solve a growing number of problems and produce a more-general mastery of human and natural affairs

- Demonstrated limits to scientific rationalism and technocratic approaches in solving 
key social and international problems, from poverty to homelessness to crime
- Recurring legitimacy challenges to scientific rationalism in specific crises (economic, 
organizational, social)
- Rising skepticism of science and scientific institutions - Growing concern about risks of two leading technologies: AI and biotech - Some evidence that scientific progress has slowed, with new breakthroughs more 
focused on incremental and consumer-oriented productsb

A well-functioning marketplace of ideas in the 
public sphere that both advances shared objective 
understanding and forms the basis of democratic 
deliberation

- A fragmentation of sources of information ruins the coherence of the marketplace of 
ideas
- Sources championing misinformation and disinformation corrupt the information 
environment, both from within societies and as strategically used by rivals from without
- Agreed fact base of societies decays
Economic and social advances that generate shared experience of wealth and progress
- Rising levels of inequality within many societies (although declining between nations)
- Rising expectation gaps based on anticipation of degree of shared wealth from 
industrial era
- Thinning intermediary social institutions impair social cohesion and shared 
opportunity
- Role of AI remains uncertain but could threaten significant numbers of careers
Continually expanding human autonomy, freedom, dignity, and social and political freedoms
- Democratic decline in parts of the world
- Emergence of potentially enduring nondemocratic models able to compete in a new 
paradigm
- Threats to stability of liberal democratic systems resulting from the above trends 
(populist nationalism)
- Natural culminating point of secular trend toward freedom when forms of autonomy 
begin to conflict
| Characteristic of Scientific-Industrial-Bureaucratic Era                                  |
|-------------------------------------------------------------------------------------------|
| Economic and social value of networks of exchange,                                        |
| especially trade                                                                          |
| - Costs of trade have become more salient than gains from trade; trade opening            |
| process on hold                                                                           |
| - In era of intensifying rivalry, national security considerations are constraining free  |
| exchange (especially in U.S.-China relations, but radiating out from there)               |
| - Another main network of exchange—people—also more constrained both because of           |
| political salience of immigration issues and national security fears                      |
| - "Weaponized interdependence" increasing costs and risks of networks of exchange         |
| c                                                                                         |
| Large bureaucratic-administrative entities both essential                                 |
| and effective                                                                             |
| - Excessive growth of bureaucratic-administrative functions imposes significant           |
| efficiency penalty on public and private sector organizations alike                       |
| - Popular loss of faith in such entities                                                  |
| - Rising predatory arbitrariness of some institutions intensifying resentment and lack of |
| faith                                                                                     |
| Military power as a function of technological                                             |
| sophistication and industrial might                                                       |
| - Information networks and very rapid sense-target-kill chains becoming the core          |
| military advantage                                                                        |
| - Defense industries becoming too constrained and individual systems too complex for      |
| mass production to have the sort of decisive effect during conflicts that it did in World |
| War II                                                                                    |
| - Information vulnerabilities becoming the most direct route to decisive short-term       |
| effects                                                                                   |

SOURCE: Author analysis; the roster of trends is largely drawn from the topics addressed in the online annex. 

government and politicians seem incapable of achieving consensus. . . . In more routine ways, the political system feeds frustration and discontent with its incapacity to respond to the public's needs. There is little on the horizon to suggest solutions."52
This is admittedly a difficult claim to justify in truly measurable terms. One of the most-widely cited sets of indices on governance—the World Bank Worldwide Governance Indicators analysis, which includes measures of general government effectiveness, as well as measures This is the core of the challenge to the scientificbureaucratic-industrial age: Actual and perceived failures in the objective capacity and perceived legitimacy of mechanisms of self-healing—that is, the processes of governance—are reaching an inflection point. This inflection point represents an accumulating failure of late industrial institutions to engage in effective problem-solving in the common interest.51 As a 2023 *Washington Post* 
analysis put it, "Faced with big and challenging problems—
climate, immigration, inequality, guns, debt and deficits—
istrative state's legitimacy crisis," a situation in which citizens' "expectations meet bitter disappointment; their governments seem to serve elites with political priorities divergent from their own; and there is a growing sense, evident even among insiders, that our institutions specialize in producing dysfunction and might well be replaced by something better."54 But the trend goes beyond the public sector, embracing the rise of managerial capitalism and the growth of energy-sapping administrative functions in private firms as well.

This trend is related to the idea of state capacity, but it is somewhat distinct. States can have significant structural capacity to address problems but might still be often prevented from doing so by bureaucratic inefficiencies or sclerosis. The issue is also distinct from the danger of paralyzed political action, which can result from polarization or other causes. Failures in governance are often political rather than bureaucratic failures,55 and the measurable decline in the activity and effectiveness of legislatures in many advanced democracies is surely related to the challenge of lost legitimacy. But sclerotic bureaucraticadministrative states are a feature of the current era, and they have specific effects that can undermine competitive advantage. In the U.S. context, these include the following:

- Bureaucratic and regulatory complexity imposes 
immense unnecessary costs on public investments, especially infrastructure projects.56
- Corruption and a lack of transparency in many 
countries is persistent. Transparency International's 
2022 index found either stagnation or decline in transparency measures, with only 8 of 180 countries improving their score.57
of corruption and transparency—does not show clear trends between 1996 and 2021 for a variety of developed and developing countries. The United States and some European countries show a modest decline in overall governance effectiveness, Japan and South Korea post some improvement, and measures of developing nations span the gamut from significant declines to persistent gains.53 Many nations have the necessary bureaucratic-administrative capacity to manage basic operations of their society. When challenged in crises, such as the 2008 financial crisis or the 2020 coronavirus disease 2019 (COVID-19) pandemic, many countries saw state institutions respond rapidly and relatively effectively.

But there is still substantial evidence that both because of the demands on governance and an increasingly daunting set of challenges, differences in governing capacity— including reforms that enhance the efficiency and effectiveness of existing institutions—will constitute the essence of competitive advantage in the emerging paradigm. The next section refers to the demand side—the risks and threats of the emerging era that will place new stress on governing capacity. Our research also turned up significant evidence on the supply side: indications that existing governance mechanisms are losing their ability to effectively address the problems of the era. This evidence comes in several forms.

First, there is measurable evidence of the growing size and complexity (and costs in efficiency and effectiveness) of bureaucratic-administrative institutions. The first-phase study of the project cataloged some of the data on the growth of bureaucratic institutions and the costs in terms of efficiency, morale, and stagnation in problem-solving. One scholar referred to part of this trend as "the admin-

- The growth of administrative functions results in 
rising costs of education, which has downstream 
effects on shared opportunity.
- The public and private sectors experience lower 
levels of experimentation and innovation.58
- The private sector loses revenue and potential 
investment.59
- Citizens' perception of effective governance and fair 
treatment is ruined,60 a trend and risk that could 
accelerate with the broader use of algorithmic decisionmaking in bureaucratic settings. This could produce what two scholars have termed a "crisis of legitimacy" in administrative proceedings.61
A second source of evidence for a persistent and worsening decline in governance is perceptions. These can be misleading—partly guided by sensationalistic media treatments, citizens can hold extreme views about the quality of governance. Nonetheless, as the online annex documents, the perceived legitimacy of major institutions and measures of trust in government have declined in many countries, particularly the United States.62 The trend is not universal—in some Organization for Economic Cooperation and Development (OECD) countries, for example, trust in government to solve major problems remains robust. But there is a critical mass of countries where confidence in government is at or below 50 percent: the United States, Japan, France, Turkey, Peru, Chile, Brazil, and many others. Even in the OECD more broadly, trust in major political institutions, including political parties and national institutions, is generally lower.63
and sophistication of their societies and as governments have offered increasingly generous social safety nets, law enforcement processes, and crisis response capacities. These rising expectations can be transferred to anticipated effectiveness of bureaucracies.65 The administrative state thus, over time, has a higher and higher bar to meet in performance if it is to sustain legitimacy—even as the practical potential for efficient management of large-scale enterprises reaches a culminating point.

A third threat to governance comes from rising socioeconomic inequality,66 which undermines the perceived fairness of governance institutions. There is clear evidence of a growing sense among citizens that major governing institutions serve the interests of the wealthy and powerful and are increasingly inaccessible to average citizens. Some 
70 percent of U.S. citizens believe that the U.S. economic system "unfairly favors the powerful."67 In Europe, the percentage of people who believe that government is "run by a few big interests looking out for themselves" is low in a few countries—such as Sweden—but is over 60 percent in Germany, Spain, Portugal, the Czech Republic, and other countries and over 50 percent in Italy, Belgium, France, and elsewhere.68 Similar attitudes have spurred the rise of populism in many developed nations.

Fourth, growing debt burdens in many countries are tightening a long-term limit on the capacity of governments to take new initiatives. This is true of China and the United States but also of such countries as Japan, the United Kingdom, Italy, Spain, and Portugal. Even more than some other inputs to a governance crisis, debt is an uneven factor, affecting some countries far less than others. But it will be especially relevant to some leading industrial countries in the decades ahead.

This problem can be especially severe in advanced nations, which experience a classic crisis of rising expectations.64 The demands of citizens have risen with the wealth returns undermines productive investment. Improved problem-solving governance capabilities across the whole of society—creative, adaptable, effective, and legitimacyconferring—are one key to unlocking many other forms of competitive advantage.

## The Necessity Of Effective Governance: Dealing With Threats And Crises

Fifth, the list of specific public policy challenges, in the United States and globally, that have become seemingly immune to public policy remedies appears to be growing. In Japan, successive governments have not been able to cure the long-term stagnation of the economy. In the United States, accumulating debt, entitlement reform, immigration reform, tax code reform, and a host of other issues resist fundamental change. In China, the government seems unwilling to tackle fundamental financial reforms and to move to a new growth model.

The result of these various challenges to the ability of steering mechanisms to solve problems is what Habermas terms a *legitimation crisis*. "A legitimation deficit means that it is not possible by administrative means to maintain or establish effective normative structures to the extent required."69 One implication is a failure to learn and adapt: 
"Organizational principles limit the capacity of a society to learn without losing its identity." So "Steering problems can have crisis effects if (and only if) they cannot be resolved within the range of possibility that is circumscribed by the organizational principle of the society."70 That may be precisely what is happening today: The steering problems of the late industrial, postindustrial, or information age cannot be successfully addressed by the steering mechanisms left over from the industrial era.

These failings are especially dangerous because the quality of governance is a precondition for full realization of other sources of competitive advantage. Stagnant, calcified bureaucracies are holding back defense innovation and collaboration with allies and partners. Inability to produce a sustainable fiscal path hamstrings investments in sources of advantage, including new technologies, infrastructure, and education. Private sector obsession with short-term Challenges to many of the assumptions of the scientificbureaucratic-industrial age pose serious threats to the effectiveness and legitimacy of current models of governance. But the centrality of governance to the emerging competitive paradigm is apparent for a second reason: the rise of serious threats to the security and well-being of homelands, from both natural and human-made causes. As noted by many trends in the online annex, the emerging era is likely to be punctuated by numerous economic, ecological, technological, and geopolitical crises. Anticipating and dealing with them effectively will constitute an essential foundation for national competitive advantage—and that will demand effective and legitimate governance to build resilience, anticipate problems, and respond to crises.

The emerging era is one in which multiple threats to homelands and citizenries will demand a generalized and comprehensive societal resilience.71 In the face of environmental and political stresses and crises and continuing external threats to homelands, nations will thrive in the new paradigm to the degree that they are resilient in multiple social, political, and economic ways. One aspect of this requirement is information resilience—in terms of not only information security but also citizenries that can navigate an era of fragmenting information sources.72

AI and biotechnology 
offer tremendous potential 
in economic and social 
applications, but they 
also pose clear dangers 
to humanity and to the 
stability of individual 
societies.

ing need to promote competitive advantage in a context of 
competitive interdependence.74 The challenge is not merely 
trade or technology security—it also embraces problems 
of the predatory capture of key industrial sectors; resource 
dependencies; and information flows, including postwar 
and cross-border flows of information more generally. 
Some of these dependencies pose "high, potentially unac-
ceptable levels of risk" to national security whose interrup-
tion "would have an immediate and severe impact on criti-
cal functions" of the government or society, without "ready 
alternatives" being available.75

    A related source of risk is emerging in the form of the 
disruptive effects, as well as perils to humanity's future, 
embodied in emerging technologies. AI and biotechnol-
ogy, in particular, offer tremendous potential in economic 
and social applications, but they also pose clear dangers to 
humanity in general and to the stability of individual soci-
eties. Nations that manage to renew their quality of gover-
nance will better safeguard themselves from the effects of 
these new fields.
    Finally, the emerging competitive paradigm poses 
ever-present threats to global interdependencies. The per-
sistent competitive advantages of participating in networks 
of exchange and the growing emphasis on self-sufficiency 
in a more fractious global environment demand a new 
balance between self-reliance and interdependence. This 
implication does not endorse autarky; being at the hub of 
critical networks is still a competitive advantage. It merely 
highlights the need to strike a somewhat different and 
more self-conscious balance between two sources of advan-
tage: systemic power/networked centrality and integrated 
vulnerabilities.
    The fundamental sources of this trend are the real-
ity of continuing dependencies (for example, for resources 
and supply chain components) alongside growing interna-
tional tensions and the tendency for nations to "weaponize 
interdependence."73 These tensions are, in part, a product 
of the intensifying U.S.-China rivalry and the result-
ing pressures for divisions in trade and technology blocs. 
Those pressures do not apply equally to all issue areas, and 
many globalized supply chains will remain largely immune 
from their effects. But, especially in leading technology 
areas, there will be a continual tendency for countries to 
have to choose between two broad networks—and a result-

    One recent study described the resulting focus as "flow 
security." It "refers to the need to manage the risks that 
come with flows between actors in a highly globalized, 
highly connected world—while simultaneously fostering 

the opportunities of such flows." As straightforward as that sounds, the report notes that an emphasis on flow security would represent a "monumental shift" in approaches to globalization, "a clear departure from unregulated market reform policies espoused by Western states in the second half of the twentieth century."76
In assessing a nation's standing in this era, one key indicator of the potential for success will be an ability to mobilize national resources, will, and law on behalf of a shared effort to improve national competitive standing and, increasingly, to discover such problem-solving energy in nongovernmental institutions and processes, as well as active states.77 The emerging paradigm demands anticipatory, coordinated, and decisive action—something that polarized democracies with troubled public spheres and sclerotic bureaucratic structures (in both the public and private sectors) will struggle to provide.

An important message of this analysis is that challenges to existing governance approaches are such that tweaking around the edges of current institutions and techniques will not be enough. Measures to address these challenges demand a wide variety of processes of social reform in challenging areas designed to modernize and relegitimize the basic social contract in a country rather than narrower, more-technocratic issue-specific reform.78
can preserve a stronger degree of social equilibrium and sense of shared identity (along with natural outcomes, such as a stronger degree of faith in public institutions) will gain significant competitive advantage over countries that are more alienated, fractured, unstable, and ultimately cynical and prone to conspiratorial thinking.

Solidarity, in turn, is typically grounded in some degree of national-cultural identity. Habermas stresses the importance of culture and tradition in sustaining social cohesion and shoring a society up against crises. A "rupture in tradition, through which the interpretive systems that guarantee identity lose their social integrative power, serves as an indicator of the collapse of social system." A society loses its identity "as soon as later generations no longer recognize themselves within the onceconstitutive tradition."79 Societies must produce meaning to serve their purposes and create a stable sense of being for their members. They offer "interpretations that overcome contingency"80 and provide a basis for ontological security. But bureaucratic-administrative steering mechanisms do not produce such meaning,81 so they must earn legitimacy through performance. As Habermas puts it, the administrative functions of a society are burdened with the requirements of legitimation.82 Indeed, he argues that, as the scope of state activity grows, "the boundaries of the political system *via-a-vis* the cultural system shift as a result of this expansion," which reduces the legitimating potential of culture and increases the burden on public action.83

## The Role Of Solidarity, Tradition, And Culture In Underwriting Competitive Advantage

Effective and legitimate governance and cultural solidarity grounded in tradition are, therefore, closely interrelated with causal links going both ways. The emerging era might well reflect advantages for nations with As a natural consequence of these various trends, societal coherence and solidarity are likely to be key differentiators in competitive outcomes in this paradigm. Nations that tradition-based cultural solidarity. The historian William McNeill argued that "A people without a full quiver of relevant agreed-upon statements, accepted in advance through education or less formalized acculturation, soon finds itself in deep trouble, for, in the absence of believable myths, coherent public action becomes very difficult to improvise or sustain." Yet myths generate implied approaches to problem-solving that are tested by experience; when the gap between them widens, it is time for a new myth. The risk is that one will not emerge. "Discrediting old myths without finding new ones to replace them erodes the basis for common action that once bound those who believed into a public body, capable of acting together."84 It will be difficult for nations to succeed in a governance revolution without the cultural and tradition-based sinews of national identity.

## The Competitive Advantage Of The Governance Revolution

importance of this capability today is especially great, for the two reasons outlined above: the rising constraints on existing modes of governance and the emerging threats to homelands that demand effective responses.

The emerging era therefore represents a competition for advantage in high-quality governance. "Better government has been one of the West's great advantages," write Micklethwait and Woolridge. "Now the Chinese want that advantage back." Developing nations are "beginning to produce some striking new ideas, eroding the West's competitive advantage in the process."85 Countries like Singapore have sought national power by searching widely for best practices both at home and abroad. That habit and mindset will be critical in the emerging paradigm: Countries with a potent thirst for governance ideas, and a willingness to adapt when they find them, will have a tremendous leg up on those stuck in path-dependent habits that beget stagnation and calcification. This is the gravitational hub of the systemic competition with China—a competition over the performance, legitimacy, and resilience of distinct governing models.

A governance revolution could include a host of specific reforms or initiatives. The most obvious is widespread experimentation in institutional responses to public challenges—in such areas as health, education, or welfare— that bring together public, nonprofit, and private sector actors in new means of gaining progress (Figure 2 offers just a handful of examples). The goal is to generate transformative change in the governing institutions of various fields; again, marginal changes will not achieve the levels of problem-solving capacity and relegitimation required for measurable competitive advantage.

The basic upshot of these trends is clear. Competitive advantage in the emerging paradigm will critically depend—at least over the long term—on the quality and effectiveness of problem-solving institutions of governance in societies. This pattern would not be new. The peak competitive periods of many modern nations have often emerged during eras of bold, open-minded experimentation with governance institutions, models, and techniques—the multiple public sector responses to the Great Depression in the United States, China's decades of flexible trial and error during its growth surge, earlier periods of governance experimentation in Great Britain during the Victorian Era and in Bismarck's Prussia. But the Such an agenda is likely to require a new era of business activism in solving social problems, such as some of the mid-century trends in the United States—as opposed to the more-recent, fragmented corporate self-interest documented by Mark Mizruchi.86 It will require fresh ways of reforming existing bureaucratic-administrative institutions for greater flexibility and competence—in some cases, radical reforms in personnel and tightened standards for rulemaking that accept higher degrees of individual risk in society. It will demand changes in various rules and, in some cases, voting processes to revive the capacities of legislatures to make consistent and meaningful policy. It will call, in some cases, for entirely new ways of managing social risks or opportunities that go beyond the governance mechanisms we can conceive of today.87
In the broadest sense, this paradigm implies a need to update and rethink the nature of the social contract on offer during the industrial era. A singular emphasis on economic growth, technological progress, and higher standards of living and guarantees against risk are not sustainable in a financial or political sense. "The crisis of the state is more than an organizational crisis," Micklethwait and Woolridge argue in *The Fourth Revolution*. "It is a crisis of ideas. The social contract between the state and the individual needs to be scrutinized in much the same spirit that Hobbes and Mill reexamined it."88 Such a rethinking of the core social contract of the new era will ultimately call for hard choices on significant, and controversial, issues—such as society's risk tolerance in terms of personal safety and the ways in which regulatory structures impose outcomes that are perceived to be fair.

The emerging era will then generate new trends of its own. In his magisterial history of the 19th century, The Transformation of the World, Jürgen Osterhammel identifies what he saw as the "five characteristics of the century."89 These included such factors as "asymmetric efficiency growth," mobility of people and ideas, a "tension between equality and hierarchy," and emancipation. Osterhammel uses almost one thousand captivating pages to arrive at these simple but telling points about a historical era. Based on this far briefer survey of the future, the trends and themes of an age of governance revolution might generate their own main characteristics.

Future historians writing on the era that will mature through the next half century to make a similar list might identify the following characteristics of the governance revolution. It is likely to be a period of One of the implications of a governance revolution is to allow more space for a complex web of localized and national strategies for achieving similar goals. A major impetus of the scientific-bureaucratic-industrial era was a degree of homogenization: universally shared scientific understandings, similar goals under industrial modernity, similar bureaucratic-administrative structures, similar social structures and habits under a cosmopolitan global trend, and ultimately similar economic policies under the influence of international institutions and norms. The era of governance emerges, in part, from a growing understanding of the limits of homogenization as value or strategy. Its primary engine is experimentation, the proliferation of public and private governance approaches with performance-based legitimacy as the goal.

A governance paradigm does not assume any specific character of the emerging international system. It is a template for national success, not a description of world politics—any more than the societal, scientific, or industrial paradigms specified the nature of the international system at the time. Many trends are underway in world politics that parallel the developments in the paradigm— primarily, of course, an intensifying U.S.-China rivalry but also the rise of an important group of developed and developing countries not aligned with either. Nor does the paradigm itself speak to the risk of conflict: The incidence and risk of major system-shaping wars was high for a time during the industrial age, then declined significantly under the influence of nuclear weapons and democratic globalization. It might be rising again today, but that is a distinct question from the nature of the competitive paradigm.

- asymmetric enactment of transformative governance 
models, both in state and nongovernmental institutions, leading to differential degrees of competitive advantage in various domains, from education to technology to public health
- intense debates over the scope and nature of 
the authority of various social actors, featuring 
renewed arguments about public versus private responsibilities—debates whose outcomes will have significant implications for a nations' problemsolving capacities
- proof—or absence—of the ability of governance 
structures to control leading technologies, such as AI and biotechnology, in ways that both grant competitive advantage to some nations and create opportunity and peril for all
- highly uneven crisis response during a period of 
extreme events, grounded in the governance capacities and inherent societal resilience of nations, which will create more differences in levels of national performance over time
- struggles to mitigate the embedding of polarized 
politics and corrupted information environments in 
all societies, including great powers, with varying levels of success conferring competitive advantage or penalty.

## Lessons For U.S. Competitive Positioning

Many of the essential requirements for national competitive success in the emerging paradigm continue key themes from the industrial era. As noted above, competitive paradigms overlap with one another. In every historical case reviewed for this analysis, major components of competitive advantage from one paradigm persist into the next. In the current case, this means that core competitive advantages relative to the late industrial era—notably, large-scale manufacturing capacity, scientific and technological sophistication (including research, skilled labor, and entrepreneurial applications), effective large-scale social institutions, and more—will be essential building blocks of competitive advantage. An age of governance is still built on the essential economic, social, and political foundations of the scientific-bureaucratic-industrial era.

The seven characteristics identified in the first-phase study on societal determinants of competitive standing remain the essential basis for success in the emerging paradigm. The same essential societal characteristics continue to be critically important to master the themes of the emerging competitive paradigm, and aspects of the emerging paradigm challenge these characteristics in new ways. Focused national ambition and will are essential to fuel the investments and reforms needed for competitive advantage. National disunity and fragmentation strike directly at the ability to fulfill many essential demands of the new era. Shared opportunity, effective institutions, and diversity and pluralism can all continue to magnify the effects of competitive advantages. And the two final characteristics—a learning and adapting mindset and the productive initiatives of an active state—are arguably even more critical in the emerging era than they were in the last one.

Our findings about the potential character of the emerging competitive paradigm suggest several lessons for U.S. 

national security strategy.

An inability to fashion a renewed and revalidated social contract, in the form of reformed and creative approaches to governance, could well be the most significant risk to the United States' ability to thrive in the emerging competitive paradigm. The United States retains tremendous residual strength in the emerging model for value creation: Its vibrant, internationally networked private sector can, with sufficient government support and international collaboration, hold even with or outcompete a surging China in these fields. But trends in faith in public institutions, perceptions of shared opportunity and fairness, and other issues related to a legitimate social contract could pose a greater long-term threat to U.S. competitive standing. Many of the most powerful trends in the emerging era—rising inequality, political polarization, rising alienation, growing threats to homelands, anger at predatory bureaucracies—directly challenge nations' 

The trends surveyed 
for this analysis do not 
support a simplistic claim 
that the emerging era 
is the AI (or biotech or 
information) age.

Innovating in and applying for value-producing emerging technologies, especially AI, biotechnology, and renewable energy technologies, remain essential to competitive success in the emerging era.90 The emerging paradigm will be grounded in the productive application of an interrelated set of emerging technologies (including AI, thick infospheres, advanced manufacturing, and biotech) as the foundation for industrial dynamism, military strength, and social efficiency. Every area of social life—including education, health, public safety, urban planning, and the operation of social services—will be deeply affected by these technologies. AI capabilities will play central roles in medical diagnoses, searches for essential data and knowledge, design of anti-crime campaigns, and hundreds of other social choices and plans. Nations that lag badly behind in the development and application of these technologies will be at a significant competitive disadvantage.

Yet the trends surveyed for this analysis do not support a simplistic claim that the emerging era is the AI (or biotech or information) age. We have suggested that the structures of governance that provide the supportive context for technology development (and many other competitive requirements) represent the most essential foundation for national success. Nonetheless, each competitive paradigm is defined to an important degree by the leading technologies of the time, and the emerging era is no different. AI, in particular, offers the potential for applications in dozens of critical economic and social fields; nations that cannot keep pace with its development and applications might fall behind in broader competitive terms just as surely as those who failed to modernize fell behind in the industrial era.

as one scholar has put it, was that "The French revolution unleashed new energies which found their expression in a revolutionary form of warfare": fully mobilized populations spurred by nationalist fervor, molded into highly trained, fast-moving strike forces capable of being wielded in new ways by a leader of genius.93 In a similar but arguably even more fundamental sense, the social structures of the United States and China will produce competitive benefits and costs as they struggle for relative influence over the international system.

If the United States falls short of the demands of the emerging competitive paradigm, it might not do so for reasons parallel to Great Britain's relative decline in the 20th century, which stemmed from an inability to keep up with second-phase industrial manufacturing and leading technologies. What imperils U.S. competitive standing most could be fractures in its social fabric, its political coherence and effectiveness, the health of its information environment—and, as a product of all these factors, poor decisionmaking on major challenges.

The United States confronts a challenging historical tendency: Leading nations very often cannot adapt sufficiently to the demands of new competitive paradigms. As noted at the beginning of this report, many more leading nations fail to realign themselves to the demands of a new paradigm than succeed at that difficult task. Even Britain, which rode the first Industrial Revolution to global hegemony, ultimately could not continue to adjust itself to the second Industrial Revolution and fell behind the United States, Germany, and others in competitive standing in key industries and its industrial output. Like Japan, it entered a long, slow decline in competitiveness, reflecting a trend that came to be known as the *British disease*. If the United ability to sustain viable social contracts. Major areas of concern tend to relate to negative social and political trends in key areas related to the criteria for success: the state of U.S. governing institutions, faith in social institutions more broadly, intensifying political and social polarization, rising domestic extremism and its offshoots (such as threat of domestic violence), the corruption of the national information environment, and lagging progress in education and building a skilled workforce.91 These are more-difficult barriers to overcome than lagging performance in key industries or technologies.

The most critical form of response from the United States, then, would take the form of widespread reforms of existing governance institutions. These could include changes in procedures and norms in legislatures to enhance their effectiveness, deep reform of federal bureaucracies, or new experiments in decentralized governance. In defense and domestic policy alike, starting anew and rethinking governance structures from the ground up— as politically and practically difficult as it would be— constitutes the indispensable basis for renewed competitive advantage.

The era thus reflects a larger insight that the social structures of nations—their arrangement of economic and political power and social hierarchies—compose one of the most important foundations of competitive advantage. This conclusion partly mirrors Carl von Clausewitz's emphasis on the sociological basis of military power. He looked in awe at the energies released by the French revolution, which created a situation in which "a people of thirty millions" were now unified in a national endeavor so that "War, untrammeled by any conventional restraints, had broken loose in all its elemental fury."92 The result, 

- access to international networks of goods, ideas, and 
people
- a system capable of generating pockets of experimentation and innovation, at least partially unconstrained by any central guidance
- a research, educational, and information context 
supportive of ongoing responsiveness to innovation and resilience
- effective and transparent governing institutions—
both formal public sector departments and agencies and more informal social institutions
- investment capital to promote new technologies and 
capabilities and an overall context with sufficient private sector flexibility.
States is to recast itself to succeed in the emerging era, it will, to some extent, be seeking to violate a well-established historical tendency.

The emerging paradigm might offer less competitive advantage to fully open societies, either politically or economically. Although it appeared, for a time, that centrally planned societies would be able to compete in industrial terms with the United States, the West, and Japan, societies that were politically and economically more open and market based eventually proved to have significant advantages. The argument from early in the Cold War was that Western democratic systems were destined to prevail, in part, because of their inherent, bottom-up, grassroots dynamism, which offered economic and technological benefits and a form of legitimacy that the centrally constricted Soviet system could never match. One could argue that politically constrained societies could make rapid progress (as, for example, South Korea did even before its democratization) and that a strong government role in state-led development was a boon to growth rather than a hindrance. China may properly be seen as a variant of these mixed models rather than a comprehensively statedirected system, which partly explains its own astonishing growth. Nonetheless, many analysts have held that eventually, high-growth, highly developed societies are incompatible with strict political control and a massive state role in the economy.

Yet the emerging competitive paradigm might further qualify the advantages of open political and economic systems. In the emerging paradigm, it could be that nations do not need democracy or truly free markets to succeed. What they do need is the following:94
Measured in these terms, the existing Chinese system certainly appears to have greater potential to compete effectively than the Soviet Union in the industrial and postindustrial paradigms. (This conclusion builds on a long-standing contention—by Beijing and others—that China's development model reflects a distinct approach that marries a flexible and adaptable form of state control with a large, innovative market,95 although the argument here is more specific to the nature of the emerging era.) AI technologies, combined with highly efficient manufacturing, can perhaps substitute—in terms of long-term growth and productivity—for some of the effects of dynamic market economies. Applications of AI could theoretically out-think grassroots experimentation in dealing with social challenges—much as AI programs have repeatedly out-thought human competitors in such games as chess, poker, and multiplayer video games. A stronger state role might be better able to provide resilience and sustainability and to impose major reforms to deal with inequality and social alienation.96
of Beijing's approach to domestic rule or international relations will meet these criteria.

For one thing, China is just as much in the crosshairs of an era of governance as the United States or any other modern power. Key sources of competitive risk associated with this theme—sclerotic bureaucratic-administrative institutions, loss of faith in the future, rising inequality, and perception of rule by a self-interested few, and more— threaten the CCP's model and the U.S. one. China, after all, is seeking a highly technocratic version of a scientificbureaucratic-industrial society—indeed, one grounded in a basic philosophy, Marxism-Leninism, which is a fundamentally scientific-modernist theory of social life. China could retain a stronger unifying myth and emphasis on traditions and share sociocultural values, but these efforts also might be brittle, because the values connected to the current regime rely on the continued legitimacy of the CCP, which cannot be taken for granted.

The CCP system might, therefore, be no better positioned in its inherent characteristics to flourish in a paradigm emphasizing the necessity for effective and legitimate governance. China is investing massively in new technologies, for example, but its efforts to impose central direction on these immense programs could both fail and prove unable to nourish bottom-up innovation ecosystems.98 It could face irresolvable dilemmas in continuing to generate the levels of economic growth that have been essential to the legitimacy of the CCP.99
Beijing is certainly trying to use AI, control of its information environment, and surveillance technologies— along with massive state investment in critical technology sectors—to create a controlled, almost algorithmic model of societal success.97 It faces multiple challenges, but, at least in theory, the emerging paradigm does allow for such an approach to outperform open societies, especially ones gripped with significant social, political, and informational volatility.

A related competitive handicap for the United States could be that many of the security aspects of the emerging paradigm emphasize areas of technology that are mainly in the private sector. Yet private firms often behave in ways that place profits ahead of national interests: in locating manufacturing for critical technologies abroad, seeking to escape paying taxes into their home country's treasury, or (in the case of social media platforms) prioritizing usage over combatting misinformation. China, on the other hand, has demonstrated a willingness to bend quasi-private firms to its will.

Despite the potential for a nondemocratic route to success, the specific approach of the Chinese Communist Party (CCP) could impede its competitive position in the emerging era. Certain elements of the emerging competitive paradigm, therefore, at least open the possibility that well-functioning autocracies displaying effective rule, strong social solidarity and legitimacy, potent investment in key emerging technologies, and strong international partnerships to fuel development could pose a competitive challenge more lasting and robust than the Soviet Union in the Cold War. Yet it is not clear if the current trajectory Its policy choices might also be creating competitive disadvantage in the emerging paradigm. On its current trajectory, leadership in Beijing could be veering from a sustainable version of a statist model for competitive success into an embrace of patterns and habits that will advantage when they can build society-wide steering mechanisms, both public and private, that perform effective problem-solving and do so while preserving fiscal sustainability and acting in the name of shared sociocultural traditions. Given the limits to bureaucratic technocracy, the most likely avenue to such an outcome is not through universal programs but rather—as the first-phase study of this project suggested—through a portfolio of experiments providing numerous options suitable to different contexts.101
undermine its dynamism and relative standing. The domestic push toward recentralization and imposition of a new orthodoxy based on Xi Jinping thought is likely to injure the society's ability to innovate and grow and could create strains within the society over the medium term. Its confrontational brand of nationalism is already limiting China's access to foreign ideas, people, private sector partnerships, technology, and capital. Its belligerence on several geopolitical issues and repeated efforts to coerce others into its preferred behavior has damaged its reputation in many countries and is producing a strong balancing effect in security and economic domains.

These patterns could, in fact, reflect an inherent flaw in nondemocratic systems whose sources of legitimacy stem from ideology, nationalist narratives, and provision of material advances rather than participation, a degree of openness, and some degree of popular representation. Autocracies might inexorably slide into such constrained and confrontational patterns of behavior—a sort of "autocracy spiral"100—at least when they are unable to offer endless economic growth and foreign expansion as ongoing legitimation tools. If this is true (and, separately, if China continues to tighten political control, impose morerestrictive orthodoxies, limit foreign contacts, and generate intensifying global pushback), then China will deny itself the route to competitive success that is theoretically open to it.

## Conclusions

If the emerging era indeed represents a competition in governance mechanisms, unlike in the Cold War, the United States might not be able to count on its inherent social energy to prevail this time. China has a conceivable pathway to success in the emerging competitive paradigm in ways that the Soviet Union never did.

Unlike some other great powers during transitions in eras, the United States is not fatally misaligned to the requirements of the emerging competitive paradigm. It is not the late Ottoman Empire or the later Soviet Union, with inherent social, political, and economic characteristics that almost guaranteed failure in sustained competitive advantage against the leading powers of the day. Given that the era continues to reflect many aspects of the industrial era, this ought to come as no surprise. But a persistent narrative of U.S. decline and Chinese overtaking has perhaps camouflaged the incredible residual strengths the United States has accumulated, such as continued leading position in many technology areas and effective governing institutions at multiple levels (state, federal, municipal, and local).

The biggest question, however—as we stressed in our first-phase study—is not whether the United States has the raw materials to thrive in the emerging paradigm but whether intensifying and interlinked economic, political, We have argued that the emerging paradigm that will help determine competitive advantage is best understood as a governance revolution. Nations will gain competitive Germany. This represented "the sudden transformation of the leading and most dynamic industrial economy into the most sluggish and conservative, in the short space of thirty to forty years" (roughly 1860 to 1900).102
Britain, Hobsbawm argued, "failed to adapt to new conditions, not because she could not, but because she did not wish to. The question is, why not?" Part of the explanation came from hubris: "A nation which is already at the top politically and economically," Hobsbawm argued, "and tends to look down on the rest of the world with selfsatisfaction and a little contempt, inevitably does" lose "certain non-economic spurs to enterprise." He continued:
The degree to which the United States aligns itself to an age of a governance revolution will play a major role in making the difference in which future the United States confronts.

Americans and Germans might dream of making their destiny manifest; the British knew that it had manifested itself already. There is no doubt, for instance, that a nationalist desire to catch up with the British was largely responsible for the systematic German reinforcement of industry by scientific research: The Germans said so.103
and social problems will keep it from making the necessary choices to do so. If the United States failed to adjust to the demands of a new era, it would not be the first global hegemon to be defeated by such a transition. Hobsbawm has described Britain's slow decline during the mid to late industrial era from an unprecedented position dominating the world economy to a laggard in key economic, social, and military terms. Britain was not able to adjust to the second, more mass manufacturing– and science-oriented Industrial Revolution and ultimately could not keep up with industrial-era powerhouses like the United States and That lack of national motivation expressed itself, in part, in a shift from making world-leading goods to making money. "The British economy as a whole tended to retreat from industry into trade and finance, where our services reinforced our actual and future competitors, but made very satisfactory profits." Britain, he argues, "was becoming a parasitic rather than a competitive economy, living off the remains of world monopoly, the underdeveloped world, her past accumulations of wealth and the advance of her rivals."104

## Notes

1  For the first phase of the study, see Mazarr, The Societal Foundations of National Competitiveness.

2  The term *paradigm* is sometimes more narrowly associated with the prevailing modes of thought in a specific field, but it has broader meanings as well. With its use here, we mean to suggest the dominant pattern of economic, technological, social, and political sources of growth, dynamism, and advantage at any moment in history—the model of national qualities associated with success. We discuss these concepts in more detail.

3  Mazarr, *The Societal Foundations of National Competitiveness*, p. 312.

Our analysis does not suggest that the United States stands on the precipice of a similar decline from world economic leadership. It benefits from powerful embedded strengths—the size of its economy, its ability to attract capital and skilled immigrants—and stands in a very different competitive position relative to the emerging paradigm. 

The United States' choice is likely not between predominance and rapid decline but rather between a very modest but lasting degree of slight relative advantage and a world in which China's status as first among equals becomes more undeniable, and the legitimacy and appeal of the U.S. model declines more rapidly. The degree to which the United States aligns itself to an age of a governance revolution will play a major role in making the difference in which future the United States confronts.

4  Mazarr, *The Societal Foundations of National Competitiveness*, p. 313. 

Japan scholar Carol Gluck suggests a very similar idea in her concept of the "available modern" (Gluck, *Japan's Modern Myths*, pp. 26–30) or the global model for state success on offer to aspiring great powers. That paradigm in the 20th century, she argues, came to be defined by the nation-state, industrialization, and global webs of commerce and ideas. Leaders of Meiji Japan recognized this emerging model—and the gaps between their society and its demands—and sought to make Japan more competitive within this modern paradigm. For a recent argument about Meiji Japan's pursuit of the available modern, see Vries, Averting a Great Divergence.

5  Mazarr, *The Societal Foundations of National Competitiveness*, p. 311. 6  A related question is whether great powers make the times—whether they shape the character of the prevailing paradigm through their actions. This is true to a degree, but our research suggests that the more important influence relationship runs the other way—nations' prospects being determined by the demands of their era.

7  Hobsbawm, *Industry and Empire*, p. xi. 8  Kennedy, *The Rise and Fall of the Great Powers*, pp. 148–149. As Kennedy notes, however (p. 152), while Britain remained the world's leading maritime power, it never made the decisions necessary to translate this massive economic advantage directly into military power—in the way that, for example, Nazi Germany would later (or that would be true, for different reasons, for the United States after 1989). Britain's economic predominance therefore did not translate into equivalent military superiority.

9  Ikenberry, "The Triumph of Broken Promises."
10  Some would argue that such a period—half a century or less—is too brief. Previous historical eras that came to reflect a competitive paradigm lasted from over a century (in the case of the industrial age) to a millennium or more (in the ancient world). Yet we believe that considering developments over the next three decades makes sense for three reasons. First, objective indicators suggest that the pace of history's march through distinct eras is accelerating and that a smaller slice of time can represent a significant transformation in the competitive demands on societies (see, for example, Butler, "Tomorrow's World"; and Azhar, The Exponential Age). Partly, this is a perceptual issue based on compressed eras caused by cultural change (Perreault, "The Pace of Cultural Evolution"). Second, our research has identified trends and developments currently underway that are already creating powerful competitive demands and that are likely to mature over the next several decades. Third, no periodization can be definitive or absolute—competitive ages always blur into one another—and our purpose is to offer a snapshot of emerging demands that can inform U.S. thinking about international rivalries. In fact, the emergence of a new paradigm is already underway and has been for some time, so that the period ultimately assigned to this era could stretch well beyond a half century.

11  The basic insight of Michael Porter's work remains relevant in the advantage conferred by what he termed *cluster activities* (Porter, The Competitive Advantage of Nations).

12  This is the approach taken in Thomas Kuhn's famous work about changes in scientific paradigms of understanding (Kuhn, The Structure of Scientific Revolutions). Even Kuhn, however, includes other aspects of shared practice and professional institutions in his concept: An accepted way of understanding the world radiates outward into habits of research, organization, and institutional rules and norms that govern the life of professional fields from law to science.

13  Smith quoted in Barker, *Paradigms*, p. 31. 14  Barker, *Paradigms*, p. 32.

15  Barker, *Paradigms*, p. 37. 16  Barker, *Paradigms*, p. 51. 17  Hundley, *Past Revolutions, Future Transformations*. 18  Abramovitz, "The Elements of Social Capability," p. 29. 19  Toynbee, *A Study of History*, pp. 152, 140.
20  We are grateful to Wouter Jurgens, at a meeting of The Hague Center for Strategic Studies kindly organized by Tim Sweijs, for the specific framing and term of *social contract* to capture the essence of what we meant to convey with this half of the strategic paradigm.

21  For recent examinations of the shifting landscape for modern social contracts, see Shafik, "A New Social Contract."
22  For evidence of the negative effects on economic growth, see Butkiewicz and Yanikkaya, "The Impact of Sociopolitical Instability on Economic Growth"; and Alesina et al., "Political Instability and Economic Growth." For evidence of the negative effects on the pace of innovation, see Allard, Martinez, and Williams, "Political Instability, Pro-Business Market Reforms and Their Impacts on National Systems of Innovation."
23  Maddison, The World Economy: A Millennial Perspective. 24  A superb chronicle of this period is Vries, Averting a Great Divergence.

25  Berkeley Economic Review, "The Japanese Economic Miracle."

26  See, for example, Vogel, *Japan as Number One*. 27  Crawford, "Reinterpreting the Japanese Economic Miracle." 28  Toh, "Living Standards Are Still Falling in Japan. That's a Recipe for 
More Stagnation."
29  Shapin, *The Scientific Revolution*; Wootton, *The Invention of Science*; 
Lehmann-Hasemeyer, Prettner, and Tscheuschner, "The Scientific Revolution and Its Role in the Transition to Sustained Economic Growth." 
30  Mokyr, *The Lever of Riches*; Deane, *The First Industrial Revolution*; 
Scott, *How the Old World Ended* (especially pp. 27–43).

31  In military terms, various scholars have periodized major epochs in different ways. One of the most basic, and helpful, is Trevor Dupuy's division of military history into several overlapping eras: an "age of muscle" running through about 1500; "the age of gunpowder" from about 1400 through 1815; and "the age of technological change" beginning in 1800 and running through the present day, involving all the major industrial era technologies of the time and postwar computer and nuclear technologies (Dupuy, The Evolution of Weapons and Warfare). One of Dupuy's key messages was that the pace of advance had increased over time. The age of technological change involved far more individual advances in weaponry, tactics, and military organization than all the others. He also stressed the importance of ideas as opposed to technologies: "The application of sound, imaginative thinking to existing weapons has caused the great developments in military affairs and influenced international relations." He continues, "The importance of new or imaginative ideas in military affairs—as opposed simply to new things—can best be gauged by the fact that new ideas have often permitted inferior military forces to overcome forces that were larger and/or better equipped" (Dupuy, The Evolution of Weapons and Warfare, p. 316).

Bernard and Fawn Brodie quickly survey forms of warfare in antiquity and the Middle Ages and then describe a phase of the "impact of gunpowder"; the rise of scientific applications to war in the 17th century; 18th-century Napoleonic techniques and capabilities; and the effect of 19th-century science on war, nuclear war, and postwar technological advances (Brodie and Brodie, *From Crossbow to H-Bomb*). 

Martin van Creveld refers to "the age of tools" spanning ancient times to about 1500 and involving mostly handheld implements but also the earliest gunpower weapons; "the age of machines" from 1500 to 1830 with the first widespread use of steam power and gunpowder weapons; "the age of systems" from 1830 to 1945, in which mass mobilization, true combined-arms warfare, and aircraft were introduced; and "the age of automation" from 1945 to the 1980s (Van Creveld, Technology and War: From 2000 B.C. to the Present).

32  Micklethwait and Woolridge, *The Fourth Revolution*, pp. 228–229. 33  Kennedy, *The Rise and Fall of the Great Powers*, p. 369. 34  Bhutada, "The U.S. Share of the Global Economy over Time." 35  One defining work on this subject is Castells, The Information Age: 
End of Millennium. See, also, Gompert, "National Security in the Information Age."
36  Mau and Drobyshevskaya, "Modernization and the Russian Economy." 
37  Frankel and Orszag, "Retrospective on American Economic Policy in the 1990s."
38  DeLong, "What Went Right in the 1990s?" 39  Jorgenson et al., "Raising the Speed Limit"; Oliner and Sichel, "The Resurgence of Growth in the Late 1990s." 
40  These were National Intelligence Council, *Global Trends 2040*. 

Notable among the previous versions of the same report are National Intelligence Council, *Global Trends 2030*; National Intelligence Council, Global Trends. See, also, Brilliant, "10 Trends in 2022: Global Perspectives for Business"; World Economic Forum, The Global Risks Report 
2022; Army Futures Command, *Future Operational Environment*; Joint Chiefs of Staff, *Joint Operating Environment 2035*; Becker, "Joint Operating Environment 2040"; and United Kingdom Ministry of Defence, Strategic Trends Programme.

41  These sources are referenced in the online annex in the more detailed discussion of the trends.

42  The online annex offers detailed evidence and sources that support this characterization of trends. Three of these trends—growing inequality, a perception of elite rule, and increasing polarization—were extensively documented in Chapter 10 of the first-phase study and are not treated again in the online annex.

43  See Besley and Persson, "The Origins of State Capacity"; and Cingolani, "The State of State Capacity."
44  Micklethwait and Woolridge, *The Fourth Revolution*, pp. 5, 14. 45  For a useful introduction to and history of the term, see Bevir, 
"Governance." 
46  United Nations Human Rights Office of the High Commissioner, 
"About Good Governance." 
47  These criteria overlap with, but are not equivalent to, the concept of good governance (primarily a set of standards for the use of state capacity that refers to transparent and effective use of public resources in the common interest). The focus here is on the general practice of governance across social actors.

48  For useful examples that cataloge these themes, see Antonio, "After Postmodernism"; Berger and Luckman, Modernity, Pluralism, and the Crisis of Meaning; Berger, *The Desecularization of the World*; Giddens, The Consequences of Modernity; Giddens, *Runaway World*; Inglehart, Modernization and Postmodernization; Löwy and Sayre, Romanticism Against the Tide of Modernity; and Thiele, "Twilight of Modernity."
49  The idea of challenges to the characteristics of modern, industrial life is certainly not new—it has arguably been the leading thread of socioeconomic and political criticism over the last half century. From this immense literature, some of the more general and philosophical treatments include Giddens, *Modernity and Self-Identity*; Himmelfarb, Past and Present; Smith, *Modernity and Its Discontents*; Toulmin, Cosmopolis; Rosa, *Social Acceleration*; Bauman, *Liquid Modernity*; Bauman, Postmodernity and Its Discontents. Peter Turchin's theory of societal collapse focuses explicitly on social and bureaucratic complexity and its role in creating ever-escalating energy demands that exhaust the resources of societies (see Turchin, *Historical Dynamics*). 

50  Habermas, *Legitimation Crisis*, pp. 1–5. 51  Acemoglu, "The End of Democratic Capitalism?"
52  Balz and Morse, "American Democracy Is Cracking. These Forces Help Explain Why."
53  World Bank, "DataBank." 

54  Wallach, "The Administrative State's Legitimacy Crisis," p. 1. 55  Meier et al., "Bureaucracy and the Failure of Politics." 56  Demsas, "Why Does It Cost So Much to Build Things in America?" 57  Transparency International, *Corruption Perceptions Index 2022*.
58  Lofgren, McNamara, and Modigliani, Commission on Defense Innovation Adoption.

59  Hamel and Zanini, "Excess Management Is Costing the U.S. $3 Trillion per Year."

60  Neal, "How Bad Bureaucracy Sabotages Democracy." 61  Calo and Citron, "The Automated Administrative State." 62  Saad, "Historically Low Faith in U.S. Institutions Continues."
63  The world numbers come from Gallup data reported in Balz and Morse, "American Democracy Is Cracking. These Forces Help Explain Why." OECD data is from OECD, "Trust in Government." 
64  For a description of the phenomenon, see Chandra and Williams, 
"The 'Revolution of Rising Expectations,' Relative Deprivation, and the Urban Social Disorders of the 1960s."
65  Researchers have long made this connection in developing contexts; 
rising expectations are connected to the perceived effectiveness of development administration. See, for example, Ilchman, "Review." 
66  Habermas emphasizes that divisions in class structure are a critical part of any legitimation deficit (see Habermas, *Legitimation Crisis*, p. 73). He worries, too (p. 39), that advanced societies fragment social action among so many groups that class consciousness in broader terms can no longer arise, a serious obstacle to decisive pressure for change.

68  Transparency International, "Global Corruption Barometer EU." 69  Habermas, *Legitimation Crisis*, pp. 46–47. 70  Habermas, *Legitimation Crisis*, p. 7.
71  The 2022 U.S. National Defense Strategy prominently features the requirement for resilience as a component of deterrence (U.S. Department of Defense, *2022 National Defense Strategy*). The French National Strategic Review of the same year also emphasizes the role of a resilient autonomy as the foundation of national security; the document notes that a key goal for France is "the long-term maintenance of its autonomy of decision and sovereign action in the face of all the threats that arise." The "strengthening of our strategic autonomy" is the first of its national security pillars. "Strategic autonomy is the sine qua non condition for the protection of our fundamental interests. At its core are the capacities for autonomous assessment, decisionmaking and action" (French Republic, *National Strategic Review 2022*, pp. 19–20).

72  Gross, "How Finland Is Teaching a Generation to Spot Misinformation."

73  Farrell and Newman, "Weaponized Interdependence."
74  For one recent estimate of the efficiency price tag of such a shift, see Giles, "'Friendshoring' Is a Risk to Growth and Financial Stability, Warns IMF." The International Monetary Fund (IMF) study cited there estimated a long-term cost of 2 percent of global output from a strong move away from globalized integration.

75  Verhagen, Chavannes, and Bekkers, Flow Security in the Information Age, p. 35.

76  Verhagen, Chavannes, and Bekkers, Flow Security in the Information Age, pp. 7–8.

77  Our RAND colleague King Mallory offered critical input to this point.

78  One set of scholars considering the implications of slower economic growth, for example, suggested that the necessary response must involve a process of "guided civic renewal," with the involvement of active states in shaping a future with stronger democratic institutions, improved social integration and tempered inequality, better education, and more robust returns on public and private investment. Burgess et al., "Prepare Developed Democracies for Long-Run Economic Slowdowns," pp. 1615–1618.

79  Habermas, *Legitimation Crisis*, p. 4.

80  Habermas, *Legitimation Crisis*, p. 119. 81  Habermas, *Legitimation Crisis*, p. 70. 82  Habermas, *Legitimation Crisis*, p. 36. 83  Habermas, *Legitimation Crisis*, p. 71. 84  McNeill, "The Care and Repair of Public Myth." 85  Micklethwait and Woolridge, *The Fourth Revolution*, pp. 1, 17. 86  Mizruchi, *The Fracturing of the American Corporate Elite*.
87  In the process, a governance revolution would take aim at an important source of the productivity slowdown in advanced economies in recent decades. The economist William Baumol argued in the 1960s that "productivity increases much more slowly in labor-intensive industries than in industries where capital in the form of machinery can be substituted for labor." Public sector governance tends to be dominantly concerned with labor-intensive industries, such as education and health care, whose productivity has been stuck. The economist Larry Summers noted that if U.S. incomes are measured against manufactured goods, such as a television, they have grown tenfold. But incomes have fallen relative to the growing costs of health care. Nations held down by the anchor of low productivity and efficiency gains in labor-intensive sectors will be at a significant competitive disadvantage, especially because services have become the dominant components of advanced economies and many advanced nations are reaching the natural limits of added productivity in manufacturing. A governance revolution would be aimed primarily at such labor-intensive sectors of economies, including the public sector, health care, and education. Micklethwait and Woolridge, *The Fourth Revolution*, pp. 109–110.

88  Micklethwait and Woolridge, *The Fourth Revolution*, p. 222. 89  Osterhammel, *The Transformation of the World*, pp. 907–919. 90  Schmidt, "Innovation Power." 91  The first-phase study reviewed evidence for these trends; see Mazarr, Societal Foundations of National Competitiveness, Chapter 10.

92  Von Clausewitz, *On War*, pp. 592–593.

93  Roxborough, "Clausewitz and the Sociology of War," p. 622.
94  For a useful summary of China's sources of competitive advantage in the information age, see Carnegie Endowment, "How Chinese Technology Made China an Innovation Powerhouse."
95  For one prominent (and controversial) case for a distinct Chinese version of modernization, see Jacques, *When China Rules the World*. 

For a more balanced recent assessment of China's alternative approach, see Chu, Po-Ching, and Chang Fa, Ultimate Economic Conflict Between China and Democratic Countries.

96  For an argument about world-class management techniques being pursued by Chinese private sector firms—an essay that seems perhaps unbalanced in its praise of the Chinese firms but is nonetheless instructive—see Greeven, Xin, and Yip, "How Chinese Companies Are Reinventing Management." In particular, the article emphasizes how some Chinese firms build significant degrees of work team autonomy into their operations, against the caricature of centrally planned systems that cannot provide for grassroots initiative.

97  For information on Beijing's strategy for achieving just such a result, see Tai Ming, *Innovate to Dominate*.

98  See, for example, Hofman, "Will China's New Party Tech Commission Help or Hurt?" 
99  Zongyuan and Steil, "Xi's Plan for China's Economy Is Doomed to Fail."
100  We are grateful to our RAND colleague Jordan Willcox for this phrase and idea.

101  See, for example, Watney, "But Seriously, How Do We Make an Entrepreneurial State?" For a broader treatment, see Kattel, Drechsler, and Karo, How to Make an Entrepreneurial State.

102  Hobsbawm, *Industry and Empire*, p. 156. The data on Britain's relative decline are stark (p. 160): In 1913, Germany had 60,000 university students compared with Britain's 9,000. Germany was producing 3,000 graduate engineers per year; in England and Wales together, "only 350 graduated in all branches of science, technology and mathematics with first- and second-class honors."
103  Hobsbawm, *Industry and Empire*, pp. 160–163. 104  Hobsbawm, *Industry and Empire*, pp. 166, 170. 

## References

Balz, Dan, and Clara Ence Morse, "American Democracy Is Cracking. 

These Forces Help Explain Why," *Washington Post*, August 18, 2023.

Baquer, Oriol Carreras, and Adrià Morron Salmeron, "Why Productivity Growth Is Declining," Caixa Bank, February 15, 2018.

Abramovitz, Moses, "The Elements of Social Capability," in Bon Ho Koo and Dwight H. Perkins, eds., Social Capability and Long-Term Economic Growth, St. Martin's Press, 1995. 

Barker, Joel Arthur, *Paradigms: The Business of Discovering the Future*, Harper Business, 1992.

Bauman, Zygmunt, *Liquid Modernity*, Polity Press, 2000.

Accenture, "Artificial Intelligence Poised to Double Annual Economic Growth Rate in 12 Developed Economies and Boost Labor Productivity by up to 40 Percent by 2035, According to New Research by Accenture," 
press release, September 28, 2016.

Bauman, Zygmunt, *Postmodernity and Its Discontents*, Polity Press, 
2013.

Acemoglu, Daron, "The End of Democratic Capitalism? How Inequality and Insecurity Fueled a Crisis in the West," *Foreign Affairs*, July/August 
2023.

Becker, Jeff, "Joint Operating Environment 2040," Mad Scientist Laboratory blog, December 10, 2020. 

Adrian, Tobias, and Tommaso Mancini Griffoli, "The Rise of Digital Money," International Monetary Foundation *Fintech Notes*, July 15, 2019.

Bergeaud, Antonin, Gilbert Cette, and Rémy Lecat, "Long-Term Growth and Productivity Trends: Secular Stagnation or Temporary Slowdown?" 
Revue de l'OFCE, Vol. 157, No. 3, 2018.

Akcigit, Ufuk, and Sina T. Ates, "What Happened to U.S. Business Dynamism?" National Bureau of Economic Research, April 2019.

Berkeley Economic Review, "The Japanese Economic Miracle," webpage, January 26, 2023. As of October 16, 2023: https://econreview.berkeley.edu/the-japanese-economic-miracle/
Alderucci, Dean, Lee G. Branstetter, E. Hovy, Andrew Runge, Maria Ryskina, and Nick Zolas, "Quantifying the Impact of AI on Productivity and Labor Demand: Evidence from U.S. Census Microdata," National Bureau of Economic Research, 2019.

Besley, Timothy, and Torsten Persson, "The Origins of State Capacity: 
Property Rights, Taxation, and Politics," *American Economic Review*, Vol. 99, No. 4, September 2009.

Alesina, Alberto, Sule Özler, Nouriel Roubini, and Phillip Swagel, 
"Political Instability and Economic Growth," Journal of Economic Growth, Vol. 1, No. 2, 1996.

Berger, Peter, ed., *The Desecularization of the World*, William Eerdmans, 
1999.

Allard, Gayle, Candace A. Martinez, and Christopher Williams, "Political Instability, Pro-Business Market Reforms and Their Impacts on National Systems of Innovation," *Research Policy*, Vol. 41, No. 3, 2012.

Berger, Peter, and Thomas Luckman, Modernity, Pluralism, and the Crisis of Meaning: The Orientation of Modern Man, Bertelsmann Foundation Publishers, 1995.

Allen, Jennifer, Baird Howland, Markus Mobius, David Rothschild, and Duncan J. Watts, "Evaluating the Fake News Problem at the Scale of the Information Ecosystem," *Science Advances*, Vol. 6, No. 14, April 3, 2020.

Bevir, Mark, "Governance," Encyclopedia Britannica, webpage, undated. As of October 16, 2023: https://www.britannica.com/topic/governance Altman, Steven A., and Caroline R. Bastian, "The State of Globalization in 2022," *Harvard Business Review*, April 12, 2022.

Bhutada, Govind, "The U.S. Share of the Global Economy over Time," Visual Capitalist, January 14, 2021.

Antonio, Robert J., "After Postmodernism: Reactionary Tribalism," 
American Journal of Sociology, Vol. 106, No. 1, 2000.

Bloom, David E., and Leo Zucker, "Aging Is the Real Population Boom," 
Finance & Development, 2022. 

Army Futures Command, Future Operational Environment: Forging the Future in an Uncertain World 2035–2050, U.S. Army, 2021.

Borghard, Erica D., "A Grand Strategy Based on Resilience," War on the Rocks, January 4, 2021.

Arsov, Ivailo, and Benjamin Watson, "Potential Growth in Advanced Economies," *Reserve Bank of Australia Bulletin*, December 2019. Azhar, Azeem, The Exponential Age: How Accelerating Technology Is Transforming Business, Politics and Society, Diversion Books, 2021.

Brackup, Julia, Sarah Harting, and Daniel Gonzales, Digital Infrastructure and Digital Presence: A Framework for Assessing the Impact on Future Military Competition and Conflict, RAND 
Corporation, RR-A877-1, 2022. As of October 17, 2023: 
https://www.rand.org/pubs/research_reports/RRA877-1.html Brady, Henry E., and Thomas B. Kent, "Fifty Years of Declining Confidence and Increasing Polarization in Trust in American Institutions," *Daedalus*, Vol. 151, No. 4, 2022.

Bremmer, Ian, "Globalization Isn't Dead: The World Is More Fragmented, but Interdependence Still Rules," *Foreign Affairs*, October 25, 2022. Brilliant, Myron, "10 Trends in 2022: Global Perspectives for Business," 
U.S. Chamber of Commerce, January 21, 2022.

Brodie, Bernard, and Fawn M. Brodie, From Crossbow to H-Bomb: The Evolution of the Weapons and Tactics of Warfare, Indiana University Press, 1973. Brower, Derek, and Amanda Chu, "The U.S. Plan to Become the World's Cleantech Superpower," *Financial Times*, February 16, 2023.

Bughin, Jacques, Jeongmin Seong, James Manyika, Michael Chui, and Raoul Joshi, "Notes from the AI Frontier: Modeling the Impact of AI on the World Economy," McKinsey & Company, September 4, 2018. Burgess, Matthew G., Amanda R. Carrico, Steven D. Gaines, Alessandro Peri, and Steve Vanderheiden, "Prepare Developed Democracies for Long-Run Economic Slowdowns," *Nature Human Behavior*, Vol. 5, No. 12, 2021. Burke, Sharon E., Llewelyn Hughes, Phung Quoc Huy, Kristin Vekasi, and Yu-Hsuan Wu, Critical Minerals: Global Supply Chains and Indo- Pacific Geopolitics, Special Report No. 102, National Bureau of Asian Research, 2022. Butkiewicz, James L., and Halit Yanikkaya, "The Impact of Sociopolitical Instability on Economic Growth: Analysis and Implications," *Journal of Policy Modeling*, Vol. 27, No. 5, 2005.

Butler, Declan, "Tomorrow's World," *Nature*, Vol. 530, February 2016.

Calo, Ryan, and Danielle Keats Citron, "The Automated Administrative State: A Crisis of Legitimacy," *Emory Law Journal*, Vol. 70, No. 4, 2021.

Carnegie Endowment, "How Chinese Technology Made China an Innovation Powerhouse," video, January 10, 2023. As of October 17, 2023: https://www.youtube.com/watch?v=VXJLKIzWYk8
Castells, Manuel, *The Information Age: The Rise of Society*, Vol. 1, Blackwell, 1996.

Castells, Manuel, *The Information Age: The Power of Identity*, Vol. 2, Blackwell, 1997.

Castells, Manuel, *The Information Age: End of Millennium*, Vol. 3, Blackwell, 1998.

Castillo, Rodrigo, and Caitlin Purdy, "China's Role in Supplying Critical Minerals for the Global Energy Transition: What Could the Future Hold?" Brookings Institution, August 1, 2022. Chandra, Siddharth, and Angela Williams, "The 'Revolution of Rising Expectations,' Relative Deprivation, and the Urban Social Disorders of the 1960s: Evidence from State-Level Data," *Social Science History*, Vol. 29, No. 2, 2005. 

Chazan, Guy, and Patricia Nilsson, "Germany Confronts a Broken Business Model," *Financial Times*, December 6, 2022. Chorzempa, Martin, The Cashless Revolution: China's Reinvention of Money and the End of America's Domination of Finance and Technology, PublicAffairs, 2022.

Chu, C. Y. Cyrus, Po-Ching Lee, and Chang Fa Lo, Ultimate Economic Conflict Between China and Democratic Countries, Routledge, 2022.

Chui, Michael, Matthias Evers, James Manyika, Alice Zheng, and Travers Nisbet, "The Bio Revolution: Innovations Transforming Economies, Societies, and Our Lives," McKinsey & Company, May 13, 2020. Cingolani, L., "The State of State Capacity: A Review of Concepts, Evidence and Measures," United Nations University, Maastricht Economic and Social Research Institute on Innovation and Technology, 2013. Cockburn, Iain M., Rebecca Henderson, and Scott Stern, "The Impact of Artificial Intelligence on Innovation," National Bureau of Economic Research, Working Paper No. 24449, March 2018. Collison, Patrick, and Michael Nielsen, "Science Is Getting Less Bang for Its Buck," *The Atlantic*, November 16, 2018. Congressional Budget Office, The Budget and Economic Outlook: 2022 to 2032, May 2022. Congressional Budget Office, *The 2022 Long-Term Budget Outlook*, July 
2022. Connors, Tom, "How China May Soon Lead the Bio-Revolution," Bloomberg, March 29, 2022. Crawford, Robert J., "Reinterpreting the Japanese Economic Miracle," 
Harvard Business Review, January–February 1998.

Damioli, Giacomo, Vincent Van Roy, and Daniel Vertesy, "The Impact of Artificial Intelligence on Labor Productivity," Eurasian Business Review, Vol. 11, 2021. De Vries, Jan, The Industrious Revolution: Consumer Behavior and the Household Economy, 1650 to Present, Cambridge University Press, 2008.

Deane, Phyllis, *The First Industrial Revolution*, Cambridge University Press, 1980.

Fletcher, Richard, Alessio Cornia, Lucas Graves, and Rasmus Kleis Nielsen, "Measuring the Reach of 'Fake News' and Online Disinformation in Europe," Reuters Institute and University of Oxford, February 2018.

DeLong, J. Bradford, "What Went Right in the 1990s? Sources of American and Prospects for World Economic Growth," Reserve Bank of Australia Conference Proceedings, 2000.

Frankel, Jeffrey, and Peter Orszag, "Retrospective on American Economic Policy in the 1990s," Brookings Institution, November 2, 2001.

Demsas, Jerusalem, "Why Does It Cost So Much to Build Things in America?" Vox, June 28, 2021.

Freeman, Rebecca, and Richard Baldwin, "Trade Conflict in the Age of COVID-19," *VoxEU*, May 22, 2020.

DiChristopher, Tom, "2018 Was Earth's Fourth-Hottest Year on Record, NOAA and NASA Report," CNBC.com, February 6, 2019.

French Republic, *National Strategic Review 2022*, 2022.

Dieppe, Alistair, "The Broad-Based Productivity Slowdown, in Seven Charts," World Bank, July 14, 2020.

Fukuyama, Francis, Trust: The Social Virtues and the Creation of Prosperity, Free Press, 1995.

Furman, Jason, and Robert Seamans, "AI and the Economy," National Bureau of Economic Research, Working Paper 24689, June 2018.

Diffenbaugh, Noah S., Deepti Singh, Justin S. Mankin, and Bala Rajaratnam, "Quantifying the Influence of Global Warming on Unprecedented Extreme Climate Events," Proceedings of the National Academy of Sciences, Vol. 114, No. 19, April 24, 2017.

Gaspar, Vitor, Paulo Medas, and Roberto Perrelli, "Global Debt Reaches a Record $226 Trillion," *International Monetary Fund* blog, December 15, 2021.

Dupuy, Trevor N., *The Evolution of Weapons and Warfare*, Da Capo Publishing, 1994.

Giddens, Anthony, *The Consequences of Modernity*, Stanford University Press, 1990.

Eberstadt, Nicholas, "With Great Demographics Comes Great Power: 
Why Population Will Drive Geopolitics," *Foreign Affairs*, July/August 
2019.

Giddens, Anthony, Modernity and Self-Identity: Self and Society in the Late Modern Age, Stanford University Press, 1991.

Eberstadt, Nicholas, "America Hasn't Lost Its Demographic Advantage: 
Its Rivals Are in Much Worse Shape," *Foreign Affairs*, May 24, 2021.

Giddens, Anthony, Runaway World: How Globalization Is Reshaping Our Lives, Routledge, 2000.

Edelman, *2023 Edelman Trust Barometer: Global Report*, 2023.

Gilding, Paul, The Great Disruption: Why the Climate Crisis Will Bring on the End of Shopping and the Birth of a New World, Bloomsbury, 2011.

Edwards, Chris, "Federal Debt and Unfunded Entitlement Promises," 
Cato at Liberty blog, January 21, 2022.

Giles, Chris, "'Friendshoring' Is a Risk to Growth and Financial Stability, Warns IMF," *Financial Times*, April 5, 2023.

"Excess Deaths Are Soaring as Health-Care Systems Wobble," The Economist, January 19, 2023.

Giles, Chris, "Global Economy Set for Years of Weak Growth, IMF Chief Kristalina Georgieva Warns," *Financial Times*, April 6, 2023.

"Fake News Flourishes When Partisan Audiences Crave It," The Economist, April 5, 2018.

Gluck, Carol, *Japan's Modern Myths: Ideology in the Late Meiji Period*, Princeton University Press, 1985.

Faroohar, Rana, Homecoming: The Path to Prosperity in a Post-Global World, Crown, 2022.

Gompert, David C., "National Security in the Information Age," Naval War College Review Vol. 51, No. 4, 1998.

Farrell, Henry, and Abraham L. Newman, "Weaponized Interdependence: How Global Economic Networks Shape State Coercion," *International Security*, Vol. 44, No. 1, 2019.

Goodheart, Charles, and Manoj Pradhan, The Great Demographic Reversal: Ageing Societies, Waning Inequality, and an Inflation Revival, Palgrave Macmillan, 2020.

Fedasiuk, Ryan, "Regenerate: Biotechnology and U.S. Industrial Policy," Center for a New American Security, July 28, 2022.

Gordon, Robert J., "Is U.S. Economic Growth Over? Faltering Innovation Confronts the Six Headwinds," National Bureau of Economic Research, August 2012.

Federal Reserve Bank of New York, "Global Supply Chain Pressure Index (GSCPI)," webpage, undated. As of October 17, 2023: https://www.newyorkfed.org/research/policy/gscpi#/overview Greeven, Mark J., Katherine Xin, and George S. Yip, "How Chinese Companies Are Reinventing Management," *Harvard Business Review*, March–April 2023. Gross, Jenny, "How Finland Is Teaching a Generation to Spot Misinformation," *New York Times*, January 10, 2023. Habermas, Jürgen, *Legitimation Crisis*, trans. by Thomas McCarthy, Beacon Press, 1975.

Hamel, Gary, and Michele Zanini, "Excess Management Is Costing the U.S. $3 Trillion per Year," *Harvard Business Review*, September 5, 2016.

Hanlon, Michael, "The Golden Quarter," Aeon, 2014. Hanspach, Philip, "Internet Infrastructure and Competition in Digital Markets," European University Institute Department of Economics, October 21, 2022. Harmstone, Andrew, "Five Sectors That Cannot Escape Climate Change," Morgan Stanley, February 2020. Hetherington, Marc J., "The Political Relevance of Political Trust," 
American Political Science Review, Vol. 92, No. 4, 1998.

Heylen, Frederik, Evelien Willems, and Jan Beyers, "Do Professionals Take Over? Professionalisation and Membership Influence in Civil Society Organisations," *Voluntas*, Vol. 31, 2020.

Hillman, Jonathan E., The Digital Silk Road: China's Quest to Wire the World and Win the Future, Harper Business, 2021.

Himmelfarb, Gertrude, Past and Present: The Challenges of Modernity, from the Pre-Victorians to the Postmodernists, Encounter Classics, 2017. Hobsbawm, Eric, Industry and Empire: The Birth of the Industrial Revolution, New Press, 1968. 

Hoehn, Andrew, and Thom Shanker, Age of Danger: Keeping America Safe in an Era of New Superpowers, New Weapons, and New Threats, Hachette Books, 2023. Hofman, Bert, "Will China's New Party Tech Commission Help or Hurt?" *Bert's Newsletter* blog, April 10, 2023.

Horowitz, Michael, Elsa B. Kania, Gregory C. Allen, and Paul Scharre, "Strategic Competition in an Era of Artificial Intelligence," Center for a New American Security, July 25, 2018.

"How the West Fell Out of Love with Economic Growth," The Economist, December 11, 2022.

Huang Yanzhong, "The Coronavirus Outbreak Could Disrupt the U.S. Drug Supply," Council on Foreign Relations, March 5, 2020.

Hundley, Richard, Past Revolutions, Future Transformations: What Can the History of Revolutions in Military Affairs Tell Us About Transforming the U.S. Military? RAND Corporation, MR-1029-DARPA, 1999. As of October 16, 2023: https://www.rand.org/pubs/monograph_reports/MR1029.html Igielnik, Ruth, "70% of Americans Say U.S. Economic System Unfairly Favors the Powerful," Pew Research Center, January 9, 2020.

Ignatius, David, "How the Algorithm Tipped the Balance in Ukraine," 
Washington Post, December 19, 2022.

Ikenberry, G. John, "The Triumph of Broken Promises: The End of the Cold War and the Rise of Neoliberalism," *Foreign Affairs*, February 28, 
2023. Ilchman, Warren F., "Review: Rising Expectations and the Revolution in Development Administration," *Public Administration Review*, Vol. 25, No. 4, December 1965.

Inglehart, Ronald F., Modernization and Postmodernization: Cultural, Economic, and Political Change in 43 Societies, Princeton University Press, 1997.

Inglehart, Ronald F., *Cultural Evolution*, Cambridge University Press, 
2018.

International Energy Agency, The Role of Critical Minerals in Clean Energy Transitions, May 5, 2021. International Energy Agency, Net Zero by 2050: A Roadmap for the Global Energy Sector, May 11, 2021. International Monetary Fund Fiscal Affairs Department, 2022 Global Debt Monitor, December 2022. Jacques, Martin, When China Rules the World: The End of the Western World and the Birth of a New Global Order, Penguin, 2012. Joint Chiefs of Staff, Joint Operating Environment 2035: The Joint Force in a Contested and Disordered World, July 14, 2016.

Jones, Charles I., "The End of Economic Growth? Unintended Consequences of a Declining Population," *American Economic Review*, Vol. 112, No. 11, 2022. Jones, Jeffrey M., "Confidence in U.S. Institutions Down; Average at New Low," Gallup, July 5, 2022. Jorgenson, Dale W., Kevin J. Stiroh, Robert J. Gordon, and Daniel E. Sichel, "Raising the Speed Limit: U.S. Economic Growth in the Information Age," *Brookings Papers on Economic Activity*, Vol. 2000, No. 1, 2000.

J. P. Morgan Asset Management, The Lifeboat Economy: Implications of a Fracturing World Economy, 2022.

Lipset, Seymour Martin, and William Schneider, "The Decline of Confidence in American Institutions," *Political Science Quarterly*, Vol. 98, No. 3, 1983.

Jyothi, Sangeetha Abdu, "Solar Superstorms: Planning for an Internet Apocalypse," Proceedings of Special Interest Group on Data Communication 2021, August 23–27, 2021.

Liu Chuyu and Johannes Urpelainen, "Why the United States Should Compete with China on Global Clean Energy Finance," Brookings Institution, January 7, 2021. Liu Zongyuan Zoe and Benn Steil, "Xi's Plan for China's Economy Is Doomed to Fail," *Foreign Affairs*, June 29, 2023.

Kattel, Rainer, Wolfgang Drechsler, and Erkki Karo, How to Make an Entrepreneurial State: Why Innovation Needs Bureaucracy, Yale University Press, 2022.

Lofgren, Eric, Whitney M. McNamara, and Peter Modigliani, Commission on Defense Innovation Adoption: Interim Report, Atlantic Council, April 2023.

Keane, Michael, and Haiqing Yu, "A Digital Empire in the Making: 
China's Outbound Digital Platforms," International Journal of Communication, Vol. 13. 2019.

Löwy, Michael, and Robert Sayre, Romanticism Against the Tide of Modernity, trans. by Catherine Porter, Duke University Press, 2001.

Kearney, Melissa, "America's Shrinking Population Is Here to Stay," 
Financial Times, December 29, 2022.

Macaskill, William, "The Beginning of History," *Foreign Affairs*, Vol. 101, No. 5, September–October 2022.

Kennedy, Paul, *The Rise and Fall of the Great Powers*, Penguin Random House, 1987.

Maddison, Angus, *The World Economy: A Millennial Perspective*, Organization for Economic Cooperation and Development, 2001.

Klein, Ezra, "The Story Construction Tells About America's Economy Is Disturbing," *New York Times*, February 5, 2023.

Manak, Inu, and Logan Kolas, "Supply Chains and Interdependence: Is This Really a Problem That Needs Solving?" Cato Institute, June 17, 2020.

Kim, Sung-Young, "National Competitive Advantage and Energy Transitions in Korea and Taiwan," *New Political Economy*, Vol. 26, No. 3, 
2021.

Manjoo, Farhad, *True Enough: Learning to Live in a Post-Fact Society*, John Wiley, 2008.

KOF Swiss Economic Institute, "KOF Globalisation Index," webpage, undated. As of October 18, 2023: https://kof.ethz.ch/en/forecasts-and-indicators/indicators/kofglobalisation-index.html Mann, Thomas E., and Norman J. Ornstein, The Broken Branch: How Congress Is Failing America and How to Get It Back on Track, Oxford University Press, 2008.

Krantz, Matt, "13 Firms Hoard $1 Trillion in Cash (We're Looking at You Big Tech)," *Investors Business Daily*, February 3, 2022.

Mau, Vladimir, and Tatiana Drobyshevskaya, "Modernization and the Russian Economy: Three Hundred Years of Catching Up," June 1, 2012.

Kuhn, Thomas S., *The Structure of Scientific Revolutions*, University of Chicago Press, 1996.

Mazarr, Michael J., "Time for a New Approach to Defense Strategy," 
War on the Rocks, July 29, 2021.

Ladurie, Emmanuel LeRoy, "Motionless History," *Social Science History*, Vol. 1, No. 2, 1977.

Lee, Kai-Fu, AI Superpowers: China, Silicon Valley, and the New World Order, Houghton, Mifflin, Harcourt, 2018.

Mazarr, Michael J., The Societal Foundations of National Competitiveness, RAND Corporation, RR-A499-1, 2022. As of October 16, 2023: https://www.rand.org/pubs/research_reports/RRA499-1.html McArdle, Megan, "From the Hoover Dam to the Second Avenue Subway, America Builds Slower," *Washington Post*, June 27, 2023.

Lehmann-Hasemeyer, Sibylle, Klaus Prettner, and Paul Tscheuschner, "The Scientific Revolution and Its Role in the Transition to Sustained Economic Growth," November 29, 2021.

McCarty, Michael, "What Happened to the Biotechnology Revolution?" 
Lancet, June 2, 2007.

Leruth, Luc, and Adnan Mazarei, "Who Controls the World's Minerals Needed for Green Energy?" Peterson Institute for International Economics, August 9, 2022.

McGrath, Michael, "Beyond Distrust: When the Public Loses Faith in American Institutions," *National Civic Review*, Vol. 106, No. 2, 2017.

McKinsey & Company, *Energy: A Key to Competitive Advantage*, 2009.

McNeill, William H., "The Care and Repair of Public Myth," Foreign Affairs, Fall 1982.

Meier, Kenneth J., Mallory Compton, John Polga-Hecimovich, Miyeon Song, and Cameron Wimpy, "Bureaucracy and the Failure of Politics: 
Challenges to Democratic Governance," *Administration and Society*, Vol. 51, No. 10, 2019.

Meredith, Sam, and Lucy Handley, "'It Is Entirely Doable, and It Is Doable Fast': Experts on How to Navigate the Energy Transition," CNBC.com, November 22, 2021.

Micklethwait, John, and Adrian Woolridge, The Fourth Revolution: The Global Race to Reinvent the State, Penguin Books, 2014.

Mizruchi, Mark S., *The Fracturing of the American Corporate Elite*, Harvard University Press, 2013.

Mokyr, Joel, The Lever of Riches: Technological Creativity and Economic Progress, Oxford University Press, 1992.

Mosleh, Mohsen, and David G. Rand, "Measuring Exposure to Misinformation from Political Elites on Twitter," Nature Communications, Vol. 13, No. 7144, 2022. 

Moss, Emily, Ryan Nunn, and Jay Shambaugh, "The Slowdown in Productivity Growth and Policies That Can Restore It," Hamilton Project Framing Paper, June 2020. Muhammed T., Sadiq, and Saji K. Mathew, "The Disaster of Misinformation: A Review of Research in Social Media," International Journal of Data Science Analysis, Vol. 13, 2022.

Murray, Brian, Jonas Monast, Chi-Jen Yang, and Justine Chow, "The United States, China, and the Competition for Clean Energy," Duke University Nicholas Institute for Environmental Policy Solutions, July 2011.

Nahm, Jonas, Collaborative Advantage: Forging Green Industries in the New Global Economy, Oxford University Press, 2021.

Nahm, Jonas, "Clean Energy Technology and U.S. Industrial Policy," Center for a New American Security, September 2022. National Centers for Environmental Information, "U.S. Climate Extremes Index Puts Extremes into Historical Context," webpage, updated December 2, 2021. As of October 18, 2023: https://www.ncei.noaa.gov/news/us-climate-extremes-index National Intelligence Council, Global Trends 2030: Alternative Worlds, December 2012.

National Intelligence Council, *Global Trends: Paradox of Progress*, January 2017.

National Intelligence Council, Global Trends 2040: A More Contested World, March 2021. Neal, Kohl, "How Bad Bureaucracy Sabotages Democracy," Current Affairs, December 16, 2020.

Neely, Christopher J., "Inflation and the Real Value of Debt: A Double-
Edged Sword," Federal Reserve Bank of St. Louis, August 1, 2022. Nightingale, Paul, and Paul Martin, "The Myth of the Biotech Revolution," *Trends in Biotechnology*, Vol. 22, No. 11, November 1, 2004. OECD—See Organization for Economic Cooperation and Development.

Oliner, Stephen D., and Daniel E. Sichel, "The Resurgence of Growth in the Late 1990s: Is Information Technology the Story," Board of Governors of the Federal Reserve System, April 2000.

Organization for Economic Cooperation and Development, The Next Production Revolution: Implications for Governments and Business, May 10, 2017. Organization for Economic Cooperation and Development, "Trust in Government," webpage, undated. As of October 17, 2023: https://www.oecd.org/governance/trust-in-government/ Orlik, Tom, Ana Galvao, and Scott Johnson, "A Practical Way to Make Sense of All the Shocks Hammering the Global Economy," Bloomberg Businessweek, November 3, 2022. Osterhammel, Jürgen, The Transformation of the World: A Global History of the Nineteenth Century, Princeton University Press, 2014.

Park, Michael, Erin Leahey, and Russell J. Funk, "Papers and Patents Are Becoming Less Disruptive over Time," *Nature*, Vol. 613, January 4, 
2023.

Perreault, Charles, "The Pace of Cultural Evolution," Public Library of Science One, Vol. 7, No. 9, September 14, 2012.

Pew Research Center, "Americans' Views of Government: Decades of Distrust, Enduring Support for Its Role," June 6, 2022. Pew Research Center, "Public Trust in Government: 1958–2022," June 6, 2022.

Porter, Michael, *The Competitive Advantage of Nations*, Harvard Business School Press, 1990. Posen, Adam S., "The End of Globalization? What Russia's War in Ukraine Means for the World Economy," *Foreign Affairs*, March 17, 
2022.

Prasad, Eswar S., The Future of Money: How the Digital Revolution Is Transforming Currencies and Finance, Harvard University Press, 2021. PricewaterhouseCoopers, Sizing the Prize: PwC's Global Artificial Intelligence Study: Exploiting the AI Revolution, 2017. 

Pummerer, Lotte, Robert Böhm, Lau Lilleholt, Kevin Winter, Ingo Zettler, and Kai Sassenberg, "Conspiracy Theories and Their Societal Effects During the COVID-19 Pandemic," Social Psychological and Personality Science, Vol. 13, No. 1, 2022.

Rachel, Łukasz, and Lawrence H. Summers, "On Secular Stagnation in the Industrialized World," *Brookings Papers on Economic Activity*, Spring 2019. Rainie, Lee, Scott Keeter, and Andrew Perrin, "Trust and Distrust in America," Pew Research Center, July 22, 2019.

Rauch, Jonathan, Demosclerosis: The Silent Killer of American Government, Random House, 1994.

Reinhart, Carmen M., Vincent R. Reinhart, and Kenneth S. Rogoff, "Public Debt Overhangs: Advanced-Economy Episodes Since 
1800," *Journal of Economic Perspectives*, Vol. 26, No. 3, 2012.

Rennert, Kevin, Frank Errickson, Brian C. Prest, Lisa Rennels, Richard G. Newell, William Pizer, Cora Kingdon, Jordan Wingernroth, Roger Cooke, Bryan Parthum, David Smith, Kevin Cromar, Delavane Diaz, Frances C. Moore, Ulrich K. Müller, Richard J. Plevin, Adrian E. Raftery, Hana Ševčiková, Hannah Sheets, James H. Stock, Tammy Tan, Mark Watson, Tony E. Wong, and David Anthoff, 
"Comprehensive Evidence Implies a Higher Social Cost of CO2," *Nature*, Vol. 610, 2022. Reza, Abeer, and Subrata Sarker, "Is Slower Growth the New Normal in Advanced Economies?" *Bank of Canada Review*, Autumn 2015.

Ritchie, Hannah, Lucas Rodés-Guirao, Edouard, Mathieu, Marcel Gerber, Esteban Ortiz-Ospina, Joe Hasell, and Max Roser, "Population Growth," Our World in Data, webpage, undated. As of October 23, 2023: https://ourworldindata.org/population-growth Ronfeldt, David, and John Arquilla, Whose Story Wins: Rise of the Noosphere, Noopolitik, and Information-Age Statecraft, RAND 
Corporation, PE-A237-1, 2020. As of October 18, 2023: https://www.rand.org/pubs/perspectives/PEA237-1.html Rosa, Hartmut, *Social Acceleration: A New Theory of Modernity*, trans. 

by Jonathan Trejo-Mathys, Columbia University Press, 2015.

Rossi, Emilio, "Deglobalization or Slowbalization?" *Aspenia Online*, May 16, 2022.

Roxborough, Ian, "Clausewitz and the Sociology of War," British Journal of Sociology, Vol. 45, No. 4, 1994.

Saad, Lydia, "Historically Low Faith in U.S. Institutions Continues," Gallup, July 6, 2023.

Scharre, Paul, "AI's Inhuman Advantage," *War on the Rocks*, April 20, 
2023.

Scheidel, Walter, The Great Leveler: Violence and the History of Inequality from the Stone Age to the Twenty-First Century, Princeton University Press, 2018. Schmidt, Eric, "Innovation Power: Why Technology Will Define the Future of Geopolitics," *Foreign Affairs*, March/April 2023. Scott, Jonathan, How the Old World Ended: The Anglo-Dutch-American Revolution 1500–1800, Yale University Press, 2019.

Serbu, Jared, "DoD Suddenly Abandons $374 Million Plan to Replace Defense Travel System," Federal News Network, June 1, 2023.

Shafik, Nemat, "A New Social Contract," International Monetary Fund Finance and Development, December 2018. Shapin, Steven, *The Scientific Revolution*, University of Chicago Press, 
1998. Shih, Willy C., "Are the Risks of Global Supply Chains Starting to Outweigh the Rewards?" *Harvard Business Review*, March 21, 2022.

Sitaraman, Ganesh, "A Grand Strategy of Resilience: American Power in the Age of Fragility," *Foreign Affairs*, September–October 2020. Skocpol, Theda, Diminished Democracy: From Membership to Management in American Civic Life, University of Oklahoma Press, 
2004.

Smith, Steven B., Modernity and Its Discontents: Making and Unmaking the Bourgeois from Machiavelli to Bellow, Yale University Press, 2018. Special Competitive Studies Project, Mid-Decade Challenges to National Competitiveness, September 2022.

Stenling, Cecilia, and Michael Sam, "Professionalization and Its Consequences: How Active Advocacy May Undermine Democracy," 
European Sport Management Quarterly, Vol. 20, No. 5, 2020. Steuerle, Eugene, "Restoring Fiscal Democracy," *Milken Institute Review*, First Quarter 2016. Stocking, Galen, Amy Mitchell, Katerina Eva Matsa, Regina Widjaya, Mark Jurkowitz, Shreenita Ghosh, Aaron Smith, Sarah Naseer, and Christopher St. Aubin, The Role of Alternative Social Media in the News and Information Environment, Pew Research Center, October 6, 2022. 

United Kingdom Ministry of Defence, Strategic Trends Programme: 
Global Strategic Trends—Out to 2045, 5th ed., 2014.

Suarez-Ledo, Victor, and Javier Alvarez-Galvez, "Prevalence of Health Misinformation on Social Media: Systematic Review," Journal of Medical Internet Research, Vol. 23, No. 1, January 2021.

Summers, Lawrence H., "The Age of Secular Stagnation: What It Is and What to Do About It," *Foreign Affairs*, February 15, 2016.

United Nations, "Population Division," webpage, undated. As of October 23, 2023: https://www.un.org/development/desa/pd/
Sunstein, Cass R., #republic: Divided Democracy in the Age of Social Media, Princeton University Press, 2017.

United Nations Human Development, Uncertain Times, Unsettled Lives: Shaping Our Future in a Transforming World, 2022.

Szczepański, Marcin, *Economic Impacts of Artificial Intelligence (AI)*, European Parliament, 2019. 

United Nations Human Rights Office of the High Commissioner, "About Good Governance," webpage, undated. As of October 16, 2023: https://www.ohchr.org/en/good-governance/about-good-governance Tai Ming Cheung, Innovate to Dominate: The Rise of the Chinese Techno-Security State, Cornell University Press, 2022.

U.S. Department of Defense, *2022 National Defense Strategy*, October 27, 
2022.

Tertrais, Bruno, "The Seven Demographic Trends That Will Determine Our Global Future," *World Politics Review,* August 24, 2021.

Thiele, Leslie Paul, "Twilight of Modernity: Nietzsche, Heidegger, and Politics," *Political Theory*, Vol. 22, No. 3, 1994.

U.S. Office of Management and Budget and Federal Reserve Bank of St. Louis, "Gross Federal Debt as Percent of Gross Domestic Product," webpage, undated. As of October 18, 2023: https://fred.stlouisfed.org/series/GFDGDPA188S
Van Creveld, Martin, Technology and War: From 2000 B.C. to the Present, Touchstone, 1991.

Thomson-DeVeaux, Amelia, and Zoha Qamar, "What Happens When Americans Don't Trust Institutions?" Five Thirty-Eight.com, July 8, 2022.

Verhagen, Paul, Esther Chavannes, and Frank Bekkers, Flow Security in the Information Age, The Hague Center for Strategic Studies, 2020.

Toh, Michelle, "Living Standards Are Still Falling in Japan. That's a Recipe for More Stagnation," CNN, April 12, 2023.

Vogel, Ezra F., *Japan as Number One: Lessons for America*, Harvard University Press, 1979.

Torsekar, Mihir, "Intermediate Goods Imports in Key U.S. Manufacturing Sectors," U.S. International Trade Commission, 2017.

Vollrath, Dietrich, Fully Grown: Why a Stagnant Economy Is a Sign of Success, University of Chicago Press, 2020.

Toulmin, Stephen, *Cosmopolis: The Hidden Agenda of Modernity*, University of Chicago Press, 1992.

Von Clausewitz, Carl, *On War*, ed. and trans. by Michael Howard and Peter Paret, Princeton University Press, 1976.

Toynbee, Arnold J., A Study of History: Abridgement of Volumes I–VI by D. C. Somervell, Oxford University Press, 1947.

Vries, Peer, Averting a Great Divergence: State and Economy in Japan, 1868–1937, Bloomsbury Academic, 2019.

Transparency International, "Global Corruption Barometer EU: People Worried About Unchecked Abuses of Power," June 15, 2021.

Transparency International, *Corruption Perceptions Index 2022*, 2023.

Wallach, Philip, "The Administrative State's Legitimacy Crisis," Center for Effective Management at Brookings, April 2016. Watney, Caleb, "But Seriously, How Do We Make an Entrepreneurial State?" *American Affairs*, Vol. VI, No. 4, Winter 2022.

Tsafos, Nikos, "China's Climate Change Strategy and U.S.-China Competition," testimony before the U.S.-China Economic and Security Review Commission, Center for Strategic and International Studies, March 17, 2022.

Weiss, Jason, and Dan Patt, "Software Defines Tactics," War on the Rocks, January 23, 2023.

Turchin, Peter, *Historical Dynamics: Why States Rise and Fall*, Princeton University Press, 2003.

The White House, "The Rising Costs of Extreme Weather Events," 
Council of Economic Advisors blog, September 1, 2022.

Tytell, Irina, Dirk Hofschire, and Jacob Weinstein, Secular Outlook for Global Growth: The Next 20 Years, Fidelity, 2023.

Williams, Heather J., and Caitlin McCulloch, Truth Decay and National Security: Intersections, Insights, and Questions for Future Research, RAND Corporation, PE-A112-2, 2023. As of October 18, 2023: https://www.rand.org/pubs/perspectives/PEA112-2.html Willis, Derek, and Paul Kane, "How Congress Stopped Working," ProPublica, November 5, 2018.

Wootton, David, The Invention of Science: A New History of the Scientific Revolution, Harper, 2015.

World Bank, "DataBank: Worldwide Governance Indicators," webpage, undated. As of October 17, 2023: https://databank.worldbank.org/source/ 
worldwide-governance-indicators World Economic Forum, *The Global Risks Report 2022,* 17th ed., 2022.

World Economic Forum, *The Global Risks Report 2023*, 18th ed., January 2023. Wrede, Insa, "Can the EU Do Without Metals from China?" DW.com, April 15, 2022. Wright, Nicholas, "How Artificial Intelligence Will Reshape the Global Order," *Foreign Affairs*, July 10, 2018.

Yan Xiao and Ziyang Fan, "3 Ways Digital Currencies Could Change Global Trade," World Economic Forum, January 13, 2022. Yeatman, William, "The Decline of Lawmaking Reflects a Diminished Congress," Cato Institute, May 10, 2021. Yeoh, Kenson, and Dingding Chen, "China-US Competition: Who Will Win the Digital Infrastructure Contest?" *The Diplomat*, December 21, 
2022.

## Acknowledgments

This paper represents the views of the authors and is partly informed by a wide range of RAND Corporation work on the future of the international order and strategic competition. The authors would like to thank James Baker and David Epstein in the Office of Net Assessment for supporting the research and providing thoughtful and helpful comments in various drafts; Jim Mitre of the RAND International Security and Defense Program for his assistance; and James Goldgeier and Karl Mueller for extraordinarily helpful peer reviews which substantially enhanced the final product.

## About This Paper

This paper is part of a larger project that considers the societal sources of national dynamism and competitive advantage. The paper investigates a specific idea in relation to societal sources of advantage: that competitive success in great-power rivalries comes, in part, from being effectively aligned to the demands of the essential characteristics of a historical era—the competitive paradigm of the period. This paper investigated that idea in more depth, offering one theory of an emerging competitive paradigm and defining measurable indicators of success. The paper contends that the emerging strategic paradigm is best understood as a governance revolution. This paper was completed in September 2023 and underwent security review with the sponsor before public release.

The RAND Corporation is a research organization that develops solutions to public policy challenges to help make communities throughout the world safer and more secure, healthier and more prosperous. RAND is nonprofit, nonpartisan, and committed to the public interest. 

Research Integrity Our mission to help improve policy and decisionmaking through research and analysis is enabled through our core values of quality and objectivity and our unwavering commitment to the highest level of integrity and ethical behavior. To help ensure our research and analysis are rigorous, objective, and nonpartisan, we subject our research publications to a robust and exacting quality-assurance process; avoid both the appearance and reality of financial and other conflicts of interest through staff training, project screening, and a policy of mandatory disclosure; and pursue transparency in our research engagements through our commitment to the open publication of our research findings and recommendations, disclosure of the source of funding of published research, and policies to ensure intellectual independence. For more information, visit www.rand.org/about/research-integrity. RAND's publications do not necessarily reflect the opinions of its research clients and sponsors. 

 is a registered trademark.

Limited Print and Electronic Distribution Rights This publication and trademark(s) contained herein are protected by law. This representation of RAND intellectual property is provided for noncommercial use only. Unauthorized posting of this publication online is prohibited; linking directly to its webpage on rand.org is encouraged. Permission is required from RAND to reproduce, or reuse in another form, any of its research products for commercial purposes. For information on reprint and reuse permissions, please visit www.rand.org/pubs/permissions.

For more information on this publication, visit www.rand.org/t/PEA2611-1.

© 2024 RAND Corporation www.rand.org RAND National Security Research Division This paper was completed within the International Security and Defense Policy Center of the RAND National Security Research Division (NSRD , which operates the RAND National Defense Research Institute (NDRI), a federally funded research and development center (FFRDC) sponsored by the Office of the Secretary of Defense, the Joint Staff, the Unified Combatant Commands, the Navy, the Marine Corps, the defense agencies, and the defense intelligence enterprise. This paper was made possible by NDRI exploratory research funding that was provided through the FFRDC contract and approved by NDRI's primary sponsor. For more information on the RAND International Security and Defense Policy Center, see www.rand/org/nsrd/isdp or contact the director (contact information is provided on the webpage).