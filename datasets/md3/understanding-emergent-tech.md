
## Perspective On A Timely Policy Issue Swaptik Chowdhury, Timothy Marler Understanding And Measuring Hype Around Emergent Technologies I

naccurate or excessive hype surrounding emerging technologies can have several negative effects. Appropriate policy can help manage this issue, but current methods for measuring technology hype are insufficient.

In recent years, there has been a remarkable surge of interest in emergent technologies, such as the metaverse and cryptocurrency.1 
Fueled by viral social media campaigns, celebrity endorsements, and high-profile success stories, cryptocurrencies, for example, captured the imagination of the masses. The meteoric rise (and fall) of Dogecoin—a cryptocurrency that was initially created as a meme—and the multimillion-dollar sale of virtual real estate in Decentraland exemplify the power of unfettered technology optimism and hype to transform abstract digital entities into cultural phenomena.2
When a new technology emerges, it is often met with a wave of eager anticipation and optimism. However, this optimism may be detached from actual utility and practical implications, potentially The United States needs a comprehensive approach to understanding and assessing public discourse–driven hype surrounding emerging technologies.

nologies. Hype-driven narratives can also influence public awareness of the benefits and risks of emerging technologies. Given the risks associated with technology hype, it is necessary to establish a mechanism for quantifying public discourse–driven technology hype.

Unfortunately, the evaluation of technology hype in literature has been limited to technical indicators, such as the number of patents and research papers associated with a given technology; assessment of public discourse–driven hype has yet to receive much attention. In response to this gap, we employed a systematic and comprehensive mixedmethods approach to understand and assess the public discourse–driven hype surrounding emerging technologies. This exploratory work provides a foundation for a more nuanced understanding of the relationships among emerging technologies, public perception, and policy decisions, ultimately facilitating informed decisionmaking in the face of a rapidly evolving technological landscape.

signifying a state of unfettered hype. Such unfounded enthusiasm can have detrimental consequences, fostering a climate of false optimism and erroneous decisionmaking. In turn, this can have a negative impact on critical research and development (R&D) portfolio planning, U.S. Department of Defense (DoD) acquisition, and national security. For R&D portfolio planning, the hype around emergent technologies can lead organizations to allocate excessive resources and investments without fully considering the technologies' long-term viability or potential risks. For DoD acquisition, hype can result in ill-informed procurement decisions and rushed acquisitions or partnerships without a thorough evaluation of a technology's operational value or alignment with strategic objectives. This can result in wasted resources and the adoption of technologies that do not adequately address military requirements. Perhaps most importantly, hype can have a potentially detrimental impact on national security. Hype may sway decisionmakers, who may lack deep technical expertise, by inflating expectations and fostering insufficient assessment of risks and vulnerabilities associated with emergent tech-

## A Systematic Approach For Measuring Technology Hype

Answering the following **four guiding questions** helps characterize hype around emergent technologies in the public discourse:

1. What are the constituting themes of technology 
hype?
2. What factors led to the emergence of technology 
hype in public discourse?
3. How do these factors influence the topological 
attributes of technology hype networks in public discourse?
4. How can these factors and attributes inform a quantitative metric to measure hype?
Text analysis is a technique that automatically extracts frequently used words and expressions from a body of text. natural language programming (NLP) is a branch of artificial intelligence that allows computers to understand and process human language. We used text analysis and NLP to extract meaningful insights from the textual content of the tweets in our dataset. We used *topic modeling* to identify latent themes in the tweets, and we used *keyword analysis* and *term frequency–inverse document frequency* (TF-IDF) 
analysis to validate the hype factors that we identified in our literature review.3 We used *sentiment analysis* to gain insight into the sentiment of the public discourse and help further validate the hype factors. Finally, we used network analysis and *community detection algorithms* to explore the connections and interactions among users discussing Web 3.0, which provided valuable insights into the patterns and dynamics of the public discourse.

A two-step process can help address these open questions:

## Consistent Factors Emerge

Seven consistent factors signaling technology hype in public discourse emerge from the literature:

1. **Step 1:** Conduct an environmental scan of the pertinent literature to identify defining themes and 
factors influencing hype about the technology in public discourse.
2. **Step 2:** Use **text and network analysis** to help validate the various factors.
1. market creation efforts by stakeholders 2. limited large-scale adoption despite widespread 
discussion
3. the influence of highly trafficked stories and 
narratives
4. lack of diversity in discourse, with a predominance 
of hopeful sentiment and fictional outcomes with the ability to shape actual outcomes
5. lack of notable improvements in productivity or 
employment despite the use of technology
With Step 1 of the approach, mixed methods address the first two questions and set the stage for Step 2, which addresses the second two research questions. With Step 2, we used Web 3.0 as a case study. After conducting a literature review to identify themes and factors in the hype surrounding Web 3.0, we analyzed a set of tweets from the microblogging site formerly known as Twitter to gain insights on the public discourse on Web 3.0. Figure 1 provides an overview of the analysis. 

| Method                 | Results   |
|------------------------|-----------|
| Literature review      |           |
| Definitions of hype    |           |
| Data                   | Method    |
| Twitter data           |           |
| on Web 3.0             |           |
| Natural language       |           |
| programming and        |           |
| network analysis       |           |
| - Downloaded using     |           |
| Brandwatch             |           |
| - TF-IDF analysis      |           |
| - Keywork analysis     |           |
| -                      |           |
| Time:                  |           |
| September 5–12, 2022   |           |
| - Topic modeling       |           |
| - Sentiment analysis   |           |
| -                      |           |
| Query terms:           |           |
| Web 3.0,               |           |
| Web3, web3, Web3.0,    |           |
| web3.0                 |           |
| - Network analysis and |           |
| community detection    |           |

6. overestimation of practical capabilities of the
technology
7. specific sentiments emerging in public discourse
about the technology, influenced by the coalescing hype that mediates societal expectations.

Results
Factors of hype
Results
Confirmation 
of hype factor

Again, different text analysis techniques (topic modeling, sentiment analysis, keyword analysis, and TF-IDF modeling) and standard network analysis methods (including community detection algorithms) can help validate these factors. In fact, initial work under Step 2 demonstrates that just a limited set of computational methods could confirm the presence of multiple factors associated with hype.

## Common Themes In Technology Hype Implications And Necessary Future Work

- Abstract final outcome of technology - Constitutive nature - High collective expectation 
- Shared rhetoric about the technology

## Notes

1 Zheng Yan, Rui Gaspar, and Tingshao Zhu, "How Humans Behave with Emerging Technologies During the COVID-19 Pandemic?" Human Behavior and Emerging Technologies, Vol. 3, No. 1, January 2021. 

2 Elizabeth Howcroft, "Virtual Real Estate Plot Sells for Record $2.4 
Million," Reuters, November 24, 2021.

Although hype has existed for centuries, accelerating technologies highlight its potentially detrimental impact. Hype driven by public discourse has been observed throughout history, with examples dating back to the Salem witch trials and tulip mania.4 Emerging technologies exacerbate the potentially detrimental impacts of inaccurate and excessive hype. Given the significant economic impact of current hype around emergent technologies, researchers and policymakers must more thoroughly understand the emergence of hype and its broader social, economic, and political implications.

3 TF-IDF is a statistical method in text analysis that measures the importance of a term within a document relative to a collection of documents. 

This work is an initial step toward developing a comprehensive understanding of technology hype, paving the way for further research and policy discussions. Future work could involve the following: 
4 John Felip Acevedo, "Witch-Hunts and Crime Panics in America," *Social Science Research Network*, March 3, 2019; Dian Ross, Edmond Cretu, and Victoria Lemieux, "NFTs: Tulip Mania or Digital Renaissance?" 2021 IEEE International Conference on Big Data (Big Data), December 15–18, 2021.  

1. refining the text analysis to gain deeper insights 
into how technology hype factors evolve over time
2. using the identified factors to create a supervised 
machine-learning model for classifying technology hype
3. examining how network features evolve as hype 
matures and identifying relationships among various network metrics to predict hype in diverse technological contexts.
By delving deeper into these aspects, we can uncover valuable insights into the factors driving technology hype and its societal implications. This knowledge will benefit academic researchers and inform policymakers and industry stakeholders as they navigate the complex landscape of emergent technologies and their societal impacts.

## About This Paper

As new technologies emerge with broadening dissemination and impact, their significance can often be overstated. The hype around these technologies can eclipse their actual value. Inaccurate hype can disrupt healthy public discourse and stifle development of effective regulations and policy. Yet, there are few, if any, systematic and objective approaches for assessing and thus mitigating hype. In the initial feasibility study described in this paper, we explored methods for measuring and quantifying public discourse-drive hype surrounding emerging technologies. This can help provide a more nuanced understanding of the relationships between emerging technologies, public perception, and policy decisions. 

Center for Scalable Computing and Analysis RAND Global and Emerging Risks is a division at RAND that develops novel methods and delivers rigorous research on potential catastrophic risks confronting humanity. This work was undertaken by the division's Center for Scalable Computing and Analysis, which supports the transformation of data science by fostering a community of expertise on best practices for the use of big data. For more information, visit www.rand.org/global-and-emerging-risks.html or contact the division director (contact information is provided on the webpage). 

## Funding Funding For This Work Was Provided By Gifts From Rand Supporters.

RAND is a research organization that develops solutions to public policy challenges to help make communities throughout the world safer and more secure, healthier and more prosperous. RAND is nonprofit, nonpartisan, and committed to the public interest. 

Research Integrity Our mission to help improve policy and decisionmaking through research and analysis is enabled through our core values of quality and objectivity and our unwavering commitment to the highest level of integrity and ethical behavior. To help ensure our research and analysis are rigorous, objective, and nonpartisan, we subject our research publications to a robust and exacting quality-assurance process; avoid both the appearance and reality of financial and other conflicts of interest through staff training, project screening, and a policy of mandatory disclosure; and pursue transparency in our research engagements through our commitment to the open publication of our research findings and recommendations, disclosure of the source of funding of published research, and policies to ensure intellectual independence. For more information, visit www.rand.org/about/research-integrity. RAND's publications do not necessarily reflect the opinions of its research clients and sponsors. 

 is a registered trademark.

Limited Print and Electronic Distribution Rights This publication and trademark(s) contained herein are protected by law. This representation of RAND intellectual property is provided for noncommercial use only. Unauthorized posting of this publication online is prohibited; linking directly to its webpage on rand.org is encouraged. Permission is required from RAND to reproduce, or reuse in another form, any of its research products for commercial purposes. For information on reprint and reuse permissions, please visit www.rand.org/pubs/permissions.

For more information on this publication, visit www.rand.org/t/PEA3137-1.

© 2024 RAND Corporation www.rand.org

## About The Authors

Swaptik Chowdhury is an assistant policy researcher at RAND. His research interests include climate change adaptation and mitigation, emergent technology, science and technology policy, and diversity, equity, and inclusion studies. Chowdhury is pursuing his doctoral degree in public policy and has a master's in structural engineering. 

Timothy Marler is a senior research engineer at RAND. His work revolves around emerging technology and science-and-technology policy, with a focus on multi-objective optimization, human modeling and simulation, advanced training technology, virtual reality/augmented reality, machine learning, biotechnology, and human-centered design. Marler holds a Ph.D. in mechanical engineering. 