
## The Era Of Coast Guards Combating Gray Zone Tactics Through An Indo-Pacific Combined Maritime Force Of Law Enforcement Partners T

he Indo-Pacific has become the most important region of the world, containing more than half of the global population and likely driving global growth in the next several decades.1 U.S. strategy in the region clearly declares the United States' commitment to a free and open international order upheld by partnerships and a shared vision.2 Despite renewed emphasis by Joe Biden's, Donald Trump's, and Barack Obama's administrations, the strategy to defend democratic principles and protect the rules-based paradigm has not made much progress as international norms slowly erode under China's pressure and its Belt and Road Initiative. For example, although China is a signatory to the United Nations (UN) Convention on the Law of the Sea (UNCLOS), it has ignored decisions by the UN 
arbitral tribunal that do not match the state's preferred resolution of events.3 Furthermore, in violation of UNCLOS, China passed legislation in 2021 allowing China's Coast Guard to use lethal force against foreign military ships operating in waters in what China considers to be under its jurisdiction.4 Moreover, China has empowered fishing vessels to operate as the People's Armed Forces Maritime Militia (PAFMM) 
and harass and forcefully coerce other vessels to comply with its wishes.5 To change the tide, the United States must look for new ways to assert influence and combat China's gray zone tactics—activities below armed conflict but above normal peacetime behaviors. The answer could lie in a combined maritime force (CMF) of international law enforcement partners.

The United States' use of instruments of national power is often constrained by a myopic focus on diplomatic, informational, military, and economic approaches, with a strong emphasis on the military and diplomatic elements and typically less weight on informational and economic measures.6 However, the often-neglected financial, intelligence, and law enforcement elements of national power offer critical means for the United States and its allies and partners to meet strategic objectives in the Indo-Pacific, especially given China's frequent use of these approaches to subvert international norms.7 As the United States strives for a free and open Indo-Pacific, the 

## Abbreviations

| APEC                                | Asia-Pacific Economic Cooperation      |
|-------------------------------------|----------------------------------------|
| ASEAN                               | Association of Southeast Asian Nations |
| CMF                                 | combined maritime force                |
| CMF-CGLE                            | combined maritime force of coast       |
| guards and law enforcement agencies |                                        |
| CTF                                 | combined task force                    |
| IPMDA                               | Indo-Pacific Partnership for Maritime  |
| Domain Awareness                    |                                        |
| IUU                                 | illegal, unreported, and unregulated   |
| JIATF                               | joint interagency task force           |
| NDAA                                | National Defense Authorization Act     |
| PAFMM                               | People's Armed Forces Maritime Militia |
| UN                                  | United Nations                         |
| UNCLOS                              | United Nations Convention on the Law   |
| of the Sea                          |                                        |

foundations of international law need to be reinforced as the bedrock of global relationships. In particular, sustained enforcement of established maritime legal principles is the best approach that democratic societies can employ to reinforce the rule of law in the Indo-Pacific. 

This paper describes how a CMF of international coast guards and maritime law enforcement agencies, under the leadership of the U.S. Coast Guard, might institute a shared vision focused on the enforcement of maritime law and international norms. First, it discusses China's gray zone tactics and why such tactics call for a CMF. Next, it defines CMF and discusses the potential benefits of such a force. 

Then it explains in more detail what such a force would look like in terms of its focus areas, leadership, and membership. It concludes with a brief discussion of next steps.

## China'S Gray Zone Tactics

A gray zone is an operational space between peace and war, involving coercive actions to change the status quo below a threshold that, in most cases, would prompt a conventional military response, often blurring the line between military and nonmilitary actions and the attribution of events.8 
The purpose of China's gray zone tactics is "to thwart, destabilize, weaken, or attack an adversary, and they are often tailored toward the vulnerabilities of the target state."9 
China's expansive nine-dash line claims most of the South China Sea, and the country's enforcement of its claimed territorial waters is a clear example of gray zone tactics.10 China's stance is misaligned with international norms—specifically, UNCLOS, of which China is a signatory. In 2016, the Hague-based Permanent Court of Arbitration unanimously declared that China's nine-dash line claim was "contrary to the Convention and without lawful effect to the extent that they [China's claims to rights] 
exceed the geographic and substantive limits of China's maritime entitlements under the Convention."11 Despite this ruling, China ignored the court—alleging that it was not relevant—and has continued to assert influence and defend its claim with the multipronged maritime forces of the People's Liberation Army Navy, China's Coast Guard, and the PAFMM.

Although China has officially disavowed the PAFMM, the militia is a key component of the country's strategy. It consists of a conglomeration of fishing vessels staffed with crew members who are armed, trained, and loyal to the government. The PAFMM uses intimidation, harassment, and other unlawful actions—including blocking and ramming other countries' vessels—to impose China's illegal claims but with plausible deniability from the Chinese government: "In the view of some observers, the PAFMM—even more than China's navy or coast guard—is the leading component of China's maritime forces for asserting its maritime claims, particularly in the [South China Sea]."12 Figure 1 illustrates the increasing number of PAFMM vessels operating near disputed islands in the South China Sea.

On August 28, 2023, the Ministry of Natural Resources of the People's Republic of China released a new ten-dash line, replacing previous maps and reemphasizing the country's claim to the South China Sea (Figure 2). The number of dashes in the region has changed between nine and eleven dashes since China's original claim in 1946, but the line continues to demarcate areas that China identifies as its historical waters.

Such actions by China's Coast Guard and the PAFMM 
are not only dangerous but also out of alignment with global agreements, including UNCLOS and the Convention on the International Regulations for Preventing Collisions at Sea, 1972. In one example from 2020, Chinese fishing vessels enforcing China's maritime claims near the disputed Paracel Islands placed the lives of 16 Vietnamese fishers in peril when the Chinese vessels rammed and nearly sank the fishing vessel, causing the crew to abandon ship.13 This behavior goes against the international rulesbased order and specifically against the obligation outlined in UNCLOS to ensure safety at sea. China uses these PAFMM actions as a gray zone tactic to intimidate other countries and will continue unless checked by a persistent and internationally backed coalition of law enforcement agencies that are engaged in upholding international law.

Although gray zone tactics are below the threshold of what would constitute acts of war, they can and should be addressed through legal avenues. UNCLOS stipulates that warships and government ships will be immune from the jurisdiction of foreign nations while on the high seas, but this immunity does not apply to the PAFMM, which consists of vessels that are classified as commercial or private, including registration by China as fishing vessels.14 When actions resulting in collision and death occur in the territorial seas of another country, Article 27 of UNCLOS stipulates that these actions are under the criminal jurisdiction of that country's laws "if the consequences of the crime extend to the coastal State; [or] if the crime is of a kind to disturb the peace of the country or the good order of the territorial sea."15
piracy off Africa following the 1856 Declaration of Paris.17 
Today, the U.S. Coast Guard and U.S. Navy routinely participate in international maritime coalitions on critical issues, including combating transnational criminal organizations under Joint Interagency Task Force (JIATF) South and JIATF West. The United States also leads the Bahrainbased CMF consisting of 38 member nations, "which exists to uphold the rules-based international order by countering illicit non-state actors on the high seas and promoting security, stability, and prosperity."18 
A significant advantage of the CMF concept is that the level of involvement is voluntary and scalable for each country. For example, the CMF based in Bahrain is a coalition of the willing and does not proscribe a specific level of participation from any member nation. The contribution from each country, therefore, varies depending on its ability to contribute assets and the availability of those assets at any given time.19 
Therefore, if PAFMM vessels are conducting criminal activities, as defined in UNCLOS, while operating in waters already determined by the Permanent Court of Arbitration to be territorial waters under a country's (e.g., the Philippines) jurisdiction, PAFMM crew members are subject to law enforcement action by that country's law enforcement agency. Enforcement against these activities would likely also be supported under the right of selfdefense and customary international law, which "permits a state to take reasonable measures to defend itself from aggressive threats to its political security or territorial integrity."16 Furthermore, the Convention for the Suppression of Unlawful Acts Against the Safety of Maritime Navigation provides another enforcement avenue against a vessel that commits violence against a person or damages another vessel. 

To proactively counter China's use of gray zone tactics and to increase U.S. influence in the Indo-Pacific, the United States must develop a structure to coordinate regional response options. The establishment of a combined force focused on maritime law enforcement and consisting of international coast guards and maritime law enforcement agencies working together to address illegal activity is a solution to the continued degradation of rules-based order and would underpin a free and open Indo-Pacific.

## What Is A Combined Maritime Force?

CMFs are not a new concept. International maritime coalitions have been in existence since the 19th century, when American, British, and French forces worked to eradicate Some countries might be able to provide only a single member to serve as a liaison officer, while others might be able to conduct training or provide ships and aircraft to support operations. Additionally, there does not have to be geographic restriction on which countries can join a CMF. For example, China supports antipiracy operations in coordination with the Bahrain-based CMF, and the Netherlands supports JIATF South counterdrug operations in the Caribbean.

There is currently no equivalent CMF operating in the Indo-Pacific, although countries work together intermittently to address specific issues. Ongoing law enforcement initiatives in the region include the Oceania Maritime Security Initiative, the Southeast Asia Maritime Law Enforcement Initiative, and the Southeast Asia Cooperation and Training exercise, but each operates independently without a common architecture or unifying organization. Another recent example is Operation Aiga, which was "designed to integrate [U.S.] Coast Guard capabilities and operations with our Pacific Island Country partners in order to effectively and efficiently protect shared national interests . . . and strengthen maritime governance on the high seas."20 These independent initiatives and unilateral efforts are valuable, but such activities could be improved by operating under the umbrella of a dedicated CMF construct that aligns comprehensive objectives and incorporates more partners. 

## Advantages Of A Law Enforcement–Led Combined Maritime Force Over A Military- Led Approach

gray zone threats in the Indo-Pacific require a different approach. The U.S. Navy is already fully engaged in the region, providing forward presence, theater ballistic missile defense, and military–military cooperation. Adding the responsibility of maritime law enforcement to the Navy's repertoire could divert its focus from its other missions and would likely heighten tensions between China and the United States. In addition, the creation of a CMF in the region has high potential of receiving a negative response from China; in this context, organizing an Indo-Pacific CMF under the purview of the 7th Fleet could compound tensions because of a perceived increase of militarization in the region. 

The situation in the Indo-Pacific in late 2023 calls for the employment of softer power that reduces the potential for armed conflict. An international coalition of maritime law enforcement agencies would be perfectly poised to demonstrate appropriate conduct by applying international standards in the maritime domain. Moreover, the U.S. Coast Guard possesses many capabilities that make it ideally suited to support the U.S. contribution in a leadership role. The service maintains tactical skill sets through its expert boarding parties and training teams, conducts routine operational engagement with partners in the region through various existing conventions and shiprider agreements, and supports a strategic approach outlined in its plans and in the President's goals for the Indo-Pacific.22 
The U.S. Coast Guard's diverse responsibilities beyond law enforcement, including its humanitarian mission, make it a palatable partner in the region, where countries are already wary of major power competition. Derek Grossman, a senior defense analyst at the RAND Corpora-
International coalitions have repeatedly demonstrated the ability to enhance multinational partnerships and develop synergies to tackle global issues in the maritime commons. The Bahrain-based CMF has demonstrated impressive efficacy and has basically eradicated piracy off the Horn of Africa, enforced UN sanctions around the region, stopped the flow of hundreds of thousands of pounds of narcotics, and, in 2021 alone, seized more than 8,700 illegal weapon systems and rocket-propelled grenades.21 As illustrated in Figure 3, the CMF's Combined Task Force (CTF) 151, Counter-Piracy, established in 2009, was instrumental in slashing the number of piracy attacks without a subsequent increase in illegal activity in the past decade.

Although the U.S. Navy has been successful in promoting U.S. presence and developing partnerships in the Middle East through the CMF based in Bahrain, the cooperation are likely to be welcomed throughout the region.23 
tion, recently testified to Congress about U.S. policy in the region, explaining that more needs to be done to build trust with Pacific Island states, who still believe Washington is primarily interested in geostrategic competition rather than helping them on issues of importance in the region, such as climate change, poverty alleviation, health security, and transnational crime. Softer forms of Although several countries possess dedicated coast guards, many have singular maritime services, which are more similar to the U.S. Coast Guard than to the U.S. Navy. These forces are focused more on sovereignty and coastal protection than on global force projection. Thus, concentrating on maritime law enforcement rather than on military 

## The Potential Benefits Of A Combined Maritime Force Of Coast Guards And Law Enforcement Agencies

The establishment of a regional, coalitional CMF of coast guards and law enforcement agencies (CMF-CGLE) would have three distinct advantages: (1) It would create a cohesive structure for addressing critical issues in the Indo- Pacific; (2) it would enhance partnerships that focus on reinforcing a rules-based order; and (3) it would counterbalance China's hegemonic approach. 

## A Cohesive Structure

competition makes sense. The U.S. Coast Guard also brings a mix of other types of authority and expertise important to Indo-Pacific partners and can easily operate alongside foreign militaries, coast guards, and maritime police. 

Several U.S. strategies already support the development of an international maritime law enforcement coalition in the Indo-Pacific.24 The United States routinely conducts patrols in Oceania with regional partners, including through its permanently based Coast Guard cutters in Guam. Eleven Pacific Island countries have signed shiprider agreements with the United States to share resources when conducting boardings to enforce laws and treaties.25 The U.S. Coast Guard has established a strong regimen of tailored training through its International Mobile Training Branch in support of the Pacific Partnership Strategy.26 Additionally, Japan-based U.S. Coast Guard Activities Far East provides routine maritime security assistance across the Indo-Pacific and Oceania through the International Port Security Program.27 In a show of trust and confidence in the U.S. Coast Guard, both Palau and the Federated States of Micronesia recently signed agreements that allow the United States to enforce maritime law on behalf of those countries without having a representative onboard.28 With the help of U.S. leadership, these sorts of agreements could be expanded under a coalition of countries. Aligning these bilateral efforts under a single multinational organization is a sensible solution to challenges in the Indo-Pacific. 

The development of a CMF-CGLE would build a muchneeded structure to consistently address critical issues in the Indo-Pacific. A cohesive organization would allow a more efficient exchange of information and a more effective distribution of resources to address challenges. The greatest current threat is illegal, unreported, and unregulated (IUU) fishing, as evidenced by the fact that 76 countries signed the Agreement on Port State Measures to Prevent, Deter and Eliminate Illegal, Unreported and Unregulated Fishing.29 In the 1980s and 1990s, a coalition of six countries joined together to enforce laws against illegal high-seas driftnet fishing under Operation North Pacific Guard. The U.S. Coast Guard reported that "our collective efforts have been overwhelmingly successful in nearly eliminating illegal high seas driftnet fishing in the North Pacific Ocean."30 The CMF based in Bahrain has also built unity of effort and shown effectiveness in the Middle East and nearly eliminated piracy.31 A collaborative structure of this sort in the Indo-Pacific could have a similar effect.

## Rules-Based Enforcement Partnerships

das. The PAFMM currently creates an asymmetric advantage for China, but building capacity and coordination now and fostering rules-based order through a CMF provide a bulwark against this asymmetry.

The creation of a CMF-CGLE would not be easy and is not guaranteed to be successful. China has already criticized a cooperative agreement between Taiwan and the U.S. Coast Guard, and increased international engagement could be perceived as an encroachment and threat to Chinese sovereignty and security.34 Increased presence could also result in miscommunication, accidents, or unplanned escalation by competing maritime forces. Despite these risks, investing in the region would demonstrate U.S. commitment and increased presence, which would allow the United States to provide constructive support to partners. This would likely lead to more-robust participation by the United States in decisions about governance architecture rather than letting China control the rules through coercion.

## The Foundation Of A Combined Maritime Force Of Coast Guards And Law Enforcement Agencies

The Indo-Pacific maritime domain has become an ungoverned and sometimes purposefully misgoverned region. In a report on security in the Indo-Pacific for the Daniel K. Inouye Asia-Pacific Center for Security Studies, Ben Crowell and Wade Turvold noted that "there must be an international effort to bring governance to these presently ungoverned sea spaces."32 The development of an international coalition of coast guard and law enforcement agencies would be built by like-minded countries that govern under a rules-based approach and use enforcement to emphasize agreed-upon international norms. Without the looming consequence of enforcement backed by regional partners, "[c]onfrontations between increasingly militarized fishing fleets from China, Vietnam, and the Philippines will likely continue—if not increase in frequency— creating the potential for inadvertent escalation into a regional conflict."33 Experience has shown that, when a white-hulled ship with a red racing stripe (i.e., anything resembling the Coast Guard) appears on the horizon, vessels cease engaging in illegal activity. 

## Gray Zone Tactic Counterbalance

The CMF in Bahrain consists of five task forces that are geographic or mission driven.35 The creation of a CMF-
CGLE should follow a similar structure of diverse CTFs but focused on the most-pressing threats to the Indo-Pacific. 

The road to success in building strong international partnerships and an effective CMF-CGLE is to build a solid foundation focused on the issues that are important to the region and developing agreement about how to address these challenges. Two key regional bodies in the Indo-
The third, and possibly most significant, advantage of a CMF-CGLE is that the presence and influence of a multinational force will create a unified front against China's hegemonic activities. As previously noted, China has used gray zone tactics to advance its political and strategic agen-
South Asia, Southeast Asia, and the Pacific Islands using advanced commercial satellite data.38 
Pacific are the Association of Southeast Asian Nations (ASEAN) and the Asia-Pacific Economic Cooperation (APEC). ASEAN lists its maritime security priority areas of cooperation as follows:

- Shared Awareness and Exchange of Information 
and Best Practices
- Confidence Building Measures based on International and Regional Legal Frameworks, Arrangements and Cooperation including the 1982 UNCLOS, and
- Capacity Building and Enhancing Cooperation 
of Maritime Law Enforcement Agencies in the 
Region.36
The APEC Ocean and Fisheries Working Group lists combating IUU fishing as its top priority.37 Given the priorities of these regional organizations, the logical foundation for a CMF-CGLE would center on the establishment of three dedicated CTFs focused on The development of comprehensive maritime domain awareness is critical when establishing a law enforcement presence. Understanding the type, location, and activities of maritime vessels is a critical step before applying limited enforcement resources across a vast ocean. This approach has already been successful in identifying illegal fishing by Chinese vessels in Oman's exclusive economic zone despite the vessel operators' efforts to conceal their electronic transponder locations.39 In addition to providing these data to the four fusion centers in India, Singapore, Solomon Islands, and Vanuatu, IPMDA should be closely linked with this first CTF focused on information and maritime domain awareness. With additional intelligence provided by international partners, this CTF could provide a better picture and understanding of what is happening in the region to support operations by deployed assets and regional law enforcement agencies.

- information and maritime domain awareness
- training, exercises, and capacity-building 
- countering IUU fishing. 

## Training, Exercises, And Capacity-Building Information And Maritime Domain Awareness

Concerning the first task force, President Biden outlined a way forward when he directed U.S. federal agencies to design and facilitate the implementation of a multi lateral initiative, the Indo-Pacific Partnership for Maritime Domain Awareness (IPMDA). [The purpose of this partnership is] to strengthen maritime domain awareness and maritime security in The second task force would focus on building the capacity of regional partners. Even when a nation-state has the will, it might lack the background, expertise, or resources to fully enforce laws within its jurisdiction. A core tenet of maritime security is that all countries are interested primarily in protecting their own resources and waters and can then add value to a joint approach. To enable success, the international community should establish a dedicated arm focused on training and exercises to build this capability, competency, and consistency for the least capable countries. Structured training programs and exercises are critical components of developing capabilities. A systematic approach under a CMF-CGLE would allow international partners to participate in a persistent training and exercise regimen that is coordinated, complementary, and concentrated on priority concerns. Classroom training can be reinforced through on-the-job operational training implemented through shiprider agreements on afloat resources performing duties under an operational CTF.

## Countering Illegal, Unreported, And Unregulated Fishing

ing is not just about fish: it is a multi-faceted problem that covers other core policy concerns, including human rights, food security, and maritime security."42 A CMF-CGLE 
would create the foundation for addressing the core components of combating IUU fishing, including informationsharing, joint operations, and international prosecution. 

A cohesive and coordinated maritime force in the region could also serve to quickly transition to search-and-rescue service, environmental pollution response, or a natural disaster–relief response force. Moreover, such a force would establish the foundation for a robust law enforcement presence to respond to UNCLOS violations and enforce international norms.

## Leadership And Membership Of A Combined Maritime Force Of Coast Guards And Law Enforcement Agencies Leadership

The third task force of the CMF-CGLE and the first component consisting of operational assets should focus on IUU-fishing enforcement and creating sustainable fisheries. In 2020, according to statistics from the Food and Agriculture Organization of the United Nations, 85 percent of the world's fishers and aquaculture workers are in Asia, and the waters around the continent account for more than 
60 percent of the global catch.40 The top locations for illegal fishing were in the western, central, and South Pacific (see Figure 4).41 Dwindling fish stocks in the Indo-Pacific are a global problem, and the actions that countries perform independently of one another are not enough. Commercial fishing activity is not expected to decrease, and fish stocks will continue to decline without a coordinated methodology. The development of a CMF-CGLE would create the beginnings of a unified approach to address the IUU- fishing problem and the depletion of fish stocks. 

The establishment of a CMF-CGLE would do more than just build a coalition of countries to address IUU fishing. As highlighted in the National Oceanic and Atmospheric Administration's strategy, "addressing IUU fish-
Creating a CMF-CGLE offers a key opportunity for the United States to show commitment and leadership while building partnerships that focus on the issues that matter most in the Indo-Pacific region. As implied earlier in this paper, the United States should consider coordinating with regional partners on establishing a U.S. Coast Guard admiral as the CMF-CGLE leader.43 The Coast Guard has already committed to supporting Indo-Pacific operations through its routine patrols by national security cutters, law enforcement detachment deployments, and home-porting fast response cutters in the region. Having the U.S. Coast Guard as the lead would be a natural fit and would likely promote unity of effort among other countries that are already engaged in multilateral Coast Guard forums, including in the north Pacific and the Arctic and the ASEAN Coast Guard Forum. Indo-Pacific and Oceania countries would likely agree to this sort of approach. As described previously, the Coast Guard already engages in multinational exercises, including the Southeast Asia Maritime Law Enforcement Initiative and Southeast Asia Cooperation and Training, and a combined multinational force with U.S. backing provides opportunities for countries without robust maritime law enforcement agencies to provide other contributions and reap the benefits of CMF- CGLE operations. 

## Membership

tunity to participate and cooperate with the U.S. Coast Guard and U.S. Navy with significant capability, including serving as on-scene leaders supporting CMF-CGLE missions. Moreover, in February 2023, the United States and the Philippines restarted joint patrols in the South China Sea, further enabling a coordinated approach to an operational arm of a CMF-CGLE. 

The eventual goal is to have Indo-Pacific countries participate in a CMF-CGLE with the purpose of a free and transparent Indo-Pacific. Some countries might be concerned about upsetting the balance with China and hesitate to join a coalition, but I submit that a large majority of countries would support this approach. Moreover, focusing on rules-based order through law enforcement with an emphasis on maritime domain awareness, training, and countering IUU fishing could temper that concern.

## Resource Contributions

Like with other CTFs and joint task forces, although the CMF-CGLE would be led by the United States, the task forces would be led by revolving leaders selected from member countries. The Quad countries (Australia, India, Japan, and the United States) are already invested in the IPMDA and would be likely to join.44 Pacific Island nations would likely make fish-stock concerns a high priority and seek to become part of a coalition. Grossman has noted that, "for American Samoa, in particular, Chinese IUU fishing activities have depleted tuna stocks within its maritime EEZ [exclusive economic zone] and disrupted the local economy."45 Countries that are parties to existing international conventions—of which there are many— would also likely be interested in participating. In addition, the U.S. Coast Guard has already transferred ownership of former high endurance cutters to the Philippines, Vietnam, Bangladesh, and Sri Lanka. These ships provide the oppor-
Like with other international coalitions, funding for a CMF-CGLE should be spread across participating countries to the level that each can afford. For the United States, Congress might have already provided an avenue for funding through the Maritime Security and Fisheries Enforcement (SAFE) Act as part of the 2020 National Defense Authorization Act (NDAA).46 This legislation directed agencies to focus on combating IUU fishing, including assessing opportunities to use the CMF in Bahrain and to create partnerships in priority regions. Building on this legislation—and potentially in coordination with the Pacific Deterrence Initiative47—Congress has an opportunity to authorize and appropriate funds for the establishment of a CMF-CGLE in support of the U.S. Indo-Pacific and House committees should build on this language and authorize and appropriate funds for the establishment of a new CMF-CGLE to focus on law enforcement as a key enabler of the vision of a free and open Indo-Pacific. 

## Notes

1 White House, "Fact Sheet."
strategy. Recognizing that smaller countries might not be able to marshal resources, discussions about reinvestment of ship seizure sales, judicial proceeds, and profits from captured biomass of participating countries should be considered to offset costs. This could help incentivize the involvement of these countries, particularly those "at high risk for IUU fishing activity . . . [and that] lack the capacity to fully address the illegal activity."48
2 National Security Council, *Indo-Pacific Strategy of the United States*. 3 Campbell and Salidjanova, "South China Sea Arbitration Ruling." 

## Conclusion

4 Japan Ministry of Defense, "The Coast Guard Law of the People's Republic of China." 
5 Grossman and Ma, "A Short History of China's Fishing Militia and What It May Tell Us." 
6 Joint Chiefs of Staff, *Strategy*. Joint Doctrine Note 1-18 highlights that diplomatic, informational, military, and economic measures have long been the primary instruments of national power, but there are other options, including financial, intelligence, and law enforcement instruments.

7 China uses primarily nonmilitary and financial sources to influence countries in the region. For a more in-depth discussion, see Paul et al., A Guide to Extreme Competition with China.

8 Morris et al., *Gaining Competitive Advantage in the Gray Zone*, p. 8. 9 Forward-defense experts, "Today's Wars Are Fought in the 'Gray Zone.'" 
10 *Nine-dash line* refers to China's claim to waters in the South China Sea; China claims maritime rights over everything inside that line despite its overlap with other countries' claims. For more information, see, for example, "How the 'Nine-Dash Line' Fuels Tensions in the South China Sea."
11 In the Matter of the South China Sea Arbitration Before an Arbitral Tribunal Constituted Under Annex VII to the 1984 United Nations Convention on the Law of the Sea Between the Republic of the Philippines and the People's Republic of China, Chapter V, Section (F)(1)(b)215.

The United States is losing its strategic influence in the Indo-Pacific region by allowing China to conduct gray zone activities unchecked and failing to provide consistent leadership on priority issues. The United States must act on its rhetoric outlined in its Indo-Pacific strategies and implement a CMF-CGLE that sets the standard for acceptable conduct and enforces transgressions with measurable consequences. Establishing a CMF-CGLE focused on law enforcement as a soft-power approach would provide a cohesive structure, improve partnerships, and push back against China's hegemonic ambitions. 

The road to success in building strong international partnerships and an effective CMF-CGLE is to build a solid foundation focused on the issues that are important to the region and to develop agreement about how to address these challenges. A CMF-CGLE can address priority issues in the region, including maritime domain awareness, capacity-building, and countering IUU fishing.

Congress should fund a CMF-CGLE as soon as possible as a first step and bipartisan issue. Congress already authorized the expansion of "counter-IUU fishing as part of the mission of the Combined Maritime Forces."49 Senate 
13 "Chinese Vessel Rams Vietnamese Fishing Boat in S. China Sea." 
29 Agreement on Port State Measures to Prevent, Deter and Eliminate Illegal, Unreported and Unregulated Fishing.

14 Manullang, Siswandi, and Dewi, "The Status of Maritime Militia in the South China Sea Under International Law Perspective."
30 U.S. Coast Guard, Illegal, Unreported, and Unregulated Fishing Strategic Outlook, p. 2. 

15 UNCLOS, Part II, Section 3, Subsection B, Article 27(1).

31 Combined Maritime Forces, "Combined Maritime Forces Chairs the 
35th SHADE Counter-Piracy Conference." 
16 Reuland, "Interference with Non-National Ships on the High Seas," 
p. 1206.

17 Gale, "Barbary's Slow Death"; Jenkins, "Piracy." 
32 Crowell and Turvold, "Illegal, Unreported, and Unregulated Fishing and the Impacts on Maritime Security," p. 214.

18 Combined Maritime Forces, homepage. 

33 Apling et al., "Pivoting the Joint Force." 
19 Combined Maritime Forces, homepage.

34 "China Denounces US–Taiwan Coast Guard Cooperation Agreement." 
20 U.S. Coast Guard, "Coast Guard Cutter Completes Operation Aiga 
'23." 
21 U.S. Naval Forces Central Command, "Record Seizures in 2021 After NAVCENT and CMF Increase Patrols." 
35 The five task forces are CTF 150, Maritime Security; CTF 151, Counter-Piracy; CTF 152, Gulf Maritime Security; CTF 153, Red Sea Maritime Security; and CTF 154, Maritime Security Training. CTF 154 was established in May 2023 and is based in and focuses on the Middle East. See Combined Maritime Forces, homepage. 

36 ASEAN, "Priority Areas of Cooperation." 
22 National Security Council, *Indo-Pacific Strategy of the United States*. 

Only one agency—the U.S. Coast Guard—is specifically mentioned in the entire strategy document.

37 APEC, "Ocean and Fisheries." 
23 Grossman, Chinese Strategy in the Freely Associated States and American Territories in the Pacific, p. 11.

38 Biden, "Combating Illegal Unreported, and Unregulated Fishing and Associated Labor Abuses." 

39 "Keeping Tabs on China's Murky Maritime Manoeuvres." 
24 National Security Council, *Indo-Pacific Strategy of the United States*; 
White House, *Pacific Partnership Strategy of the United States*; U.S. Department of Defense, The Department of Defense Indo-Pacific Strategy Report. 

40 Food and Agricultural Organization of the United Nations, The State of World Fisheries and Aquaculture. 

25 U.S. Mission China, "Pacific Islands Forum." These countries are Cook Islands, Federated States of Micronesia, Fiji, Kiribati, Marshall Islands, Nauru, Palau, Samoa, Tonga, Tuvalu, and Vanuatu.

41 Grossman, Chinese Strategy in the Freely Associated States and American Territories in the Pacific.

42 U.S. Interagency Working Group on IUU Fishing, National 5-Year Strategy for Combating Illegal, Unreported, and Unregulated Fishing 2022–2026, p. 7. 

26 U.S. Coast Guard Forces Micronesia/Sector Guam, "U.S. Coast Guard Forces Micronesia Sector/Guam's [sic] Fast Response Cutters Bolster Pacific Partnership Strategy and Strengthen Pacific Island Country Relations."
27 U.S. Coast Guard Pacific Area, "International Port Security Program." 
43 Although an analysis of this issue was beyond the scope of this paper, this arrangement would be challenging for the U.S. Coast Guard in its current structure. A reorganization might be necessary, including revisiting the JIATF West organization, which is headed by a Coast Guard admiral but operates under the umbrella of U.S. Indo-Pacific Command.

28 U.S. Coast Guard, "U.S. and Republic of Palau Sign Agreement to Strengthen Ties with New Chapter in Maritime Security and Stewardship in the Pacific."
44 "The partnership is known formally as the 'Quad', not the Quadrilateral Security Dialogue, noting its nature as a diplomatic, not security, partnership" (Department of Foreign Affairs and Trade, "The Quad").

45  Grossman, Chinese Strategy in the Freely Associated States and American Territories in the Pacific, p. 9.

46  Public Law 116-92, NDAA for Fiscal Year 2020, Division C, Title XXXV, Subtitle C. 

47  U.S. Department of Defense, *Pacific Deterrence Initiative*. 48  U.S. Interagency Working Group on IUU Fishing, National 5-Year Strategy for Combating Illegal, Unreported, and Unregulated Fishing 2022–2026, p. 5. 

49  Public Law 116-92, NDAA for Fiscal Year 2020, Section 3544(a)(3).

## References

Agreement on Port State Measures to Prevent, Deter and Eliminate Illegal, Unreported and Unregulated Fishing, rev. ed., approved November 22, 2009, entered into force June 2016. 

APEC—See Asia-Pacific Economic Cooperation.

Apling, Scott C., Martin Jeffery Bryant, James A. Garrison, and Oyunchimeg Young, "Pivoting the Joint Force: National Security Implications of Illegal, Unregulated, and Unreported Fishing," Joint Force Quarterly, Vol. 107, Fourth Quarter 2022. 

ASEAN—See Association of Southeast Asian Nations.

Asia Maritime Transparency Initiative, "The Ebb and Flow of Beijing's South China Sea Militia," November 9, 2022. Asia-Pacific Economic Cooperation, "Ocean and Fisheries," webpage, last page update September 2021. As of September 5, 2023: https://www.apec.org/groups/ 
som-steering-committee-on-economic-and-technical-cooperation/ working-groups/ocean-and-fisheries Association of Southeast Asian Nations, "Priority Areas of Cooperation," webpage, undated. As of September 5, 2023: https://asean.org/our-communities/ 
asean-political-security-community/peaceful-secure-and-stable-region/ maritime-security/ 
Biden, Joseph R., Jr., "Combating Illegal, Unreported, and Unregulated Fishing and Associated Labor Abuses," memorandum for the Secretaries of State, Treasury, Defense, Commerce, Labor, and Homeland Security; the attorney general; the U.S. Trade Representative; the U.S. representative to the United Nations; the director of the Office of Science and Technology Policy; and the administrator of the U.S. Agency for International Development, National Security Memorandum 11, White House, June 27, 2022. 

Caldwell, Stephen L., and Chris P. Currie, Maritime Security: Ongoing U.S. Counterpiracy Efforts Would Benefit from Agency Assessments, U.S. 

Government Accountability Office, GAO-14-422, June 2014. Campbell, Caitlin, and Nargiza Salidjanova, "South China Sea Arbitration Ruling: What Happened and What's Next?" issue brief, U.S.–China Economic and Security Review Commission, July 12, 2016. 

Cheng, Dean, "China," *2023 Index of U.S. Military Strength*, Heritage Foundation, October 18, 2022. "China Denounces US–Taiwan Coast Guard Cooperation Agreement," Associated Press, March 26, 2021. "Chinese Vessel Rams Vietnamese Fishing Boat in S. China Sea," 
Maritime Executive, June 14, 2020. 

Combined Maritime Forces, homepage, undated. As of September 5, 2023: https://combinedmaritimeforces.com/ Combined Maritime Forces, "Combined Maritime Forces Chairs the 35th SHADE Counter-Piracy Conference," webpage, April 26, 2015. As of September 27, 2023: https://combinedmaritimeforces.com/2015/04/26/combined -maritime-forces-chairs-the-35th-shade-counter-piracy-conference/ Convention for the Suppression of Unlawful Acts Against the Safety of Maritime Navigation, signed March 10, 1988, entered into force March 1, 1992. Convention on the International Regulations for Preventing Collisions at Sea, 1972, adopted October 20, 1972, entered into force July 15, 1977. Crowell, Ben, and Wade Turvold, "Illegal, Unreported, and Unregulated Fishing and the Impacts on Maritime Security," in Alexander L. Vuving, ed., Hindsight, Insight, Foresight: Thinking About Security in the Indo- Pacific, Daniel K. Inouye Asia-Pacific Center for Security Studies, September 2020. Department of Foreign Affairs and Trade, Australian Government, "The Quad," webpage, undated. As of November 14, 2023: https://www.dfat.gov.au/international-relations/regional-architecture/ quad Food and Agricultural Organization of the United Nations, The State of World Fisheries and Aquaculture: Towards Blue Transformation, 2022. 

Forward-defense experts, Atlantic Council, "Today's Wars Are Fought in the 'Gray Zone.' Here's Everything You Need to Know About It," blog post, February 23, 2022. As of September 5, 2023: https://www.atlanticcouncil.org/blogs/new-atlanticist/ todays-wars-are-fought-in-the-gray-zone-heres-everything-you-need -to-know-about-it/ 
Gale, Caitlin M., "Barbary's Slow Death: European Attempts to Eradicate North African Piracy in the Early Nineteenth Century," 
Journal for Maritime Research, Vol. 18, No. 2, 2016. Grossman, Derek, Chinese Strategy in the Freely Associated States and American Territories in the Pacific: Implications for the United States, RAND Corporation, CT-A2768-1, 2023. As of September 5, 2023: https://www.rand.org/pubs/testimonies/CTA2768-1.html Grossman, Derek, and Logan Ma, "A Short History of China's Fishing Militia and What It May Tell Us," RAND *Blog*, April 6, 2020. As of November 5, 2023: https://www.rand.org/pubs/commentary/2020/04/ a-short-history-of-chinas-fishing-militia-and-what.html 
"How the 'Nine-Dash Line' Fuels Tensions in the South China Sea," The Economist, February 10, 2023. In the Matter of the South China Sea Arbitration Before an Arbitral Tribunal Constituted Under Annex VII to the 1982 United Nations Convention on the Law of the Sea Between the Republic of the Philippines and the People's Republic of China, Permanent Court of Arbitration Case 2013-19, award, July 12, 2016. Japan Ministry of Defense, "The Coast Guard Law of the People's Republic of China," webpage, undated. As of September 27, 2023: https://www.mod.go.jp/en/d_act/sec_env/ch_ocn/index.html Jenkins, John Philip, "Piracy," *Encyclopaedia Britannica*, last updated August 14, 2023. 

Joint Chiefs of Staff, *Strategy*, Joint Doctrine Note 1-18, April 25, 2018.

"Keeping Tabs on China's Murky Maritime Manoeuvres," The Economist, August 15, 2023. 

Manullang, Novena Clementine, Achmad Gusman Siswandi, and Chloryne Trie Isana Dewi, "The Status of Maritime Militia in the South China Sea Under International Law Perspective," Jurnal Hukum IUS QUIA IUSTUM, Vol. 27, No. 1, January 2020.

Morris, Lyle J., Michael J. Mazarr, Jeffrey W. Hornung, Stephanie Pezard, Anika Binnendijk, and Marta Kepe, Gaining Competitive Advantage in the Gray Zone: Response Options for Coercive Aggression Below the Threshold of Major War, RAND Corporation, RR-2942-OSD, 
2019. As of September 5, 2023: https://www.rand.org/pubs/research_reports/RR2942.html National Security Council, Executive Office of the President, Indo- Pacific Strategy of the United States, February 2022. O'Rourke, Ronald, U.S.–China Strategic Competition in South and East China Seas: Background and Issues for Congress, Congressional Research Service, R42784, version 143, June 5, 2023. Paul, Christopher, James Dobbins, Scott W. Harold, Howard J. Shatz, Rand Waltzman, and Lauren Skrabala, A Guide to Extreme Competition with China, RAND Corporation, RR-A1378-1, 2021. As of September 5, 
2023: https://www.rand.org/pubs/research_reports/RRA1378-1.html Public Law 116-92, National Defense Authorization Act for Fiscal Year 2020, December 20, 2019. Reuland, Robert C. F., "Interference with Non-National Ships on the High Seas: Peacetime Exceptions to the Exclusivity Rule of Flag-State Jurisdiction," *Vanderbilt Journal of Transnational Law*, Vol. 22, No. 5, 
1989. 

UNCLOS—See United Nations Convention on the Law of the Sea.

United Nations Convention on the Law of the Sea, signed December 10, 1982, entered into force November 16, 1994. 

U.S. Coast Guard, U.S. Department of Homeland Security, Illegal, Unreported, and Unregulated Fishing Strategic Outlook, September 2020. 

U.S. Coast Guard, U.S. Department of Homeland Security, "Coast Guard Cutter Completes Operation Aiga '23," press release, February 27, 2023. U.S. Coast Guard, U.S. Department of Homeland Security, "U.S. and Republic of Palau Sign Agreement to Strengthen Ties with New Chapter in Maritime Security and Stewardship in the Pacific," press release, August 29, 2023. U.S. Coast Guard Forces Micronesia/Sector Guam, U.S. Department of Homeland Security, "U.S. Coast Guard Forces Micronesia Sector/ Guam's [sic] Fast Response Cutters Bolster Pacific Partnership Strategy and Strengthen Pacific Island Country Relations," press release, August 18, 2023. 

U.S. Coast Guard Pacific Area, U.S. Department of Homeland Security, 
"International Port Security Program," webpage, undated. As of September 5, 2023: https://www.pacificarea.uscg.mil/Our-Organization/District-14/ D14-Units/Activities-Far-East-FEACT/FEACT-Maritime-Security/ 
U.S. Department of Defense, The Department of Defense Indo-Pacific Strategy Report: Preparedness, Partnerships, and Promoting a Networked Region, June 1, 2019. 

U.S. Department of Defense, Pacific Deterrence Initiative: Department of Defense Budget Fiscal Year (FY) 2024, March 2023. 

U.S. Interagency Working Group on IUU Fishing—See U.S. Interagency Working Group on Illegal, Unreported, and Unregulated Fishing. U.S. Interagency Working Group on Illegal, Unreported, and Unregulated Fishing, National 5-Year Strategy for Combating Illegal, Unreported, and Unregulated Fishing 2022–2026, 2022. 

U.S. Mission China, "Pacific Islands Forum: U.S. Engagement in the Pacific Islands," webpage, August 20, 2019. As of September 5, 2023: https://china.usembassy-china.org.cn/ pacific-islands-forum-u-s-engagement-in-the-pacific-islands/ U.S. Naval Forces Central Command, "Record Seizures in 2021 After NAVCENT and CMF Increase Patrols," press release, January 18, 2022. White House, "Fact Sheet: In Asia, President Biden and a Dozen Indo- Pacific Partners Launch the Indo-Pacific Economic Framework for Prosperity," press release, May 23, 2022. 

White House, *Pacific Partnership Strategy of the United States*, September 2022. Wilcox, Chris, Vanessa Mann, Toni Cannard, Jessica Ford, Eriko Hoshino, and Sean Pascoe, A Review of Illegal, Unreported and Unregulated Fishing Issues and Progress in the Asia-Pacific Fishery Commission Region, Food and Agriculture Organization of the United Nations and Commonwealth Scientific and Industrial Research Organisation, 2021. 

## Acknowledgments

This paper represents my views and is informed partly by a wide variety of RAND work on the Indo-Pacific and on China's gray zone tactics. I would like to thank Shelly Culbertson for her support of this paper. I would also like to thank Kristin Leuschner for her meticulous review of the paper, including detailed comments that immeasurably improved its quality and content. Expert review and recommendations by my colleagues Thomas F. Atkin and Jonathan Welch were instrumental in fixing and refining the final product.

## About This Paper

This paper describes using a coalition of maritime law enforcement agencies to increase transparency, push back against China's aggression, and improve the rule of law in the Indo-Pacific region. The author reviews existing coalitions and explains why using coast guards and maritime law enforcement agencies is better than taking a military approach in establishing a free and open Indo-Pacific. Finally, the author provides a recommended foundation for establishing this coalition force and recommends that the U.S. Congress authorize and allocate funding toward its realization. The paper provides an alternative for military planners to address gray zone activities and should interest maritime security experts and analysts of China and the Indo-Pacific.

About the RAND Homeland Security Research Division This research was conducted in the Infrastructure, Immigration, and Security Operations Program of the RAND Homeland Security Research Division (HSRD). HSRD operates the Homeland Security Operational Analysis Center, a federally funded research and development center sponsored by the U.S. Department of Homeland Security. HSRD also conducts research and analysis for other federal, state, local, tribal, territorial, and public- and private-sector organizations that make up the homeland security enterprise, within and outside the Homeland Security Operational Analysis Center contract. In addition, HSRD conducts research and analysis on homeland security matters for U.S. allies and private foundations.

RAND is a research organization that develops solutions to public policy challenges to help make communities throughout the world safer and more secure, healthier and more prosperous. RAND is nonprofit, nonpartisan, and committed to the public interest. 

Research Integrity Our mission to help improve policy and decisionmaking through research and analysis is enabled through our core values of quality and objectivity and our unwavering commitment to the highest level of integrity and ethical behavior. To help ensure our research and analysis are rigorous, objective, and nonpartisan, we subject our research publications to a robust and exacting quality-assurance process; avoid both the appearance and reality of financial and other conflicts of interest through staff training, project screening, and a policy of mandatory disclosure; and pursue transparency in our research engagements through our commitment to the open publication of our research findings and recommendations, disclosure of the source of funding of published research, and policies to ensure intellectual independence. For more information, visit www.rand.org/about/research-integrity. RAND's publications do not necessarily reflect the opinions of its research clients and sponsors. 

 is a registered trademark.

Limited Print and Electronic Distribution Rights This publication and trademark(s) contained herein are protected by law. This representation of RAND intellectual property is provided for noncommercial use only. Unauthorized posting of this publication online is prohibited; linking directly to its webpage on rand.org is encouraged. Permission is required from RAND to reproduce, or reuse in another form, any of its research products for commercial purposes. For information on reprint and reuse permissions, please visit www.rand.org/pubs/permissions.

For more information on this publication, visit www.rand.org/t/PEA3044-1.

© 2024 RAND Corporation www.rand.org

Funding
Funding for this research was made possible by the independent 
research and development provisions of RAND Corporation contracts 
for the operation of its U.S. Department of Defense federally funded 
research and development centers.

## About The Author

Eric "Coop" Cooper is a senior policy researcher at the RAND Cor-
poration. His areas of research interest are border and port security, 
counter terrorism, critical infrastructure protection, emergency pre-
paredness, emerging technologies, homeland security, military ships 
and naval vessels, military strategy, military technology, and security 
cooperation. He holds an M.S. in mechanical engineering, an M.A. in 
national security and strategic studies, and an M.A. in security studies 
(homeland security).

