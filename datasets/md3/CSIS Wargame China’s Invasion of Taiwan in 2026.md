navalnews.com

## Csis Wargame: China'S Invasion Of Taiwan In 2026 - Naval News

Peter Ong
11–14 minutes The Center for Strategic and International Studies (CSIS) wargame panel consisted of Mark Cancian, Lt Gen David A. Deptula USAF (Ret.), Becca Wasser, and Professor William S. Murray. The CSIS wargame scenario of a Taiwan invasion by China took place in the future year of 2026 and was played in 2022, giving four years from 2022. Mark Cancian, Senior Adviser, International Security Program, and the host of this CSIS webinar held LIVE on January 9, 2023 said that the wargame was played 24 times by CSIS and came to two conclusions: China was unlikely to succeed in occupying Taiwan and the cost of war for all sides was high with estimates of 10,000+ total casualties. The U.S. lost 10-20 warships, two aircraft carriers, 200-400 warplanes, and around 3,000+ troops were killed in three weeks of fighting. China loses 90% of its amphibious fleet, 52 major surface warships, and 160 warplanes were lost.

CNN summarizes the unclassified CSIS wargame in their January 10, 2022 video shown here.

This CSIS webinar video story has been edited for clarity and brevity. The entire CSIS wargame scenario report can be found here. "China achieves too little too late," CSIS surmised regarding the wargame scenario on why China was unlikely to succeed in its objective of occupying Taiwan permanently. Four critical conditions were discovered by CSIS to achieve this outcome:

- Taiwan must resist the invasion. - The U.S. must intervene immediately.
- The U.S. must conduct military operations from Japan.
- The U.S. must have an adequate supply of Anti-ship missiles.
One of the surprising findings was that 90% of allied aircraft was destroyed on the ground by Chinese missile attacks and that the current range of allied warplanes was a severe limitation. The CSIS wargame also did factor in strikes against mainland China, mainly using airpower, and noted that the risk of a nuclear confrontation was a possibility. Certain aspects were
"not thoroughly mentioned" or "off the table" by CSIS such as Chinese attacks on the continental United States and also nuclear weapons. This was mainly a conventional wargame fought in the air and at sea around Taiwan. The key strategy recommendation is to make Taiwan a
"porcupine," too stiff and prickly with defensive weapons that an invasion would be deemed too costly.

Mark Cancian would make a wargame finding and then pass the finding to the panel members for discussion. Below are their observations.

## Lt Gen David A. Deptula, Usaf (Ret.) Dean, Mitchell Institute For Aerospace Studies

Lt General David A. Deptula noted the massive attrition from the
24 played wargames and said that the allies need to get creative in deterring China and needs to arm Taiwan with Anti-ship missiles. Furthermore, Stealth bombers provided an enormous advantage and especially Long-range Anti-ship Missiles (LRASM) was key but LRASMs are currently in low supply.

Compared to surface ships, airfields were more resilient to missile attacks as there are about 600 airfields within 2,500
miles of Taiwan.

Other findings:

- Nuclear-powered attack submarines (SSNs) are key assets.
- Non-stealthy aircraft are not even survivable against China.
- The U.S. Navy has thought through issues of enemy Anti-ship
missiles and hypersonics, but to what degree are AEGIS
warships' surface-to-air missiles (SAMs) effective against Chinese missiles? Furthermore, it will be very difficult for allied
warships to operate inside China's Anti-Access/Area Defense
(A2/AD).
- "Hard aircraft shelters are a no-brainer," said Lt. Gen. Deptula.
The U.S. needs to build them at Guam and that was his 2008 recommendation, but as of 2023, none were built, he noted.
- Regarding allied warplane range limitations, "We've been
operating fighters with ranges over 1,000-miles for over 30 years
now such as from England air bases to Kosovo on 10–12-hour missions with F-15s." This requires increased tanker support. Thus, the ranges of allied aircraft can be increased beyond being a limiting factor.
- The U.S. Air Force (USAF) would need distributed operations of
USAF warplanes [just like the U.S. Navy needs to distribute its
warships], but this is not an easy solution because it needs investment, fuel, and effort all made ahead of time. The United States can't wait for war to happen to disperse its aerial forces.

- The allies will need to strike Mainland China to win. - "95% of U.S. military personnel came aboard after 9/11 and
never experienced [a] major conflict [outside of the War on
Terror in Afghanistan and Iraq]," said Lt. Gen. Deptula, asking that the United States Department of Defense (DoD) leadership
understands this but do Congress and the U.S. public
understand this? "If we're not prepared," said Lt. Gen. Deptula, "it's going to take a loss for the U.S. to wake up."

## Becca Wasser Senior Fellow, Center For A New American Security (Cnas) Defense Program

Wasser said that there were Classified and Unclassified wargames and that wargames are a "safe to fail environment. Wargames are a powerful tool for communicating and socializing. One of the easiest ways to get decision makers onboard." Some of Wasser's key takeaways from the 24 played CSIS "Invasion of Taiwan 2026" wargames were:

- "The U.S. was not winning as handily as it once did [with
previous wars]" and thus did not have a decisive defeat of China. "Taiwan was the most stressing scenario that the DoD can face in INDO-PACOM."
- "Wargame to try to prevent the war from happening." Have DoD
make the smart choices now to deter war with China.
- U.S. Navy ships need to stay outside the range of China's A2/
AD. The U.S. Navy needs to make greater investments in undersea capabilities (SSNs and Unmanned Underwater Vehicles (UUVs)) because these are more survivable.

- The USAF needs to be able to withstand an attack and operate
again, such as using AEGIS Ashore and hardened (aircraft)
shelters. This requires investment and so far investment has been underwhelming as is funding the need for both active and passive defenses. The USAF has good dispersal of bases so
far, but needs more dispersal and more Forward Arming and
Refueling Points (FARPs). Can we base allied warplanes in Philippines and Australia? To achieve this, the USAF would need greater investment and enhanced access to hosting nations.
- The "USAF is not structured to fight in INDO-PACOM," said
Wasser as not all warplanes are able to fight from extended
range and the USAF needs to factor that in and the kinds of munitions, meaning more Long-range Precision Strike munitions are required.
- Wasser asked, "What would happen if U.S. territory was struck,
such as Guam?" How about a China nuclear response?
- "Is the U.S. ready to accept losses if Carrier Strike Groups
(CSGs) are sunk?" Such an occurrence would affect broader
U.S. societal change. This is the reason why deterrence is so important so that a Chinese invasion of Taiwan would not be
profitable.

## William S. Murray Professor, United States Naval War College

"A conflict with China would be a disaster…therefore, we have to deter this conflict. Taiwan should become a porcupine," said Professor Murray alluding that Taiwan should get so "defensive prickly with weapons" that an invasion is not worth it.

Asymmetric warfare is the best approach and Taiwan will need things that are small, many, mobile, and lethal. Lethal but not complex—as in a high school graduate can use these weapons. Taiwan must have resilient Kill Chains (some of these are based on lessons learned in Ukraine) and also need systems that are almost autonomous such as unmanned surface vessels (USVs)
with explosives, sea mines, kamikaze drones, Land-based Antiship Ballistic Missiles (LBASMs), and mobile radar and targeting fire control sensors on trucks. Professor Murray alluded to the example of "Force the larger power to defend a body against bullets." Regarding Long-range Precision Fires (LRPFs)…"targets that cannot move will die. Hide in, sense from, and shoot from clutter. Become a porcupine." Regarding the vulnerability of allied warships, Professor Murray commented that it is "Very hard to execute a successful attack against a surface ship." A lot of parameters need to happen sequentially to perform a successful attack and the how-to and countermeasures of this "Kill Chain" process is often Classified.

However, most surface ships are detectable through communications, radar emissions, sea wakes, noise and visual detection. Regarding warplane vulnerability, the allies and Taiwan will need more decoys for warplanes' survivability. An historical example from World War Two was Guadalcanal where that battle showed what happened when the U.S. didn't train well against a well-planned enemy attack.

## Audience Questions Both At The Meeting And From Online: Q: Did Csis Model A Wargame Where The U.S. Allies Won Against The Taiwan Invasion But China Hunkered Down For A Protracted War Afterwards And Strangled Taiwan With A Long-Term Blockade?

CSIS: Mark Cancian: Great question and that is something CSIS
needs to follow up and work on in the future.

## Q: Did Arming Ukraine Take Away From Arming Taiwan?

CSIS: Mark Cancian: "No. Ukraine is a ground war whereas the CSIS Taiwan wargame is an air and naval war."
"This should be an absolute wake-up call for Taiwan," said Eric Heginbotham, Principal Research Scientist, Massachusetts Institute of Technology (MIT) Center for International Studies Becca Wasser said Taiwan is down on the U.S.'s Foreign Military Sales' priority list. It is an issue over the long run, but not now because the U.S. DoD still has time to arm Taiwan [as the CSIS wargame took place in year 2026]. How can the U.S. Defense Industry ramp up to arm Taiwan? These arming issues are real, but they can be solved.

## Q: Any Hypersonic Missile Involvement?

CSIS: Limited numbers of hypersonics were produced and available in 2026 so they were factored into the wargame in limited numbers.

## Q: We Have Four Years To Prepare Taiwan For An Invasion In Your Csis 2026 Wargame…Is That Enough Time To Prepare?

CSIS: Becca Wasser said "No one knows [if four years into the future is enough time to prepare]. Most Chinese military leaders don't know because the decision [to invade Taiwan] is not theirs."  The U.S. and its allies need to take steps now in case the Chinese invasion schedule is sooner and the allies need to make it harder for China to invade Taiwan. Lt. Gen. Deptula said yes, we have time to make the investments to deter China, but it is complex. "There is no Silver Bullet," he said and noted that the U.S. needs lots of munitions of the right sorts.

## Q: How Much Would China And Taiwan Use Sea Mines?

CSIS: Matthew Cancian, Senior Researcher, United States Naval War College, said China needs the right sorts of mines and have to deploy them appropriately. However, he noted in the wargame that sea mines made the U.S. submarine fleet more difficult to operate in mine-infested waters.

## Q: What Size Forces Does China Need To Win In Taiwan?

CSIS: Mark Cancian said China has lots of assets, but protecting and transporting those assets are issues for any Taiwanese invasion. Eric Heginbotham said China can sprintspeed in building more weapon systems to have a much better chance of winning by seizing Taiwanese airfields and ports, but China would really need to build more of everything to win such as more amphibious lift, stealth warplanes, warships, weapons, missiles, etc., basically putting China on a wartime production footing.