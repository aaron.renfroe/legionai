Perspective Perspective

## Expert Insights On A Timely Policy Issue

EXPERT INSIGHTS ON A TIMELY POLICY ISSUE

## Karishma R. Mehta, Abbie Tingstad, Emily Allendorf, Emily Haglund, Krista Romita Grocholski, Gary Cecchine Existing And Emerging Topics And Potential Priorities For The Safety And Security Of The Maritime Environment T

HS AC
HOMELAND SECURITY
OPERATIONAL ANALYSIS CENTER
he United States is a maritime nation, with shoreline totaling just more than 95,000 miles in length and borders along three oceans (Atlantic, Pacific, and Arctic) (National Oceanic and Atmospheric Administration [NOAA], 2023a). The exclusive economic zone (EEZ) of the United States covers 3.4 million square nautical miles of ocean, the largest in the world (NOAA, undated). The EEZ comprises areas surrounding Alaska and Hawaii, along the East and West Coasts of the continental United States, in the Gulf of Mexico and the Caribbean (e.g., Puerto Rico and the U.S. Virgin Islands), and around several islands in the Pacific Ocean, such as Guam and American Samoa. The marine or blue economy of the United States has contributed an estimated $300 billion to the nation's GDP, and many of the country's large cities are located in coastal areas (including Los Angeles, Miami, New York, and Savannah, which host some of the world's busiest port facilities) (NOAA, 2023b). 

An FFRDC operated by the RAND Corporation under contract with DHS

Abbreviations

## Important Changes In The Maritime Environment Will Transform Needs For Safety And Security

| DHS            | U.S. Department of Homeland Security   |
|----------------|----------------------------------------|
| EEZ            | exclusive economic zone                |
| NGO            | nongovernmental organizations          |
| NOAA           | National Oceanic and Atmospheric       |
| Administration |                                        |
| R&D            | research and development               |
| USCG           | United States Coast Guard              |

Key umbrella issues for maritime safety and security were identified through literature review, research team expertise, and a December 2022 workshop. These are further described below, and include the blue economy, strategic competition, asymmetric threats, and climate change. The literature review examined strategy, academic, and public media documents obtained through Google searches (Bueger, Edmunds, and Ryan, 2019; Fickling, 2022; Germond and Mazaris, 2019; Koehring, 2022; Okafor- Yarwood, 2020; Paris, 2022; United States Coast Guard [USCG], 2018; United States Navy, United States Marine Corps, and USCG Tri-Service Strategy, 2020; U.S. Customs and Border Protection, 2019). We also leveraged the team's diverse expertise to identify important topics for policymakers and planners (e.g., Resetar et al., 2020; Tingstad et al., 2020; Vedula et al., 2023; Wirth et al., 2021).

## Blue Economy: Sustainable Economic Development Of The Oceans

Maintaining safety and security along maritime borders in support of maritime activities and marine environmental management is fundamentally important to the security of the United States. Opportunities and threats in this space are constantly evolving because of changes in economics, geopolitics, society, demography, and other factors. This Perspective summarizes key existing and emerging maritime safety and security topics for policymakers and highlights some important considerations for technological modernization to support homeland security missions in this space. 

The Maritime Safety and Security Program in the U.S. Department of Homeland Security's (DHS's) Science and Technology Directorate sponsored a workshop about future maritime programming in support of homeland security on December 7, 2022, as part of ongoing research and development (R&D) planning. This Perspective reflects themes from the input from subject-matter experts that went into the development of the workshop, and selected discussions among participants at the event, which included DHS headquarters and component representatives and RAND subject-matter experts. 

The *blue economy* refers to sustainable economic development of ocean areas and is inclusive of fisheries, seabed mining, ocean-based energy and transportation, tourism, and other activities. The economic development and use of marine and coastal resources is not new. However, only in recent decades has there been increasing recognition that the limits of resource development and use must also address resource management, which includes issues of safety and security. At the same time, developments in technologies across many areas—including automation, fishing, alternative fuels (and associated engines and logistics), undersea extractive capabilities, icebreaking, and port infrastructure—have been instrumental in enhancing the diversity and capacity of activities on, under, and around oceans.

Management of the blue economy can include distinct governance, services, and law enforcement aspects. These are related but distinct: Governance involves policy development, rulemaking, and negotiation; services enable development and provisioning; and law enforcement ensures rules are followed (including for safety) and responds to contingencies related to securing lives, property, and services. The trend toward blue economic development—the emphasis on sustainability coinciding with scaling up and diversifying activities—has tremendous potential to continue shaping maritime safety and security needs, activities, and capabilities, including for domain awareness. Enforcing updated safety and environmental regulations, monitoring undersea mining and elusive illegal fishing activities, identifying new migration (human, animal) and trafficking (human, drug, other goods) patterns, and preventing and responding to disasters of increasing frequency and intensity will certainly evolve the seascape for strategists and planners, first responders, law enforcement, and other important entities. 

## Strategic Competition: The Politics Of Resources And International Affairs

economy related to competition over resources. The more-recent usage of this term in connection with international affairs also ties to the blue economy, considering that setting guidelines for managing ocean-based activities involves (1) international negotiations through the United Nations, International Maritime Organization, etc.; 
(2) negotiations between countries managing ship traffic and fisheries along shared maritime borders; and (3) other bilateral and multilateral channels. 

One way to think about strategic competition is as a concept at the intersection of contention, influence, and resource scarcity (Mazarr, Frederick, and Crane, 2022). With the largest EEZ in the world—including areas that are not connected to the lower 48 and include the Pacific islands and the Arctic—and a global economic and geopolitical reach, the United States will likely increasingly contend with issues of strategic competition around its EEZ and in the high seas and, as a result, with changing needs for maritime border and coastal area domain awareness, law enforcement, and emergency response. This trend might also push domestic first responders into even closer partnership or contact with defense authorities. China has expanded its presence and increased its investments in the Caribbean for more than a decade and recently sought to strengthen diplomatic and economic partnerships with multiple Pacific island nations (Ellis, 2020; Needham, 2022). Russia claims a vast Arctic EEZ (though Canada and Alaska also have sizable maritime areas surrounding them) and is a strong force in the region in terms of capacity for Arctic economic and military operations (Brigham, 2022; 
Pincus, 2020).1 China and Russia have collaborated in the Arctic before. Their established partnership on liquified Strategic competition is a term that is increasingly being discussed in mainstream policy research, academia, and the media. Historically, it has a connection to the blue natural gas has been in place since at least 2017 (Rehman, 2022).

## Asymmetric Threats: Physical And Digital Dangers At Sea And Along Coasts

training and skills to identify and respond. In responding to even natural disasters, personnel training programs, infrastructure, equipment, and plan design will need to consider the potential for asymmetric threats to opportunistically cause additional problems. For example, sometimes it is not immediately clear whether a particular adverse effect on utilities infrastructure has been caused by a natural hazard or something more overtly nefarious.

## Climate Change: The Ultimate Asymmetric Threat

Climate change and its effects are perhaps the ultimate asymmetric threat in the maritime domain and are linked with the topics previously discussed. *Climate change* refers to long-term shifts in temperatures and weather patterns (United Nations, undated). In the maritime safety and security environment climate change contributes to shoreline erosion and extreme weather conditions, and has other consequences for the Arctic environment and coastal communities. The blue economy has a focus on sustainability because of projected climate change and population growth. There are also indirect links between climate change and strategic competition, through the increasing access to the Arctic and issues of climate diplomacy (e.g., China's outreach to Pacific island nations), and the effect of climate change on immigration. Negotiations on climate policy issues related to energy transition and equity, and on loss and damage compensation (e.g., Conference of the Parties under the United Nations Framework Convention on Climate Change) could also be affected by strategic competition. 

New and increasingly diverse and elusive asymmetric threats can be expected with changes in the maritime environment brought about by growth in the blue economy and intensification of strategic competition. Asymmetric threats come from sources that have substantially different strength, strategy, or tactics than their intended targets and are typically difficult to identify, surveil, and attribute. They can include physical threats (e.g., uncrewed vehicles can be used for both physical effects and surveillance, and legacy vessels can be disabled to block ingress and egress points), cyber threats (e.g., attacks on digital maritime security infrastructure, defined in the next section), and algorithmic threats (malicious algorithms can, in theory, be used to trick sensors and other hardware using their own machine-learning or other algorithms to guide machine-to-human or machine-to-machine interactions). 

Changes in the asymmetric threat landscape have a strong impact on the maritime safety and security enterprise, both directly and indirectly. Asymmetric threats can also indirectly affect the economy and public perceptions of law enforcement efficacy because of the limited ability of government entities and their partners to address such threats. Changes in surveillance and response equipment, decisionmaking tools, personnel training and skills, and tactics might be needed. There will also be a need to harden infrastructure—physically and digitally—to account for asymmetric threats. Personnel will require Climate change will both (1) help shape the missions that DHS, its components, and other U.S. government departments and agencies conduct and (2) influence the risks that could inhibit the success of these missions. Consider, for example, the path of extreme storms, patterns of climate-induced migration, alterations in where drugs are produced and how they travel (including ecosystems that support any plants used as ingredients), and greater physical ability to access the Arctic and (to a lesser extent) the Antarctic. 

In thinking about risks to missions related to climate change, fixed and mobile infrastructure are pertinent and tangible examples—facilities can be inundated by sea level rise, dust storms during dry conditions can prevent aircraft from taking off, and communications and energy infrastructure can be destroyed during storms. Another example is the effect climate change could have on human migration patterns, depending on where food and water security are limited and where there could be resources to sustain and support growing populations. 

## Advances In Computing Could Enhance Capabilities But Require Investments In Physical Infrastructure, Policy, Personnel, And Training

Although the promise of being able to use data and algorithms to support a broad variety of applications with increasing levels of automation has been discussed in computer science and related fields for decades, it is only much more recently that two necessary factors—computing power and the ability to gather data at scale—have been realized, paving the way for making mathematical aspirations a reality. The maritime environment is undergoing its own digitalization, encompassing a shift from manual operations, pen-and-paper records, and hardware-focused approaches to automation, digitally collected and managed data, and software-focused applications. This technological transition could enable more-timely and more-accurate monitoring and decisionmaking using data for applications from scanning for asymmetric threats; enforcing vessel safety requirements; and tracking illegal, unreported, and unregulated fishing. These changes necessitate harnessing data that already exist (e.g., from the automatic information system), removing physical and policy barriers to integrating and analyzing data, and developing processes that use reliable information to support decisions.

An aspect of digitalization includes leveraging a "web of things," which can also involve humans. Devices worn on or implanted inside the human body could, in theory, enhance human performance. These technologies, which range from a variety of smart devices and fitness trackers to human microchip implants, can control access to physical and digital spaces, monitor health and location, and deliver or transmit information. Just as automated vehicles sense their own state and the environment around them to optimize performance under a given set of conditions, so too could digital technologies aid human performance. However, there are serious privacy and security concerns surrounding the use of these technologies. 

Taking full advantage of data and digital technologies while minimizing their risks requires technological modernization, something that several U.S. institutions involved in maritime safety and security are challenged maritime environment,2 which can require rapid, largescale response and monitoring. 

The scalability of architecture is also important to consider in an environment characterized by growing volumes of data. As maritime activities diversify to include more vessels on the water, law enforcement agencies will likely need to gather more data to distinguish legitimate traffic and activities from illegitimate ones. Future architectures must rely on features that can filter the firehose of data and scale down the amount of information delivered to the user from a single query. 

As maritime activities expand to increasingly collaborate with partners, user-friendly policies can further expand to consider barriers to sharing and communication.

## Data Management: Foundations For Enabling Data-Driven Decisionmaking

by in the 2020s because of rapid technological change, the emergence of new threats in the digital domain, and inertia in organizational culture and processes. Facilitating modernization is multifaceted, as we will discuss. 

## Data Architecture: An Asset For Improving Collaboration

Generally speaking, *data architecture* refers to how data are gathered, processed, analyzed, stored, and consumed. Data architectures include virtual and physical elements. Building and modifying data architectures to ensure that they are accessible, collaborative, adaptable, and scalable are enabled by data management mechanisms, which we describe next. Furthermore, the adaptability and efficiency of data architectures could be critical to contend with the current and future threats that climate change poses to the One definition of *data management* involves establishing "policies, procedures and best practices to ensure that data [are] understandable, trusted, visible, accessible and interoperable" in such processes as "planning, modeling, security, information assurance, access control and quality" (Instruction Manual 262-12-001-01, 2017, p. 144). Together with data architecture, data management helps implement standards that ensure access to quality data for those who need it to inform decisionmaking at different levels of an organization. This involves enabling access to data and an ability to employ appropriate analytic methods, but also extends to policies that ensure data are used efficiently and appropriately. 

Prioritizing education in data literacy and userfriendly design can help modernize data management and nurture an informed data culture. This is important across an organization—from data practitioners who harvest data and build analytical tools, to analysts who study the results of those tools, to senior leaders who shape organizational strategy and policy. 

Setting standards to enable user-friendly design is also important. For example, policies that encourage high-quality data cleaning and preparation so that data are maximally usable to those who seek it are key to data accessibility. These types of policies help avoid data siloes (physical and policy-related) that can inhibit data-sharing. As maritime activities expand to increasingly collaborate with nongovernmental organization (NGO), international, or other partners, user-friendly policies can further expand to consider barriers to sharing and communication. These include language barriers, operating system compatibility, and the handling of classified information, though there are limits to how extensively data can be shared to prevent potentially adverse actors from carrying out threats or otherwise establishing unsafe conditions in the maritime environment. 

Another notable roadblock to technology modernization is the historical tendency for organizations to fund large, expensive, marquee initiatives and deprioritize moremodest efforts. This approach can hinder possibilities to take advantage of incremental opportunities for the modernization of data management and architecture. Naturally, an overarching strategy for modernization must take into account the big picture, but investment opportunities might come incrementally. 

Finally, the concept of systems thinking employs hierarchical structures for information, modeling a system as a collection of processes, and incorporating feedback loops (Sánchez-Silva, 2022). A systems thinking paradigm that incorporates flexibility recognizes unknown uncertainties and has the ability to pivot when necessary (Sánchez-
Silva, 2022), making planning processes more flexible and responsive to external shifts in threats and technological capabilities, for example. This paradigm means flexible decision processes are not sequential decisions, but a process with actions and loops that have different possible outcomes (Sánchez-Silva, 2022). 

## Cultural Shifts: Modernization Is About More Than Technology Collaborators And Partners Offer Opportunities To Advance Research And Development When Circumstances Permit

Strategic partnerships and collaborations might help advance digital modernization to support the maritime safety and security community in contending with challenges related to managing the blue economy, strategic competition, asymmetric threats, and climate change. Each The amount of data to which the maritime safety and security community has access is growing exponentially. Properly handling and using a growing volume of data necessitates support from an overarching organizational data-driven culture. This means that the organization prioritizes data architecture and management, and subsequently ensures that people are trained to use data to make informed decisions. Mechanisms to inform culture can be explicit or implicit: Explicit mechanisms appear in policy and doctrine, which then can direct such implicit mechanisms as standards, procedures, and best practices. 

organizations can be limited in scope because coastal communities are not necessarily familiar with all of the safety and security topics that federal agencies and other partners might be considering. 

NGOs are valuable resources when it comes to gathering information because some of their missions are adjacent to what government agencies do. For example, the USCG develops Memorandums of Understanding with NGOs, most recently with Global Fishing Watch in 2018 (Bladen, 2018). This allows the USCG to receive data that are more operational in nature to help combat illegal fishing and slavery at sea as well as to help manage ecosystems.

With international waters making up anywhere from one-half to two-thirds of the globe, international partnerships are essential. Strategic international partnerships can help curb the behavior of bad actors in this time of strategic competition. A notable example is the one set over almost three decades by the Arctic Council, consisting of the eight Arctic countries, permanent participants (Indigenous groups), and observers (e.g., European Union, China, and Japan). Although the Arctic Council explicitly does not deal with military issues, this forum—despite being paused since Russia's intensification of its invasion of Ukraine—has served as an arena for collaboration on such key maritime issues as search and rescue, oil spill prevention and response, and fisheries and environmental management. International partnerships bring opportunities to collaborate in research. These partnerships spread cost and risk and assist in diplomacy and the pursuit of violations of criminality on the seas. However, individual nation regulations supersede any partnership alliance.

type of partnership comes with its advantages and limitations. Strategic partnerships at every level can assist in not only fostering relationships between entities but also in reducing stovepipes that prevent data-sharing and in the dispersing of mission or tasks. Partnerships can include those between U.S. government entities, with NGOs, and with international partners. 

U.S. government entity partnerships for maritime safety and security can build alliances across DHS and other federal departments and agencies with state, local, and tribal governments. For example, certain cities, coastlines, and infrastructure are directly affected by climate change. The city of New Orleans is one of the most vulnerable cities in the United States to climate change because of its low elevation. The city of New Orleans, the Federal Emergency Management Agency, and other entities have collaborated to create hazard mitigation plans to combat short-term disasters and develop long-term strategies and plans to reduce risk.3 
Infrastructure is not the only area for potential partnerships in the maritime safety and security area. Partnerships with NGOs and community organizations have the ability to provide information about the maritime domain and assist in information-gathering. Community organizations are familiar with the maritime domain in their area and the surrounding marine ecosystem, making them leading subject-matter experts about their area. For example, partnerships can allow for knowledge transfers from the local and indigenous communities to the federal level on such topics as fish migration and reproductive and other life-cycle patterns, which would be important for understanding environmental and food security issues. However, importantly, information from community 

## Conclusion

SOURCE: Notes from the December 7, 2022, workshop about future maritime programming in support of homeland security. NOTE: The larger the keyword, the more times it was referenced in workshop discussions according to notes taken at the event. 

policy to enable automation and artificial intelligence, thus generating the conditions for data-driven decisionmaking at scale in an evolving maritime environment. Collaborations at different levels can add capacity to address emerging challenges and inform efficient R&D efforts through knowledge exchange. 

Accompanying changes in organizational culture and planning processes appear needed to take full advantage of advances in technologies. Having the right data at the right time and place does not automatically imply improved decisions. There must also be an agile process in place to use the resulting insights for decisionmaking. 

The maritime environment is a dynamic one, which creates challenges for policymakers, practitioners, and operators trying to plan for future investments. One key aspect of this planning is guiding technological modernization. 

Figure 1 emphasizes selected key words related to maritime safety and security that arose during the course of the research used to inform this Perspective. It highlights the exceptionally diverse suite of existing and emerging issues and solutions that stakeholders must consider in their respective decisions.

The maritime environment is dynamic along many dimensions. Several emerging activities, hazards, and threats will likely diversify and geographically expand, altering missions and needs. Key examples include the impacts of climate change on missions related to shipping, fishing, and disaster response; the growth of undersea activities in the blue economy; and the intensification and diversification of threats across the spectrum of strategic to tactical. These trends could drive growth and change in investment portfolios geared toward the development of technologies related to the maritime environment and climate; maritime economy; and coastal, port, and waterway security. 

Designing R&D to keep pace is difficult, but there are technologies, collaborations, and planning approaches that can meet the challenge. Data infrastructure and domain awareness remain important areas for the future. Continuing momentum in these areas is linked to broader digital modernization, which will be facilitated by the parallel focus on software and hardware (including for remote maritime technologies), connectivity, and data management 

## Notes

1  The United States and its allies and like-minded partners in the region also have a strong presence. One key distinction is that the United States has historically emplaced assets in Alaska and Greenland (Thule) for the purposes of global power projection and continental defense, as opposed to having a primary focus on enabling operations in the region itself. 

2  See, for example, the growing body of research supporting the positive relationship between Earth's warming (both atmospheric and oceanic) and extreme weather events (Buis, 2020).

3  See, for example, the City of New Orleans' plan in collaboration with the state of Louisiana's Office of Homeland Security and Emergency Preparedness (City of New Orleans, 2021). The plan was structured in accordance with federal requirements for accessing Federal Emergency Management Agency Hazard Mitigation Assistance Programs (City of New Orleans, 2020). 

## References

Bladen, Sarah, "Global Fishing Watch and U.S. Coast Guard Look to Advance Research on Illegal Fishing," Global Fishing Watch, December 11, 2018. 

Brigham, Lawson W., The Russian Maritime Arctic: Region of Great Change in the 21st Century, U.S. Naval War College, 2022. 

Bueger, Christian, Timothy Edmunds, and Barry J. Ryan, "Maritime Security: The Uncharted Politics of the Global Sea," International Affairs, Vol. 95, No. 5, September 2019. 

Buis, Alan, "How Climate Change May Be Impacting Storms over Earth's Tropical Oceans," *Ask NASA Climate*, March 10, 2020. 

City of New Orleans, Hazard Mitigation Plan, "The Orleans Parish Multi-Jurisdictional Mitigation 2020 Update Planning Process," webpage, updated 2020. As of May 12, 2023: https://ready.nola.gov/hazard-mitigation/about/planning-process/ City of New Orleans, Office of Homeland Security and Emergency Preparedness, *Hazard Mitigation Plan: City of New Orleans*, 2021. 

Ellis, R. Evan, *China's Advance in the Caribbean*, Wilson Center, 2020. Fickling, David, "Shipping's Oil Era Is Coming to an End," Washington Post, December 21, 2022. 

Germond, Basil, and Antonios D. Mazaris, "Climate Change and Maritime Security," *Marine Policy,* Vol. 99, No. 1, January 2019. Instruction Manual 262-12-001-01, *DHS Lexicon Terms and Definitions*, Management Directorate, U.S. Department of Homeland Security, October 16, 2017. Koehring, Martin, "How to Shift the Ocean Narrative for Sustainable Blue Economy," *Economist Impact: World Ocean Initiative* blog, October 27, 2022. As of January 25, 2023: https://impact.economist.com/ocean/sustainable-ocean-economy/ how-to-shift-the-ocean-narrative-for-a-sustainable-blue-economy Mazarr, Michael J., Bryan Frederick, and Yvonne K. Crane, Understanding a New Era of Strategic Competition, RAND Corporation, 
2022. As of May 12, 2023: https://www.rand.org/pubs/research_reports/RRA290-4.html National Oceanic and Atmospheric Administration, "What Is the 'EEZ'?" webpage, undated. As of May 22, 2023: https://oceanexplorer.noaa.gov/facts/useez.html National Oceanic and Atmospheric Administration, "How Long Is the U.S. Shoreline?" webpage, January 20, 2023a. As of May 22, 2023: https://oceanservice.noaa.gov/facts/shorelength.html National Oceanic and Atmospheric Administration, "New Blue Economy," webpage, February 13, 2023b. As of February 23, 2023: https://www.noaa.gov/blue-economy Needham, Kirsty, "China, Pacific Islands Unable to Reach Consensus on Regional Pact," Reuters, May 30, 2022.

NOAA—See National Oceanic and Atmospheric Administration.

Okafor-Yarwood, Ifesinachi, "The Cyclical Nature of Maritime Security Threats: Illegal, Unreported, and Unregulated Fishing as a Threat to Human and National Security in the Gulf of Guinea," *African Security*, Vol. 13, No. 2, 2020. Paris, Costas, "Shipping Industry Balks at Green Energy Transition," 
Wall Street Journal, December 22, 2022. Pincus, Rebecca, "Three-Way Power Dynamics in the Arctic," Strategic Studies Quarterly, Vol. 14, 2020. Rehman, Maria, Changing Contours of Arctic Politics and the Prospects for Cooperation Between Russia and China, Arctic Institute, August 23, 
2022. 

Resetar, Susan A., Abbie Tingstad, Joshua Mendelsohn, Miriam E. 

Marlier, Beth E. Lachman, Katherine Anania, Chandra Garber, and David M. Adamson, Recovery Planning for Natural Resources and Parks in Puerto Rico, Vol. 1, Natural and Cultural Resources Sector Report, Homeland Security Operational Analysis Center operated by the RAND Corporation, RR-2605-DHS, 2020. As of February 23, 2023: https://www.rand.org/pubs/research_reports/RR2605.html Sánchez-Silva, Mauricio, "Designing Flexible Infrastructure Systems: 
An Opportunity to Better Handle Uncertainty and to Improve Sustainability," briefing slides, Homeland Security Operational Analysis Center operated by the RAND Corporation, December 7, 2022. Tingstad, Abbie, Michael T. Wilson, Katherine Anania, Jordan R. Fischbach, Susan A. Resetar, Scott Savitz, Kristin Van Abel, R. J. Briggs, Aaron C. Davenport, Stephanie Pezard, Kristin Sereyko, Jonathan Theel, Marc Thibault, and Edward Ulin, Developing New Future Scenarios for the U.S. Coast Guard's Evergreen Strategic Foresight Program, Homeland Security Operational Analysis Center operated by the RAND Corporation, RR-3147-DHS, 2020. As of February 23, 2023: https://www.rand.org/pubs/research_reports/RR3147.html United Nations, "What Is Climate Change?" undated. As of June 5, 2023: https://www.un.org/en/climatechange/what-is-climate-change United States Coast Guard, *Maritime Commerce Strategic Outlook*, Washington, D.C., October 5, 2018. United States Navy, United States Marine Corps, and United States Coast Guard Tri-Service Strategy, Advantage at Sea: Prevailing with Integrated All-Domain Naval Power, Washington, D.C., December 17, 
2020. 

U.S. Customs and Border Protection, Vision and Strategy 2030: Air and Marine Operations, Safeguarding the Nation from the Air and Sea, Washington, D.C., November 2019. Vedula, Padmaja, Abbie Tingstad, Lance Menthe, Karishma R. Mehta, Jonathan Roberts, Robert A. Guffey, Natalie W. Crawford, Brad A. 

Bemish, Richard Payne, and Erik Schuh, Outsmarting Agile Adversaries in the Electromagnetic Spectrum, RAND Corporation, RR-A981-1, 2023. 

As of February 22, 2023: https://www.rand.org/pubs/research_reports/RRA981-1.html Wirth, Anna Jean, Dulani Woods, Katherine Anania, Gary Cecchine, and Debra Knopman, Developing the Great Lakes National Center of Expertise for Oil Spill Preparedness and Response: An Opportunity to Reduce Risk and Impacts of Future Spills in Freshwater, Homeland Security Operational Analysis Center operated by the RAND Corporation, RR-A1222-1, 2021. As of February 22, 2023: https://www.rand.org/pubs/research_reports/RRA1222-1.html

## Acknowledgments

We acknowledge the Maritime Safety and Security Program Roadmap Project principal investigators Angela Putney and Brendan Toland and project team for their support in the workshop; our sponsor Matthew Barger; and Emma Westerman, Kelly Klima, Doug Yeung, and Abbie Tingstad from Management, Technology, & Capabilities Program leadership. We would also like to thank the workshop speakers, panelists, facilitators, and moderators for offering their time and expertise to make the workshop possible—specifically, Kate Anania, ret. Vice Admiral Dan Abel, Matthew Barger, Juan Bauza, Majory Blumenthal, Hon. Sharon E. Burke, Raphael Cohen, Monika Cooper, ret. Captain Aaron C. Davenport, Krista Romita Grocholski, Katherine Guthrie, Sam Howerton, Brian Michael Jenkins, Elicia John, Lee Kindberg, Ajay Kochar, Eric Landree, Tim Marler, Jack Riley, James Ryseff, Mauricio Sánchez- Silva, ret. RADM Lars Saunes, Scott Savitz, Howard J. Shatz, David Spirk, Karlyn Stanley, Daniel Tapia, Brendan Toland, Courtney Weinbaum, N. Peter Whitehead, and Dulani Woods. To make a workshop of this magnitude happen, there were many people working behind the scenes to make things run smoothly; we acknowledge Quiana Fulton, Lily Hoak, Emily Kampe, Erin Rehban, Carmen Richard, Jason Rogers, Nina Ryan, Beth Seitzinger, Leanna Shrader, Donna Thomas, Rachel Uselton, Francisco Walter, and Adam Wheat. We thank our reviewers Irv Blickstein, Patricia Stapleton, and Gene Germanovich, as well as our Quality Assurance Manager Carter Price and our Quality Assurance Coordinator Britney Lomax.

We dedicate this work to our friend and colleague  
Irv Blickstein (1939–2023).

## About This Perspective

This Perspective reviews the key topics of maritime safety and security and the subsequent discussions that were presented at the public December 7, 2022, workshop to identify the issues and priorities that the maritime safety and security community should be aware of for the next 10–20 years. The overarching issues and priorities that were identified were data infrastructure and domain awareness; maritime activities, threats, and hazards diversifying the environment; and the need for changes to organizational culture and planning processes. These priorities and issues were underscored by themes that spanned the panels and discussion—such as climate change, strategic partnerships, and systems thinking. This research was sponsored by the U.S. Department of Homeland Security's (DHS's) Science and Technology Directorate and conducted in the Management, Technology, and Capabilities Program of the RAND Homeland Security Research Division (HSRD), which operates the Homeland Security Operational Analysis Center (HSOAC).

Homeland Security Operational Analysis Center The Homeland Security Act of 2002 (Public Law 107-296, § 305, as codified at 6 U.S.C. § 185) authorizes the Secretary of Homeland Security, acting through the Under Secretary for Science and Technology, to establish one or more federally funded research and development through the Under Secretary for Science and Technology, to establish one or more federally funded research and development centers (FFRDCs) to provide independent analysis of homeland security issues. The RAND Corporation operates the Homeland Security Operational Analysis Center (HSOAC) as an FFRDC for DHS under contract 70RSAT22D00000001. The HSOAC FFRDC provides the government with independent and objective analyses and advice in core areas important to the Department in support of policy development, decisionmaking, alternative approaches, and new ideas on issues of significance. The HSOAC FFRDC also works with and supports other federal, state, local, tribal, and public- and private-sector organizations that make up the homeland security enterprise. The HSOAC FFRDC's research is undertaken by mutual consent with DHS and is organized as a set of discrete tasks.

## Hs Ac Homeland Security Operational Analysis Center

The information presented in this publication does not necessarily reflect official DHS opinion or policy.

For more information on this publication, visit www.rand.org/t/PEA2110-1.

This Perspective was published in 2023. Approved for public release; distribution is unlimited.

An FFRDC operated by the RAND Corporation under contract with DHS

centers (FFRDCs) to provide independent analysis of homeland secu-
rity issues. The RAND Corporation operates the HSOAC as an FFRDC 
for DHS under contract 70RSAT22D00000001. The HSOAC FFRDC 
provides the government with independent and objective analyses 
and advice in core areas important to the department in support of 
policy development, decisionmaking, alternative approaches, and 
new ideas on issues of significance. The HSOAC FFRDC also works 
with and supports other federal, state, local, tribal, and public- and 
private-sector organizations that make up the homeland security 
enterprise. The HSOAC FFRDC's research is undertaken by mutual 
consent with DHS and is organized as a set of discrete tasks. This 
report presents the results of research and analysis conducted under 
70RSAT22FR0000032, Maritime Safety & Security Program Capability 
Roadmap. 

The results presented in this Perspective do not necessarily reflect 
official DHS opinion or policy. For more information on HSRD, see  
www.rand.org/hsrd.

