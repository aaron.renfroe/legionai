BRYAN FREDERICK, CAITLIN MCCULLOCH 
Beyond the Eye of the Storm

## Mapping Out A Comprehensive Research Agenda For The National Security Implications Of Climate Change T

he Department of Defense (DoD), including the Department of the Air Force (DAF), has been late to assess the full potential of climate change to affect U.S. national security. While such issues as base resilience or disruptions to logistics flows are studied extensively, climate change is not just a series of isolated extreme weather events. It is a long-term, global phenomenon with the potential to alter political, economic, and social systems in ways that could have far-reaching effects on U.S. security interests. This concept paper lays out a research agenda to pursue in order to better understand the possible scale and scope of these effects and their potential implications for the DAF and DoD. Pursuing this research agenda would enable the DAF and DoD to be better prepared for a greatly altered future with more and more impacts slated to arrive over the next ten to 20 years (see Figure 1). 

After years of debate, there is growing awareness within the DAF, DoD, and the U.S. government more broadly that climate change has the potential to pose a serious national security threat, and some efforts to cope with this challenge are underway. As summarized in the Department of Defense Climate Adaptation Plan, published in 2021, there are an array of ongoing military adaptation and resilience efforts, from building a climate-ready force to improving supply chain resilience.1 The DAF published its own Climate Action Plan in 2022 and an implementation-focused Climate Campaign Plan in 2023, with three priorities: maintaining air and space dominance in the face of climate risks, making climate-informed decisions, and optimizing energy use and pursuing alternative energy sources.2
The first and third priorities reflect the emphasis that DoD 
has placed on adapting to physical challenges and further reductions in greenhouse gas emissions. However, the second priority—the need to make climate-informed decisions and, in particular, incorporate climate considerations into operational and campaign plans—highlights the need for broader research on potential climate effects. 

These reports represent only some of the U.S. government's recent attempts to better understand and identify ways to respond to the problems that climate change may pose for the military.3 Perhaps the broadest U.S. government assessment of the national security implications of climate change can be found in the 2021 National Intelligence Estimate (NIE) on this topic.4 The NIE makes the interests.5 It also addresses the possibility that states and individuals will increasingly assign blame to past or ongoing polluters in ways that could alter geopolitical relationships or generate novel security concerns. These currently under-investigated ways in which climate change might affect U.S. national security interests could, in turn, have substantial nearer-term implications for the DAF and DoD, potentially altering investment and posture decisions that are being made today. 

## Projected Changes To Climate

Projections of the physical effects of climate change decades into the future remain uncertain, because of both modeling challenges and uncertainty regarding the extent to which humanity will reduce greenhouse gas emissions in the interim. However, outlines of the range of possible effects are increasingly coming into focus. They emphasize that serious climatic changes are now very likely to occur within the next 20 years. The Intergovernmental Panel on Climate Change (IPCC) has projected a 1.5°C increase in 

## Abbreviations

CO2
carbon dioxide
DAF
Department of the Air Force
DoD
Department of Defense
HA/DR
humanitarian assistance and disaster relief
IPCC
Intergovernmental Panel on Climate Change
NATO
North Atlantic Treaty Organization
SAI
stratospheric aerosol injection
SSP
Shared Socioeconomic Pathway

case for a wide variety of possible future effects of climate change on U.S. national security, including through effects on resources and the physical environment, as well as through effects on geopolitical issues. Although, by design, the NIE does not provide detailed evidence or arguments for its conclusions, it represents a good starting point for further investigations into these issues. However, many of the geopolitical implications that the NIE highlights have not become part of subsequent DoD strategies and assessments, which remain focused on efforts to enhance DAF and DoD operating resilience. Therefore, questions remain: Which climate-related issues should the DAF and DoD focus on? To what extent will broader geopolitical concerns require more-fundamental shifts in DAF or DoD missions, posture, or investments? 

This paper argues that, in order to answer these questions, there is still an enormous amount to be learned about how climate change may affect U.S. national security and whether the United States and the DAF may, as a result, be missing important opportunities to respond. To make this case, this paper first summarizes scientific projections on the ways in which the earth's climate is likely to change in the years to come. Second, it summarizes recent or ongoing research on the ways in which climate change is currently assessed to affect U.S. national security interests. Third, it lays out an argument for why several important gaps remain in current research and maps out how future research could address those gaps in ways that would leave the United States better prepared for the future. Specifically, building on the literature and integrating additional potential insights, this paper explores how political and social systems might attempt to adapt to or mitigate climate change in ways that could affect U.S. national security global temperatures relative to preindustrial levels within the next 20 years, even if significant decreases in humancaused greenhouse gas emissions were to occur, as shown in Figure 1.6 The figure shows several possible projected futures, organized into Shared Socioeconomic Pathways (SSPs), which reflect how the world would likely look under different assumptions about future greenhouse gas emissions. Even the lowest projection, model SSP1-1.9, which reflects sharp reductions in emissions from current levels, shows a temperature rise of more than 1.5°C. The possibility that emissions could fall quickly enough to avoid even this degree of temperature rise is, at this point, considered unlikely. For context, a 2°C increase is anticipated to be associated with various elevated risks to human life.7
Lab offers a projection of the likely effects of climate change on human mortality in different geographic areas, as shown in Figure 2. 

As the figure shows, global mortality risk is projected to increase dramatically across much of Northern Africa and the Middle East by the middle of the 21st century. 

More-moderate increases are anticipated across much of South and Central Asia, Australia, and the southern and central United States. By contrast, more-northern or higher-elevation locations, such as Russia, Canada, Scandinavia, and Tibet, are expected to experience reductions in mortality risks due to more-moderate temperatures. These varied physical effects are likely to play an important role in driving the political and social reactions to climate change, as we discuss below. 

Some climate risks, such as sea level rise, are existential threats to entire countries. For just one example, as of 2022, up to 40 percent of the capital district of Tuvalu was underwater at high tide.10 Tuvalu at its highest points is barely 9.8 feet, or 3 meters, above sea level,11 and the country already suffers from coastal flooding and erosion.12 
Without flooding remediation, a significant portion of the country would become uninhabitable. Tuvalu has also been especially vulnerable to increasingly erratic rainfall, more-intense cyclones, drought, and ocean acidification for a decade.13 Climate changes like these could affect almost every aspect of Tuvalu's land, economy, and continued survival as a sovereign country. The 2022 U.S. National Defense Strategy highlights the increasing strategic importance of the Indo-Pacific region, where such island states as Tuvalu will face increasingly dire challenges with sea level rise and more-extreme natural hazards.14 
Temperature rise is just one of many potential climate impacts that the world faces in the coming years. Shifts in precipitation, sea level rise, ocean acidification, and unprecedented weather events are also likely to accelerate in the years to come,8 and these shifts will result in land that is less habitable for humans; oceans that are less habitable for fish; an increase in natural hazards, such as flooding and drought; and more-intense tropical cyclones.9 
While uncertainty remains about the extent of these shifts, they are no longer simply possibilities; rather, they are eventualities, and the key question has become how extreme they will be. 

Complicating future assessments of the risks and challenges of climate change is the fact that the effects of climate change will not be evenly distributed geographically. Instead, there will likely be geographic winners and losers: Some—primarily colder—areas will become more livable, and other—primarily hotter—areas will become dramatically less so. To illustrate this point, the Climate Impact SOURCE: Reproduced from Carleton et al., "Valuing the Global Mortality Consequences of Climate Change Accounting for Adaptation Costs and Benefts." Used with NOTE: GDP = gross domestic product. The moderate-emissions scenario shown in this fgure refects the Representative Concentration Pathway 4.5 estimates produced by the IPCC, indicative of moderate further increases in greenhouse gas emissions through 2059. The fgure explores *temperature-related mortality risk*, which is the impact of temperature on hospital visits, health risks, and death. Using quantitative data on subnational human mortality around the world, the Carleton et al. study not only identifes the importance of temperature in increasing mortality but also explores the role of income and adaptations, such as air conditioning, in safeguarding public health. The study stresses the serious impact of especially extreme heat on public health and highlights differential subnational or geographic impacts.

eration would lead to considerably more sea level rise, while consistency would offer a more linear sea level rise in line with historical shifts. Although we recognize that these projections and estimates show a variety of futures, there is scholarly agreement that climate change will continue to occur and will have significant effects on human behavior and life. The uncertainty that remains is around how large the effects will ultimately be.15
Notwithstanding these detailed projections, it is important to emphasize that there remains substantial uncertainty about the *extent* of these and other climate impacts, even while the overall *direction* in which the climate is moving is clear. For example, sea level rise is occurring, but how much rise occurs is dependent on how ice sheets and glaciers respond to shifts in temperature and whether their melting accelerates or stays consistent. Accelis primarily academic in focus, although a notable body of policy-focused research also exists.16 

## Links Between U.S. National Security And Climate In Current Research Reactions To Extreme Weather Events And Changes To Climatic Baselines

Several expansive bodies of literature explore the rise in extreme weather events, including increases in the number of natural hazards and their severity. These weather events may have direct effects on U.S. military operations or geopolitical relations, or they may have wider-ranging, indirect effects on resource scarcity. Climate change will not only make extreme weather events of greater concern; it will also shift climatic baselines, such as average temperatures or rainfall levels, in ways that are likely to have pronounced effects on human societies and, as a result, on U.S. national security interests. Substantial research has explored these potential pathways.

Of note, however, extreme weather events and changes to climatic baselines are thought to affect U.S. national security interests through some of the same intermediate variables—including, most notably, resource scarcity. It is resource scarcity that is most often assessed to have the potential to drive many broader changes, ranging from migration to armed conflict. The potential pathways linking extreme weather events and changes to climatic baselines that have already been robustly explored include the following:
Research on the ways in which climate change may affect U.S. national security is a large and constantly growing area of both academic and policy research. This body of research has identified several interesting potential pathways, although the likely scale and scope of the potential effects along these pathways remain unsettled. Figure 3 provides a visual summary of the most-prominent or most firmly established pathways in the existing academic and policy literature connecting climate change with U.S. national security implications. Although certainly not exhaustive, it does highlight the areas in which existing research on the effects of climate change has been most robust. The figure should be read from left to right: The anticipated physical or environmental effects of climate change are listed first (at left), followed by intervening variables that may serve to translate these effects into human social systems and, finally, the anticipated areas in which U.S. national security implications may occur (at right, in the black boxes). 

As the figure shows, research has linked climate change to possible increases in civil conflict, interstate conflict, migration, and other phenomena with the potential to affect U.S. national security. We summarize this literature in greater detail below, but we emphasize from the outset that the review offered in this paper is truncated. Academic literature on this topic is rapidly increasing, although most of it continues to fall along the pathways identified in Figure 3. Furthermore, the literature we highlight below 

- Civil conflict has been linked to climate change 
through multiple mechanisms, including the scarcity of water or food being exacerbated by climate change and that scarcity having the potential to lead 
to more instability.17 Other literature tests both the 
importance of scarcity and the importance of other factors, such as increasing uncertainty over where 
water resources will remain, in driving conflict.18 
- This literature includes papers linking changes in 
rainfall levels to changes in economic growth, with reductions in economic growth strongly related 
to increases in the risk of internal instability.19 
Another, less direct pathway of interest in the civil conflict literature is the linkage of climate migration to civil unrest and instability, a pathway that 
has also been linked to interstate conflict.20 RAND 
has done work on climate change and migration.21
- RAND has conducted extensive work on preparing 
for resilience to the acute impacts of climate change 
(especially, extreme weather events). RAND has also 
done extensive work on climate change impacts on U.S. Air Force and overall DoD basing, infrastructure, and logistics chains.22 This body of work has 
been expanded to include examination of direct impacts on national critical functions, providing a broader look at disaster impacts on the security of the United States.23
conflict or an escalation to violence in interstate 
rivalry.26 The literature in this area often makes the 
argument that the inability of the state to deal with increasing scarcity will lead leadership to seek diversionary wars to distract the populace. 
- Again, more-diffuse pathways link topics outside 
civil conflict or interstate conflict to such topics as terrorism (some research suggests that scarcity leads to an increase in discontent and an increase in 
radicalization to terrorism27) and repression (some 
research suggests that increased grievances and declining state control put pressure on leadership to 
repress civil society action28).
- There is a more significant and established body 
of work that indicates that natural disasters might 
lead to *less* conflict, given the right context. Scholars who study disaster diplomacy have extensively explored the complex pathways linking natural disasters to a lessening of ongoing friction between 
two countries.29 This hypothesis is supported by 
limited quantitative work linking less rainfall to less interstate conflict.30

## Additional Areas Of Climate Change Research

- Other work stretches even further, into moreemotional impacts. For example, increases in anger 
have been linked to increases in heat.24 There is 
enough work linking climate change to conflict that researchers have conducted several large metaanalyses to try to map the pathways linking the two and assess the strength of the evidentiary support for those different pathways.25
Beyond diversity in impacts and diversity in methodological or paradigmatic lenses, there is a plethora of relevant overlapping regional work, specifically on such topics as the impacts of climatic shifts on the Arctic and Russia.31 
RAND has done extensive regional research (especially on the Arctic region32), some of which is focused on or 

- Climate change has also been linked to interstate 
conflict through other pathways. There is limited 
work linking natural disasters with territorial 
conflict or refugee flows and uses these observed relationships to assess possible future risks from climate change. This approach is certainly a good starting point, as climate change is anticipated to generate increasing numbers of such extreme weather events and increasing variation in temperature. However, when it comes to assessing the political and social effects that may result, relying on evidence of historical reactions to extreme weather events and other physical phenomena is likely to miss crucial dynamics with important national security implications. Future political and social reactions to climate change are likely to differ from political and social reactions to past events for at least two important reasons: 

- Climate change represents a dramatic, rapid shift in 
the *pattern and intensity* of extreme weather events, 
rather than just a one-off occurrence. 
- Climate change is being driven, in large part, by the 
past and ongoing behaviors (e.g., carbon emissions) 
of specific states and organizations. 
includes discussion of the geopolitical implications of climate change.

RAND also has a rich body of research on the broader impacts of climate change, from disaster insurance to robust decisionmaking.33 However, although these less direct impacts may soon have defense implications, we do not review them in this paper. 

The literature about climate change and conflict is several decades in the making. At present, there is a significant and rich volume of supportive literature on this topic, especially the linkages between climate change–driven scarcity and civil conflict. Some newer topics, such as the linkages between climate change and interstate war, are less developed. In short, there is still much to do in establishing and better specifying the pathways summarized in Figure 3, despite a rich foundation of previous work. Recent work has called attention to the need for disaggregated data, a willingness to examine indirect pathways, and more of a focus on climate change beyond extreme events.34 Climate researcher and professor Joshua Busby has noted that the field still lacks policy-relevant insights about what to do about the trends toward instability and conflict highlighted in the academic literature.35 

## Gaps In Research On Climate Change And National Security

Much of the academic and policy research on the possible effects of climate change on U.S. national security relies on studying the effects of extreme weather events, temperature increases, and other physical phenomena in the past.36 
That is, it looks at the relationships between historical droughts, floods, or storms and such phenomena as armed These key distinctions have the potential to radically alter political and social reactions and, through them, to affect U.S. national security interests in ways that historical evaluations of reactions to extreme weather events and other physical phenomena have the potential to miss. 

In the remainder of this paper, we review the potential national security implications of climate change and areas for research we have identified that stem from each of these implications. Figure 4 summarizes the overall research agenda that we have identified, including a list of potential research areas. This list is by no means exhaustive, and it may or may not prove to contain the most-important areas. But it is a place to start, and it likely includes many areas of interest to U.S. policymakers. 

## A Rapidly Changing Pattern

The durability and persistence of anticipated changes to the climate, and to the patterns and intensity of extreme weather events, could lead to political and social reactions that are very different from historical reactions to droughts, floods, heat waves, or storms. Similarly, changes to climatic baselines, such as average temperatures or precipitation levels, may move beyond historical norms, raising the possibility that societies may react differently from how they have reacted to past changes. At least on any near-term time horizon, glaciers will not refreeze, sea levels will not decline, habitats will continue to be disrupted, and the overall frequency and intensity of storms will not decrease, although, of course, there will be substantial localized variations. Moreover, as summarized above, the pace of this change is expected to be comparatively rapid: Rather than taking place over centuries, changes are likely to be clear within years or decades, well within individual human lifetimes, enhancing their political and social salience. 

Given this rapidly changing pattern, how would we expect states and societies to react, not only to isolated events that may be manifestations of this broader change but to the changing pattern as a whole? Two general categories of possible reactions stand out, each of which may have acute national security implications. 

## Attempts At Mitigation

As states and societies perceive that climate change may negatively affect them, they may attempt to do something about it. Several such efforts are already underway, from efforts to negotiate international agreements that reduce greenhouse gas emissions to more-localized attempts within countries or sectors of the economy to use energy resources more efficiently. *Mitigation* is the direct attempt to reduce climate change and is made up of attempts to restrain or avoid human interference with the climate to the extent possible. It includes, for example, behavior change around land use, alternative fuel sources, and other technological and behavioral shifts. While such efforts may be vitally important for reducing greenhouse gas emissions, they may or may not have direct national security implications.37 We have chosen to highlight two mitigation efforts that appear most likely to have important national security implications: the ongoing transformation in the production of energy and geoengineering. 

China is a critical supplier and manufacturer of clean energy products and has a significant share of the production and mining of rare earth minerals and lithium, which are key components for electric vehicles and solar power.41 
China is also responsible for a majority of the manufacturing for the global wind power market.42 As electronic vehicles and alternative sources of energy become or remain critical for militaries around the world, China looks poised to be one of the economic winners in this arena. In addition, Chinese control of these markets could lead to critical vulnerabilities in the ability of the U.S. military to protect its critical supply chains. 

These changes in energy production and distribution are likely to be gradual. But as they manifest in the coming years and decades, they will raise many important questions for U.S. national security, including the following: 
The Ongoing Energy Transition Driven largely by concerns about climate change, the world is in the early stages of a transformation of the energy industry—away from fossil fuels and toward renewable energy technologies—that is similar in scale and scope to that which occurred more than a century ago following the widespread exploitation of fossil fuels. At that time, the discovery of large oil deposits in the Middle East, North America, and the Caucasus, as shown in Figure 5, helped reshape the geopolitical map of the world in ways that are still visible.38 This transformation created dependencies on energy-exporting regions of the world as oil became a key requirement for modern economies, even while most countries produced very little or none of it on their own. It also created vulnerabilities, as disruptions to the supply of oil to a particular country could dramatically affect both its economic and military capabilities.39 
The ongoing energy transformation away from fossil fuels and toward renewable energy sources is likely to have quite different effects from prior energy transitions. More energy is likely to be produced locally, although some areas will be more focused on increasing production of solar, hydro, or wind energy than others.40 But the net effect will likely be a reduction in the international trade in energy— and, as a result, a very different pattern of economic winners and losers.

- What effects will these changes have on the overall 
patterns of power and wealth throughout the world? 
- How will a sharp decline in demand for oil affect 
oil-exporting states? Will some U.S. allies or adversaries be at risk of collapse or instability? Will other states find ways to produce and export renewable energy efficiently? (E.g., will there be a "Saudi Arabia of solar"?) Or will energy production tend to become substantially more distributed or democratized? 
- How will a reduction in dependence on foreign 
energy sources affect efforts to deter aggression from adversaries? For example, how would a dramatically reduced need for China to import energy from the Middle East and Africa affect efforts to deter aggression by China against its neighbors? 
How would it affect assessments of China's abil-
ficient to maintain a stable climate and that it is important to research alternative means of forestalling impacts on the environment.44 
Geoengineering projects currently being discussed vary widely in their approaches, costs, and likely degrees of predictability, as discussed in the box below. The localized effects of these efforts are likely to be challenging to assess in advance, increasing the possibility that states may come to differing conclusions about the likely effects of a given course of action. In addition, many of these technologies, if successfully employed, will likely generate climate winners and losers. Wealthy states will have access to climate options, such as cloud-seeding and stratospheric aerosol injection (SAI), that less developed states might 

## Stratospheric Aerosol Injection And China

ity to sustain lengthy military operations? Will the DAF be called on to develop alternative missions to disrupt adversary energy supplies in the event of a conflict? 
- Correspondingly, how will the shifts of European 
and other allies toward renewable energy limit 
the power of adversary states? Russia's invasion of Ukraine highlighted how fossil fuel–producing states may attempt to use energy exports as an economic and diplomatic weapon to coerce changes in the behaviors of states that depend on those exports. Although Russia's shutoff of gas exports to Europe ultimately proved to be unsuccessful in coercing European states to cease their support for Ukraine, European states still bore substantial costs in adapting to the shutoff, and the prospect of the shutoff was an important factor in geopolitical calculations leading up to the conflict. How might U.S. allies react differently as the salience of disruptions in fossil fuel imports declines? 

SAI is a proposed method of solar geoengineering that 
was created to reduce global warming. In this method, 
scientists introduce aerosols into the stratosphere to increase 
reflection of solar radiation and cool the planet. The IPCC has 
indicated that this is one of the most researched methods of 
geoengineering and could likely reduce warming,a at a cost that 
is not prohibitive. In theory, it sounds like a perfect next step to 
protect the climate. In practice, things are more complicated. 
There are concerns that this method (1) might permanently alter 
rainfall patterns,b (2) might make countries that emit heavily 
feel that they no longer need to reduce their emissions,c and 
(3) might easily ramp up rivalry; China has already expressed 
concern that SAI might be used militarily against it.d Thus, 
although SAI provides a possible solution to a warming planet 
in theory, the social and political realities of the process—and 
the risks of negative scientific externalities—mean that it might 
not be the valuable solution it first appears. 

a Allen et al., "Summary for Policymakers."
b Florin et al., International Governance Issues on Climate Engineering.
c Tang and Kemp, "A Fate Worse Than Warming?"
d Moore and Freymann, "China Doesn't Want a Geoengineering Disaster."

Geoengineering As understanding of the consequences of climate change increases, some states or organizations may decide to pursue geoengineering projects. *Geoengineering* refers to the use of technology to manipulate the environment, with a specific focus on partially offsetting the impacts of climate change. Examples range from seeding the atmosphere with reflective materials to reduce the absorption of heat from the sun to extracting carbon dioxide (CO2) 
from the atmosphere and storing it (known as carbon sequestration).43 Proponents of geoengineering argue that limiting greenhouse gas emissions is no longer sufnot. Moreover, such geoengineering options, if deployed for the advantage of wealthier states, may have negative externalities for these less developed states, either predictably or unpredictably. For example, if a wealthy state took steps to increase rainfall within its borders, a neighboring state could experience floods as a result, and states in other regions could experience droughts. There is already a growing body of research concerned with the possible unintended impacts of these technologies.45

dissuade states from following this path? Will the DAF be called on to develop military options to prevent such projects from taking effect? 
- What are the risks that unilateral geoengineering 
projects may have unintended consequences, and what are the potential scale and scope of these consequences? These consequences may directly affect air dominance; how will the DAF adapt? Regardless of the actual effects of these projects, what are the 
risks that they will be *perceived* as having these consequences, leading to preemptive conflict to prevent their implementation? 
- What steps should the United States be prepared to 
take if it assesses that a future geoengineering project would lead to substantial negative effects on the 
United States? 

## Attempts At Adaptation

As the destructive effects of climate change intensify, these dynamics could create an area ripe for misunderstanding. One state's prudent attempt to prevent widespread devastation could be viewed in another state as a reckless gamble risking its population's well-being—a sort of climate security dilemma. Divergent incentives and divergent information and analysis among states could create conditions for conflict over the employment of geoengineering projects, given their inherently crossborder effects. 

Efforts to establish international management of geoengineering efforts are ongoing.46 Collective decisions regarding which approaches may be worth trying would help reduce the geopolitical risks involved, although perhaps not the environmental ones. But there is no guarantee that such efforts will succeed or that they will not break down in the future, under greater stress, as the effects of climate change intensify. Therefore, it is worth taking seriously the potential national security implications of unilateral or contested geoengineering projects. For example,

- Which states may be the most incentivized to 
undertake—and the most capable of undertaking— unilateral geoengineering projects? Are there steps 
that the United States and its allies could take to 
As the long-term effects of climate change are fully realized, states and societies will be forced to adapt. Humanity has gone through the process of adaptation to major climate shifts before, such as when the end of the last Ice Age reshaped settlement patterns and ways of life throughout the world.47 But the rapidity of the potential changes in the years and decades to come will be unprecedented, as will the resources and technologies at humanity's disposal. These changes may have transformative effects on patterns of economic or social organization, from changing the construction of cities to changing the types of food that people eat, but their national security implications may be more indirect. However, there are a few ways in which adaptation efforts could more directly affect U.S. national security. 

Here, we briefly highlight two possibilities. 

intensified effects of climate change may help reduce the risks of conflict in those countries.52
As climate migration intensifies, national security research will likely have to address such questions as the following: 
Migration There is substantial research on the potential for extreme weather events to trigger increasing flows of refugees, either directly, through economic dislocation, or indirectly, by triggering armed conflict (as may have occurred through the civil war in Syria48). But even leaving aside these more time-sensitive pressures for populations to relocate, there will likely be longer-term climate changes, such as more-extreme average temperature, precipitation, and sea level rise, that will trigger population movement.49

- What are the likely scale and scope of future 
climate-driven migration? What are the most-likely or most-affected countries from both immigration and emigration? 
- Which countries are likely to be large recipients 
of climate migrants, and how well positioned are they—politically, economically, and socially—to handle this influx? Are current or anticipated DAF operating locations likely to be affected by these movements?
- Which countries are poised to embrace the flow 
of migration? Which countries have processes to employ and support these migrants, especially to employ them in national security institutions? What long-term benefits or penalties might migration impose economically or in personnel availability?
Funds Going to Adaptation Instead of Defense Depending on their levels of wealth and the severity of localized effects of climate change, states and societies may choose to invest resources to allow populations to stay where they are. A rise in sea level is likely to lead not to the abandonment of Manhattan, for example, but to the investment of substantial resources to prevent its flooding.53 An early example of this phenomenon is the construction of the MOSE system—an enormously expensive endeavor—to limit flooding in Venice;54 see the box on the next page.

There are already substantial economic incentives for individuals to move from comparatively poorer countries to richer ones, and, as noted above, wealthier countries in the Northern Hemisphere are less likely than developing countries nearer the equator to be disadvantaged by climate change. So, the overall effects may be similar in direction to existing migration patterns, only more intensified. However, just because the patterns are similar does not mean that the effects will be identical. Handling immigration pressures remains one of the most difficult political and social challenges for many wealthy states.50 Migration can put very real pressure on militaries and their capacities and readiness. For example, the North Atlantic Treaty Organization (NATO) is contributing to efforts to stem illegal trafficking and migration in the Aegean Sea.51 As a result, the NATO maritime groups involved in these efforts are unavailable to meet other challenges that NATO might face. As migration flows grow, the demands on militaries to help manage these flows may correspondingly increase.

Even an intensification of existing trends may produce widely divergent effects on political and social systems if these pressures exceed some states' capacities to cope with them. Meanwhile, migration from countries experiencing 

## Flooding In Venice

An increasing need for investment in climate adaptation leads to such questions as the following: 

A $6 billion engineering project kept Venice from massive 
flooding in 2022. Metal barriers, installed at key points in the 
lagoon around the city, required 30 years of planning and 20 
years of constructiona—an immense investment to protect the 
city. Even with this expenditure, with the additional expected 
sea level rises due to climate change, this system is not 
anticipated to work indefinitely. Beyond that, while saving 
Venice is a worthy goal, climate adaptations like this one 
might be far out of reach for smaller countries. For example, 
Sri Lanka also faces a future rife with flooding, but the MOSE 
project costs 72 percent of Sri Lanka's 2021 government 
expenditures.b Cheaper or more-scalable approaches will be 
required if larger population areas are to be protected. 

- Which countries are likely to see the greatest 
demand for spending on climate adaptation measures in the decades to come? Could this variation in adaptation needs drive apart countries in multilateral institutions, such as the European Union, as they face very different environmental investment needs for adaptation in the future?
- What is the potential for demand for this spending 
to affect defense expenditures in these countries? 
- What is the potential for the demands of U.S. climate adaptation to affect the long-term sustainability of DAF and DoD budgets? 

a Harlan and Pitrelli, "An Engineering Marvel Just Saved Venice from a 
Flood."
b World Bank, "General Government Final Consumption Expenditure (Cur-
rent US$)—Sri Lanka."

## Someone To Blame

As the demand for projects like these increases and the costs of adaptation rise, many countries will face much more acute trade-offs between spending on climate change adaptation and spending on national defense—to say nothing of other anticipated budget pressures.55 As an example of the rising cost of adaptation, should projections of sea level rise come to pass, much of Florida may require extensive adaptation measures to maintain the viability of current population centers, forcing a choice between (1) investing substantial resources to do so and (2) relocating affected populations and, thus, abandoning large segments of the state—either option an immensely expensive proposition.56 The high cost of adaptation could also further feed the negative cycle of winners and losers of climate change, with poor countries unable to invest in technology that might make them more resilient to the impacts of climate change.

A key feature of climate change today is that it is primarily *anthropogenic*, or caused by human activity. Numerous activities, ranging from deforestation to the burning of fossil fuels to widespread agricultural practices, are driving climate change and, therefore, are indirect drivers of the greater destruction and loss of life that are projected to result.57 
Awareness that human activity is driving climate change could be key for efforts to anticipate how political and social systems are likely to react to the effects of climate change. Rather than extreme weather events being seen as random acts of fate, they may increasingly be seen popularly as results of past and ongoing actions of specific states, organizations, or even individuals. The direct scientific link between any individual extreme weather event and climate change is difficult to definitively identify, but observation more explicitly, depicting 2021 CO2 emissions per capita. The figure shows that most of the states that are projected to be the most affected by climate change—with the exception of some states in the Middle East—are also those that are currently less responsible for the emissions driving those changes. This inequity could be combustible, in political and diplomatic terms, if countries begin to experience widespread destruction or death as a result of climate change. Such political and diplomatic tensions could have numerous implications for U.S. national security. Even today, these inequities in historical and current emissions have led many states that have not historically emitted to the extent of the earlier industrialized countries claiming that they are owed the ability to emit to a certain level in order to meet future economic development needs.59 The Loss and Damage Fund, established at the 2022 United Nations Climate Change Conference, is a first step toward addressing some of these inequities, offering to provide financial assistance to those states that are the most vulnerable to the effects of climate change.60 However, although there is agreement that the fund should exist, most of the contentious issues regarding how resources will be provided for it have been pushed off, as deadlock continues over who will pay into it—and how much.61 

## State Reactions

Reactions to the human-caused nature of climate change and the unequal distribution of blame for its effects may occur at the regional, state, or substate level. In terms of potential state reactions with implications for U.S. national security, we consider the effects of the potential to assign the crucial questions we highlight here are whether, as extreme weather events and other manifestations of climate change intensify, people will increasingly come to believe that such a link exists and, if so, how their behavior may change as a result. 

Climate change is driven by human activity, but not all states, organizations, or individuals have been or are currently contributing equally to climate change. Efforts are underway to require the countries that are most responsible for climate change to assist the most-affected countries in dealing with the effects of climate change, and it is conceivable that, as these effects worsen, additional political and social pressures will be brought to bear. Two figures provide important context to this discussion. Figure 6 
summarizes 2017 emissions of CO2, an important greenhouse gas, by country. China is the largest emitter of CO2, followed by the United States and the European Union. 

However, CO2 and other greenhouse gases are persistent in the atmosphere, so efforts to identify the countries "responsible" for climate change may consider a more comprehensive historical look at emissions. Figure 7 shows an estimate of cumulative CO2 emissions from 1751 to 2017. 

By this accounting, European countries have historically been the largest contributors to global CO2 emissions, followed by the United States and then, more distantly, China. Given the greater pace of China's current emissions, China is expected to overtake the United States and Europe on this measure by 2050, but that trajectory is uncertain.58 
In addition to the differences in emissions among larger, wealthier countries, it is important to note the vast differences in emissions between wealthier countries and poorer countries, as emissions have historically been highly correlated with economic activity. Figure 8 illustrates this blame for climate change on geopolitical alignment and state partnering and engagement.

Chinese infrastructure and humanitarian assistance and disaster relief (HA/DR) support through China's Belt and Road Initiative or similar programs, even if there might be strings attached.

Given the United States' focus on long-term competition with China, how countries around the world come to perceive U.S. and Chinese responsibility for climate change or commitments to mitigating climate change could become an increasingly important diplomatic concern. Competition in this area could take the form of national-level policy changes to restructure the U.S. and Chinese economies. Competition could also take the form of racing to provide adaptation or mitigation assistance to affected countries, in the form of either infrastructure or technology. The climate policies of the donor countries and the climate implications of any infrastructure projects could increase in importance as factors for states deciding between offers from Beijing and Washington. 

The rising geopolitical salience of climate issues also applies to the United States' competition with Russia for influence. An article published in 2020 explores how the Russian public is aware of the importance of climate diplomacy in winning support for Russia abroad but ultimately is uninterested in environmental change, leading Russia to make a show of participating in negotiations without having any real interest in mitigation.63 The melting of Arctic sea ice and the resulting opening of the region to ships is another key area in which geopolitical shifts may occur and in which there is a rich body of literature, as we touched on earlier. Potential research questions with implications for U.S. national security include the following: 
Geopolitical Alignment States select their geopolitical allies and partners for a variety of reasons, including mutual concerns about threats posed by other states or ideological or cultural affinities.62 
As the effects of climate change become increasingly apparent, states may begin to incorporate climate considerations into these calculations. For states suffering heavily from the effects of climate change, will it be politically feasible or advantageous for them to partner with states that their populations perceive as being partly responsible for that suffering? For states that are committed to mitigating climate change for more-ideological or more-humanitarian reasons, how will they balance these interests with their direct security concerns? 

These shifts could affect geopolitical relationships in many ways. States committed to mitigating climate change could form new alliances with one another out of a desire to reduce the effects of climate change on their territory or out of broader ideological commitments to fairness or conservation. Similar alliances could arise among states committed to continuing to emit high levels of greenhouse gases, either for economic reasons or because of assessments that these states are more likely to benefit from projected changes than to be harmed by them. 

Perhaps most directly relevant to this paper are the potential implications of shifting geopolitical relationships for the ongoing U.S.-China rivalry. Many of the countries that are the least prepared to adapt to climate change will face some of its worst effects. These are often the countries that are therefore the most likely to be interested in 

- How do key states in the competition for influence 
between the United States and China—and, to a 
grant such access. In addition, states may modulate their participation in other types of security cooperation, ranging from military exercises to senior leader visits, to signal their pleasure or displeasure with climate policies. Important related questions for research include the following: 

lesser extent, in the competition between the United States and Russia—perceive the climate actions and responsibilities of the major powers? How prevalent are climate concerns in the foreign policy decisionmaking of these key states? 
- What policy levers does the United States have 
available to mitigate or take advantage of either negative or positive perceptions of its climate policy? How do these levers compare with those available to China and Russia? 
- What are the potential risks to U.S. national security of the United States losing influence because of climate policy? For example, if the Pacific Island countries align themselves more with China because of its greater willingness to provide climate 
mitigation support, how will that shift affect U.S. strategic interests in the region? 
- What are the risks that the specific countries on 
which the DAF relies for basing and access in the Indo-Pacific and Europe may curtail or refuse access because of U.S. climate policies? Would these risks likely be driven by popular reactions or elite preferences? Alternatively, what is the potential for these key states to be more willing to provide access because of popular or elite anger over Chinese or Russian climate policy? 
- What is the likelihood of partners in such regions as 
the Indo-Pacific increasingly placing environmental restrictions or requirements on access for U.S. military forces? Are there partners that might appreciate the provision of HA/DR support more than they would appreciate traditional security cooperation? 
- What is the potential for angering allies or partners 
by electing to leave bases that may no longer be viable because of increasing threats from extreme weather, sea level rise, or heat if these allies or partners would prefer a larger U.S. presence as a deterrent to other states? 
- What are the risks that China or Russia might use 
climate issues in information operations or diplomatic efforts to undermine local support for U.S. access and basing arrangements? What are the risks that adversary states might exploit climate vulnerability to gain access for themselves by offering 
Partnering and Access A more specific application of the U.S.-China competition for influence—with direct national security implications— lies in efforts to convince states to participate in security cooperation activities, including agreements to host U.S. military forces on their territory. Such access agreements are currently of acute concern to U.S. military planners charged with identifying ways to defend U.S. allies and partners in the Indo-Pacific, despite the vast distances between those states and the U.S. mainland and the proximity of those states to China.64 
As the effects of climate change become more pronounced, states may begin to incorporate their own assessments of the climate policies of the United States and China into their decisionmaking regarding whether to 

HA/DR support or infrastructure support in the face of climate change?
- What effects could these potential changes have on 
DAF and DoD operational plans, and how should they inform U.S. assessments of risks or the need to explore alternative operating locations? 

## Societal Reactions

Biden's green subsidies.66 But while these national-level policies are important, what remains particularly understudied is how anger over responsibility for climate change may alter economic patterns at the societal level. 

There is a limited history of subnational pressure for boycotts or divestments to alter or complicate the policies of states. The most prominent example involved widespread efforts by North American and European populations to boycott the South African government in the 1980s as a result of its continuing apartheid policies.67 These efforts complicated the U.S. government preference during the Reagan administration to rely on apartheid South Africa as a key bulwark against communism in Africa and became a substantial diplomatic and economic concern for Pretoria.68 Looking forward, it may be important to assess the following: 
Political reactions to climate change are unlikely to be confined to the state level. As the effects of climate change increase, the political salience of climate policy within states is likely to escalate as well. This increasing salience of climate policy could alter states' domestic politics in numerous ways that may or may not have direct national security implications for the United States, including by increasing or decreasing support for particular political parties or economic policies. We can identify at least two possible ways in which these political reactions could have direct effects on national security: (1) pressures for boycotts and divestments and (2) ecoterrorism. 

- What is the likelihood of similar boycott campaigns 
in the future that target states perceived to be contributing in an outsize manner to climate change? 
- Which states would likely be the most at risk of such 
campaigns, and in which countries would these campaigns most likely be organized? 
- How effective might these campaigns be under different potential climate futures, and which geopolitical relationships might they disrupt? 
Ecoterrorism The literature on climate change and violence includes extensive work on the role that climate change can play in inflaming existing tensions by, as discussed earlier, intensifying resource competition and increasing stress on populations. However, if climate change produces increasingly widespread devastation and loss of life, it could also Boycotts and Divestments As noted throughout this paper, climate change has already had significant impacts on industry and trade. These impacts can be seen through trade embargoes, sanctions, or increased tariffs, as well as boycotts or divestments. Countries are aggressively attempting to speed their own green energy transitions, preferably with domestic industry.65 As discussed above, China already controls majority stakes in several green energy markets. There are several economic levers that could escalate tensions with adversary states or introduce friction with U.S. allies. For example, European allies have already objected to President Joe radicalize populations around the imperative of stopping it, an area in which there has not yet been extensive research. This radicalization could occur in the most-affected countries or in populations that are sympathetic to their suffering. While most individuals would likely limit their actions to peaceful methods, some might not. If individuals come to believe that the effects of climate change are responsible for the deaths of thousands or even millions of people, and if they feel that existing efforts to mitigate climate change are ineffective or sclerotic, they might view themselves as justified in taking direct, violent action. Their targets could include visible symbols of the countries that are viewed as the most responsible for ongoing greenhouse gas emissions. They could also choose more-localized targets, such as corporations or segments of the global economy that are viewed to be particularly responsible. The current status of ecoterrorism is discussed in greater detail in the box below. 

## Contemporary Ecoterrorism

Today, ecoterrorism is a comparatively limited concern, making up a relatively small percentage of the overall terrorist actions that have occurred in the past ten years.a However, ecoterrorist or radical environmentalist groups have emerged across partisan divides. The more traditional, eco-radical leftist strain has grown with the rise of such groups as Individualists Tending to the Wild, which emerged in Mexico in 2011 as an eco-anarchist group, and the Deep Green Resistance, another relatively new eco-anarchist group. Both groups have expressed willingness to use violence.b There are also new neofascist environmentalist groups, such as Avocado Politics,c and the overall alt-right environmentalist movement has grown further.d The broader concern is not so much that these particular groups grow or become more active but that, as the climate threat increases, public opinion becomes more supportive of taking direct actions on behalf of the environment, including violent actions that are currently not widely supported.e a START (National Consortium for the Study of Terrorism and Responses to Terrorism), Global Terrorism Database. 

b Spadaro, "Climate Change, Environmental Terrorism, Eco-Terrorism and Emerging Threats." c Spadaro, "Climate Change, Environmental Terrorism, Eco-Terrorism and Emerging Threats." d Loadenthal, "Feral Fascists and Deep Green Guerrillas." e Zeitzoff and Gold, "Green Rage Against the Machine."
Going forward, it might become increasingly important to assess the following: 

- What might a widespread campaign of ecoterrorism in response to climate change look like? Which populations might be most at risk of radicalization, and which entities might be most at risk of 
being targeted? 
- In the global war on terrorism, the United States 
decided to play the central role in organizing international efforts to combat transnational terrorist groups. What policies might the United States adopt in response to a climate change–driven challenge of ecoterrorism? What missions could the DAF be tasked with as a result? 

## Conclusion

in the 2030 to 2040 time frame and pursuing relationships and agreements to enhance forward posture options in the Indo-Pacific. As discussed above, climate change could complicate these plans, although in ways that might take time to come to fruition. The potential for physical effects on U.S. bases or operations in the region is increasingly well appreciated and studied. But it is unlikely that the effects of climate change will be confined to these direct physical effects. Some allies and partners that are considered vital for U.S. basing and access might be disproportionately likely to be negatively affected by climate change, and, as a result, the politics regarding partnering with the United States or appearing to oppose China might shift. Long-term plans for managing protracted conflicts might not reflect the possibility that the United States or its adversaries might develop alternative energy sources, reducing the effectiveness of some tools of statecraft, such as blockades. The political stability of U.S. allies, partners, and adversaries might also be affected by climate change, in ways that should be reflected in DoD's planning for contingencies. In short, climate change might cause additional and more-fundamental changes to the Indo-Pacific operating environment that might, in turn, require morefundamental shifts in DAF or DoD missions, posture, or investments to address. To better understand these and many other potential changes and how they should inform DAF and DoD policy choices, from security cooperation and posture decisions to investment priorities, the time to both widen the scope of issues under investigation and deepen the attention paid to them is now. 

This concept paper makes the case that the burgeoning research into the national security implications of climate change remains too narrowly focused on the physical effects of climate change, paying short shrift to many plausible social or political implications that may affect the broader future operating environment faced by DoD and the DAF. It is important to acknowledge that researchers have only just begun to think seriously about the full scope and scale of effects that climate change may have on U.S. national security. Climate change is not reshaping just a handful of discrete events, or the next few years; it is reshaping the map of the world. Therefore, we should expect it to affect many aspects of national security, potentially in transformative ways. 

While climate change is, by definition, a long-term process, with many of its effects in the physical and environmental realm arriving gradually, political and social systems may behave much less predictably, not showing signs of change until a certain threshold is reached and more-dramatic effects can be seen. Therefore, researchers should not assume that political and social changes will move at the same pace as environmental ones. Identifying where the pressures for political and social changes might be building up and where the effects of these pressures are likely to be felt is a critical issue for U.S. national security and a necessary area for research to better inform decisions. 

DoD and the DAF are increasingly focused on making investments to enhance both capabilities and readiness 

## Notes

1  DoD, *Department of Defense Climate Adaptation Plan*; DoD, Department of Defense Climate Risk Analysis. 

2  DAF, Office of the Assistant Secretary for Energy, Installations, and Environment, *Department of the Air Force Climate Action Plan*; DAF, Office of the Assistant Secretary for Energy, Installations, and Environment, *Department of the Air Force Climate Campaign Plan*.

3  Others include Schwartz and Randall, An Abrupt Climate Change Scenario and Its Implications for United States National Security; DoD, National Defense Strategy and Its Implications for United States National Security; DoD, Department of Defense Climate Change Adaptation Roadmap; DoD, *2014 Climate Change Adaptation Roadmap*; DoD, National Security Implications of Climate-Related Risks and a Changing Climate; 
Office of the Under Secretary of Defense for Acquisition, Technology, and Logistics, Climate-Related Risk to DoD Infrastructure Initial Vulnerability Assessment Survey (SLVAS) Report; DoD, Report on Effects of a Changing Climate to the Department of Defense; the Office of Management and Budget Sustainability Scorecards; DoD Directive 4715.21, Climate Change Adaptation and Resilience; Government Accountability Office, *Climate Change Adaptation*; Government Accountability Office, Climate Resilience: DOD Needs to Assess Risk and Provide Guidance on Use of Climate Projections in Installation Master Plans and Facilities Designs; Government Accountability Office, Climate Resilience: DOD Coordinates with Communities, but Needs to Assess the Performance of Related Grant Programs; Government Accountability Office, "National Security Snapshot"; and White House, *National Security Strategy*. 

4  Office of the Director of National Intelligence, National Intelligence Estimate. 

5  *Adaptation* is defined as altering human behavior, systems, and ways of life to protect humanity from the impacts of climate change. Mitigation is defined as efforts to reduce or prevent emission of greenhouse gases in order to limit climate change.

6  Allen et al., "Summary for Policymakers." 7  Fendt and Ivanova, "Why Did the IPCC Choose 2° C as the Goal for Limiting Global Warming?" 
8  Intergovernmental Panel on Climate Change, Working Group I, "Climate Information Relevant for Disaster Management and Insurance." 
9  Intergovernmental Panel on Climate Change, Working Group I, "Climate Information Relevant for Disaster Management and Insurance"; Abram et al., "Summary for Policymakers."
10  Craymer, "Tuvalu Turns to the Metaverse as Rising Seas Threaten Existence." 
11  United Nations Development Programme, "Tuvalu." 12  Oppenheimer et al., "Sea Level Rise and Implications for Low-Lying Islands, Coasts and Communities." 
13  Tuvalu Meteorological Service, Australian Bureau of Methodology, Commonwealth Scientific and Industrial Research Organisation, Current and Future Climate of Tuvalu. 

14  DoD, 2022 *National Defense Strategy of the United States of America*. 15  This scholarly agreement regarding the significant anticipated effects of climate change on human behavior and life is clear throughout the body of IPCC research, even under the lowest projections of change.

16  This policy-focused literature has been increasingly robust since at least 2007. See Busby, *Climate Change and National Security*; Busby, "Who Cares About the Weather?"; and Campbell et al., The Age of Consequences.

17  Brochmann and Gleditsch, "Shared Rivers and Conflict"; Gleditsch, 
"This Time Is Different!"; Homer-Dixon, Environment, Scarcity, and Violence; Homer-Dixon, "On the Threshold"; Klare, *Resource Wars*; 
Theisen, Holtermann, and Buhaug, "Climate Wars?" 
18  Schmidt, Lee, and Mitchell, "Climate Bones of Contention." 19  Miguel, Satyanath, and Sergenti, "Economic Shocks and Civil Conflict." Ciccone notes that this could be due to growth in rainfall rather than overall rainfall level (Ciccone, "Economic Shocks and Civil Conflict").

20  Koubi, "Climate Change and Conflict"; Mitchell and Pizzi, "Natural Disasters, Forced Migration, and Conflict"; Petrova, "Natural Hazards, Internal Migration and Protests in Bangladesh." 
21  Blake, Clark-Ginsberg, and Balagna, *Addressing Climate Migration*. 22  Recent examples of this kind of work include Best et al., Climate and Readiness; Narayanan, Knopman, et al., Air Force Installation Energy Assurance; Narayanan, Mills, et al., How Can DoD Compare Damage Costs Against Resilience Investment Costs for Climate-Driven Natural Hazards?; and RAND Corporation, "The Growing Exposure of Air Force Installations to Natural Disasters."
23  Miro et al., Assessing Risk to the National Critical Functions as a Result of Climate Change.

24  Van Lange, Rinderu, and Bushman, "Aggression and Violence Around the World." 
25  Hsiang, Burke, and Miguel, "Quantifying the Influence of Climate on Human Conflict"; Koubi, "Climate Change and Conflict"; Sweijs, de Haan, and van Manen, *Unpacking the Climate Security Nexus*. 

26  Lee et al., "Disasters and the Dynamics of Interstate Rivalry"; 
Nelson, "When Disaster Strikes." 
27  This idea is discussed in greater depth later in this paper. 28  Wood and Wright, "Responding to Catastrophe." 29  Kelman, *Disaster Diplomacy*; Slettebak, "Don't Blame the Weather!" 
A recent example in practice is in Kickbusch and Liu, "Global Health Diplomacy." 
30  Devlin and Hendrix, "Trends and Triggers Redux," pp. 27–39. 31  Kraska, *Arctic Security in an Age of Climate Change*; Gustafson, Klimat; Rico, *NATO and Climate Change*. 

32  Recent examples include Pezard et al., China's Strategy and Activities in the Arctic; and Sacks et al., Exploring Gaps in Arctic Governance. 

33  For a broader view of RAND's climate work, see RAND Corporation, 
"RAND's History of Climate Research."
34  von Uexkull and Buhaug, "Security Implications of Climate Change." 
35  Busby, "Beyond Internal Conflict." 36  One recent publication that does take a more forward-looking approach is Efron, Klein, and Cohen, Environment, Geography, and the Future of Warfare. 

37  There are some notable counterexamples in which efforts to reduce emissions may have more-direct national security implications, such as the impacts of restrictions imposed on militaries to limit their emissions or the possible future impacts of environmental mitigation shifts on interoperability.

38  Yergin, *The Prize*.

39  Lanteigne, "China's Maritime Security and the 'Malacca Dilemma.'" 40  Ritchie, Roser, and Rosado, "Energy." 41  International Energy Agency, *Energy Technology Perspectives 2023*. 42  Okamoto, "Chinese Manufacturers Dominate Wind Power, Taking 
60% of Global Market." 
43  Sovacool, "Reckless or Righteous?" 44  Oxford Geoengineering Programme, "Why Consider It?" 
45  Beevers, "Geoengineering"; Dabelko et al., *Backdraft*; Gilmore and Buhaug, "Climate Mitigation Policies and the Potential Pathways to Conflict." 

46  Grisé et al., *Climate Control*. 47  Straus, "The World at the End of the Last Ice Age." 48  Selby et al., "Climate Change and the Syrian Civil War Revisited." 49  For an assessment of these pressures within the United States, see 
Muyskens et al., "More Dangerous Heat Waves Are on the Way." 
50  See, for example, Semyonov, Raijman, and Gorodzeisky, "The Rise of 
Anti-Foreigner Sentiment in European Societies, 1988–2000." 
51  NATO, "Assistance for the Refugee and Migrant Crisis in the Aegean Sea." 
52  Bosetti, Cattaneo, and Peri, "Should They Stay or Should They Go?" 53  There are already expensive investment plans in place to address flooding issues in New York City. See Humphries, "The 'Mind-Boggling' Task of Protecting New York City from Rising Seas." 
54  MOSE Venezia, "MOSE System." 55  It is also possible that states will choose less adaptation-focused investments and find scapegoats or turn to more-populist avenues to rally support. If climate change is treated as an external threat, it might drive nationalist fervor or defense and could lead to "rally-'round-theflag" increases in external conflict.

56  Raimi, Keyes, and Kingdon, *Florida Climate Outlook*. 57  U.S. Environmental Protection Agency, "Causes of Climate Change." 58  Stevens, "The United States Has Caused the Most Global Warming." 
59  Ülgen, "How Deep Is the North-South Divide on Climate Negotiations?" Specifically, see Ülgen's citation of Sandeep Sengupta.

60  United Nations Environment Programme, "What You Need to Know About the COP27 Loss and Damage Fund." 
61  Abnett, "Developing Countries Propose $100 Billion Climate Damage Fund." 
62  Fordham and Poast, "All Alliances Are Multilateral"; 
Schimmelfennig, "NATO Enlargement." 
63  Korppoo, "Domestic Frames on Russia's Role in International Climate Diplomacy." 
64  Lin et al., Regional Responses to U.S.-China Competition in the Indo-Pacific. 

65  Swanson, "Climate Change May Usher In a New Era of Trade Wars." 66  Rauhala and Nakashima, "European Officials Object to Biden's Green Subsidies as Protectionist." 
67  Teoh, Welch, and Wazzan, "The Effect of Socially Activist Investment Policies on the Financial Markets." 
68  Fatton, "The Reagan Foreign Policy Toward South Africa." 

## References

Abnett, Kate, "Developing Countries Propose $100 Billion Climate Damage Fund," Reuters, September 6, 2023. Abram, Nerilie, Carolina Adler, Nathaniel L. Bindoff, Lijing Cheng, So-Min Cheong, William W. L. Cheung, Matthew Collins, Chris Derksen, Alexey Ekaykin, Thomas Frölicher, et al., "Summary for Policymakers," in H.-O. Pörtner, D. C. Roberts, V. Masson-Delmotte, P. 

Zhai, M. Tignor, E. Poloczanska, K. Mintenbeck, A. Alegría, M. Nicolai, A. Okem, et al., eds., IPCC Special Report on the Ocean and Cryosphere in a Changing Climate, Intergovernmental Panel on Climate Change, Cambridge University Press, 2019. Allen, Myles, Mustafa Babiker, Yang Chen, Heleen de Coninck, Sarah Connors, Renée van Diemen, Opha Pauline Dube, Kristie L. Ebi, Francois Engelbrecht, Marion Ferrat, et al., "Summary for Policymakers," in V. Masson-Delmotte, P. Zhai, H.-O. Pörtner, D. Roberts, J. Skea, P. R. Shukla, A. Pirani, W. Moufouma-Okia, C. Péan, R. Pidcock, et al., eds., Global Warming of 1.5°C. An IPCC Special Report on the Impacts of Global Warming of 1.5°C Above Pre-Industrial Levels and Related Global Greenhouse Gas Emission Pathways, in the Context of Strengthening the Global Response to the Threat of Climate Change, Sustainable Development, and Efforts to Eradicate Poverty, Intergovernmental Panel on Climate Change, Cambridge University Press, 2018. Beevers, Michael, "Geoengineering: A New and Emerging Security Threat?" in Michel Gueldry, Gigi Gokcek, and Lui Hebron, eds., Understanding *New Security Threats*, Routledge, 2019.

Best, Katharina Ley, Scott R. Stephenson, Susan A. Resetar, Paul W. Mayberry, Emmi Yonekura, Rahim Ali, Joshua Klimas, Stephanie Stewart, Jessica Arana, Inez Khan, and Vanessa Wolf, Climate and Readiness: Understanding Climate Vulnerability of U.S. Joint Force Readiness, RAND Corporation, RR-A1551-1, 2023. As of December 15, 
2023: https://www.rand.org/pubs/research_reports/RRA1551-1.html Blake, Jonathan S., Aaron Clark-Ginsberg, and Jay Balagna, Addressing Climate Migration: A Review of National Policy Approaches, RAND 
Corporation, PE-A1085-1, December 2021. As of December 15, 2023: https://www.rand.org/pubs/perspectives/PEA1085-1.html Bosetti, Valentina, Cristina Cattaneo, and Giovanni Peri, "Should They Stay or Should They Go? Climate Migrants and Local Conflicts," 
Journal of Economic Geography, Vol. 21, No. 4, July 2021.

Brochmann, Marit, and Nils Petter Gleditsch, "Shared Rivers and Conflict—A Reconsideration," *Political Geography*, Vol. 31, No. 8, November 2012.

Busby, Joshua W., Climate Change and National Security: An Agenda for Action, Council on Foreign Relations, CSR No. 32, November 2007.

Busby, Joshua W., "Who Cares About the Weather? Climate Change and U.S. National Security," *Security Studies*, Vol. 17, No. 3, 2008. 

Busby, Joshua W., "Beyond Internal Conflict: The Emergent Practice of Climate Security," *Journal of Peace Research*, Vol. 58, No. 1, January 
2021.

Campbell, Kurt M., Jay Gulledge, J. R. McNeill, John Podesta, Peter Ogden, Leon Fuerth, R. James Woolsey, Alexander T. J. 

Lennon, Julianne Smith, Richard Weitz, and Derek Mix, The Age of Consequences: The Foreign Policy and National Security Implications of Global Climate Change, Center for Strategic and International Studies and Center for a New American Security, November 2007. Carleton, Tamma, Amir Jina, Michael Delgado, Michael Greenstone, Trevor Houser, Solomon Hsiang, Andrew Hultgren, Robert E. Kopp, Kelly E. McCusker, Ishan Nath, et al., "Valuing the Global Mortality Consequences of Climate Change Accounting for Adaptation Costs and Benefits," *Quarterly Journal of Economics*, Vol. 137, No. 4, November 
2022. Ciccone, Antonio, "Economic Shocks and Civil Conflict: A Comment," 
American Economic Journal: Applied Economics, Vol. 3, No. 4, October 
2011. Craymer, Lucy, "Tuvalu Turns to the Metaverse as Rising Seas Threaten Existence," Reuters, November 15, 2022. Dabelko, Geoffrey D., Lauren Herzer, Schuyler Null, Meaghan Parker, and Russell Sticklor, eds., Backdraft: The Conflict Potential of Climate Change Adaptation and Mitigation, Woodrow Wilson International Center for Scholars, Environmental Change and Security Program, Vol. 14, No. 2, 2013.

DAF—See Department of the Air Force. Department of Defense, *National Defense Strategy*, June 2008. Department of Defense, Department of Defense Climate Change Adaptation Roadmap, 2012. Department of Defense, 2014 *Climate Change Adaptation Roadmap*, 
2014. 

Department of Defense, National Security Implications of Climate- Related Risks and a Changing Climate, 2015. Department of Defense, Report on Effects of a Changing Climate to the Department of Defense, January 2019.

Department of Defense, Department of Defense Climate Adaptation Plan, September 1, 2021. Department of Defense, *Department of Defense Climate Risk Analysis*, October 2021.

Department of Defense, 2022 National Defense Strategy of the United States of America, October 2022. Department of Defense Directive 4715.21, Climate Change Adaptation and Resilience, Department of Defense, January 14, 2016.

Department of the Air Force, Office of the Assistant Secretary for Energy, Installations, and Environment, Department of the Air Force Climate Action Plan, October 2022. 

Department of the Air Force, Office of the Assistant Secretary for Energy, Installations, and Environment, Department of the Air Force Climate Campaign Plan, July 2023.

Devlin, Colleen, and Cullen S. Hendrix, "Trends and Triggers Redux: 
Climate Change, Rainfall, and Interstate Conflict," *Political Geography*, Vol. 43, November 2014.

DoD—See Department of Defense.

Efron, Shira, Kurt Klein, and Raphael S. Cohen, Environment, Geography, and the Future of Warfare: The Changing Global Environment and Its Implications for the U.S. Air Force, RAND 
Corporation, RR-2849/5-AF, 2020. As of December 18, 2023: https://www.rand.org/pubs/research_reports/RR2849z5.html Fatton, Robert, "The Reagan Foreign Policy Toward South Africa: The Ideology of the New Cold War," *African Studies Review*, Vol. 27, No. 1, March 1984. Fendt, Lindsay, and Maria Ivanova, "Why Did the IPCC Choose 2° C as the Goal for Limiting Global Warming?" Ask MIT Climate, Massachusetts Institute of Technology Climate Portal, June 22, 2021. Florin, M.-V., ed., and P. Rouse, A.-H. Hubert, M. Honegger, and J. 

Reynolds, International Governance Issues on Climate Engineering: Information for Policymakers, EPFL International Risk Governance Center, 2020. Fordham, Benjamin, and Paul Poast, "All Alliances Are Multilateral: 
Rethinking Alliance Formation," *Journal of Conflict Resolution*, Vol. 60, No. 5, August 2016. Gilmore, Elisabeth A., and Halvard Buhaug, "Climate Mitigation Policies and the Potential Pathways to Conflict: Outlining a Research Agenda," *WIREs Climate Change*, Vol. 12, No. 5, September–October 
2021.

Gleditsch, Nils Petter, "This Time Is Different! Or Is It? NeoMalthusians and Environmental Optimists in the Age of Climate Change," Journal of Peace Research, Vol. 58, No. 1, January 2021. Government Accountability Office, Climate Change Adaptation: DOD Needs to Better Incorporate Adaptation into Planning and Collaboration at Overseas Installations, GAO-18-206, November 2017.

Government Accountability Office, Climate Resilience: DOD Needs to Assess Risk and Provide Guidance on Use of Climate Projections in Installation Master Plans and Facilities Designs, GAO-19-453, June 2019.

Government Accountability Office, Climate Resilience: DOD Coordinates with Communities, but Needs to Assess the Performance of Related Grant Programs, GAO-21-46, December 2020.

Government Accountability Office, "National Security Snapshot: Climate Change Risks to National Security," GAO-22-105830, September 2022. Grisé, Michelle, Emmi Yonekura, Jonathan S. Blake, David DeSmet, Anusree Garg, and Benjamin Lee Preston, Climate Control: International Legal Mechanisms for Managing the Geopolitical Risks of Geoengineering, RAND Corporation, PE-A1133-1, 2021. As of December 15, 2023: https://www.rand.org/pubs/perspectives/PEA1133-1.html Gustafson, Thane, *Klimat: Russia in the Age of Climate Change*, Harvard University Press, 2021. Harlan, Chico, and Stefano Pitrelli, "An Engineering Marvel Just Saved Venice from a Flood. What About When Seas Rise?" *Washington Post*, last updated November 27, 2022. Homer-Dixon, Thomas F., "On the Threshold: Environmental Changes as Causes of Acute Conflict," *International Security*, Vol. 16, No. 2, Fall 
1991. 

Homer-Dixon, Thomas F., *Environment, Scarcity, and Violence*, Princeton University Press, 1999. Hsiang, Solomon M., Marshall Burke, and Edward Miguel, 
"Quantifying the Influence of Climate on Human Conflict," *Science*, Vol. 341, No. 6151, September 13, 2013. Humphries, Courtney, "The 'Mind-Boggling' Task of Protecting New York City from Rising Seas," *MIT Technology Review*, April 24, 2019. 

Intergovernmental Panel on Climate Change, Working Group I, "Climate Information Relevant for Disaster Management and Insurance," fact sheet, 2021. 

International Energy Agency, *Energy Technology Perspectives 2023*, 2023. 

Kelman, Ilan, Disaster Diplomacy: How Disasters Affect Peace and Conflict, Routledge, 2012.

Kickbusch, Ilona, and Austin Liu, "Global Health Diplomacy—
Reconstructing Power and Governance," *The Lancet*, Vol. 399, No. 10341, June 4, 2022.

Klare, Michael T., *Resource Wars: The New Landscape of Global Conflict*, Metropolitan, 2001. 

Korppoo, Anna, "Domestic Frames on Russia's Role in International Climate Diplomacy," *Climate Policy*, Vol. 20, No. 1, 2020. Koubi, Vally, "Climate Change and Conflict," Annual Review of Political Science, Vol. 22, 2019. Kraska, James, ed., *Arctic Security in an Age of Climate Change*, Cambridge University Press, 2011. Lanteigne, Marc, "China's Maritime Security and the 'Malacca Dilemma,'" *Asian Security*, Vol. 4, No. 2, 2008.

Lee, Bomi K., Sara McLaughlin Mitchell, Cody J. Schmidt, and Yufan Yang, "Disasters and the Dynamics of Interstate Rivalry," Journal of Peace Research, Vol. 59, No. 1, January 2022.

Lin, Bonny, Michael S. Chase, Jonah Blank, Cortez A. Cooper III, Derek Grossman, Scott W. Harold, Jennifer D. P. Moroney, Lyle J. Morris, Logan Ma, Paul Orner, Alice Shih, and Soo Kim, Regional Responses to U.S.-China Competition in the Indo-Pacific: Study Overview and Conclusions, RAND Corporation, RR-4412-AF, 2020. As of March 2, 
2023: https://www.rand.org/pubs/research_reports/RR4412.html Loadenthal, Michael, "Feral Fascists and Deep Green Guerrillas: 
Infrastructural Attack and Accelerationist Terror," Critical Studies on Terrorism, Vol. 15, No. 1, 2022.

Masson-Delmotte, Valérie, Panmao Zhai, Anna Pirani, Sarah L. Connors, Clotilde Péan, Yang Chen, Leah Goldfarb, Melissa I. Gomis, J. B. Robin Matthews, Sophie Berger, et al., eds., Climate Change 2021: The Physical Science Basis: Summary for Policymakers, Technical Summary, Frequently Asked Questions and Glossary, Intergovernmental Panel on Climate Change, 2021. Miguel, Edward, Shanker Satyanath, and Ernest Sergenti, "Economic Shocks and Civil Conflict: An Instrumental Variables Approach," 
Journal of Political Economy, Vol. 112, No. 4, August 2004.

Miro, Michelle E., Andrew Lauland, Rahim Ali, Edward W. Chan, Richard H. Donohue, Liisa Ecola, Timothy R. Gulden, Liam Regan, Karen M. Sudkamp, Tobias Sytsma, Michael T. Wilson, and Chandler Sachs, Assessing Risk to the National Critical Functions as a Result of Climate Change, RAND Corporation, RR-A1645-7, 2022. As of December 15, 2023: https://www.rand.org/pubs/research_reports/RRA1645-7.html Mitchell, Sara McLaughlin, and Elise Pizzi, "Natural Disasters, Forced Migration, and Conflict: The Importance of Government Policy Responses," *International Studies Review*, Vol. 23, No. 3, September 
2021. Moore, Scott, and Eyck Freymann, "China Doesn't Want a Geoengineering Disaster," *Foreign Policy*, February 21, 2023. 

MOSE Venezia, "MOSE System: The Mobile Barriers for the Protection of Venice from High Tides," webpage, undated. As of March 3, 2023: https://www.mosevenezia.eu/project/?lang=en Muyskens, John, Andrew Ba Tran, Anna Phillips, Simon Ducroquet, and Naema Ahmed, "More Dangerous Heat Waves Are on the Way: See the Impact by Zip Code," *Washington Post*, August 15, 2022. 

Narayanan, Anu, Debra Knopman, James D. Powers, Bryan Boling, Benjamin M. Miller, Patrick Mills, Kristin Van Abel, Katherine Anania, Blake Cignarella, and Connor P. Jackson, Air Force Installation Energy Assurance: An Assessment Framework, RAND Corporation, RR-2066-AF, 2017. As of December 15, 2023: https://www.rand.org/pubs/research_reports/RR2066.html Narayanan, Anu, Patrick Mills, Tobias Sytsma, Kelly Klima, and Rahim Ali, How Can DoD Compare Damage Costs Against Resilience Investment Costs for Climate-Driven Natural Hazards? Overview of an Analytic Approach, Its Advantages, and Its Limitations, RAND 
Corporation, RR-A1860-1, 2023. As of December 15, 2023: https://www.rand.org/pubs/research_reports/RRA1860-1.html National Archives and Records Administration, "Records of the Office of the Secretary of War," record group 107, entry 191, Office of the Assistant Secretary of War, Planning Branch, box 100, undated. 

NATO—See North Atlantic Treaty Organization.

Nelson, Travis, "When Disaster Strikes: On the Relationship Between Natural Disaster and Interstate Conflict," Global Change, Peace & Security, Vol. 22, No. 2, June 2010.

North Atlantic Treaty Organization, "Assistance for the Refugee and Migrant Crisis in the Aegean Sea," webpage, last updated January 17, 2023. As of September 27, 2023: 
https://www.nato.int/cps/en/natohq/topics_128746.htm Office of the Director of National Intelligence, National Intelligence Estimate: Climate Change and International Responses Increasing Challenges to US National Security Through 2040, NIC-NIE-2021-
10030-A, October 21, 2021. Office of the Under Secretary of Defense for Acquisition, Technology, and Logistics, Climate-Related Risk to DoD Infrastructure Initial Vulnerability Assessment Survey (SLVAS) Report, Department of Defense, January 2018.

Okamoto, Yasuki, "Chinese Manufacturers Dominate Wind Power, Taking 60% of Global Market," *Nikkei Asia*, August 19, 2023. 

Oppenheimer, Michael, Bruce C. Glavovic, Jochen Hinkel, Roderik van de Wal, Alexandre K. Magnan, Amro Abd-Elgawad, Rongshuo Cai, Miguel Cifuentes-Jara, Robert M. DeConto, Tuhin Ghosh, et al., "Sea Level Rise and Implications for Low-Lying Islands, Coasts and Communities," in H.-O. Pörtner, D. C. Roberts, V. Masson-Delmotte, P. 

Zhai, M. Tignor, E. Poloczanska, K. Mintenbeck, A. Alegría, M. Nicolai, A. Okem, et al., eds., IPCC Special Report on the Ocean and Cryosphere in a Changing Climate, Cambridge University Press, 2019.

Oxford Geoengineering Programme, "Why Consider It?" webpage, University of Oxford, undated. As of September 28, 2023: https://www.geoengineering.ox.ac.uk/www.geoengineering.ox.ac.uk/ 
what-is-geoengineering/why-consider-geoengineering/ Petrova, Kristina, "Natural Hazards, Internal Migration and Protests in Bangladesh," *Journal of Peace Research*, Vol. 58, No. 1, January 2021.

Pezard, Stephanie, Stephen J. Flanagan, Scott W. Harold, Irina A. Chindea, Benjamin J. Sacks, Abbie Tingstad, Tristan Finazzo, and Soo Kim, China's Strategy and Activities in the Arctic: Implications for North American and Transatlantic Security, RAND Corporation, RR-A1282-1-v2, 2022. As of December 15, 2023: https://www.rand.org/pubs/research_reports/RRA1282-1-v2.html Raimi, Daniel, Amelia Keyes, and Cora Kingdon, Florida Climate Outlook: Assessing Physical and Economic Impacts Through 2040, Resources for the Future, Report 20-01, January 30, 2020. RAND Corporation, "RAND's History of Climate Research," webpage, undated. As of December 18, 2023: https://www.rand.org/well-being/ community-health-and-environmental-policy/portfolios/ climate-research-at-rand.html RAND Corporation, "The Growing Exposure of Air Force Installations to Natural Disasters," RB-A523-1, 2021. As of December 15, 2023: 
https://www.rand.org/pubs/research_briefs/RBA523-1.html Rauhala, Emily, and Ellen Nakashima, "European Officials Object to Biden's Green Subsidies as Protectionist," *Washington Post*, December 4, 
2022.

Rico, Lucía García, NATO and Climate Change: A Climatized Perspective on Security, Belfer Center for Science and International Affairs, Harvard Kennedy School, August 2022.

Ritchie, Hannah, Max Roser, and Pablo Rosado, "CO2 Emissions," 
webpage, Our World in Data, 2020. As of December 21, 2023: https://ourworldindata.org/co2-emissions Ritchie, Hannah, Max Roser, and Pablo Rosado, "Energy," webpage, Our World in Data, 2023. As of December 15, 2023: https://ourworldindata.org/energy Sacks, Benjamin J., Scott R. Stephenson, Stephanie Pezard, Abbie Tingstad, and Camilla T. N. Sørensen, Exploring Gaps in Arctic Governance: Identifying Potential Sources of Conflict and Mitigating Measures, RAND Corporation, RR-A1007-1, 2021. As of December 15, 
2023: 
https://www.rand.org/pubs/research_reports/RRA1007-1.html Schimmelfennig, Frank, "NATO Enlargement: A Constructivist Explanation," *Security Studies*, Vol. 8, No. 2–3, 1998.

Schmidt, Cody J., Bomi K. Lee, and Sara McLaughlin Mitchell, "Climate Bones of Contention: How Climate Variability Influences Territorial, Maritime, and River Interstate Conflicts," *Journal of Peace Research*, Vol. 58, No. 1, January 2021.

Schwartz, Peter, and Doug Randall, An Abrupt Climate Change Scenario and Its Implications for United States National Security, Jet Propulsion Laboratory, California Institute of Technology, October 2003. Selby, Jan, Omar S. Dahi, Christiane Fröhlich, and Mike Hulme, 
"Climate Change and the Syrian Civil War Revisited," Political Geography, Vol. 60, September 2017.

Semyonov, Moshe, Rebeca Raijman, and Anastasia Gorodzeisky, "The Rise of Anti-Foreigner Sentiment in European Societies, 1988–2000," 
American Sociological Review, Vol. 71, No. 3, June 2006.

Slettebak, Rune T., "Don't Blame the Weather! Climate-Related Natural Disasters and Civil Conflict," *Journal of Peace Research*, Vol. 49, No. 1, January 2012. Sovacool, Benjamin K., "Reckless or Righteous? Reviewing the Sociotechnical Benefits and Risks of Climate Change Geoengineering," 
Energy Strategy Reviews, Vol. 35, May 2021.

Spadaro, Paola Andrea, "Climate Change, Environmental Terrorism, Eco-Terrorism and Emerging Threats," *Journal of Strategic Security*, Vol. 13, No. 4, 2020. START (National Consortium for the Study of Terrorism and Responses to Terrorism), Global Terrorism Database, last updated May 2022. As of December 15, 2023: 
https://www.start.umd.edu/gtd Stevens, Harry, "The United States Has Caused the Most Global Warming. When Will China Pass It?" *Washington Post*, March 1, 2023. 

Straus, Lawrence Guy, "The World at the End of the Last Ice Age," in Lawrence Guy Straus, Berit Valentin Eriksen, Jon M. Erlandson, and David R. Yesner, eds., Humans at the End of the Ice Age: The Archaeology of the Pleistocene–Holocene Transition, Plenum Press, 1996.

Swanson, Ana, "Climate Change May Usher In a New Era of Trade Wars," *New York Times*, January 25, 2023. Sweijs, Tim, Marleen de Haan, and Hugo van Manen, Unpacking the Climate Security Nexus: Seven Pathologies Linking Climate Change to Violent Conflict, Hague Centre for Strategic Studies, March 2022.

Tang, Aaron, and Luke Kemp, "A Fate Worse Than Warming? 

Stratospheric Aerosol Injection and Global Catastrophic Risk," Frontiers in Climate, Vol. 3, November 2021.

Teoh, Siew Hong, Ivo Welch, and C. Paul Wazzan, "The Effect of Socially Activist Investment Policies on the Financial Markets: Evidence from the South African Boycott," *Journal of Business*, Vol. 72, No. 1, January 1999. Theisen, Ole Magnus, Helge Holtermann, and Halvard Buhaug, "Climate Wars? Assessing the Claim That Drought Breeds Conflict," 
International Security, Vol. 36, No. 3, Winter 2011–2012. Toprani, Anand, Oil and Grand Strategy: Great Britain and Germany, 1918-1941, dissertation, Georgetown University, ProQuest LLC, August 20, 2012. Tuvalu Meteorological Service, Australian Bureau of Methodology, Commonwealth Scientific and Industrial Research Organisation, Current and Future Climate of Tuvalu, 2011. 

Ülgen, Sinan, "How Deep Is the North-South Divide on Climate Negotiations?" Carnegie Europe, October 6, 2021. United Nations Development Programme, "Tuvalu," webpage, undated. As of December 15, 2023: https://www.adaptation-undp.org/explore/polynesia/tuvalu United Nations Environment Programme, "What You Need to Know About the COP27 Loss and Damage Fund," last updated November 29, 2022. U.S. Environmental Protection Agency, "Causes of Climate Change," webpage, last updated April 25, 2023. As of December 15, 2023: https://www.epa.gov/climatechange-science/causes-climate-change Van Lange, Paul A. M., Maria I. Rinderu, and Brad J. Bushman, 
"Aggression and Violence Around the World: A Model of Climate, Aggression, and Self-Control in Humans (CLASH)," Behavioral and Brain Sciences, Vol. 40, 2017.

von Uexkull, Nina, and Halvard Buhaug, "Security Implications of Climate Change: A Decade of Scientific Progress," Journal of Peace Research, Vol. 58, No. 1, January 2021. White House, *National Security Strategy*, October 2022. 

Wood, Reed M., and Thorin M. Wright, "Responding to Catastrophe: 
Repression Dynamics Following Rapid-Onset Natural Disasters," 
Journal of Conflict Resolution, Vol. 60, No. 8, December 2016.

World Bank, "General Government Final Consumption Expenditure (Current US$)—Sri Lanka," online data tool, undated. As of March 1, 2023: https://data.worldbank.org/indicator/NE.CON.GOVT.CD?locations=LK
Yergin, Daniel, *The Prize: The Epic Quest for Oil, Money and Power*, Simon & Schuster, 2011. Zeitzoff, Thomas, and Grace Gold, "Green Rage Against the Machine: Climate Threat and Support for Eco Direct Actions," preliminary draft, July 2022. 

## Acknowledgments About The Authors

We thank Stephanie Young for her support of this research. Molly Dunigan, Jonathan Wong, Anna Jean Wirth, Stacie Pettyjohn, and Susan Resetar provided insightful reviews that improved this paper. We are also indebted to Emily Ward and Elise Ricotta for their efforts in preparing the document for publication. The views expressed in this paper are those of the authors. 

Bryan Frederick is a senior political scientist at RAND 
and the associate director of the Strategy and Doctrine Program within RAND Project AIR FORCE. His research interests include interstate deterrence and escalation management, conflict forecasting, military interventions, territorial disputes, international norms, the law of armed conflict, and regional security issues in Europe, East Asia, South Asia, and the Middle East. He has a Ph.D. in international relations.

Caitlin McCulloch is an associate political scientist at RAND. Her primary research interests are alliance ties, security cooperation, misinformation and information warfare, Eastern Europe, and the impact of environmental change on conflict. Her work at RAND has focused on security cooperation, information warfare, and climate change and conflict. She has a Ph.D. in government and politics. 

## About This Paper

Climate change has become an increasingly prominent concern in U.S. national security strategies and assessments in recent years, but the issues thought to be affected by climate change remain comparatively limited. Although the need to better prepare for the physical effects of climate change on military operations and basing is well understood, how climate change might affect other aspects of the international security environment remains relatively understudied. This paper develops a potential research agenda to help close this gap. It summarizes the areas in which existing research on the effects of climate change is comparatively robust and then explores the areas in which the broader effects of climate change on states and societies are not well understood. It highlights several specific areas for further research and illustrates how the impacts of climate change in these areas may affect the Department of the Air Force and the Department of Defense more broadly. This research was conducted within RAND Project AIR FORCE's Resource Management Program. 

RAND Project AIR FORCE
RAND Project AIR FORCE (PAF), a division of the RAND Corporation, is the Department of the Air Force's (DAF's) federally funded research The RAND Corporation is a research organization that develops solutions to public policy challenges to help make communities throughout the world safer and more secure, healthier and more prosperous. RAND is nonprofit, nonpartisan, and committed to the public interest. 

Research Integrity Our mission to help improve policy and decisionmaking through research and analysis is enabled through our core values of quality and objectivity and our unwavering commitment to the highest level of integrity and ethical behavior. To help ensure our research and analysis are rigorous, objective, and nonpartisan, we subject our research publications to a robust and exacting quality-assurance process; avoid both the appearance and reality of financial and other conflicts of interest through staff training, project screening, and a policy of mandatory disclosure; and pursue transparency in our research engagements through our commitment to the open publication of our research findings and recommendations, disclosure of the source of funding of published research, and policies to ensure intellectual independence. For more information, visit www.rand.org/about/research-integrity. RAND's publications do not necessarily reflect the opinions of its research clients and sponsors. 

 is a registered trademark.

Limited Print and Electronic Distribution Rights This publication and trademark(s) contained herein are protected by law. This representation of RAND intellectual property is provided for noncommercial use only. Unauthorized posting of this publication online is prohibited; linking directly to its webpage on rand.org is encouraged. Permission is required from RAND to reproduce, or reuse in another form, any of its research products for commercial purposes. For information on reprint and reuse permissions, please visit www.rand.org/pubs/permissions.

For more information on this publication, visit www.rand.org/t/PEA2944-1.

© 2024 RAND Corporation www.rand.org

and development center for studies and analyses, supporting both the 
United States Air Force and the United States Space Force. PAF pro-
vides the DAF with independent analyses of policy alternatives affect-
ing the development, employment, combat readiness, and support of 
current and future air, space, and cyber forces. Research is conducted 
in four programs: Strategy and Doctrine; Force Modernization and 
Employment; Resource Management; and Workforce, Development, 
and Health. The research reported here was prepared under contract 
FA7014-22-D-0001.

Additional information about PAF is available on our website: 
www.rand.org/paf/

Funding
Funding for this research was made possible through the concept 
formulation provision of the Department of the Air Force–RAND Spon-
soring Agreement. PAF uses concept formulation funding to support 
a variety of activities, including research plan development; direct 
assistance on short-term, decision-focused Department of the Air 
Force requests; exploratory research; outreach and communications 
initiatives; and other efforts undertaken in support of the develop-
ment, execution, management, and reporting of PAF's approved 
research agenda.

